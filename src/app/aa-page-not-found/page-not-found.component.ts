import { Component, OnInit } from '@angular/core';
import {GlobalVarService} from '../aa-common/global-var.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor(public globalVar: GlobalVarService) { }

  ngOnInit() {
  }

}
