import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule,ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData, DatePipe } from '@angular/common';
import es from '@angular/common/locales/es';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { OrderModule } from 'ngx-order-pipe';
import { FilterPipeModule } from 'ngx-filter-pipe';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { NavComponent } from './aa-nav/nav.component';
import { PageNotFoundComponent } from './aa-page-not-found/page-not-found.component';

import { ContactComponent } from './aa-contact/contact.component';
import { HelloService } from './aa-contact/hello.service';
import { InfoComponent } from './aa-info/info.component';

import { LoginComponent } from './acc-login/login.component';

import  {GlobalErrorHandlerService} from './aa-errors/global-error-handler.service'; 
import { ErrorIntercept } from './aa-errors/error.interceptor';
import { ErrorService } from './aa-errors/error.service';
import { ShowErrorComponent } from './aa-errors/show-error.component';
import { ShowMsgObjectComponent } from './aa-errors/show-msg-object.component';
import { GetDeleteComfirmationComponent } from './aa-errors/get-delete-confirmation.component';

import { ValidationMessagesComponent } from './aa-common/validation-messages.component';
import { ValidationService } from './aa-common/validation.service';

import { ValidInputBootstrapCssDirective } from './aa-common/dir-valid-input.directive';
import { InvalidInputBootstrapCssDirective } from './aa-common/dir-invalid-input.directive';
import { SelectRequiredValidatorDirective } from './aa-common/dir-select-required-validator.directive';
import { EnterDateInformationComponent } from './aa-common/enter-date-information.component';
import { EnterDateComponent } from './aa-common/enter-date.component';
import { EnterDateRangeComponent } from './aa-common/enter-dateRange.component';
import { EnterDecimalNumbersComponent } from './aa-common/enter-decimal-numbers.component';
import { EnterSelectDecimalsComponent } from './aa-common/enter-select-decimals.component';
import { SelectValueRecordListComponent } from './aa-common/select-VRList.component';
import { SelectManyVRListComponent } from './aa-common/select-manyVRList.component';

import { SelectMonthComponent } from './aa-common/select-month.component';
import { EnterMonthComponent } from './aa-common/enter-month.component';
import { GlobalVarService } from './aa-common/global-var.service';

import { ToolsComponent } from './aa-tools/tools.component';

import { GbViewsComponent } from './dbc-gbviews/gbviews.component';
import { GbViewsService } from './dbc-gbviews/gbviews.service';

import { BaresComponent } from './cfg-config/bares.component';
import { BaresCreateComponent } from './cfg-config/bares-create.component';

import { ConfigService } from './cfg-config/config.service';

import { FieldsCreateComponent } from './dbc-fields/fields-create.component';
import { ValuesCreateComponent } from './dbc-fields/values-create.component';
import { FieldsService } from './dbc-fields/fields.service';

import { HdComponent } from './dbx-hd/hd.component';
import { HdTpvsComponent } from './dbx-hd/hd-tpvs.component';
import { HdSumsComponent } from './dbx-hd/hd-sums.component';
import { HdTypeCreateComponent } from './dbx-hd/hdType-create.component';
import { HdService } from './dbx-hd/hd.service';

import { ProvsComponent } from './dbc-provs/provs.component';
import { ProvsCreateComponent } from './dbc-provs/provs-create.component';
import { MoreProvComponent } from './dbc-provs/moreProv.component';
import { ProvsSelectComponent } from './dbc-provs/provs-select.component';
import { ProvsService } from './dbc-provs/provs.service';

import { ArtsComponent } from './dbc-arts/arts.component';
import { ArtsListComponent } from './dbc-arts/arts-list.component';
import { ArtsCreateComponent } from './dbc-arts/arts-create.component';
import { ArtsSelectComponent } from './dbc-arts/arts-select.component';
import { DepsListComponent } from './dbc-arts/deps-list.component';
import { DepsCreateComponent } from './dbc-arts/deps-create.component';
import { SubDepsListComponent } from './dbc-arts/subDeps-list.component';
import { SubDepsCreateComponent } from './dbc-arts/subDeps-create.component';
import { ArtTypesListComponent } from './dbc-arts/artTypes-list.component';
import { ArtTypesCreateComponent } from './dbc-arts/artTypes-create.component';
import { ArtService } from './dbc-arts/art.service';

import { FactsComponent } from './dbx-facts/facts.component';
import { FactsCreateComponent } from './dbx-facts/facts-create.component';
import { FactsSeeOneComponent } from './dbx-facts/facts-seeone.component';
import { FactModelCreateComponent } from './dbc-factmodels/factmodel-create.component';
import { FactModelSelectComponent } from './dbc-factmodels/factmodel-select.component';
import { FactsHdComponent } from './dbx-facts/facts-hd.component';
import { FactsService } from './dbx-facts/facts.service';
import { ArtFactsListComponent } from './dbx-facts/art-facts-list.component';
import { FactModelsService } from './dbc-factmodels/factmodel.service';

import { CardsComponent } from './dbx-cards/cards.component';
import { CardTpvsCreateComponent } from './dbx-cards/cardTpvs-create.component';
import { CardPayoutsCreateComponent } from './dbx-cards/cardPayouts-create.component';
import { CardsHdComponent } from './dbx-cards/cards-hd.component';
import { CardsTotalsComponent } from './dbx-cards/cards-totals.component';
import { CardsTotalsCreateComponent } from './dbx-cards/cards-totals-create.component';
import { CardsTotalsGroupComponent } from './dbx-cards/cards-totals-group.component';

import { CardsService } from './dbx-cards/cards.service';

import { MonthsComponent } from './dbx-months/months.component';
import { MonthsHdsComponent } from './dbx-months/months-hds.component';
import { MonthsCardsComponent } from './dbx-months/months-cards.component';
import { MonthsDinStartComponent } from './dbx-months/months-dinstart.component';
import { MonthsDinoutComponent } from './dbx-months/months-dinout.component';
import { MonthsSummComponent } from './dbx-months/months-summ.component';
import { MonthsService } from './dbx-months/months.service';

import { MaqsComponent } from './dbx-maqs/maqs.component';
import { MaqsCreateComponent } from './dbx-maqs/maqs-create.component';
import { MaqsService } from './dbx-maqs/maqs.service';


registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    NavComponent,
    PageNotFoundComponent,
    ContactComponent,
    InfoComponent,
    LoginComponent,
    ShowErrorComponent, 
    ShowMsgObjectComponent,
    GetDeleteComfirmationComponent,
    ValidationMessagesComponent,
    ValidInputBootstrapCssDirective,
    InvalidInputBootstrapCssDirective,
    SelectRequiredValidatorDirective,
    EnterDateInformationComponent,
    EnterDateComponent,
    EnterDateRangeComponent,
    EnterDecimalNumbersComponent,
    EnterSelectDecimalsComponent,
    SelectMonthComponent,
    EnterMonthComponent,
    SelectValueRecordListComponent,
    SelectManyVRListComponent,
    ToolsComponent,
    GbViewsComponent,
    BaresComponent, 
    BaresCreateComponent,  
    FieldsCreateComponent,
    ValuesCreateComponent,
    HdComponent,
    HdTpvsComponent,
    HdSumsComponent,
    HdTypeCreateComponent,
    ProvsComponent,
    ProvsCreateComponent,
    MoreProvComponent,
    ProvsSelectComponent,
    ArtsComponent,
    ArtsListComponent,
    ArtsCreateComponent,
    ArtsSelectComponent,
    DepsListComponent,
    DepsCreateComponent,
    SubDepsListComponent,
    SubDepsCreateComponent,
    ArtTypesListComponent,
    ArtTypesCreateComponent,
    FactsComponent,
    FactsCreateComponent,
    FactsSeeOneComponent,
    FactsHdComponent,
    FactModelCreateComponent,
    FactModelSelectComponent,
    ArtFactsListComponent,
    CardsComponent,
    CardTpvsCreateComponent,
    CardPayoutsCreateComponent,
    CardsHdComponent,
    CardsTotalsComponent,
    CardsTotalsCreateComponent,
    CardsTotalsGroupComponent,
    MonthsComponent,
    MonthsHdsComponent,
    MonthsCardsComponent,
    MonthsDinStartComponent,
    MonthsDinoutComponent,
    MonthsSummComponent,
    MaqsComponent,
    MaqsCreateComponent, 

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    OrderModule,
    FilterPipeModule,
    AppRoutingModule
  ],
  providers: [ DatePipe,{ provide: LOCALE_ID, useValue: 'es-ES' }, 
              { provide: HTTP_INTERCEPTORS, useClass: ErrorIntercept, multi: true },
              { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
                GlobalVarService, ErrorService, ValidationService, HelloService, GbViewsService, ConfigService,
                FieldsService, HdService, ProvsService, ArtService, FactsService, FactModelsService, CardsService, 
                MonthsService, MaqsService ],
  exports:[
    BsDatepickerModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [  ShowErrorComponent, ShowMsgObjectComponent, GetDeleteComfirmationComponent, ValidationMessagesComponent,
                      EnterDateInformationComponent,EnterDateComponent, EnterDateRangeComponent, EnterMonthComponent, SelectMonthComponent,
                      EnterDecimalNumbersComponent, EnterSelectDecimalsComponent, 
                      SelectValueRecordListComponent, SelectManyVRListComponent,
                      FieldsCreateComponent, ValuesCreateComponent, BaresCreateComponent,
                      HdTpvsComponent, HdSumsComponent, HdTypeCreateComponent, 
                      ProvsCreateComponent, ProvsSelectComponent, MoreProvComponent, 
                      ArtsSelectComponent, ArtsListComponent, ArtsCreateComponent, DepsListComponent, DepsCreateComponent, 
                      SubDepsListComponent, SubDepsCreateComponent, ArtTypesListComponent, ArtTypesCreateComponent,
                      FactsCreateComponent, FactsSeeOneComponent, FactsHdComponent, FactModelCreateComponent, FactModelSelectComponent,
                      CardTpvsCreateComponent, CardPayoutsCreateComponent, CardsHdComponent,
                      CardsTotalsComponent, CardsTotalsCreateComponent, CardsTotalsGroupComponent,
                      MonthsHdsComponent, MonthsCardsComponent, MonthsDinStartComponent, MonthsDinoutComponent, MonthsSummComponent,
                      ArtFactsListComponent, MaqsCreateComponent ]
})
export class AppModule { }