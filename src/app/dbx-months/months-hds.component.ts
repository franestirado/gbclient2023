import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { Hd, HdString } from '../dbx-hd/hd'; 

@Component({
  selector      :'app-months-hds',
  templateUrl   :'./months-hds.component.html',
  styleUrls     :['./months.component.css'] 
})
export class MonthsHdsComponent implements OnInit {
  public hdsMonth       :Hd[];
  callback              :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {     
    'monthHeader'         :'Resumen Mes',
    'monthChange'         :'Cambiar Mes',
    'monthListYearString' :'',
    'monthListMonth'      :'Mes ',
    'monthListMonthString':'',
    'monthListStart'      :'',
    'monthListEnd'        :'',
    'monthEdit'           :'Hojas Dia',
    'monthsHdsTotalString':'',
  };
  public formLabels = { 
    'id'                :'#######',
    'hdId'              :'HdId',
    'hdDate'            :'Fecha',
    'hdName'            :'Nombre',
    'hdType'            :'Tipo',
    'hdPlace'           :'Posicion',
    'hdAmount'          :'Total',
    'hdOrder'           :'Orden',
    'hdNotes'           :'Notas',    
    'hdStatus'          :'Estado',
    'monthTpvName'      :'Caja',  
    'monthTpvTickets'   :'Tickets',
    'monthTpvTicket'    :'Ticket',
    'monthTpvMoney'     :'Dinero',
    'monthTpvDiff'      :'Dif.Din-Ticket',
    'monthConcept'      :'Concepto',
    'monthFactsProv'    :'Proveedor',
    'monthFactsType'    :'Tipo Fact',    
    'monthResName'      :'Concepto',
    'monthResDStart'    :'D.Inicio',
    'monthResTSum'      :'T.Sumas',
    'monthResTFact'     :'T.Fact',
    'monthResDEnd'      :'D.Fin',
    'monthResDCont'     :'D.Contado',
  };
  private logToConsole          :boolean;
  private sortedCollection      :any[];
  public  caseInsensitive       :boolean;
  public  orderMonthHdsList     :string;
  public  reverseMonthHdsList   :boolean;
  public  showSearchMonthHds    :boolean;
  public  searchMonthHds        :HdString;
  private monthsHdsNamesList    :ValueRecord[];
  private monthsHdsTypesList    :ValueRecord[];
  private monthsHdsPlacesList   :ValueRecord[];
  private selectedMonthsHdsName :string;
  private selectedMonthsHdsType :string;
  private selectedMonthsHdsPlace :string;
  public  monthsHdsTotalString  :string;

  /*---------------------------------------MONTHS HDS -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private globalVar: GlobalVarService) { }
  /*---------------------------------------MONTHS HDS -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.monthListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.globalVar.workingDate.getFullYear();
    this.formTitles.monthListYearString = workingYear.toString();
    this.formTitles.monthListStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
    this.formTitles.monthListEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
    this.searchMonthHds = new HdString();
    this.searchMonthHds.hdName = '';
    this.searchMonthHds.hdType = '';
    this.searchMonthHds.hdPlace = '';
    this.monthsHdsTotalString = '';
    this.calculateMonthsHdsTotal();
    this.prepareSelectVRLists();
  }
  /*---------------------------------------MONTHS HD -- FORMS---------------------------------------------------------------*/

  /*---------------------------------------MONTHS HD -- GENERAL ------------------------------------------------------------*/
  calculateMonthsHdsTotal(){
    if (this.hdsMonth.length < 1) return; 
    let hdsMonthTotal = 0;
    let hdsMonthQty = 0;
    if ( (this.searchMonthHds.hdName === '') && (this.searchMonthHds.hdType === '') && (this.searchMonthHds.hdPlace === '') ) {
        for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
    }
    if ( (this.searchMonthHds.hdName != '') && (this.searchMonthHds.hdType != '') && (this.searchMonthHds.hdPlace != '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdName === this.selectedMonthsHdsName) && (this.hdsMonth[i].hdType === this.selectedMonthsHdsType) && 
              (this.hdsMonth[i].hdPlace=== this.selectedMonthsHdsPlace) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName === '') && (this.searchMonthHds.hdType != '') && (this.searchMonthHds.hdPlace != '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdType === this.selectedMonthsHdsType) && (this.hdsMonth[i].hdPlace=== this.selectedMonthsHdsPlace) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName != '') && (this.searchMonthHds.hdType === '') && (this.searchMonthHds.hdPlace != '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdName === this.selectedMonthsHdsName) && (this.hdsMonth[i].hdPlace=== this.selectedMonthsHdsPlace) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName != '') && (this.searchMonthHds.hdType != '') && (this.searchMonthHds.hdPlace === '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdName === this.selectedMonthsHdsName) && (this.hdsMonth[i].hdType === this.selectedMonthsHdsType) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName === '') && (this.searchMonthHds.hdType === '') && (this.searchMonthHds.hdPlace != '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if ( (this.hdsMonth[i].hdPlace=== this.selectedMonthsHdsPlace) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName != '') && (this.searchMonthHds.hdType === '') && (this.searchMonthHds.hdPlace === '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdName === this.selectedMonthsHdsName) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    if ( (this.searchMonthHds.hdName === '') && (this.searchMonthHds.hdType != '') && (this.searchMonthHds.hdPlace === '') ) {
      for (let i = 0 ; i < this.hdsMonth.length ; i++ ) {
        if (  (this.hdsMonth[i].hdType === this.selectedMonthsHdsType) ) {
          hdsMonthTotal = hdsMonthTotal + this.hdsMonth[i].hdAmount;
          hdsMonthQty = hdsMonthQty + 1;
        }
      }
    }
    this.monthsHdsTotalString = hdsMonthQty.toFixed(0)+'..Hds..Total->' + hdsMonthTotal.toFixed(2);
  }
  prepareSelectVRLists(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs HDs->prepareSelectVRLists->starting->', null);
    if (this.hdsMonth.length < 1) return;    
    this.sortedCollection = this.orderPipe.transform(this.hdsMonth,'hdName',true,true);
    this.hdsMonth = this.sortedCollection;
    this.monthsHdsNamesList = new Array();
    var index = 0;
    this.monthsHdsNamesList[index]       = new ValueRecord();
    this.monthsHdsNamesList[index].id    = index;
    this.monthsHdsNamesList[index].value = 'Selecc.Nombre';
    index = index + 1;
    this.monthsHdsNamesList[index]       = new ValueRecord();
    this.monthsHdsNamesList[index].id    = index;
    this.monthsHdsNamesList[index].value = this.hdsMonth[0].hdName;
    index = index + 1;
    for (var i=1; i<this.hdsMonth.length; i++) {
      if (this.hdsMonth[i].hdName!=this.hdsMonth[i-1].hdName) {
        this.monthsHdsNamesList[index]       = new ValueRecord();
        this.monthsHdsNamesList[index].id    = index;
        this.monthsHdsNamesList[index].value = this.hdsMonth[i].hdName;
        index = index + 1;
      } 
    } 
    this.sortedCollection = this.orderPipe.transform(this.hdsMonth,'hdType',true,true);
    this.hdsMonth = this.sortedCollection;
    this.monthsHdsTypesList = new Array();
    var index = 0;
    this.monthsHdsTypesList[index]       = new ValueRecord();
    this.monthsHdsTypesList[index].id    = index;
    this.monthsHdsTypesList[index].value = 'Selecc.Tipo';
    index = index + 1;
    this.monthsHdsTypesList[index]       = new ValueRecord();
    this.monthsHdsTypesList[index].id    = index;
    this.monthsHdsTypesList[index].value = this.hdsMonth[0].hdType;
    index = index + 1;
    for (var i=1; i<this.hdsMonth.length; i++) {
      if (this.hdsMonth[i].hdType!=this.hdsMonth[i-1].hdType) {
        this.monthsHdsTypesList[index]       = new ValueRecord();
        this.monthsHdsTypesList[index].id    = index;
        this.monthsHdsTypesList[index].value = this.hdsMonth[i].hdType;
        index = index + 1;
      } 
    }     
    this.sortedCollection = this.orderPipe.transform(this.hdsMonth,'hdPlace',true,true);
    this.hdsMonth = this.sortedCollection;
    this.monthsHdsPlacesList = new Array();
    var index = 0;
    this.monthsHdsPlacesList[index]       = new ValueRecord();
    this.monthsHdsPlacesList[index].id    = index;
    this.monthsHdsPlacesList[index].value = 'Selecc.Posicion';
    index = index + 1;
    this.monthsHdsPlacesList[index]       = new ValueRecord();
    this.monthsHdsPlacesList[index].id    = index;
    this.monthsHdsPlacesList[index].value = this.hdsMonth[0].hdPlace;
    index = index + 1;
    for (var i=1; i<this.hdsMonth.length; i++) {
      if (this.hdsMonth[i].hdPlace!=this.hdsMonth[i-1].hdPlace) {
        this.monthsHdsPlacesList[index]       = new ValueRecord();
        this.monthsHdsPlacesList[index].id    = index;
        this.monthsHdsPlacesList[index].value = this.hdsMonth[i].hdPlace;
        index = index + 1;
      } 
    } 
    this.sortedCollection = this.orderPipe.transform(this.hdsMonth,'hdName',true,true);
    this.hdsMonth = this.sortedCollection;
  }
  /*---------------------------------------MONTHS HD -- BTN CLICK-----------------------------------------------------------*/
  closeMonthHdsModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();  
    }
  }
  selectMonthsHdsNameBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs HDs->selectMonthsHdsNameBtnClick->starting->', null); 
    if (this.monthsHdsNamesList.length < 1) { return;  }   
    const modalInitialState = {
      localValueRecordList  :this.monthsHdsNamesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar CONCEPTO",
      selectMessage         :"Seleccione el CONCEPTO de los registros que quiere ver",
      selectLabel           :"Concepto Hojas Dia:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsHdsName = result; } 
      else { this.selectedMonthsHdsName = ''; }
      this.searchMonthHds.hdName = this.selectedMonthsHdsName;
      this.calculateMonthsHdsTotal();
    })
  }
  selectMonthsHdsTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs HDs->selectMonthsHdsTypeBtnClick->starting->', null); 
    if (this.monthsHdsTypesList.length < 1) { return;  }    
    const modalInitialState = {
      localValueRecordList  :this.monthsHdsTypesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar TIPO",
      selectMessage         :"Seleccione el TIPO de los registros que quiere ver",
      selectLabel           :"Tipo Hojas Dia:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsHdsType = result; } 
      else { this.selectedMonthsHdsType = ''; }
      this.searchMonthHds.hdType = this.selectedMonthsHdsType;  
      this.calculateMonthsHdsTotal(); 
    })
  }
  selectMonthsHdsPlaceBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs HDs->selectMonthsHdsPlaceBtnClick->starting->', null); 
    if (this.monthsHdsPlacesList.length < 1) { return;  }   
    const modalInitialState = {
      localValueRecordList  :this.monthsHdsPlacesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar POSICION",
      selectMessage         :"Seleccione la POSICION de los registros que quiere ver",
      selectLabel           :"Posicion Hojas Dia:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsHdsPlace = result; } 
      else { this.selectedMonthsHdsPlace = ''; }
      this.searchMonthHds.hdPlace = this.selectedMonthsHdsPlace; 
      this.calculateMonthsHdsTotal();  
    })
  }
  searchMonthHdsBtnClick() {
    if (this.showSearchMonthHds === true) {
      this.showSearchMonthHds = false;
    } else {
      this.showSearchMonthHds = true;
    }
    return;
  }
  setOrderMonthsHdBtnClick(value: string) {
    if (this.orderMonthHdsList === value) {
      this.reverseMonthHdsList = !this.reverseMonthHdsList;
    }
    this.orderMonthHdsList = value;
  }
  /*---------------------------------------MONTHS HD -- DATABASE -----------------------------------------------------------*/
  /*---------------------------------------MONTHS HD -- DELETE -------------------------------------------------------------*/

}