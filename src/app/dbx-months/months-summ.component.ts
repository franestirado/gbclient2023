import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { Month, MonthString } from './months'; 

@Component({
  selector      :'app-months-added',
  templateUrl   :'./months-summ.component.html',
  styleUrls     :['./months.component.css'] 
})
export class MonthsSummComponent implements OnInit {
  public monthSummRecords   :Month[];
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {     
    'monthHeader'         :'Resumen Mes',
    'monthChange'         :'Cambiar Mes',
    'monthListYearString' :'',
    'monthListMonth'      :'Mes ',
    'monthListMonthString':'',
    'monthListStart'      :'',
    'monthListEnd'        :'',
    'monthEdit'           :'Resumen Mes',
    'monthsAddedTotalString':'',

  };
  public formLabels = { 
    'id'                  :'#######',
    'monthId'             :'MonthId',
    'monthDate'           :'Fecha',
    'monthName'           :'Nombre',
    'monthType'           :'Tipo',
    'monthPlace'          :'Posicion',
    'monthAmount'         :'Total',
    'monthOrder'          :'Orden',
    'monthNotes'          :'Notas',    
    'monthStatus'         :'Estado',
    'monthTpvName'        :'Caja',  
    'monthTpvTickets'     :'Tickets',
    'monthTpvTicket'      :'Ticket',
    'monthTpvMoney'       :'Dinero',
    'monthTpvDiff'        :'Dif.Din-Ticket',
    'monthConcept'        :'Concepto',
    'monthFactsProv'      :'Proveedor',
    'monthFactsType'      :'Tipo Fact',    
    'monthResName'        :'Concepto',
    'monthResDStart'      :'D.Inicio',
    'monthResTSum'        :'T.Sumas',
    'monthResTFact'       :'T.Fact',
    'monthResDEnd'        :'D.Fin',
    'monthResDCont'       :'D.Contado',
  };

  private logToConsole                  :boolean;
  private sortedCollection              :any[];
  public  caseInsensitive               :boolean;
  public  orderMonthSummRecords         :string;
  public  reverseMonthSummRecords       :boolean;
  public  showSearchMonthSummRecord     :boolean;
  public  searchMonthSummRecord         :MonthString;
  private monthsAddedNamesList          :ValueRecord[];
  private monthsAddedTypesList          :ValueRecord[];
  private monthsAddedPlacesList         :ValueRecord[];
  private selectedMonthsAddedName       :string;
  private selectedMonthsAddedType       :string;
  private selectedMonthsAddedPlace      :string;
  public  monthSummRecordsTotalString   :string;

  /*---------------------------------------MONTHS ADDED -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private globalVar: GlobalVarService ) { }
  /*---------------------------------------MONTHS ADDED -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.monthListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.globalVar.workingDate.getFullYear();
    this.formTitles.monthListYearString = workingYear.toString();
    this.formTitles.monthListStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
    this.formTitles.monthListEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
    this.monthSummRecordsTotalString = 'Total->'
    this.searchMonthSummRecord = new MonthString(); 
       
    this.searchMonthSummRecord.monthName = '';
    this.searchMonthSummRecord.monthType = '';
    this.searchMonthSummRecord.monthPlace = ''; 
    this.calculateMonthsSummRecordsTotal();
    this.prepareSelectVRLists();
  }
  /*---------------------------------------MONTHS ADDED -- FORMS---------------------------------------------------------------*/

  /*---------------------------------------MONTHS ADDED -- GENERAL ------------------------------------------------------------*/
  calculateMonthsSummRecordsTotal(){
    if (this.monthSummRecords === null) return;
    if (this.monthSummRecords.length < 1) return; 
    let monthSummRecordsTotal = 0;
    let monthSummRecordsQty = 0;
    if ( (this.searchMonthSummRecord.monthName === '') && (this.searchMonthSummRecord.monthType === '') && (this.searchMonthSummRecord.monthPlace === '') ) {
        for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
    }
    if ( (this.searchMonthSummRecord.monthName != '') && (this.searchMonthSummRecord.monthType != '') && (this.searchMonthSummRecord.monthPlace != '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthName === this.selectedMonthsAddedName) && (this.monthSummRecords[i].monthType === this.selectedMonthsAddedType) && 
              (this.monthSummRecords[i].monthPlace=== this.selectedMonthsAddedPlace) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName === '') && (this.searchMonthSummRecord.monthType != '') && (this.searchMonthSummRecord.monthPlace != '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthType === this.selectedMonthsAddedType) && (this.monthSummRecords[i].monthPlace=== this.selectedMonthsAddedPlace) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName != '') && (this.searchMonthSummRecord.monthType === '') && (this.searchMonthSummRecord.monthPlace != '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthName === this.selectedMonthsAddedName) && (this.monthSummRecords[i].monthPlace=== this.selectedMonthsAddedPlace) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName != '') && (this.searchMonthSummRecord.monthType != '') && (this.searchMonthSummRecord.monthPlace === '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthName === this.selectedMonthsAddedName) && (this.monthSummRecords[i].monthType === this.selectedMonthsAddedType) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName === '') && (this.searchMonthSummRecord.monthType === '') && (this.searchMonthSummRecord.monthPlace != '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if ( (this.monthSummRecords[i].monthPlace=== this.selectedMonthsAddedPlace) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName != '') && (this.searchMonthSummRecord.monthType === '') && (this.searchMonthSummRecord.monthPlace === '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthName === this.selectedMonthsAddedName) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    if ( (this.searchMonthSummRecord.monthName === '') && (this.searchMonthSummRecord.monthType != '') && (this.searchMonthSummRecord.monthPlace === '') ) {
      for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
        if (  (this.monthSummRecords[i].monthType === this.selectedMonthsAddedType) ) {
          monthSummRecordsTotal = monthSummRecordsTotal + this.monthSummRecords[i].monthAmount;
          monthSummRecordsQty = monthSummRecordsQty + 1;
        }
      }
    }
    this.monthSummRecordsTotalString = monthSummRecordsQty.toFixed(0)+'..Months..Total->' + monthSummRecordsTotal.toFixed(2);
}
  prepareSelectVRLists(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs SUMM->prepareSelectVRLists->starting->', null);
    if (this.monthSummRecords === null) return;  
    if (this.monthSummRecords.length < 1) return;    
    this.sortedCollection = this.orderPipe.transform(this.monthSummRecords,'monthName',true,true);
    this.monthSummRecords = this.sortedCollection;
    this.monthsAddedNamesList = new Array();
    var index = 0;
    this.monthsAddedNamesList[index]       = new ValueRecord();
    this.monthsAddedNamesList[index].id    = index;
    this.monthsAddedNamesList[index].value = 'Selecc.Nombre';
    index = index + 1;
    this.monthsAddedNamesList[index]       = new ValueRecord();
    this.monthsAddedNamesList[index].id    = index;
    this.monthsAddedNamesList[index].value = this.monthSummRecords[0].monthName;
    index = index + 1;
    for (var i=1; i<this.monthSummRecords.length; i++) {
      if (this.monthSummRecords[i].monthName!=this.monthSummRecords[i-1].monthName) {
        this.monthsAddedNamesList[index]       = new ValueRecord();
        this.monthsAddedNamesList[index].id    = index;
        this.monthsAddedNamesList[index].value = this.monthSummRecords[i].monthName;
        index = index + 1;
      } 
    } 
    this.sortedCollection = this.orderPipe.transform(this.monthSummRecords,'monthType',true,true);
    this.monthSummRecords = this.sortedCollection;
    this.monthsAddedTypesList = new Array();
    var index = 0;
    this.monthsAddedTypesList[index]       = new ValueRecord();
    this.monthsAddedTypesList[index].id    = index;
    this.monthsAddedTypesList[index].value = 'Selecc.Tipo';
    index = index + 1;
    this.monthsAddedTypesList[index]       = new ValueRecord();
    this.monthsAddedTypesList[index].id    = index;
    this.monthsAddedTypesList[index].value = this.monthSummRecords[0].monthType;
    index = index + 1;
    for (var i=1; i<this.monthSummRecords.length; i++) {
      if (this.monthSummRecords[i].monthType!=this.monthSummRecords[i-1].monthType) {
        this.monthsAddedTypesList[index]       = new ValueRecord();
        this.monthsAddedTypesList[index].id    = index;
        this.monthsAddedTypesList[index].value = this.monthSummRecords[i].monthType;
        index = index + 1;
      } 
    }     
    this.sortedCollection = this.orderPipe.transform(this.monthSummRecords,'monthPlace',true,true);
    this.monthSummRecords = this.sortedCollection;
    this.monthsAddedPlacesList = new Array();
    var index = 0;
    this.monthsAddedPlacesList[index]       = new ValueRecord();
    this.monthsAddedPlacesList[index].id    = index;
    this.monthsAddedPlacesList[index].value = 'Selecc.Posicion';
    index = index + 1;
    this.monthsAddedPlacesList[index]       = new ValueRecord();
    this.monthsAddedPlacesList[index].id    = index;
    this.monthsAddedPlacesList[index].value = this.monthSummRecords[0].monthPlace;
    index = index + 1;
    for (var i=1; i<this.monthSummRecords.length; i++) {
      if (this.monthSummRecords[i].monthPlace!=this.monthSummRecords[i-1].monthPlace) {
        this.monthsAddedPlacesList[index]       = new ValueRecord();
        this.monthsAddedPlacesList[index].id    = index;
        this.monthsAddedPlacesList[index].value = this.monthSummRecords[i].monthPlace;
        index = index + 1;
      } 
    } 
    this.sortedCollection = this.orderPipe.transform(this.monthSummRecords,'monthName',true,true);
    this.monthSummRecords = this.sortedCollection;
  }
  /*---------------------------------------MONTHS ADDED -- BTN CLICK-----------------------------------------------------------*/
  closeMonthSummModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();  
    }
  }
  selectMonthSummRecordsNameBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs SUMM->selectMonthSummRecordsNameBtnClick->starting->', null); 
    if (this.monthsAddedNamesList === null) return;
    if (this.monthsAddedNamesList.length < 1) return;
    const modalInitialState = {
      localValueRecordList  :this.monthsAddedNamesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar CONCEPTO",
      selectMessage         :"Seleccione el CONCEPTO de los registros que quiere ver",
      selectLabel           :"Concepto Registros Resumen:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsAddedName = result; } 
      else { this.selectedMonthsAddedName = ''; }
      this.searchMonthSummRecord.monthName = this.selectedMonthsAddedName;
      this.calculateMonthsSummRecordsTotal();
    })
  }
  selectMonthSummRecordsTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->MONTHs SUMM->selectMonthsAddedsTypeBtnClick->starting->', null); 
    if (this.monthsAddedTypesList === null) return;
    if (this.monthsAddedTypesList.length < 1) return;
    const modalInitialState = {
      localValueRecordList  :this.monthsAddedTypesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar TIPO",
      selectMessage         :"Seleccione el TIPO de los registros que quiere ver",
      selectLabel           :"Tipo Registros Resumen:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsAddedType = result; } 
      else { this.selectedMonthsAddedType = ''; }
      this.searchMonthSummRecord.monthType = this.selectedMonthsAddedType;  
      this.calculateMonthsSummRecordsTotal(); 
    })
  }
  selectMonthSummRecordsPlaceBtnClick(){
    if (this.monthsAddedPlacesList === null) return;
    if (this.monthsAddedPlacesList.length < 1) return;
    const modalInitialState = {
      localValueRecordList  :this.monthsAddedPlacesList,
      selectTitle1          :"Mes Resumen",
      selectTitle2          :"Seleccionar POSICION",
      selectMessage         :"Seleccione la POSICION de los registros que quiere ver",
      selectLabel           :"Posicion Registros Resumen:",       
      selectFooter          :"Mes Resumen",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) { this.selectedMonthsAddedPlace = result; } 
      else { this.selectedMonthsAddedPlace = ''; }
      this.searchMonthSummRecord.monthPlace = this.selectedMonthsAddedPlace; 
      this.calculateMonthsSummRecordsTotal(); 
    })
  }
  searchMonthSummRecordBtnClick() {
    if (this.showSearchMonthSummRecord === true) {
      this.showSearchMonthSummRecord = false;
    } else {
      this.showSearchMonthSummRecord = true;
    }
    return;
  }
  setOrderMonthSummRecordsBtnClick(value: string) {
    if (this.orderMonthSummRecords === value) {
      this.reverseMonthSummRecords = !this.reverseMonthSummRecords;
    }
    this.orderMonthSummRecords = value;
  }
  /*---------------------------------------MONTHS ADDED -- DATABASE -----------------------------------------------------------*/
  /*---------------------------------------MONTHS ADDED -- DELETE -------------------------------------------------------------*/

}