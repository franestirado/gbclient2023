import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Month, MonthTpv } from './months';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
@Injectable({  providedIn: 'root'})
export class MonthsService {
  constructor(private http: HttpClient) { }
    private json: string;
    private params: string;
    private headers: HttpHeaders;

  /*---------------------------------------- MONTHS ----------------------------------------------------------*/
  getMonthSummRecords(firstAndLastDayOfYear: Date[]): Observable<Month[]> {
    return this.http.post<Month[]>('/months/getMonths',firstAndLastDayOfYear)
  }
  getRetCountPrevMonthSummRecords(firstAndLastDayOfMonth: Date[]): Observable<Month[]> {
    return this.http.post<Month[]>('/months/getRetPrevMonths',firstAndLastDayOfMonth)
  }
  getRetStartCountMonthSummRecords(firstAndLastDayOfMonth: Date[]): Observable<Month[]> {
    return this.http.post<Month[]>('/months/getRetMonths',firstAndLastDayOfMonth)
  }
  saveInDBMonthSummRecords(newMonthSummRecords: Month[]): Observable<Month[]> {
    return this.http.post<Month[]>('/months/createMonths', newMonthSummRecords)
  }
  updateInDBMonthSummRecords(updatedMonthSummRecords: Month[]): Observable<Month[]> {
    return this.http.post<Month[]>('/months/updateMonths', updatedMonthSummRecords)
  }
  deleteMonthsRecords(firstAndLastDayOfMonth: Date[]): Observable<any>  {
    return this.http.post<number>('/months/delMonths',firstAndLastDayOfMonth)
  }
  /*---------------------------------------- MONTHS TPVS----------------------------------------------------------*/
  getMonthTpvSummRecords(firstAndLastDayOfYear: Date[]): Observable<MonthTpv[]> {
    return this.http.post<MonthTpv[]>('/months/getMonthTpvs',firstAndLastDayOfYear)
  }
  saveInDBMonthsTpvSummRecords(newMonthsTpvslist: MonthTpv[]): Observable<MonthTpv[]> {
    return this.http.post<MonthTpv[]>('/months/createMonthsTpvs', newMonthsTpvslist)
  }
}