import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray,FormControl,AbstractControl } from '@angular/forms';
import { Subscription, throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsDatepickerConfig,BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord,ValueRecordNumber } from '../dbc-fields/field';
import { ValidationService } from '../aa-common/validation.service';

import { Fact,ArtFact } from './facts';
import { FactModels } from '../dbc-factmodels/factmodel';
import { FactModelSelectComponent } from '../dbc-factmodels/factmodel-select.component';
import { FactModelCreateComponent } from '../dbc-factmodels/factmodel-create.component';
import { FactModelsService } from '../dbc-factmodels/factmodel.service';
import { FactsService } from './facts.service';
import { Prov } from '../dbc-provs/prov';
import { ProvsSelectComponent } from '../dbc-provs/provs-select.component';
import { ProvsCreateComponent } from '../dbc-provs/provs-create.component';
import { Art } from '../dbc-arts/art';
import { ArtsSelectComponent } from '../dbc-arts/arts-select.component';
import { ArtsCreateComponent } from '../dbc-arts/arts-create.component';
import { SubDep } from '../dbc-arts/art';
import { ArtService } from '../dbc-arts/art.service';

@Component({
  selector      :'app-facts-create',
  templateUrl   :'./facts-create.component.html',
  styleUrls     :['./facts.component.css']   
})
export class FactsCreateComponent implements OnInit {
  public newFact                :Fact;
  public factsTitle             :string;
  public factsDate              :Date;
  public createFact             :boolean;
  public createDetailedFact     :boolean;  
  public editFact               :boolean;
  public editDetailedFact       :boolean;
  public changeFactPayType      :boolean;
  public selectedFactPayType    :string;
  public changeFactDate         :boolean;
  public hdFlag                 :boolean;
  public monthFlag              :boolean;
  callback                      :any;
  result: Subject<any> = new Subject<any>();
   
  public formTitles = {
    'factsCreateTitle'    :'Crear Factura',  
    'factsTitle'          :'Facturas',   
    'factsDate'           :'',
    'factCreate'          :'Crear Factura',
    'factDCreate'         :'Crear Factura Detallada',
    'factEdit'            :'Modificar Factura',
    'factDEdit'           :'Modificar Factura Detallada',
    'factArtsTotal'       :'',
    'hdDate'              :'',
    'artDeleteOne'        :'¿ Seguro que quiere BORRAR este ARTÍCULO de esta FACTURA?',

  };
  public formLabels = {
    'factId'              :'Fact_Id',
    'factProvId'          :'Prov_Id',
    'factProvName'        :'Proveedor',
    'factNumber'          :'Nº Factura',
    'factDate'            :'Fecha',
    'factType'            :'Tipo',
    'factNetCost'         :'Neto',
    'factTaxesP'          :'% IVA',
    'factTaxes'           :'IVA',
    'factFlag'            :'IVA incl',
    'factTotalCost'       :'Total',
    'factPayType'         :'Pago',
    'factNote'            :'Nota/Fact',
    'factStatus'          :'Estado',
    'factModelName'       :'Modelo',
    'artId'               :'Art_Id',
    'artReference'        :'Referencia',
    'artName'             :'Nombre',
    'artLongName'         :'Descripción',
    'artType'             :'Tipo',
    'artSelect'           :'Seleccionar',
    'artAdd'              :'Añadir',
    'artNameF'            :'Artículo',
    'artUnit'             :'Ud.',
    'artTypeF'            :'Tipo',
    'artSize'             :'Size',
    'artPrice'            :'€/Ud.',            
    'units'               :'Uds/Pk.',
    'price'               :'€/Pk',
    'priceWithTax'        :'€/Pk(Imp)',
    'quantity'            :'Qty',
    'discount'            :'Dcto', 
    'netCost'             :'Coste',
    'artTax'              :'IVA',
    'tax'                 :'Imp',
    'totalCost'           :'Total', 
    'updateArt'           :'ActArt', 
    'deleteArt'           :'Borrar',
  };
  public  factForm                    :FormGroup;   
  public  artsFactForm                :FormGroup;
  public  artsFactFormArray           :FormArray;
  public  filterArtsFactFormArray     :FormArray;
  private artsFactFormArraySubs       :Subscription[];
  public  localPayFactTypesRecordList :ValueRecord[];
  public  localFactTypesRecordList    :ValueRecord[];
  private logToConsole                :boolean;
  public  detailedFact                :boolean;
  private newArtsFactList             :ArtFact[];
  public  localArtTaxesRecordList     :ValueRecord[];
  public  localArtTaxesNumberVR       :ValueRecordNumber[];
  private workingDate                 :Date;
  private newArt                      :Art;
  public  showCreateFact              :boolean;
  public  showCreateDetailedFact      :boolean;  
  public  showEditFact                :boolean;
  public  showEditDetailedFact        :boolean;
  public  showArtsFact                :boolean;
  public  showSearchArt               :boolean;
  private _searchArtsFactName          :string;
    get searchArtsFactName():string{
      return this._searchArtsFactName;
    }
    set searchArtsFactName(artsFactName :string){
      this.showArtsFact = false;
      this._searchArtsFactName = artsFactName;
      this.filterArtsFact(artsFactName);
    }
  private arts                        :Art[];
  private selectedArts                :Art[];
  private selectedArtFact             :Art;
  private factModelsArts              :Art[];
  public  artPricesWithTaxesFlag      :boolean;
  private newFactModel                :FactModels;
  private selectedFactModel           :FactModels;
  private factModels                  :FactModels[];
  private provs                       :Prov[];
  private selectedProv                :Prov;
  private newProv                     :Prov;
  private searchFactModel             :FactModels;
  private showSearchFactModels        :boolean;
  private orderFactModelsList         :string;
  private reverseFactModelsList       :boolean;
  public  subDeps                     :SubDep[];
  public  orderSubDepsList            :string;
  public  reverseSubDepsList          :boolean;
  private sortedCollection            :any[];
  private caseInsensitive             :boolean;
  public  factTotalCostFlag           :boolean;  
  private factTotalCostSubs           :Subscription;
  private factNetCostSubs             :Subscription;
  private factTaxesSubs               :Subscription;
  private factTaxesPSubs              :Subscription;
  public  factsDatePickerConfig       :Partial <BsDatepickerConfig>; 
  private localMinMode                :BsDatepickerViewMode;
  private dateContainerClass          :string;
  private dateInputFormat             :string;
  public  orderArtsList               :string;
  public  reverseArtsList             :boolean;
  private internalArtFactIdIndex      :number;
  //--------------------------------------- FACTS CREATE -- CONSTRUCTOR -------------------------------------------------------------
  constructor (   private fb:FormBuilder, private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService, 
                  private globalVar: GlobalVarService, private _errorService: ErrorService ,private _factsService: FactsService, 
                  private _factModelService:  FactModelsService, private _artService: ArtService) {  }
  //--------------------------------------- FACTS CREATE -- NG ON INIT -------------------------------------------------------------
  ngOnInit(): void { 
    // const reload=()=>window.location.reload(); 
    this.localArtTaxesNumberVR = new Array();
    for (let i=1; i<this.globalVar.artTaxesRecordList.length; i++) {
      this.localArtTaxesNumberVR[i-1] = new ValueRecordNumber();
      this.localArtTaxesNumberVR[i-1].id = i-1;
      this.localArtTaxesNumberVR[i-1].value = Number(this.globalVar.artTaxesRecordList[i].value);
    }
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.internalArtFactIdIndex = 0;
    this.showCreateFact         = false;
    this.showCreateDetailedFact = false;  
    this.showEditFact           = false;
    this.showEditDetailedFact   = false;
    this.showArtsFact           = false;
    this.artPricesWithTaxesFlag = false; 
    this.showSearchArt = false;
    this.orderArtsList    = 'artFactArtName';
    this.reverseArtsList  = true;
    this.caseInsensitive  = true;
    this.dateInputFormat = 'DD-MMM-YYYY';
    this.dateContainerClass = 'theme-dark-blue'; 
    if (this.factsDate === null) {      
      this.workingDate = this.globalVar.workingDate;
      this.formTitles.factsDate = this.workingDate.toLocaleDateString();
    } else {
      this.formTitles.factsDate = this.factsDate.toLocaleDateString();
      this.workingDate = this.factsDate;
    }
    this.formTitles.factsTitle = this.factsTitle;
    this.selectedProv = null;        
    this.selectedFactModel = null;  
    this.localArtTaxesRecordList = this.globalVar.artTaxesRecordList;
    if (this.createDetailedFact || this.editDetailedFact) this.detailedFact = true;
    this.factForm = this.fb.group({
        factId:         [{value:'',disabled:true}],
        factProvId:     [{value:'',disabled:true},Validators.required],
        factProvName:   [{value:'',disabled:false},[Validators.required]],
        factModelName:  [{value:'',disabled:false}],
        factNumber:     [{value:''},[Validators.required,Validators.minLength(1),Validators.maxLength(32)]],
        factDate:       [{value:'',disabled:!this.changeFactDate},[Validators.required,ValidationService.dateValidation]],        
        factType:       [{value:''},Validators.required],
        factFlag:       [{value:true,disabled:true},],  
        factTotalGroup: this.fb.group({
          factNetCost:  [{value:'',disabled:this.detailedFact},[Validators.required,ValidationService.decimalValidation]],
          factTaxesP:   [{value:'',disabled:this.detailedFact}],      
          factTaxes:    [{value:'',disabled:this.detailedFact},[Validators.required,ValidationService.decimalValidation]],     
          factTotalCost:[{value:'',disabled:this.detailedFact},[Validators.required,ValidationService.decimalValidation]],
          }),       
        factPayType:    [{value:'',disabled:!this.changeFactPayType},Validators.required],
        factDetailed:   [{value:''},Validators.required ],
        factNote:[{value:''}],
        factStatus:     [{value:''}],
    }) 
    this.factForm.get('factTotalGroup.factTaxesP').patchValue(this.localArtTaxesRecordList[0].value);
    this.factTotalCostFlag = true;
    this.factTaxesPSubs = this.factForm.get('factTotalGroup.factTaxesP').valueChanges.subscribe((data:any) => {
      this.checkFactTaxesP();
    });
    this.subscribeFactTotalCost();

    this.artsFactForm = this.fb.group({
      filterArtsFactFormArray: this.fb.array([])
    });
    this.localFactTypesRecordList = this.globalVar.factTypesSubDepsRecordList;
    this.localPayFactTypesRecordList = this.globalVar.factPayTypesRecordList;
    if ( (this.createFact === true) || (this.createDetailedFact === true) ) { 
      this.copyNewFactDataToFactForm(true);
    }
    if (this.createDetailedFact === true){        
        this.newArtsFactList = new Array();
    }
    if (this.monthFlag === true) { 
      let index = this.localPayFactTypesRecordList.findIndex(item => item.value === 'HojaDia'); //find index in your array
      if (index > -1) { this.localPayFactTypesRecordList.splice(index, 1); }     
      this.setDayPickerConfig(this.factsDate); 
    } else {
      this.setYearPickerConfig(this.factsDate);
    }
    if (this.editFact === true) {
        this.getOneFact(this.newFact, false);        
    }
    if (this.editDetailedFact === true){
        this.getOneFact(this.newFact, true);        
    }
    if (this.createFact === true) {
      this.showCreateFact = true;        
    }
    if (this.createDetailedFact === true) {
      this.showCreateDetailedFact = true;        
    }
  }
  //-----------------------------------------------------------------------------------------------------
  //--------------- FACTS CREATE -- FORM ARRAY FILTERING TOOLS ------------------------------------------ 
  private filterArtsFact(searchArtFactname: string){
    this.filterArtsFactFormArray= new FormArray([]);
    for(let j=0; j< this.newArtsFactList.length; j++){
      if ( this.newArtsFactList[j].artFactArtName.toLowerCase().includes(searchArtFactname.toLowerCase()) ) {
        //this.globalVar.consoleLog(this.logToConsole,'...filterArtsFactFormArray...'+j.toString()+'...included...-->', this.newArtsFactList[j].artFactArtName);
        this.filterArtsFactFormArray.push(this.addArtsFactFormGroup( this.newArtsFactList[j].artFactId, this.newArtsFactList[j].artFactFactId,
          this.newArtsFactList[j].artFactFactDate, this.newArtsFactList[j].artFactArtId, this.newArtsFactList[j].artFactArtName, 
          this.newArtsFactList[j].artFactArtType, this.newArtsFactList[j].artFactArtSize, this.newArtsFactList[j].artFactArtMeasurement, 
          this.newArtsFactList[j].artFactArtUnitPrice, this.newArtsFactList[j].artFactArtPackageUnits, 
          this.newArtsFactList[j].artFactArtPackagePrice,  this.newArtsFactList[j].artFactQuantity,
          this.newArtsFactList[j].artFactDiscount, this.newArtsFactList[j].artFactNetCost, 
          this.newArtsFactList[j].artFactArtTax, 
          this.newArtsFactList[j].artFactTax, this.newArtsFactList[j].artFactTotalCost, false));
      }
    }
    this.filterArtsFactFormArray.updateValueAndValidity();
    this.artsFactForm = new FormGroup({
      filterArtsFactFormArray: this.filterArtsFactFormArray
    });
    this.subsToFormArrayValueChanges(this.filterArtsFactFormArray);
    this.showArtsFact = true;
  }


  //--------------------------------------- FACTS CREATE -- GENERAL ------------------------------------------------------------- 
  private getValueArtTaxesNumberVR(artTaxStringValue : string):number{
    let arTaxNumberValue = 0;
    if ( isNaN(Number(artTaxStringValue)) ) return -1;
    else arTaxNumberValue = Number(artTaxStringValue);     
    for (let i=0; i<this.localArtTaxesNumberVR.length; i++) {
      if (this.localArtTaxesNumberVR[i].value === arTaxNumberValue) return arTaxNumberValue;
    }
    return -1;
  }
  private setDayPickerConfig(selectedDate: Date){
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var firstDayOfMonth = new Date(year,month,1);
    var lastDayOfMonth  = new Date(year,month+1,0); 
    this.localMinMode =  "day";
    this.factsDatePickerConfig = Object.assign ( {}, {
      containerClass:     this.dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            firstDayOfMonth,      
      maxDate:            lastDayOfMonth,
      dateInputFormat:    this.dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } );
  }
  private setYearPickerConfig(selectedDate: Date){
    var selectedDate = new Date();
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var day   = selectedDate.getDate();
    var fiveYearsBefore = new Date(year - 5, month, day);
    var fiveYearsAfter  = new Date(year + 5, month, day);
    this.localMinMode = "day";
    this.factsDatePickerConfig = Object.assign ( {}, {
      containerClass:     this.dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            fiveYearsBefore,      
      maxDate:            fiveYearsAfter,
      dateInputFormat:    this.dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } ); 
  }
  private getInternalArtFactId():number{
    this.internalArtFactIdIndex = this.internalArtFactIdIndex - 1;
    return this.internalArtFactIdIndex;
  }
  public setTwoNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(2);
    }   
  }    
  public setFourNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(4);
    }  
  }  
  public setSixNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(6);
    }  
  } 
  //--------------------------------------- FACTS CREATE -- FORMS-------------------------------------------------------------
  public changeFactFlagBtnClick(){
    let factTaxes     = this.factForm.get('factTotalGroup.factTaxes').value;
    let factNetCost   = this.factForm.get('factTotalGroup.factNetCost').value;
    let factTotalCost = this.factForm.get('factTotalGroup.factTotalCost').value;
    let newFactTotalCost = 0;  
    let newFactNetCost = 0; 
    if (this.factTotalCostFlag === true){
      this.factNetCostSubs.unsubscribe();     
      this.factTotalCostFlag = false;
      this.factForm.get('factTotalGroup.factNetCost').disable(); 
      this.factForm.get('factTotalGroup.factTotalCost').enable();
      newFactNetCost = Number(factTotalCost)- Number(factTaxes);
      this.factForm.get('factTotalGroup').patchValue({factNetCost:newFactNetCost.toFixed(2)});
      this.factTotalCostSubs = this.factForm.get('factTotalGroup.factTotalCost').valueChanges.subscribe((data:any) => {
        this.setFactNetCost();
      });        
    } else {
      this.factTotalCostSubs.unsubscribe();      
      this.factTotalCostFlag = true;
      this.factForm.get('factTotalGroup.factTotalCost').disable();
      this.factForm.get('factTotalGroup.factNetCost').enable(); 
      newFactTotalCost = Number(factNetCost) + Number(factTaxes);
      this.factForm.get('factTotalGroup').patchValue({factTotalCost:newFactTotalCost.toFixed(2)});
      this.factNetCostSubs = this.factForm.get('factTotalGroup.factNetCost').valueChanges.subscribe((data:any) => {
        this.setFactTotalCost();
      });     
    }
    this.factForm.get('factFlag').patchValue( this.factTotalCostFlag );    
  }
  private checkFactTaxesP(){
    let factTaxesP    = this.factForm.get('factTotalGroup.factTaxesP').value;
    let factNetCost   = this.factForm.get('factTotalGroup.factNetCost').value;
    let factTotalCost = this.factForm.get('factTotalGroup.factTotalCost').value;
    let newFactTaxes = 0;  
    let newFactTotalCost = 0;  
    let newFactNetCost = 0;    
    if (isNaN(Number(factTaxesP)) === true) {  
      this.factForm.get('factTotalGroup.factTaxes').enable(); 
      if (this.factTotalCostFlag === true) {
        this.factTaxesSubs = this.factForm.get('factTotalGroup.factTaxes').valueChanges.subscribe((data:any) => {
          this.setFactTotalCost();
        });
      } else {
        this.factTaxesSubs = this.factForm.get('factTotalGroup.factTaxes').valueChanges.subscribe((data:any) => {
          this.setFactNetCost();
        });
      } 
    } else {    
      this.factTaxesSubs.unsubscribe();
      this.factForm.get('factTotalGroup.factTaxes').disable();
      newFactTaxes = Number(factNetCost) * Number(factTaxesP) / 100;
      this.factForm.get('factTotalGroup').patchValue({factTaxes:newFactTaxes.toFixed(2)});
      if (this.factTotalCostFlag === true) {
        newFactTotalCost = Number(factNetCost) + newFactTaxes;
        this.factForm.get('factTotalGroup').patchValue({factTotalCost:newFactTotalCost.toFixed(2)});
      } else {
        newFactNetCost = Number(factTotalCost)- newFactTaxes;
        this.factForm.get('factTotalGroup').patchValue({factNetCost:newFactNetCost.toFixed(2)});
      }
      this.factForm.updateValueAndValidity();
    }            
  }
  private subscribeFactTotalCost(){
    this.factNetCostSubs = this.factForm.get('factTotalGroup.factNetCost').valueChanges.subscribe((data:any) => {
      this.setFactTotalCost();
    });
    this.factTaxesSubs = this.factForm.get('factTotalGroup.factTaxes').valueChanges.subscribe((data:any) => {
      this.setFactTotalCost();
    });
  }
  private unSubscribeFactTotalCost(){
    this.factNetCostSubs.unsubscribe();
    this.factTaxesSubs.unsubscribe();
  }
  private subscribeFactNetCost(){
    this.factTotalCostSubs = this.factForm.get('factTotalGroup.factTotalCost').valueChanges.subscribe((data:any) => {
      this.setFactNetCost();
    });
    this.factTaxesSubs = this.factForm.get('factTotalGroup.factTaxes').valueChanges.subscribe((data:any) => {
      this.setFactNetCost();
    });
  }
  private unSubscribeFactNetCost(){
    this.factTotalCostSubs.unsubscribe();
    this.factTaxesSubs.unsubscribe();
  }
  private setFactNetCost(){
    let factTaxesP    = this.factForm.get('factTotalGroup.factTaxesP').value;
    let factTaxes     = this.factForm.get('factTotalGroup.factTaxes').value;
    let factTotalCost = this.factForm.get('factTotalGroup.factTotalCost').value;
    let newFactTaxes = 0;     
    let newFacNetCost = 0;
    if (isNaN(Number(factTaxesP)) === true) {
      newFacNetCost = Number(factTotalCost) - Number(factTaxes);
    } else {    
      newFacNetCost = Number(factTotalCost) / ( 1 + (Number(factTaxesP)/100) ) ;
      newFactTaxes = Number(factTotalCost) - newFacNetCost;
      this.factForm.get('factTotalGroup').patchValue({factTaxes:newFactTaxes.toFixed(2)});
    }
    this.factForm.get('factTotalGroup').patchValue({factNetCost:newFacNetCost.toFixed(2)});
    this.factForm.updateValueAndValidity();      
  }
  private setFactTotalCost(){
    let factTaxesP      = this.factForm.get('factTotalGroup.factTaxesP').value;
    let factTaxes       = this.factForm.get('factTotalGroup.factTaxes').value;
    let factNetCost     = this.factForm.get('factTotalGroup.factNetCost').value;  
    let newFactTaxes = 0;  
    let newFacTotalCost = 0;
    if (isNaN(Number(factTaxesP)) === true) {
      newFacTotalCost = Number(factNetCost) + Number(factTaxes);
    } else {      
      newFactTaxes = ( Number(factNetCost) * Number(factTaxesP) / 100 );
      newFacTotalCost = Number(factNetCost) + newFactTaxes;
      this.factForm.get('factTotalGroup').patchValue({factTaxes:newFactTaxes.toFixed(2)}); 
    }
    this.factForm.get('factTotalGroup').patchValue({factTotalCost:newFacTotalCost.toFixed(2)});      
    this.factForm.updateValueAndValidity();  
  }
  private setDetailedFactTotalCost(){
    let totalNetCost = 0;
    let totalTaxes = 0;
    let totalCost = 0;
    for(let k=0;k<this.newArtsFactList.length;k++){
      totalNetCost  = totalNetCost  + this.newArtsFactList[k].artFactNetCost;
      totalTaxes    = totalTaxes    + this.newArtsFactList[k].artFactTax;
      totalCost     = totalCost     + this.newArtsFactList[k].artFactTotalCost;     
    }
    this.factForm.get('factTotalGroup').patchValue({factNetCost:  totalNetCost.toFixed(2)});
    this.factForm.get('factTotalGroup').patchValue({factTaxes:    totalTaxes.toFixed(2)});
    this.factForm.get('factTotalGroup').patchValue({factTotalCost:totalCost.toFixed(2)});
    this.factForm.updateValueAndValidity(); 
  } 
  private setArtFactTotalCost(indexOfChangedArrayElement: number){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> setArtFactTotalCost ->...Starting...->', null);   
    const artFactArtUnitPriceControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactArtUnitPrice');
    const artFactArtPackageUnitsControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactArtPackageUnits');
    const artFactArtPackagePriceControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactArtPackagePrice');
    const artFactArtPackagePriceWithTaxesControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactArtPackagePriceWithTaxes');
    const artFactQuantityControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactQuantity');
    const artFactDiscountControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactDiscount');
    const artFactDiscountControlWithTaxesControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactDiscountWithTaxes');
    const artFactNetCostControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactNetCost');
    const artFactArtTaxControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactArtTax');
    const artFactTaxControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactTax');
    const artFactTotalCostControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactTotalCost');
    var artFactArtPackagePrice = 0;
    var artFactArtUnitPrice = 0;
    if (this.artPricesWithTaxesFlag === false) {
      if ( isNaN(Number(artFactQuantityControl.value)) === false) {
        artFactArtUnitPrice = Number(artFactArtUnitPriceControl.value);
      }
      if ( isNaN(Number(artFactArtPackageUnitsControl.value)) === false) {
        artFactArtPackagePrice = artFactArtUnitPrice * Number(artFactArtPackageUnitsControl.value);
      }
      artFactArtPackagePriceControl.patchValue(artFactArtPackagePrice.toFixed(6));
    } else { // ArtPackagePriceWithTaxes entered
      if ( isNaN(Number(artFactArtPackagePriceWithTaxesControl.value)) === false) {
        artFactArtPackagePrice = Number(artFactArtPackagePriceWithTaxesControl.value) / ( 1 + ( Number(artFactArtTaxControl.value)/ 100))
      }
      artFactArtPackagePriceControl.patchValue(artFactArtPackagePrice.toFixed(6));
      if ( isNaN(Number(artFactArtPackageUnitsControl.value)) === false) {
        artFactArtUnitPrice = artFactArtPackagePrice / Number(artFactArtPackageUnitsControl.value);
      }
      artFactArtUnitPriceControl.patchValue(artFactArtUnitPrice.toFixed(6));
    }
    let artFactNetCost = 0;
    if ( isNaN(Number(artFactQuantityControl.value)) === false) {
      artFactNetCost = artFactArtPackagePrice * Number(artFactQuantityControl.value);
    } 
    var artFactDiscount = 0;
    if ( isNaN(Number(artFactDiscountControl.value)) === false) {
      if (this.artPricesWithTaxesFlag === false) { 
        artFactDiscount = Number(artFactDiscountControl.value);
        artFactNetCost = artFactNetCost - artFactDiscount;
      }
      else {
        artFactDiscount = Number(artFactDiscountControlWithTaxesControl.value) / ( 1 + ( Number(artFactArtTaxControl.value)/ 100))
        artFactNetCost = artFactNetCost - artFactDiscount;
      }
    }     
    artFactNetCostControl.patchValue(artFactNetCost.toFixed(2));
    let artFactTaxes =  0;
    artFactTaxes = (artFactNetCost * artFactArtTaxControl.value) / 100 ;
    artFactTaxControl.patchValue(artFactTaxes.toFixed(2));
    let  artFactTotalCost = 0;
    artFactTotalCost = artFactNetCost + artFactTaxes;
    artFactTotalCostControl.patchValue(artFactTotalCost.toFixed(2));
    // update artFact data in newArtFact array --> this.newArtsFactList
    const artFactIdControl = this.filterArtsFactFormArray.at(indexOfChangedArrayElement).get('artFactId');
    let artFactArtIdValue = Number(artFactIdControl.value);
    for(let k=0;k<this.newArtsFactList.length;k++){
      if (this.newArtsFactList[k].artFactId === artFactArtIdValue) {
        this.newArtsFactList[k].artFactArtUnitPrice     = artFactArtUnitPrice;
        this.newArtsFactList[k].artFactQuantity         = Number(artFactQuantityControl.value);
        this.newArtsFactList[k].artFactDiscount         = artFactDiscount;
        this.newArtsFactList[k].artFactArtTax           = Number(artFactArtTaxControl.value);
        this.newArtsFactList[k].artFactArtPackagePrice  = artFactArtPackagePrice;
        this.newArtsFactList[k].artFactNetCost          = artFactNetCost;
        this.newArtsFactList[k].artFactTax              = artFactTaxes;
        this.newArtsFactList[k].artFactTotalCost        = artFactTotalCost;
      }
    }
    this.setDetailedFactTotalCost();
  }
  private copyFactFormDataToFact(){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> copyFactFormDataToFact ->...Starting...->', null);   
    this.newFact.factDate       = this.factForm.controls.factDate.value;
    this.newFact.factProvId     = this.factForm.controls.factProvId.value;
    this.newFact.factProvName   = this.factForm.controls.factProvName.value;
    this.newFact.factNumber     = this.factForm.controls.factNumber.value;
    this.newFact.factType       = this.factForm.controls.factType.value;
    this.newFact.factNetCost    = this.factForm.get('factTotalGroup.factNetCost').value;
    this.newFact.factTaxes      = this.factForm.get('factTotalGroup.factTaxes').value;
    this.newFact.factTotalCost  = this.factForm.get('factTotalGroup.factTotalCost').value;
    this.newFact.factPayType    = this.factForm.controls.factPayType.value;
    this.newFact.factNote   = this.factForm.controls.factNote.value;
    this.newFact.factDetailed   = this.factForm.controls.factDetailed.value;
    this.newFact.factStatus     = this.factForm.controls.factStatus.value;
    var factTypeName = this.globalVar.getSubDepTagFromSubDepName(this.newFact.factType);
    if (factTypeName != null) this.newFact.factType = factTypeName; 
    else this.newFact.factType = "NO SubDep Tag";
  }
  private copyNewFactDataToFactForm(newFactFlag:boolean){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> copyNewFactDataToFactForm ->...Starting...->', null);
    this.factForm.patchValue( {factId: this.newFact.factId, factProvId: this.newFact.factProvId, factProvName: this.newFact.factProvName, 
                                factNumber: this.newFact.factNumber,
                                factType: this.newFact.factType, factPayType: this.newFact.factPayType, 
                                factNote: this.newFact.factNote,
                                factDetailed: this.newFact.factDetailed, factStatus: this.newFact.factStatus} );    
    this.factForm.get('factDate').patchValue(new Date(this.newFact.factDate));
    if ( (this.createFact === true) || (this.createDetailedFact === true) ){
      this.factForm.get('factTotalGroup').patchValue( {factNetCost:''} );
      this.factForm.get('factTotalGroup').patchValue( {factTaxes:''} );
      this.factForm.get('factTotalGroup').patchValue( {factTotalCost:''} ); 
    } else {
      this.factForm.get('factTotalGroup').patchValue( {factNetCost:this.newFact.factNetCost.toFixed(2)} );
      this.factForm.get('factTotalGroup').patchValue( {factTaxes:this.newFact.factTaxes.toFixed(2)} );
      this.factForm.get('factTotalGroup').patchValue( {factTotalCost:this.newFact.factTotalCost.toFixed(2)} ); 
    }
    var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.newFact.factType);
    if (subDepName != null) this.factForm.get('factType').patchValue(subDepName);
    else this.factForm.get('factType').patchValue(this.localFactTypesRecordList[0].value);
  }
  private cleanFactFormData(isDetailedFact: boolean){
    this.factForm.patchValue( {factId:'',factProvId:'',factProvName:'',factModelName:'-',factNumber:'',factDate:'',
                                factType:this.localFactTypesRecordList[0].value, factPayType:this.localPayFactTypesRecordList[1].value,
                                factDetailed:isDetailedFact, factNote:true, factStatus:true} );
    this.factForm.get('factTotalGroup').patchValue( {factNetCost:0, factTaxes:0, factTotalCost:0 } );                          
  }
  private addArtsFactFormGroup( id:number, factId:number, factDate:Date, 
                        artId:number, artName:string, artType:string,artSize:number,artMeasurement:string,artUnitPrice:number,
                        artPackageUnits:number, artPackagePrice:number, quantity:number, discount:number, netCost:number, artTax:number,
                        tax:number,totalCost:number,isDisable:boolean): FormGroup {
    var artFactArtPackagePriceWithTaxes = artPackagePrice + (artPackagePrice * artTax / 100);
    var discountWithTaxes = discount + (discount * artTax / 100);

    return this.fb.group({
      artFactId:             [{value:id,                  disabled:true} ],
      artFactFactId:         [{value:factId,              disabled:true},[Validators.required] ],
      artFactFactDate:       [{value:factDate,            disabled:true},[Validators.required] ],
      artFactArtId:          [{value:artId,               disabled:true},[Validators.required] ],
      artFactArtName:        [{value:artName,             disabled:true},[Validators.required] ],
      artFactArtType:        [{value:artType,             disabled:true},[Validators.required] ],
      artFactArtSize:        [{value:artSize.toFixed(6),  disabled:true},[Validators.required] ],
      artFactArtMeasurement: [{value:artMeasurement,      disabled:true},[Validators.required] ],
      artFactArtUnitPrice:   [{value:artUnitPrice.toFixed(6),disabled:isDisable},[Validators.required,ValidationService.sixDecimalsValidation] ],
      artFactArtPackageUnits:[{value:artPackageUnits.toFixed(2),disabled:true},[Validators.required] ],
      artFactArtPackagePrice:[{value:artPackagePrice.toFixed(6),disabled:true},[Validators.required] ],
      artFactArtPackagePriceWithTaxes:[{value:artFactArtPackagePriceWithTaxes.toFixed(6),disabled:false},[Validators.required] ],
      artFactQuantity:       [{value:quantity.toFixed(6),disabled:isDisable},[Validators.required,ValidationService.sixDecimalsValidation] ],
      artFactDiscount:       [{value:discount.toFixed(6),disabled:isDisable},[Validators.required,ValidationService.sixDecimalsValidation] ],
      artFactDiscountWithTaxes:[{value:discountWithTaxes.toFixed(6),disabled:false},[Validators.required,ValidationService.sixDecimalsValidation] ],
      artFactNetCost:        [{value:netCost.toFixed(2),  disabled:true},[Validators.required] ],
      artFactArtTax:         [{value:artTax.toFixed(2),   disabled:isDisable},[Validators.required] ],
      artFactTax:            [{value:tax.toFixed(2),      disabled:true},[Validators.required,ValidationService.decimalValidation] ],
      artFactTotalCost:      [{value:totalCost.toFixed(2),disabled:true},[Validators.required] ],     
    });
  }
  private checkArtsFactList(localArtsFactList :ArtFact[]):ArtFact[]{
    if (localArtsFactList === null) return;
    if (localArtsFactList.length === 0) return;
    var newArtsFactList = new Array();
    for(let j=0; j< localArtsFactList.length; j++){
      if (localArtsFactList[j].artFactTotalCost != 0 ) { 
        if (localArtsFactList[j].artFactId > 0) localArtsFactList[j].artFactId = null;
        newArtsFactList.push(localArtsFactList[j]);
      }
    }
    return newArtsFactList;
  }
  private subsToFormArrayValueChanges(localArtsFactFormArray :FormArray){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> subsToFormArrayValueChanges ->...Starting...->', null);
/*
    if ( (this.artsFactFormArraySubs != null) && (this.artsFactFormArraySubs.length > 0) ) {
      this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> subsToFormArrayValueChanges ->...if...->', null);
      for(let j=0; j< this.artsFactFormArraySubs.length; j++) { 
        this.artsFactFormArraySubs[j].unsubscribe(); 
        this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> subsToFormArrayValueChanges ->...for...->', null);
      }
    }

    this.artsFactFormArraySubs = new Array();
    */
    if (this.artPricesWithTaxesFlag === false) {
      localArtsFactFormArray.controls.forEach(
        control => {
          control.get('artFactArtUnitPrice').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactQuantity').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactDiscount').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactArtTax').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
        }
      )
    } else {
      localArtsFactFormArray.controls.forEach(
        control => {
          control.get('artFactArtPackagePriceWithTaxes').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactQuantity').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactDiscountWithTaxes').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
          control.get('artFactArtTax').valueChanges.subscribe(
                  () => { this.setArtFactTotalCost(localArtsFactFormArray.controls.indexOf(control)); } )
        }
      )
    }
    localArtsFactFormArray.updateValueAndValidity();      
  }
  private addSelArtsFactlistToArtsFactForm(selArtsFactList :ArtFact[]){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> addSelArtsFactlistToArtsFactForm ->...Starting...->', null);
    // Adding selected arts to this.newArtsFactList, then ordering, and creating formArray
    for(let j=0; j< selArtsFactList.length; j++){
      this.newArtsFactList.push(selArtsFactList[j]);
    }
    var data = new Array();
    data = this.newArtsFactList;
    this.newArtsFactList = this.orderList(data,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
    this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
    this.formTitles.factArtsTotal = '..'+this.artsFactFormArray.length+'..Arts';         
    this.setDetailedFactTotalCost();
  }
  private createArtsFactListFromArtsFactModel(localArtsFactModel :Art[]):ArtFact[]{    
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createArtsFactListFromArtsFactModel ->...Starting...->', null);
    //  Create newArtsFactList with factModelsArts
    var newArtFact :ArtFact;
    var localArtsFactList = new Array();
    for ( let j=0 ; j<localArtsFactModel.length ; j++ ) {  
      newArtFact =new ArtFact();
      newArtFact.artFactArtId             = localArtsFactModel[j].artId ;
      newArtFact.artFactArtMeasurement    = localArtsFactModel[j].artMeasurement ;
      newArtFact.artFactArtName           = localArtsFactModel[j].artName ;
      newArtFact.artFactArtPackagePrice   = localArtsFactModel[j].artPackagePrice ;
      newArtFact.artFactArtPackageUnits   = localArtsFactModel[j].artPackageUnits ;
      newArtFact.artFactArtSize           = localArtsFactModel[j].artSize ;
      newArtFact.artFactArtTax            = Number(localArtsFactModel[j].artTax);
      newArtFact.artFactArtType           = localArtsFactModel[j].artType ;
      newArtFact.artFactArtUnitPrice      = localArtsFactModel[j].artUnitPrice ;
      newArtFact.artFactDiscount          = 0 ;
      newArtFact.artFactFactDate          = this.newFact.factDate ;
      newArtFact.artFactFactId            = this.newFact.factId ;
      newArtFact.artFactId        = this.getInternalArtFactId(); 
      newArtFact.artFactNetCost   = 0 ;
      newArtFact.artFactQuantity  = 0 ;
      newArtFact.artFactTax       = 0 ;
      newArtFact.artFactTotalCost = 0 ;
      localArtsFactList.push(newArtFact);
    }
    var data = new Array();
    data = localArtsFactList;
    localArtsFactList = this.orderList(data,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
    return localArtsFactList;      
  }
  private createArtsFactFormArraysFromArtsFactList(localArtsFactList :ArtFact[]){
    // Creating Global artsFact FormArray
    this.artsFactFormArray= new FormArray([]);
    for ( let j=0 ; j<localArtsFactList.length ; j++ ) {     
      this.artsFactFormArray.push(this.addArtsFactFormGroup( localArtsFactList[j].artFactId, localArtsFactList[j].artFactFactId,
                      this.globalVar.workingDate, localArtsFactList[j].artFactArtId, localArtsFactList[j].artFactArtName, 
                      localArtsFactList[j].artFactArtType, localArtsFactList[j].artFactArtSize, localArtsFactList[j].artFactArtMeasurement,
                      localArtsFactList[j].artFactArtUnitPrice, localArtsFactList[j].artFactArtPackageUnits,
                      localArtsFactList[j].artFactArtPackagePrice, localArtsFactList[j].artFactQuantity,localArtsFactList[j].artFactDiscount,
                      localArtsFactList[j].artFactNetCost, Number(localArtsFactList[j].artFactArtTax), 
                      localArtsFactList[j].artFactTax, localArtsFactList[j].artFactTotalCost, false));
    }
    this.filterArtsFactFormArray = this.artsFactFormArray;
    this.artsFactForm = this.fb.group({
      filterArtsFactFormArray: this.filterArtsFactFormArray
    });
    this.subsToFormArrayValueChanges(this.filterArtsFactFormArray);
    this.formTitles.factArtsTotal = '..'+this.filterArtsFactFormArray.length+'..Arts';
    this.setDetailedFactTotalCost(); 
  }
  public cleanFormFactBtnClick(){
    if (this.createFact === true || this.editFact === true) {
      this.createFact = true;
      this.editFact = false;  
      this.cleanFactFormData(false);
      this.factModelsArts = new Array();
      this.artsFactFormArray= new FormArray([]);
      this.newArtsFactList = new Array();
    }
    if (this.createDetailedFact === true || this.editDetailedFact === true) {
      this.createDetailedFact = true;
      this.editDetailedFact = false;  
      this.cleanFactFormData(true); 
      this.factModelsArts = new Array();
      this.artsFactFormArray= new FormArray([]);
      this.newArtsFactList = new Array();
    }     
  }
  public cleanFormDetailedFactBtnClick(){
    for(let k=0;k<this.newArtsFactList.length;k++){
      this.newArtsFactList[k].artFactArtUnitPrice = 0;
      this.newArtsFactList[k].artFactQuantity = 0;
      this.newArtsFactList[k].artFactDiscount = 0;     
    }
    this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
  }
  //---------------------------------------- FACTS CREATE -- BTN CLICK -------------------------------------------------------------
  public createProvBtnClick(){
    const modalInitialState = {
      provTitle       :'Factura',
      provDate        :this.workingDate,
      newProv         :this.newProv,
      showCreateProv  :true,
      showEditProv    :false,
      callback        :'OK',   
    };
    this.globalVar.openModal(ProvsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){ this.newProv = result; }
    })
  } 
  public createArtBtnClick(){
    const modalInitialState = {
      newArt      :this.newArt,
      artsTitle   :'Factura',
      artsDate    :this.workingDate,
      createArt   :true,
      callback    :'OK',   
    };
    this.globalVar.openModal(ArtsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{ }) 
  }
  public createFactModelBtnClick(){
    this.showFactModel(this.newFactModel,'Factura',this.workingDate,true,false);
  }
  private showFactModel(factModel :FactModels, titleFactModel :string, dateFactModel :Date, createFactModel :boolean, editFactModel :boolean){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> showFactModel ->...Starting...->', factModel);
    const modalInitialState = {
      newFactModel        :factModel,
      modelsTitle         :titleFactModel,
      modelsDate          :dateFactModel,  
      showEditFactModel   :editFactModel,
      showCreateFactModel :createFactModel,
      callback            :'OK',   
    };
    this.globalVar.openModal(FactModelCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null){ }
    })
  } 
  public createFactBtnClick(factForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createFactBtnClick ->...Starting...->', null);
    if (this.factForm.get('factTotalGroup.factTotalCost').value === 0) return;
    this.copyFactFormDataToFact();
    this.createOneFact(this.newFact, null);   
  }
  public updateFactBtnClick(factForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> updateFactBtnClick ->...Starting...->', null);
    if (this.factForm.get('factTotalGroup.factTotalCost').value === 0) return;
    this.copyFactFormDataToFact();
    this.updateOneFact(this.newFact, null);    
  }
  public createDetailedFactBtnClick(factForm: FormGroup, artsFactForm:FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createDetailedFactBtnClick ->...Starting...->', null);
    if (this.factForm.get('factTotalGroup.factTotalCost').value === 0) return;
    this.copyFactFormDataToFact();
    this.newArtsFactList = this.checkArtsFactList(this.newArtsFactList);
    this.newFact.factDetailed = true;     
    this.createOneFact(this.newFact, this.newArtsFactList);   
  }
  public updateDetailedFactBtnClick(factForm: FormGroup, artsFactForm:FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> updateDetailedFactBtnClick ->...Starting...->', null);
    if (this.factForm.get('factTotalGroup.factTotalCost').value === 0) return;
    this.copyFactFormDataToFact();
    this.newArtsFactList = this.checkArtsFactList(this.newArtsFactList);
    this.newFact.factDetailed = true; 
    this.updateOneFact(this.newFact, this.newArtsFactList);    
  }
  public addArtFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> addArtFactBtnClick ->...Starting...->', null);
    const modalInitialState = {
      artsTitle     :'Factura',
      artsDate      :this.workingDate,
      artsSelMany   :true,
      callback      :'OK',   
    };
    this.globalVar.openModal(ArtsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){   
        this.selectedArts = result;
        //this.copyArtsFactsFormDataToNewArtsFactList();
        // Add Arts to artsFactFormArray, checking that Arts are not already included 
        //------------------------------------------------------------------------------
        //
        //for(let j=0; j<this.artsFactFormArray.length; j++){
        //  let found = this.selectedArts.some(item => item.artId === this.artsFactFormArray.at(j).get('artFactArtId').value);
        //  if (found === true) {
        //    const artIdToRemove = this.artsFactFormArray.at(j).get('artFactArtId').value;
        //    const indexArtIdToRemove = this.selectedArts.findIndex(obj => obj.artId === artIdToRemove);
        //    if (indexArtIdToRemove > -1) { this.selectedArts.splice(indexArtIdToRemove, 1); }
        //  }
        //
        //-------------------------------------------------------------------------------
        // copy selected arts to new artsFact list
        var newArtFact :ArtFact;
        var selArtsFactList = new Array();
        for ( let j=0 ; j<this.selectedArts.length ; j++ ) { 
          newArtFact =new ArtFact();
          newArtFact.artFactArtId             = this.selectedArts[j].artId ;
          newArtFact.artFactArtMeasurement    = this.selectedArts[j].artMeasurement ;
          newArtFact.artFactArtName           = this.selectedArts[j].artName ;
          newArtFact.artFactArtPackagePrice   = this.selectedArts[j].artPackagePrice ;
          newArtFact.artFactArtPackageUnits   = this.selectedArts[j].artPackageUnits ;
          newArtFact.artFactArtSize           = this.selectedArts[j].artSize ;
          newArtFact.artFactArtTax            = Number(this.selectedArts[j].artTax);
          newArtFact.artFactArtType           = this.selectedArts[j].artType ;
          newArtFact.artFactArtUnitPrice      = this.selectedArts[j].artUnitPrice ;
          newArtFact.artFactDiscount          = 0 ;
          newArtFact.artFactFactDate          = this.newFact.factDate ;
          newArtFact.artFactFactId            = this.newFact.factId ;
          newArtFact.artFactId        = this.getInternalArtFactId();
          newArtFact.artFactNetCost   = 0 ;
          newArtFact.artFactQuantity  = 0 ;
          newArtFact.artFactTax       = 0 ;
          newArtFact.artFactTotalCost = 0 ;
          selArtsFactList.push(newArtFact);
        }
        this.addSelArtsFactlistToArtsFactForm(selArtsFactList);
        this.showArtsFact = true;
      }
    })
  }
  public changeSelectedProvFactBtnClick(){
    const modalInitialState = {
      provTitle   :'Factura',
      provDate    :this.workingDate,
      callback    :'OK',   
    };
    this.globalVar.openModal(ProvsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){            
        this.selectedProv = result;           
        this.factForm.patchValue( {factProvId: this.selectedProv.provId, factProvName: this.selectedProv.provName,
          factType: this.selectedProv.provFactType, factPayType: this.selectedProv.provPayType, factNote: this.selectedProv.provFactNote } );  
          var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.selectedProv.provFactType);
          if (subDepName != null) this.factForm.get('factType').patchValue(subDepName);
          else this.factForm.get('factType').patchValue(this.localFactTypesRecordList[0].value);
          if (!this.globalVar.checkDefaultFactPayTypeIsInCurrentBar(this.selectedProv.provPayType)) {
            this.factForm.get('factPayType').patchValue(this.localPayFactTypesRecordList[0].value);
          }
      }  
      this.factForm.get('factProvName').updateValueAndValidity();
    })
  }
  public changeSelectedFactModelBtnClick(){
    const modalInitialState = {
      factModels      :null, 
      selectedProv    :this.selectedProv,
      modelsTitle     :'Facturas',
      modelsDate      :this.workingDate,   
      editFactModel   :true,  
      callback        :'OK',   
    };
    this.globalVar.openModal(FactModelSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){            
        this.selectedFactModel = result;  
        this.factForm.patchValue( {factProvId: this.selectedFactModel.factModelsProvId, factProvName: this.selectedFactModel.factModelsProvName,
                                    factPayType: this.selectedFactModel.factModelsPayType, factNote: this.selectedFactModel.factModelsNote,
                                    factModelName: this.selectedFactModel.factModelsName  } );
        var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.selectedFactModel.factModelsFactType);
        if (subDepName != null) this.factForm.get('factType').patchValue(subDepName);
        else this.factForm.get('factType').patchValue(this.localFactTypesRecordList[0].value);
        if (!this.globalVar.checkDefaultFactPayTypeIsInCurrentBar(this.selectedFactModel.factModelsPayType)) {
          this.factForm.get('factPayType').patchValue(this.localPayFactTypesRecordList[0].value);
        }
        this.getArtsFactModel(this.selectedFactModel);                          
      }  
      this.factForm.get('factProvName').updateValueAndValidity();
    })
   }  
   private saveAsFactModel(nameFactModel: string){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> saveAsFactModel ->...Starting...->', null);
    if ( nameFactModel === null || nameFactModel === "" ) { return; }
    this.newFactModel = new FactModels();    
    this.newFactModel.factModelsName = nameFactModel;
    this.newFactModel.factModelsDate = this.workingDate;
    this.newFactModel.factModelsProvId = this.factForm.get('factProvId').value;
    this.newFactModel.factModelsProvName = this.factForm.get('factProvName').value;
    var factTypeName = this.factForm.get('factType').value;
    var factTypeTag = this.globalVar.getSubDepTagFromSubDepName(factTypeName);
    this.newFactModel.factModelsFactType  = factTypeTag;
    var factPayType = this.factForm.get('factPayType').value;
    if (factPayType === this.localPayFactTypesRecordList[0].value) {
      this.newFactModel.factModelsPayType   = null;
    } else {
      this.newFactModel.factModelsPayType   = factPayType;
    }
    this.newFactModel.factModelsNote      = this.factForm.get('factNote').value;
    this.factModelsArts = new Array();    
    this.artsFactFormArray.controls.forEach(
      control => {        
          let newArt = new Art();      
          newArt.artId            = control.get('artFactArtId').value;
          newArt.artReference     = "";
          newArt.artName          = control.get('artFactArtName').value;
          newArt.artLongName      = "";
          newArt.artType          = control.get('artFactArtType').value;
          newArt.artMeasurement   = control.get('artFactArtMeasurement').value;
          newArt.artSize          = control.get('artFactArtSize').value;
          newArt.artUnitPrice     = control.get('artFactArtUnitPrice').value;
          newArt.artPackageUnits  = control.get('artFactArtPackageUnits').value;
          newArt.artPackagePrice  = control.get('artFactArtPackagePrice').value;
          newArt.artTax           = control.get('artFactArtTax').value;
          newArt.artStatus        = false;
          this.factModelsArts.push(newArt);    
      }
    )
    var isOk = this.checkProperties(this.newFactModel);
    if (!isOk) {
      this._errorService.showMsgObject("errorMsg","Factura -> Crear Factura Modelo","Factura -> Crear Factura Modelo",this.newFactModel,
                                        "Error en modelo factura","Faltan Datos","","","","","",);
    } else {
      this.createOneFactModel(this.newFactModel,this.factModelsArts);
    }
  }
  private checkProperties(obj :any):boolean {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> checkProperties ->...Starting...->', null);
    for (var key in obj) {
      if (obj[key] == null || obj[key] == "") return false;
    }
    return true;
  }
  public saveAsFactModelBtnClick() {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> getFactModelName ->...Starting...->', null);
    let currentDate = this.workingDate.toLocaleDateString();
    let nameFactModel = 'Modelo Fact.--'+currentDate.substring(0,10)+'--'+this.newFact.factProvName;
    if (nameFactModel.length > 63) nameFactModel = nameFactModel.substring(0,63);
    this.saveAsFactModel(nameFactModel);
  }
  public copyArtFactToArtBtnClick(artFactId :number){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> CopyArtFactToArtBtnClick ->...Starting...->', artFactId);
    var selectedArtFactToCopy = this.newArtsFactList.find(artFact => artFact.artFactId === artFactId);
    var selectedArtToUpdate = this.copyArtInFactFormArrayToSelectedArt(selectedArtFactToCopy);
    this.updateArtWithArtFactData(selectedArtToUpdate);
  }
  private copyArtInFactFormArrayToSelectedArt(selecteArtFactToCopy :ArtFact):Art{
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> copyArtInFactFormArrayToSelectedArt ->...Starting...->', selecteArtFactToCopy);
    var selArtFact = new Art();
    selArtFact.artUnitPrice     = selecteArtFactToCopy.artFactArtUnitPrice;
    selArtFact.artTax           = selecteArtFactToCopy.artFactArtTax.toString();
    selArtFact.artId            = selecteArtFactToCopy.artFactArtId;
    selArtFact.artName          = selecteArtFactToCopy.artFactArtName;
    selArtFact.artMeasurement   = selecteArtFactToCopy.artFactArtMeasurement;
    selArtFact.artPackagePrice  = selecteArtFactToCopy.artFactArtPackagePrice;
    selArtFact.artPackageUnits  = selecteArtFactToCopy.artFactArtPackageUnits;
    selArtFact.artSize          = selecteArtFactToCopy.artFactArtSize;
    selArtFact.artType          = selecteArtFactToCopy.artFactArtType;
    selArtFact.artDate          = selecteArtFactToCopy.artFactFactDate;
    return selArtFact;
  }
  public deleteArtFactBtnClick(artFactId :number){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> DeleteArtFactBtnClick ->...Starting...->', null);
    var selecteArtFactToDelete = this.newArtsFactList.find(artFact => artFact.artFactId === artFactId);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.factsTitle,this.formTitles.factsTitle,
                        selecteArtFactToDelete,this.formTitles.artDeleteOne,selecteArtFactToDelete.artFactArtName,
                        selecteArtFactToDelete.artFactTotalCost.toString(),"","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.newArtsFactList.splice(this.newArtsFactList.indexOf(selecteArtFactToDelete), 1);
            var data = new Array();
            data = this.newArtsFactList;
            this.newArtsFactList = this.orderList(data,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
            this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
          }
        })
  }
  public searchArtsBtnClick(){
    if (this.showSearchArt === true) {
      this.showSearchArt = false;
    } else {
      this.showSearchArt = true;
    }
  }
  public changeOrderArtsBtnClick(){
    if (this.reverseArtsList === true) this.reverseArtsList  = false;
    else this.reverseArtsList  = true;
    this.sortedCollection = this.orderPipe.transform(this.newArtsFactList,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
    this.newArtsFactList  = this.sortedCollection;
    this._searchArtsFactName = '';
    this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
  }
  private orderList( itemsList :any[], orderString :string, reverseOrder:boolean, caseInsensitive:boolean ):any[] {
    this.sortedCollection = this.orderPipe.transform(itemsList,orderString,reverseOrder,caseInsensitive);
    return this.sortedCollection;
  }
  public closeFactsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();        
      }
  }
  public artPricesWithTaxesBtnClick(){
    if (this.artPricesWithTaxesFlag === false){
      this.artPricesWithTaxesFlag = true;
      this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
      this.artsFactFormArray.controls.forEach(
        control => {        
            control.get('artFactArtUnitPrice').disable();
        }
      )
    } else {
      this.artPricesWithTaxesFlag = false;
      this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
      this.artsFactFormArray.controls.forEach(
        control => {        
            control.get('artFactArtUnitPrice').enable();
        }
      )
    }
  }
  //---------------------------------------- FACTS CREATE-- DATABASE -----------------------------------------------------------
  private getOneFact(updateFact: Fact, isDetailedFact:boolean): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> getOneFact ->...Starting...->', null);
    this._factsService.getOneFact(updateFact)
      .subscribe({next:(data) => { this.newFact = data;
                           this.copyNewFactDataToFactForm(false);
                           if ( isDetailedFact) this.getArtsFact(updateFact);
                           else  this.showEditFact = true;                
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getArtsFact(editFact: Fact): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> getArtsFact ->...Starting...->', null);
    this._factsService.getArtsFact(editFact)
      .subscribe({next:(data) => { this.newArtsFactList = this.orderList(data,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                           this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
                           this.showEditDetailedFact = true;
                           this.showArtsFact = true;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private createOneFact(newFact: Fact, newArtsFactsList: ArtFact[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createOneFact ->...Starting...->', null);
    this._factsService.createOneFact(newFact)
      .subscribe({next:(data) => { this.newFact = data;
                           if (newArtsFactsList != null) { this.createArtsFact(this.newFact, newArtsFactsList); }
                           else { this.closeFactsCreateModalBtnClick(); }                        
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneFact(updateFact: Fact, updateArtsFactsList: ArtFact[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> updateOneFact ->...Starting...->', null);
    this._factsService.updateOneFact(updateFact)
      .subscribe({next:(data) => { this.newFact = data;
                           if (updateArtsFactsList != null) {this.updateArtsFact(updateArtsFactsList); }
                           else { this.closeFactsCreateModalBtnClick(); }                            
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }  
  //---------------------------------------- FACTS CREATE -- ARTS FACTS -- DATABASE -----------------------------------------------------------
  private createArtsFact(newFact: Fact, newArtsFactList: ArtFact[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createArtFacts ->...Starting...->', null);
    for(let j=0; j< newArtsFactList.length; j++){
      newArtsFactList[j].artFactFactId = newFact.factId;
    }
    this._factsService.createArtsFact(newArtsFactList)
      .subscribe({next:(data) => { this.newArtsFactList = data;
                           this.closeFactsCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };   }});
  }
  private updateArtsFact(updateArtsFactList: ArtFact[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> updateArtsFact ->...Starting...->', null);
    this._factsService.updateArtsFact(updateArtsFactList)
      .subscribe({next:(data) => { this.newArtsFactList = data;
                           this.closeFactsCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateArtWithArtFactData(selectedArtFact: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> updateArtWithArtFactData ->...Starting...->', null);
    this._artService.updateArtWithArtFactData(selectedArtFact)
      .subscribe({next:(data) => { this._errorService.showMsgObject("infoMsg","Artículo Actualizado","Artículo Actualizado",selectedArtFact,
                              "Artículo Actualizado en la Base de Datos",selectedArtFact.artName,selectedArtFact.artUnitPrice.toFixed(2),
                              selectedArtFact.artTax,"","","",);
                          // pop up a message with art updated
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //---------------------------------------- FACTS CREATE -- MODEL FACTS -- DATABASE -----------------------------------------------------------
  private createOneFactModel(newFactModel: FactModels, artsFactModel: Art[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createOneFactModel ->...Starting...->', null);
    this._factModelService.createOneFactModel(newFactModel)
      .subscribe({next:(data) => { this.newFactModel = data;
                           this.createArtsFactModel(this.newFactModel, artsFactModel);
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private deleteOneFactModel(delFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> deleteOneFactModel ->...Starting...->', null);
    this._factModelService.delOneFactModel(delFactModel)
    .subscribe({next:(data) => { },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //---------------------------------------- FACTS CREATE -- ARTS MODEL FACTS -- DATABASE -----------------------------------------------------------
  private createArtsFactModel(newFactModel: FactModels,newArtsFactModel: Art[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> createArtsFactModel ->...Starting...->', null);
    // Insert factModelsId at the begining of arts array
    let factModelsIdArt = new Art();
    factModelsIdArt.artId = newFactModel.factModelsId;
    newArtsFactModel.splice(0,0,factModelsIdArt);
    this._factModelService.createArtsFactModel(newArtsFactModel)
        .subscribe({next:(data) => { this.factModelsArts = data;
                             this.showFactModel(this.newFactModel,'Factura',this.workingDate,false,true);
                           },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };
                           this.deleteOneFactModel(this.newFactModel);
                         }});
  }
  private getArtsFactModel(newFactModel: FactModels): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-CREATE -> getArtsFactModel ->...Starting...->', null);
    this._factModelService.getArtsFactModel(newFactModel)
      .subscribe({next:(data) => { this.factModelsArts = data;
                           this.newArtsFactList = this.createArtsFactListFromArtsFactModel(this.factModelsArts);
                           this.createArtsFactFormArraysFromArtsFactList(this.newArtsFactList);
                          this.showCreateDetailedFact = true;
                           this.showArtsFact = true;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}