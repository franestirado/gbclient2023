import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { Subject } from 'rxjs';

import { GlobalVarService } from '../aa-common/global-var.service';

import { Fact,ArtFact } from './facts';

@Component({
  selector      :'app-facts-seeone',
  templateUrl   :'./facts-seeone.component.html',
  styleUrls     :['./facts.component.css']   
})
export class FactsSeeOneComponent implements OnInit {
  public factsTitle             :string;
  public newFact                :Fact;
  public artFactsList           :ArtFact[];
  callback                      :any;
  result: Subject<any> = new Subject<any>();
   
  public formTitles = {
    'factsCreateTitle'    :'Crear Factura',  
    'factsTitle'          :'Facturas',   
    'factsDate'           :'',
    'factCreate'          :'Crear Factura',
    'factDCreate'         :'Crear Factura Detallada',
    'factEdit'            :'Modificar Factura',
    'factDEdit'           :'Modificar Factura Detallada',
    'factArtsTotal'       :'',
    'hdDate'              :'',
    'artDeleteOne'      :'¿ Seguro que quiere BORRAR este ARTÍCULO de esta FACTURA?',

  };
  public formLabels = {
    'factId'              :'Fact_Id',
    'factProvId'          :'Prov_Id',
    'factProvName'        :'Proveedor',
    'factNumber'          :'Nº Factura',
    'factDate'            :'Fecha',
    'factType'            :'Tipo',
    'factNetCost'         :'Neto',
    'factTaxesP'          :'% IVA',
    'factTaxes'           :'IVA',
    'factFlag'            :'IVA incl',
    'factTotalCost'       :'Total',
    'factPayType'         :'Pago',
    'factNote'            :'Nota/Fact',
    'factStatus'          :'Estado',
    'factModelName'       :'Modelo',
    'artId'               :'Art_Id',
    'artReference'        :'Referencia',
    'artName'             :'Nombre',
    'artLongName'         :'Descripción',
    'artType'             :'Tipo',
    'artSelect'           :'Seleccionar',
    'artAdd'              :'Añadir',
    'artNameF'            :'Artículo',
    'artUnit'             :'Ud.',
    'artTypeF'            :'Tipo',
    'artSize'             :'Size',
    'artPrice'            :'€/Ud.',            
    'units'               :'Uds/Pk.',
    'price'               :'€/Pk',
    'quantity'            :'Qty',
    'discount'            :'Dcto', 
    'netCost'             :'Coste',
    'artTax'              :'IVA',
    'tax'                 :'Imp',
    'totalCost'           :'Total', 
    'updateArt'           :'ActArt', 
    'deleteArt'           :'Borrar',
  };
  private logToConsole                :boolean;

  //--------------------------------------- FACTS CREATE -- CONSTRUCTOR -------------------------------------------------------------
  constructor (   private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService, 
                  private globalVar: GlobalVarService ) {  }
  //--------------------------------------- FACTS CREATE -- NG ON INIT -------------------------------------------------------------
  ngOnInit(): void { 
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS-SEEONE -> ngOnInit ->...Starting...->', null);   
    this.formTitles.factsDate = new Date(this.newFact.factDate).toLocaleDateString();
    this.formTitles.factsTitle = this.factsTitle;
    this.formTitles.factEdit = 'Ver Datos';
    this.formTitles.factArtsTotal = '..'+this.artFactsList.length+'..Arts';         
  }
  closeFactsSeeOneModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();        
      }
  }
}