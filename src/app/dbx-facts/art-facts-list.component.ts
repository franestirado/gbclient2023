import { Component, OnInit } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';

import { Art } from '../dbc-arts/art';
import { FactModelsService } from '../dbc-factmodels/factmodel.service';
import { ArtFact,ArtFactString } from './facts';
import { FactsService } from './facts.service';
import { ArtsFactModels } from '../dbc-factmodels/factmodel';

@Component({
  selector      :'app-art-facts-list',
  templateUrl   :'./art-facts-list.component.html',
  styleUrls     :['./facts.component.css'] 
})
export class ArtFactsListComponent implements OnInit {
  public selectedArt        :Art;
  public listArtFacts       :boolean;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'artFactList'         :'Artículo en Facturas',
    'artFactModelList'    :'Artículo en Modelos de Facturas',
    'artFactTotalString'  :'..Art Facts',
    'artFactTotals'       :'TOTALES...',
    'selectedArtTitle'    :'Artículo Seleccionado...',
  };
  public formLabels = {
    'id'                        :'#####',
    'artFactId'                 :'Id',
    'artFactFactId'             :'Id Fact',
    'artFactFactDate'           :'Fecha',
    'artFactArtId'              :'Id Art',
    'artFactArtName'            :'Nombre Art',
    'artFactArtType'            :'Tipo Art',
    'artFactArtSize'            :'Size',
    'artFactArtMeasurement'     :'Ud.',    
    'artFactArtUnitPrice'       :'€/Ud.',
    'artFactArtPackageUnits'    :'Uds/Pk.',
    'artFactArtPackagePrice'    :'€/Pk',
    'artFactArtTax'             :'% IVA',
    'artFactQuantity'           :'Qty',
    'artFactDiscount'           :'Dcto',
    'artFactNetCost'            :'Coste',   
    'artFactTax'                :'IVA',
    'artFactTotalCost'          :'Total', 
    'artsFactModelsId'          :'Id',
    'factModelsId'              :'Id Modelo Factura',
    'factModelsName'            :'Nombre Modelo Factura',
    'artId'                     :'Id Artículo',
    'artName'                   :'Nombre Artículo',
  };
  public  artFactList             :ArtFact[];
  public  addedArtFact            :ArtFact;
  public  artFactModelList        :ArtsFactModels[];
  public  addedArtFactModel       :ArtsFactModels;
  public  caseInsensitive         :boolean;
  public  orderArtFactList        :string;
  public  reverseArtFactList      :boolean;
  private sortedCollection        :any[];
  private logToConsole            :boolean;
  public  showSearchArtFact       :boolean;
  public  showArtFactList         :boolean;
  public  showArtFactModelList    :boolean;
  public  searchArtFact           :ArtFactString;
  public  searchArtFactModel      :ArtsFactModels;
  public  addedTotalCost          :number;
  public  addedTax                :number;
  public  addedNetCost            :number;
  public  addedDiscount           :number;
  public  addedQuantity           :number;
 /*---------------------------------------- ART FACT LIST -- CONSTRUCTOR ---------------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private globalVar: GlobalVarService, private _factsService: FactsService, private _factModelService: FactModelsService ) { }
/*----------------------------------------- ART FACT LIST -- NG ON INIT ----------------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.showArtFactList = false;
    this.showArtFactModelList = false;
    this.addedArtFact = new ArtFact();
    this.searchArtFact = new ArtFactString();
    this.addedArtFactModel = new ArtsFactModels();
    this.searchArtFactModel = new ArtsFactModels();
    if (this.listArtFacts === true) {
      this.getArtFactsWithArt(this.selectedArt);
    } else {
      this.getArtFactModelsWithArt(this.selectedArt);
    }
  }
  /*--------------------------------------- ART FACT LIST -- FORMS----------------------------------------------------------------------*/
  /*--------------------------------------- ART FACT LIST -- GENERAL -------------------------------------------------------------------*/
  closeArtFactListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback('OK');
      this.result.next('OK');
      this.bsModalRef.hide();
    }
  }  
  /* -------------------------------------- ART FACT LIST -- BTN CLICK------------------------------------------------------------------*/
  createArtFactBtnClick(){}
  setOrderArtFactBtnClick(sel :string){}
  /* -------------------------------------- ART FACT LIST -- ARTS DATABASE -------------------------------------------------------------*/
  /* -------------------------------------- ART FACT LIST -- DELETE --------------------------------------------------------------------*/
  /* -------------------------------------- ART FACT LIST -- ART FACTS DATABASE -------------------------------------------------------------*/
  prepareArtFactsList(recArtFactList: ArtFact[]){
    this.globalVar.consoleLog(this.logToConsole,'->ART FACT LIST->prepareArtFactsList->...Starting->', null);
    this.artFactList = recArtFactList;
    this.caseInsensitive = false;
    this.orderArtFactList = 'artFactFactDate';
    this.reverseArtFactList = false;
    this.sortedCollection = this.orderPipe.transform(this.artFactList,this.orderArtFactList,this.reverseArtFactList,this.caseInsensitive);
    this.artFactList = this.sortedCollection;
    this.formTitles.artFactTotalString =this.artFactList.length+'..Art Facts';
    this.addedArtFact = new ArtFact();
    this.addedArtFact.artFactTotalCost = 0;
    this.addedArtFact.artFactTax = 0;
    this.addedArtFact.artFactNetCost = 0;
    this.addedArtFact.artFactDiscount = 0;
    this.addedArtFact.artFactQuantity = 0;
    this.addedArtFact.artFactArtUnitPrice = 0;
    this.addedArtFact.artFactArtPackageUnits = 0;
    this.addedArtFact.artFactArtPackagePrice = 0;
    this.showArtFactList = true;
    for (let i=0; i<this.artFactList.length; i++) {
      this.addedArtFact.artFactTotalCost  = this.addedArtFact.artFactTotalCost  + this.artFactList[i].artFactTotalCost;
      this.addedArtFact.artFactTax        = this.addedArtFact.artFactTax        + this.artFactList[i].artFactTax;
      this.addedArtFact.artFactNetCost    = this.addedArtFact.artFactNetCost    + this.artFactList[i].artFactNetCost;
      this.addedArtFact.artFactDiscount   = this.addedArtFact.artFactDiscount   + this.artFactList[i].artFactDiscount;
      this.addedArtFact.artFactQuantity   = this.addedArtFact.artFactQuantity   + this.artFactList[i].artFactQuantity;
    }
  }
  getArtFactsWithArt(selArt :Art) {
    this.globalVar.consoleLog(this.logToConsole,'->ART FACT LIST->getArtFactsWithArt->...Starting->', null);
    this._factsService.getArtFactsWithArt(selArt)
      .subscribe( data => { this.prepareArtFactsList(data);  },
                  error => { if (this.globalVar.handleError(error)) { throwError(() => error) }; });
  }
  prepareArtFactModelsList(recArtFactModelsList: ArtsFactModels[]){
    this.globalVar.consoleLog(this.logToConsole,'->ART FACT LIST->prepareArtFactModelsList->...Starting->', null);
    this.artFactModelList = recArtFactModelsList;
    this.caseInsensitive = false;
    this.orderArtFactList = 'artFactModelName';
    this.reverseArtFactList = false;
    this.sortedCollection = this.orderPipe.transform(this.artFactModelList,this.orderArtFactList,this.reverseArtFactList,this.caseInsensitive);
    this.artFactModelList = this.sortedCollection;
    this.formTitles.artFactTotalString =this.artFactModelList.length+'..Art FactModels';
    this.showArtFactModelList = true;
  }
  getArtFactModelsWithArt(selArt :Art) {
    this.globalVar.consoleLog(this.logToConsole,'->ART FACT LIST->getArtFactModelsWithArt->...Starting->', null);
    this._factModelService.getArtFactModelsWithArt(selArt)
      .subscribe( data  => { this.prepareArtFactModelsList(data);  },
                  error => { if (this.globalVar.handleError(error)) { throwError(() => error) }; });
  }
}