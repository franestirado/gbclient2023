import { Component, OnInit } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { Fact,FactString } from './facts';
import { FactsCreateComponent } from './facts-create.component';
import { FactsService } from './facts.service';

import { Hd } from '../dbx-hd/hd'; 
import { HdService } from '../dbx-hd/hd.service';

@Component({
  selector      :'app-facts-hd',
  templateUrl   :'./facts-hd.component.html',
  styleUrls     :['./facts.component.css'] 
})
export class FactsHdComponent implements OnInit {
  public arrayWorkingDates  :Date[];
  public hdFlag             :boolean;
  public monthFlag          :boolean;
  public receivedHdsFacts   :Hd;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'facts'               :'Facturas',
    'factsList'           :'Facturas',
    'hdDate'              :'',
    'factEditList'        :'Editar',
    'factDelete'          :'Borrar',
    'factListMonthString' :'',
    'factListYearString'  :'',
    'factListDateStart'   :'',
    'factListDateEnd'     :'',
    'factDeleteOne'       :'¿ Seguro que quiere BORRAR esta Factura ?',
    'factDeleteAll'       :'¿ Seguro que quiere BORRAR TODAS las Factura del Día ?',
  };
  public formLabels = {
    'id'                  :'#####',
    'factId'              :'Fact_Id',
    'factProvId'          :'Prov_Id',
    'factProvName'        :'Proveedor',
    'factNumber'          :'Nº Factura',
    'factDate'            :'Fecha',
    'factType'            :'Tipo',
    'factNetCost'         :'Neto',
    'factTaxes'           :'IVA',
    'factTotalCost'       :'Total',
    'factPayType'         :'Pago',
    'factStatus'          :'Estado', 
  };
  public  caseInsensitive     :boolean;
  public  orderFactsList      :string;
  public  reverseFactsList    :boolean;
  private sortedCollection    :any[];
  private logToConsole        :boolean;
  public  showSearchFacts     :boolean;
  public  searchFact          :FactString;

  public  facts               :Fact[];
  private newFact             :Fact;
  private selectedFactPayType :string;
  private factsTotal          :number;
  public  factsTotalString    :string;
  private receivedHdsFactsList:Hd[];
  private changeFactPayType   :boolean;
  private changeFactDate      :boolean;

  private showCreateFact              :boolean;
  private showCreateDetailedFact      :boolean;  
  private showEditFact                :boolean;
  private showEditDetailedFact        :boolean;
  private localFactTypesRecordList    :ValueRecord[];
  private localPayFactTypesRecordList :ValueRecord[];

 /*---------------------------------------FACTS HD -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private _errorService: ErrorService, private globalVar: GlobalVarService, private _factsService: FactsService, 
                  private _hdService: HdService ) { }
/*---------------------------------------FACTS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.factsTotalString = '';
    this.localFactTypesRecordList = this.globalVar.factTypesSubDepsRecordList;  
    this.localPayFactTypesRecordList = this.globalVar.factPayTypesRecordList;

    this.searchFact = new  FactString();
    this.caseInsensitive = true;
    this.showSearchFacts = false;
    if (this.hdFlag) {
      this.receivedHdsFactsList = new Array();
      this.receivedHdsFactsList[0] = this.receivedHdsFacts;   
      this.formTitles.hdDate = this.arrayWorkingDates[0].toLocaleDateString();
      this.selectedFactPayType = 'HojaDia';  
      this.changeFactPayType = false;
      this.changeFactDate = false;
    }
    if (this.monthFlag) {
      this.formTitles.factListMonthString = this.arrayWorkingDates[0].toLocaleString('default',{month:'long'});
      let workingYear = this.arrayWorkingDates[0].getFullYear();
      this.formTitles.factListYearString = workingYear.toString();
      this.formTitles.factListDateStart = this.arrayWorkingDates[0].toLocaleDateString(); 
      this.formTitles.factListDateEnd = this.arrayWorkingDates[1].toLocaleDateString();      
      this.formTitles.hdDate = this.arrayWorkingDates[0].toLocaleDateString();
      this.selectedFactPayType = 'ALL';
      this.changeFactPayType = true;
      this.changeFactDate = true;     
    }
    this.getAllFactsPayTypeDates(this.selectedFactPayType,this.arrayWorkingDates);   
  }
  /*---------------------------------------FACTS HD -- FORMS-----------------------------------------------------------*/
  resetShowFactFlags(){
    this.showCreateFact         = false;
    this.showCreateDetailedFact = false;
    this.showEditFact           = false;
    this.showEditDetailedFact   = false;
  }
  /*---------------------------------------FACTS HD -- GENERAL -----------------------------------------------------------*/
  closeFactsHdModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      if (this.hdFlag) {
        this.receivedHdsFactsList[0].hdAmount = this.factsTotal;
        this.updateDateHds(this.arrayWorkingDates, this.receivedHdsFactsList);
      } else {
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();
      }
    }
  }
  calculateFactsTotal(){
    this.factsTotal = 0;
    for (let i = 0 ; i < this.facts.length ; i++ ) {
      this.factsTotal = this.factsTotal + this.facts[i].factTotalCost;
    }
    this.factsTotalString = this.facts.length+'..Facts..Total->' + this.factsTotal.toFixed(2);
  }
  /* ---------------------------------------FACTS BTN CLICK-----------------------------------------------------------*/
  private selectFactPayTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->selectFactPayTypeBtnClick->starting 1->', null); 
    if (this.localPayFactTypesRecordList.length < 1) { return;  }    
    const modalInitialState = {
      localValueRecordList  :this.localPayFactTypesRecordList,
      selectTitle1          :"Facturas Mes",
      selectTitle2          :"Seleccionar Forma Pago",
      selectMessage         :"Seleccione la Forma de Pago de las FACTURAS que quiere ver",
      selectLabel           :"Forma Pago Facturas:",       
      selectFooter          :"Facturas Mes",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) this.selectedFactPayType = result;
      else this.selectedFactPayType = 'ALL';
      this.getAllFactsPayTypeDates(this.selectedFactPayType,this.arrayWorkingDates);            
    })
  }
  createFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->createFactBtnClick->starting->', null);
    this.resetShowFactFlags();
    this.showCreateFact = true;
    this.newFact = new Fact();
    this.newFact.factDate = this.arrayWorkingDates[0];
    this.newFact.factStatus = true;
    this.newFact.factDetailed = false;
    if (this.hdFlag) {
      this.newFact.factPayType = this.localPayFactTypesRecordList[1].value;
    } else {
      this.newFact.factPayType = this.localPayFactTypesRecordList[0].value;
    }
    this.newFact.factType = this.localFactTypesRecordList[0].value;
    this.newFact.factNetCost = 0;
    this.newFact.factTaxes = 0;
    this.newFact.factTotalCost = 0;
    this.createFactModal();
  }
  createDetailedFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->createDetailedFactBtnClick->starting->', null);
    this.resetShowFactFlags();
    this.showCreateDetailedFact = true;
    this.newFact = new Fact();
    this.newFact.factDate = this.arrayWorkingDates[0];
    this.newFact.factStatus = true;
    this.newFact.factDetailed = true;
    if (this.hdFlag) {
      this.newFact.factPayType = this.localPayFactTypesRecordList[1].value;
    } else {
      this.newFact.factPayType = this.localPayFactTypesRecordList[0].value;
    }
    this.newFact.factType = this.localFactTypesRecordList[0].value; 
    this.newFact.factNetCost = 0;
    this.newFact.factTaxes = 0;
    this.newFact.factTotalCost = 0;   
    this.createFactModal();
  }
  showEditFactBtnClick(modFact: Fact){
    if (this.isFactNotBlocked(modFact)){ 
      this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->showEditFactBtnClick->editing->', null);
      this.resetShowFactFlags();     
      this.newFact = modFact;            
      if (modFact.factDetailed === false) { this.showEditFact = true; } 
      if (modFact.factDetailed === true)  { this.showEditDetailedFact = true; } 
      this.createFactModal();
    }
  }
  createFactModal(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->createFactModal->starting->', null);  
    const modalInitialState = {
      newFact               :this.newFact,
      factsTitle            :'Factura',
      factsDate             :this.arrayWorkingDates[0],
      createFact            :this.showCreateFact,             
      createDetailedFact    :this.showCreateDetailedFact,
      editFact              :this.showEditFact,
      editDetailedFact      :this.showEditDetailedFact,
      changeFactPayType     :this.changeFactPayType,
      selectedFactPayType   :this.selectedFactPayType,
      changeFactDate        :this.changeFactDate,
      hdFlag                :this.hdFlag,
      monthFlag             :this.monthFlag,
      callback              :'OK',   
    };
    this.globalVar.openModal(FactsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllFactsPayTypeDates(this.selectedFactPayType,this.arrayWorkingDates);          
    })
  }
  isFactNotBlocked(modFact: Fact):boolean{
    if (modFact.factStatus === false){
      this.globalVar.showErrorMsg2 = true;
      this.globalVar.errorMsg2 = "Factura BLOQUEADA"+"->"+modFact.factId+"->";
      return false;
    } else return true;
  }
  lockFactBtnClick(modFact: Fact){
    if (modFact.factStatus === true) modFact.factStatus = false;
    else  modFact.factStatus = true;
    this.updateOneFactHd(modFact, this.arrayWorkingDates);
  }
  searchFactsBtnClick() {
    if (this.showSearchFacts === true) {
      this.showSearchFacts = false;
    } else {
      this.showSearchFacts = true;
    }
    return;
  }
  setOrderFactBtnClick(value: string) {
    if (this.orderFactsList === value) {
      this.reverseFactsList = !this.reverseFactsList;
    }
    this.orderFactsList = value;
  }
  /* ---------------------------------------FACTS HD -- DATABASE ---------------------------------------------------------*/
  getAllFactsPayTypeDates(factsPayType:string, arrayWorkingDates: Date[]) {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->getAllFactsPayTypeDates->starting->', null);
    let factsTypeValueRecordList = this.globalVar.prepareStringDatesValueRecordList(factsPayType,arrayWorkingDates[0],arrayWorkingDates[1]);
    this._factsService.getAllFactsTypeMonth(factsTypeValueRecordList)
      .subscribe({next:(data) => { this.facts = data;
                           this.orderFactsList = 'factProvName';
                           this.reverseFactsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.facts, 'factProvName');
                           this.facts = this.sortedCollection;
                           this.calculateFactsTotal();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneFactHd(updateFact: Fact, arrayWorkingDates: Date[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->updateOneFactHd->updating->', null);
    this._factsService.updateOneFact(updateFact)
        .subscribe({next:(data) => { this.newFact = data;
                             this.getAllFactsPayTypeDates(this.selectedFactPayType,arrayWorkingDates);                                                    
                           },
                  error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };  }});
  }
  /* -------------------------------------FACTS DELETE -------------------------------------------------------------*/
  confirmDeleteOneFactBtnClick(fact: Fact){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteOneFactBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.facts,this.formTitles.facts,
                            fact,this.formTitles.factDeleteOne,fact.factProvName,fact.factId.toString(),
                            fact.factTotalCost.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            if (fact.factDetailed === false) { this.deleteOneFact(fact); }                      
            else { this.deleteOneFact(fact) } 
          }
        })
  }
  confirmDeleteAllFactsHdBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteAllFactsBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.facts,this.formTitles.facts,
                            null,this.formTitles.factDeleteAll,"","","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllFactsHd(this.arrayWorkingDates); }
        })
  } 
  deleteAllFactsHd(arrayWorkingDates: Date[]){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->deleteAllFactsHd->deleting All->', null);
    this._factsService.delAllFactsHd(arrayWorkingDates[0])
    .subscribe({next:(data) => { this.getAllFactsPayTypeDates(this.selectedFactPayType,arrayWorkingDates); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  deleteOneFact(delFact: Fact){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS HD->deleteOneFact->deleting One->', null);
    this._factsService.delOneFact(delFact)
    .subscribe({next:(data) => { this.getAllFactsPayTypeDates(this.selectedFactPayType,this.arrayWorkingDates); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  /* -------------------------------------FACTS HDS DATABASE -------------------------------------------------------------*/
  updateDateHds(arrayWorkingDates: Date[], dateHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '->FACTS HD->updateDateHds->updating->', null);
    this._hdService.updateDateHds(dateHds)
      .subscribe({next:(data) => {  //this.bsModalRef.content.callback('OK');
                            this.result.next('OK');
                            this.bsModalRef.hide();
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}