import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { Field,Value } from './field';
import { GlobalVarService } from '../aa-common/global-var.service';

@Component({
  selector      :'app-values-create',
  templateUrl   :'./values-create.component.html',
  styleUrls     :['./fields.component.css']
})
export class ValuesCreateComponent implements OnInit {
  public newField     :Field;
  public valuesTitle  :string;
  callback            :any;
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'provMoreData'      :'Mas Datos Fieldeedor',
    };
  public  formLabels = {
    'fieldId'           :'Id.Campo',
    'fieldName'         :'Nombre Campo',
    'fieldExplan'       :'Descripción Campo',
    'fieldStatus'       :'Estado',
    'fieldValuesString' :'Valores del Campo',
    'valueId'           :'Id',
    'valueName'         :'Valor para CAMPO',
  };
  public  valueForm     :FormGroup;
  private newValue      :Value;  
  private logToConsole  :boolean;
  /*---------------------------------------MORE PROVS -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private bsModalRef: BsModalRef, private fb:FormBuilder, 
                private viewContainer: ViewContainerRef, private globalVar: GlobalVarService, ) { }
  /*---------------------------------------MORE PROVS -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {  
    this.logToConsole = true;
    this.valueForm = this.fb.group({
      valueId:        [''],
      valueName:      ['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      fieldId:        [''],
    });
    this.valueForm.get('fieldId').setValue(this.newField.fieldId);    
     this.globalVar.consoleLog(this.logToConsole,'->FIELDS values CREATE->onInit->starting->', null);   
  }
  /*---------------------------------------MORE PROVS -- FORMS-----------------------------------------------------------*/
  copyValueFormDataToValue(){
    this.newValue = new Value();
    this.newValue.valueId      = this.valueForm.controls.valueId.value;
    this.newValue.fieldId      = this.valueForm.controls.fieldId.value;
    this.newValue.valueName    = this.valueForm.controls.valueName.value;
  }
  copyNewValueDataToMprovForm(){
    this.valueForm.patchValue( {valueId: this.newValue.valueId, fieldId: this.newValue.fieldId,  valueName: this.newValue.valueName, } );
  }
  cleanValueFormData(){
    this.valueForm.patchValue( {valueId:'', fieldId:'',valueName: '' });
  }
  /*---------------------------------------MORE PROVS -- GENERAL-----------------------------------------------------------*/

  /*---------------------------------------MORE PROVS -- BTN CLICK-----------------------------------------------------------*/  
  closeValuesCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){  
      //this.bsModalRef.content.callback(null);
      this.result.next(null);
      this.bsModalRef.hide();
    }
  }
  saveValueBtnClick(){
    if (this.bsModalRef.content.callback != null){
        this.copyValueFormDataToValue();
        //this.bsModalRef.content.callback(this.newValue);
        this.result.next(this.newValue);
        this.bsModalRef.hide();
      }
  }
  /*---------------------------------------MORE PROVS -- DATABASE-----------------------------------------------------------*/ 

}