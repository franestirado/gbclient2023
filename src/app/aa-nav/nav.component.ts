import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BsLocaleService } from 'ngx-bootstrap/datepicker'; 
import { defineLocale } from 'ngx-bootstrap/chronos'; 
import { esDoLocale } from 'ngx-bootstrap/locale';
import { listLocales } from 'ngx-bootstrap/chronos';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { FieldsService } from '../dbc-fields/fields.service';

//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { setTheme } from 'ngx-bootstrap/utils'; 
//import { Field } from '../dbc-fields/field';
//import { DATE } from 'ngx-bootstrap/chronos/units/constants';
//import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  public navValueRecordList: ValueRecord [][];
  isCollapsed = true;
  private logToConsole: boolean;
  locales = listLocales();

  constructor ( public router: Router, private localeService: BsLocaleService, 
                public globalVar: GlobalVarService, private _fieldsService: FieldsService ) {

    this.globalVar.consoleLog(this.logToConsole,'->NAV->constructor......Gest-Bar-Client-2023-->GIT---2023-03-12c.....', null);   

    esDoLocale.weekdaysShort = ['L', 'M', 'X', 'J', 'V', 'S', 'D'];
    esDoLocale.week.dow = 0;
    defineLocale('es', esDoLocale);  
    this.globalVar.consoleLog(this.logToConsole,'-> NAV -> constructor -> locales ->', this.locales);   
    var workingDate = new Date();
    this.globalVar.getAllBaresAndSetBar("ESGO");
    //this.globalVar.getAllBaresAndSetBar("RUBI");

    this.globalVar.changeWorkingDates(workingDate);   
    //this.globalVar.changeWorkingDates(new Date('2022/01/01'));               
    
    this.globalVar.getFieldsAndValues();
    this.globalVar.getFactTypesFromSubDeps();
    this.globalVar.getAllArtTypes();
    this.globalVar.getAllDeps();    
/*    
    this.globalVar.getFactPayTypesAndBankAccountsFromHdTypes();
    this.globalVar.getAllCardTpvs();
  */  
    this.globalVar.consoleLog(this.logToConsole,'-> NAV -> constructor ->.....Ending....->', null);   
  }

  ngOnInit() {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.localeService.use('es');
  }
  toggleMenu() {
    this.isCollapsed = !this.isCollapsed;
  }

}
