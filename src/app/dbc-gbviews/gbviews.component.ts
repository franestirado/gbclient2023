import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OrderPipe } from 'ngx-order-pipe';
import { throwError } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { GbViewsService, GbView } from './gbviews.service';
import { ValueRecord } from '../dbc-fields/field';
import { FieldsService } from '../dbc-fields/fields.service';

@Component({
  selector      :'app-gbviews',
  templateUrl   :'./gbviews.component.html',
  styleUrls     :['./gbviews.component.css']
})
export class GbViewsComponent implements OnInit {
  public  showCreateGbView          :boolean;
  public  createGbViewForm          :NgForm;
  public  showSearchGbViews         :boolean;
  public  showEditGbView            :boolean;
  public  hello                     :string;
  public  languageGbViewRecord      :ValueRecord;
  public  localScreensRecordList    :ValueRecord[];
  public  localLanguagesRecordList  :ValueRecord[];
  public  newGbView                 :GbView;
  public  searchGbView              :GbView;
  public  selectedGbView            :GbView;
  public  order                     :string;
  public  reverse                   :boolean;
  public  caseInsensitive           :boolean;
  public  sortedCollection          :any[];
  private index                     :number;
  public  gbViews = [];
  public  gbViewData = [];
  public  gbViewNewTextList = [];
  public  values = [];
  formTitles = {
    'gbViews'           :'Hojas del Día',
    'gbViewDeleteOne'   :'¿ Seguro que quiere BORRAR esta VISTA ?',
    'gbViewDeleteAll'   :'¿ Seguro que quiere BORRAR TODAS LAS VISTAS ?'
  };
  constructor(  private viewContainer: ViewContainerRef, public orderPipe: OrderPipe, private modalService: BsModalService,
                public globalVar: GlobalVarService, private _errorService: ErrorService, private _gbViewsService: GbViewsService, 
                private _fieldsService: FieldsService) {
    this.showCreateGbView = false;
    this.showSearchGbViews = false;
    this.showEditGbView = false;
    this.newGbView = new GbView ();
    this.newGbView.gbViewName = '';
    this.searchGbView = new GbView ();
    this.languageGbViewRecord = new ValueRecord();
    this.getAllGbViews();
    this.getFieldNameVRList();
    this.order = 'gbViewName';
    this.reverse = false;
    this.caseInsensitive= true;
    this.sortedCollection = orderPipe.transform(this.gbViews, 'gbViewName');
    console.log(this.sortedCollection);
    this.gbViews = this.sortedCollection ;
   }

  ngOnInit() {
    this.localScreensRecordList = this.globalVar.screensRecordList;
    this.localLanguagesRecordList = this.globalVar.languagesRecordList;
   }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
    console.log(this.order);
  }
  searchGbViews() {
    if (this.showSearchGbViews === true) {
      this.showSearchGbViews = false;
    } else {
      this.showSearchGbViews = true;
    }
    return;
  }
  confirmDeleteOneGbViewBtnClick(gbView: GbView){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.gbViews,this.formTitles.gbViews,
                            gbView,this.formTitles.gbViewDeleteOne,gbView.gbViewName,
                            gbView.objLang,gbView.objName,"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){  this.deleteOneGbView(gbView); }
        })
  }
  confirmDeleteAllGbViewsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.gbViews,this.formTitles.gbViews,
                            null,this.formTitles.gbViewDeleteOne,"","","","","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){   this.deleteAllGbViews(); }
        })
  }
  seeCreateGbView(createGbViewForm: NgForm) {
    this.showCreateGbView = true;
    this.showEditGbView = false;
    this.newGbView = new GbView();
    this.newGbView.gbViewName = this.globalVar.screensRecordList[0].value;
    return;
  }
  hideCreateGbView(createGbViewForm: NgForm) {
    createGbViewForm.reset();
    this.showCreateGbView = false;
    return;
  }
  cleanCreateGbForm(createGbViewForm: NgForm) {
    createGbViewForm.reset();
    this.globalVar.showErrorMsg1 = false;
    this.newGbView.gbViewName = this.globalVar.screensRecordList[0].value;
    return;
  }
  editOneGbView(gbView: GbView): void {
    this.showEditGbView = true;
    this.showCreateGbView = false;
    this.newGbView.gbViewName = gbView.gbViewName;
    this.newGbView.gbViewId = gbView.gbViewId;
    this.getGbViewData(gbView);
  }
  seeEditGbView(updateGbViewForm: NgForm) {
    this.showEditGbView = true;
    this.showCreateGbView = false;
    return;
  }
  hideEditGbView(updateGbViewForm: NgForm) {
    this.showEditGbView = false;
    this.showCreateGbView = false;
    return;
  }
  cleanEditGbView(updateGbViewForm: NgForm) {
    this.globalVar.showErrorMsg1 = false;
    updateGbViewForm.reset();
    return;
  }
  saveEditGbView(updateGbViewForm: NgForm) {
    for (let i = 0 ; i < this.gbViewNewTextList.length ; i++) {
     if (this.gbViewNewTextList[i].value.length < 5) {
      this.globalVar.showErrorMsg1 = true;
      this.globalVar.errorMsg1 = 'Hay campos con menos de 5 caracteres';
      return;
     }
    }
    if ( (this.languageGbViewRecord.value.length > 3) || (this.languageGbViewRecord.value.length === 0) ) {
      this.globalVar.showErrorMsg1 = true;
      this.globalVar.errorMsg1 = 'Hay que seleccionar un idioma';
      return;
    }
    this.index = this.gbViewNewTextList.length;
    this.gbViewNewTextList[this.index] = this.languageGbViewRecord;
    this.index = this.gbViewNewTextList.length;
    this.gbViewNewTextList[this.index] = new ValueRecord();
    this.gbViewNewTextList[this.index].value = this.newGbView.gbViewName;
    this.gbViewNewTextList[this.index].id = this.newGbView.gbViewId;
    this.globalVar.consoleLog(true, '->saveEditGbView->gbViewNewTextList->', this.gbViewNewTextList);
    this.createEditGbView(this.gbViewNewTextList);
  }
  updateGbViewData(updateGbViewForm: NgForm) {
    this.showEditGbView = false;
    this.showCreateGbView = false;
    return;
  }
  seeGbViewDetails(gbViewForm: NgForm) {
    this.showCreateGbView = true;
    return;
  }
  hideGbViewDetails(gbViewForm: NgForm) {
    this.showCreateGbView = false;
    this.newGbView = new GbView();
    gbViewForm.reset();
    return;
  }
  cleanGbViewDetails(): void {
    this.showCreateGbView = false;
    this.showEditGbView = false;
    this.newGbView = new GbView();
    this.getAllGbViews();
  }
  cleanGbViewData(gbViewForm: NgForm): void {
    this.showEditGbView = false;
    gbViewForm.reset();
    this.values = null;
  }
  createGbViews(createGbViewForm: NgForm) {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> createGbViews ->...Starting...->', null);
    if ( (this.newGbView.gbViewName !== null) && (createGbViewForm.valid === true)) {
      this._gbViewsService.createGbViews(this.newGbView.gbViewName)
        .subscribe({next:(data) => {
                          this.showCreateGbView = false;
                          this.showEditGbView = false;
                          this.globalVar.showErrorMsg1 = false;
                          this.newGbView = data;
                          this.globalVar.consoleLog(this.globalVar.logToConsole, '->createGbViews->data->', data);
                          this.getAllGbViews();
                         },
                 error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
    } else {
      this.globalVar.showErrorMsg1 = true;
      this.globalVar.errorMsg1 = 'Datos incorrectos en el Formulario';
    }
  }
  getFieldNameVRList() {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> getFieldNameVRList ->...Starting...->', null);
    this._fieldsService.getFieldsAndValues()
      .subscribe({next:(data) => {this.globalVar.valueRecordList = data;
                          this.globalVar.screensRecordList = null;
                          this.globalVar.languagesRecordList = null;
                          for (let i = 0 ; i < this.globalVar.valueRecordList.length ; i++ ) {
                            switch (this.globalVar.valueRecordList[i][0].value) {
                              case 'screens': {
                                  this.globalVar.screensRecordList = this.globalVar.valueRecordList[i];
                                  this.globalVar.screensRecordList[0].value = 'Select...' + this.globalVar.screensRecordList[0].value;
                                  if ( (this.globalVar.screensRecordList === null) || (this.globalVar.screensRecordList.length === 1) ) {
                                    this.globalVar.screensRecordList = [];
                                    this.globalVar.screensRecordList[0] = new ValueRecord();
                                    this.globalVar.screensRecordList[0].value = 'NO screens defined yet';
                                  }
                                  break;
                                  }
                              case 'languages': {
                                  this.globalVar.languagesRecordList = this.globalVar.valueRecordList[i];
                                  this.globalVar.languagesRecordList[0].value = 'Select...' + this.globalVar.languagesRecordList[0].value;
                                  this.languageGbViewRecord.value = this.globalVar.languagesRecordList[0].value;
                                  this.languageGbViewRecord.id = 99;
                                  if ( (this.globalVar.languagesRecordList === null) || (this.globalVar.languagesRecordList.length === 1) ) {
                                    this.globalVar.languagesRecordList = [];
                                    this.globalVar.languagesRecordList[0] = new ValueRecord();
                                    this.globalVar.languagesRecordList[0].value = 'NO languages defined yet';
                                    this.languageGbViewRecord.value = this.globalVar.languagesRecordList[0].value;
                                    this.languageGbViewRecord.id = 99;
                                  }
                                  break;
                                  }
                              default : {
                                  break;
                                  }
                            }
                          }
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getGbViewData(editGbView: GbView) {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> getGbViewData ->...Starting...->', null);
    this._gbViewsService.getGbViewData(editGbView)
      .subscribe({next:(data) => { this.gbViewData = data;
                           for (let i = 0 ; i < this.gbViewData.length; i++) {
                             this.gbViewNewTextList[i] = new ValueRecord();
                             this.gbViewNewTextList[i].id = this.gbViewData[i].gbViewId;
                             this.gbViewNewTextList[i].value = this.gbViewData[i].objText;
                           }
                           this.gbViewNewTextList.length = this.gbViewData.length;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  createEditGbView(gbViewNewTextList: ValueRecord[]) {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> createEditGbView ->...Starting...->', null);
    this.showCreateGbView = false;
    this.showEditGbView = false;
    this._gbViewsService.createEditGbView(gbViewNewTextList)
      .subscribe({next:(data) => { this.getAllGbViews(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getAllGbViews() {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> getAllGbViews ->...Starting...->', null);
    this._gbViewsService.getAllGbViews()
      .subscribe({next:(data) => { this.gbViews = data; },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  saveGbViewForm(gbViewForm: NgForm): void {
    if (this.showEditGbView === false) { this.saveGbView(gbViewForm); } 
    else { this.updateGbView(gbViewForm); }
  }
  saveGbView(gbViewForm: NgForm): void {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> updateGbView ->...Starting...->', null);
    this._gbViewsService.addOneGbView(gbViewForm.value)
      .subscribe({next:(data) => { this.newGbView = data;
                           this.showEditGbView = true;
                           this.getAllGbViews();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateGbView(gbViewForm: NgForm): void {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> updateGbView ->...Starting...->', null);
    this._gbViewsService.updateOneGbView(gbViewForm.value)
      .subscribe({next:(data) => { this.getAllGbViews(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  deleteAllGbViews(){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> deleteAllGbViews ->...Starting...->', null);
    this._gbViewsService.delAllGbViews()
    .subscribe({next:(data) => { this.cleanGbViewDetails(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  deleteOneGbView(delGbView: GbView){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> GBVIEWS -> deleteOneGbView ->...Starting...->', null);
    this._gbViewsService.delOneGbView(delGbView)
    .subscribe({next:(data) => { this.cleanGbViewDetails(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});  
  }
}
