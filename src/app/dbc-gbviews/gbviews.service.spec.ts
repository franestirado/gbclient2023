import { TestBed } from '@angular/core/testing';

import { GbViewsService } from './gbviews.service';

describe('GbViewsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GbViewsService = TestBed.get(GbViewsService);
    expect(service).toBeTruthy();
  });
});
