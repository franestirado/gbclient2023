import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';

export class GbView {
  public gbViewId: number;
  public gbViewName: string;
  public objType: string;
  public objSet:  string;
  public objName: string;
  public objLang: string;
  public objText: string;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Injectable()
export class GbViewsService {
  constructor(public globalVar: GlobalVarService, private http: HttpClient) { }
  json: string;
  params: string;
  headers: HttpHeaders;

  createGbViews(nameGbView: String): Observable<any>  {
    return this.http.post('/gbViews/create', nameGbView)
  }
  getGbViewData(editGbView: GbView): Observable<GbView[]>  {
    return this.http.post<GbView[]>('/gbViews/getdata', editGbView)
  }
  createEditGbView(gbViewNewTextList: ValueRecord[]): Observable<GbView[]>  {
    return this.http.post<GbView[]>('/gbViews/newview', gbViewNewTextList)
  }
  getAllGbViews(): Observable<GbView[]>  {
    return this.http.get<GbView[]>('/gbViews/get')
  }
  getGbViews(viewName: string): Observable<any>  {
    return this.http.post('/gbViews/getView', viewName)
  }
  addOneGbView(newGbView: GbView): Observable<any>  {
      return this.http.post('/gbViews/post', newGbView)
  }
  updateOneGbView(newGbView: GbView): Observable<any>  {
    return this.http.put('/gbViews/put', newGbView)
  }
  delOneGbView(delGbView: GbView): Observable<any> {
      return this.http.post('/gbViews/delOne', delGbView)
  }
  delAllGbViews(): Observable<any> {
    return this.http.delete('/gbViews/')
  }

}

