import { Component, OnInit } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { BsModalRef,BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';

import { GlobalVarService } from '../aa-common/global-var.service';

import { FactModels, FactModelsString } from './factmodel';
import { FactModelsService } from './factmodel.service';
import { FactModelCreateComponent } from './factmodel-create.component';

import { Prov } from '../dbc-provs/prov';
 
@Component({
  selector      :'app-factmodel-select',
  templateUrl   :'./factmodel-select.component.html',
  styleUrls     :['./factmodel-create.component.css']  
})
export class FactModelSelectComponent implements OnInit {
  public factModels     :FactModels[]; 
  public selectedProv   :Prov;
  public modelsTitle    :string;
  public modelsDate     :Date;
  public editFactModel  :boolean;
  callback: any;
  result: Subject<any> = new Subject<any>();
    
  formTitles = {
    'modelsSelTitle'          :'Seleccionar Modelo Factura de Proveedor',
    'modelsTitle'             :'',   
    'modelsDate'              :'',
  };
  public formLabels = {
      'id'                    :'#####',
      'factModelsProvId'      :'Prov_Id',
      'factModelsDate'        :'Fecha',
      'factModelsProvName'    :'Proveedor',
      'factModelsId'          :'Model_Id',
      'factModelsName'        :'Nombre Modelo Factura',
      'factModelsSelect'      :'Seleccionar',
      'factModelsEdit'        :'Editar',
  };
  public  title                   :string;
  private newFactModel            :FactModels;
  public  caseInsensitive         :boolean;
  private sortedCollection        :any[];
  private logToConsole            :boolean;
  public  showSearchFactModels    :boolean;
  public  searchFactModel         :FactModelsString;
  public  orderFactModelsList     :string;
  public  reverseFactModelsList   :boolean;
  public  showCreateFactModel     :boolean; 
  public  showEditFactModel       :boolean;
  
/*---------------------------------------FACTS MODEL SELECT -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef,  private modalService: BsModalService, private orderPipe: OrderPipe,  
                  private globalVar: GlobalVarService, private _factModelService: FactModelsService ) { }
/*---------------------------------------FACTS MODEL SELECT -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    if (this.modelsDate === null) {
      this.formTitles.modelsDate = '';
    } else {
      this.formTitles.modelsDate = this.modelsDate.toLocaleDateString();
    }
    this.formTitles.modelsTitle = this.modelsTitle;   
    this.searchFactModel = new FactModelsString();
    this.orderFactModelsList = 'factModelName';
    this.reverseFactModelsList = false;
    this.caseInsensitive = true;
    this.showSearchFactModels = true;
    this.showCreateFactModel = true; 
    this.showEditFactModel = false;
    if (this.selectedProv === null) {
      this.getAllFactModels();
    } else {
      this.getFactModelsProv(this.selectedProv);
    }
  }
  /*---------------------------------------FACTS MODEL SELECT -- FORMS-----------------------------------------------------------*/
  /*---------------------------------------FACTS MODEL SELECT  -- GENERAL -----------------------------------------------------------*/
  /*---------------------------------------FACTS MODEL SELECT --  BTN CLICK-----------------------------------------------------------*/
  closeSelectFactModelBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      }
  }
  selectedFactModelBtnClick(selFactModel: FactModels){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(selFactModel);
        this.result.next(selFactModel);
        this.bsModalRef.hide();
      }
  }
  setOrderFactModelBtnClick(value: string) {
    if (this.orderFactModelsList === value) {
      this.reverseFactModelsList = !this.reverseFactModelsList;
    }
    this.orderFactModelsList = value;
  }
  createFactModelBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->showCreateFactModelBtnClick->creating->', null);
    this.showCreateFactModel = true; 
    this.showEditFactModel = false;
    this.newFactModel = new FactModels();
    this.createFactModel();    
  }
  editFactModelBtnClick(modFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->showEditFactModelBtnClick->editing->', null);
    this.showEditFactModel = true;
    this.showCreateFactModel = false;
    this.newFactModel = modFactModel;
    this.createFactModel();
  }
  createFactModel(){
    const modalInitialState = {
      newFactModel          :this.newFactModel,
      modelsTitle           :this.modelsTitle,
      modelsDate            :this.modelsDate,  
      showCreateFactModel   :this.showCreateFactModel,
      showEditFactModel     :this.showEditFactModel,
      callback              :'OK',   
    };
    this.globalVar.openModal(FactModelCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (this.selectedProv === null) {
        this.getAllFactModels();
      } else {
        this.getFactModelsProv(this.selectedProv);
      }
    })
  }
  /*---------------------------------------FACTS MODEL SELECT -- DATABASE ---------------------------------------------------------*/
  getAllFactModels() {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS MODEL->getAllFactModels->starting->', null);
    this._factModelService.getAllFactModels()
      .subscribe({next:(data) => { this.factModels = data;
                           this.orderFactModelsList = 'factModelProvName';
                           this.reverseFactModelsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.factModels, 'factModelProvName');
                           this.factModels = this.sortedCollection; 
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getFactModelsProv(selectedProv: Prov) {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS MODEL->getFactModelsProv->starting->', null);
    this._factModelService.getFactModelsProv(selectedProv)
      .subscribe({next:(data) => { this.factModels = data;
                           this.orderFactModelsList = 'factModelProvName';
                           this.reverseFactModelsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.factModels, 'factModelProvName');
                           this.factModels = this.sortedCollection; 
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}