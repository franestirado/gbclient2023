import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subscription, throwError, Subject } from 'rxjs';
import { DatePipe } from "@angular/common"

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';

import { Maq } from './maqs';
import { MaqsService } from './maqs.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';
import { Hd } from '../dbx-hd/hd';

@Component({
  selector      :'app-maqs-create',
  templateUrl   :'./maqs-create.component.html',
  styleUrls     :['./maqs.component.css'] 
}) 
export class MaqsCreateComponent implements OnInit {
  public  newMaq        :Maq;
  public  maqRecHdsList :Hd[];
  public  createMaqFlag :boolean;
  callback: any;
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'maqsTitle'             :'Recaudación Máquina',   
    'maqsDate'              :'Desde...Hasta',
    'maqsName'              :'Máquina',
    'maqs'                  :'Resumen Máquinas',
    'maqCreate'             :'Crear',
    'maqEdit'               :'Modificar',
  };
  public  formLabels = {
    'id'                  :'#####',
    'maqId'               :'Id',
    'maqMaqBName'         :'Máquina B:',
    'maqStartDate'        :'Desde:',
    'maqEndDate'          :'Hasta:',

    'maqMechCounter'      :'Contadores Mecánicos',
    'maqEletrCounter'     :'Contadores Electrónicos',
    'maqPartCounter'      :'Contadores Parciales',
    'maqCounterInputs'    :'Entradas',
    'maqCounterOutputs'   :'Salidas',
    'maqCounterDrawer'    :'Cajón',
    'maqCounterStart'     :'Iniciales',
    'maqCounterEnd'       :'Finales',
    'maqCounterDiff'      :'Diferencia',
    'maqCounterDin'       :'Dinero',

    'maqCoinsStart'       :'Tolvas Inicio',
    'maqCoinsEnd'         :'Tolvas Fin',
    'maqCoinCoins'        :'Monedas',
    'maqCoinDin'          :'Dinero',
    'maqCoin1'            :'0,20',
    'maqCoin2'            :'1,00',
    'maqCoin3'            :'2,00',
    'maqCoin4'            :'5,00',
    'maqCoin5'            :'10,00',
    'maqCoin6'            :'20,00',
    'maqCoinsTotal'       :'TOTAL->',
    'dinCounters'         :'Dinero Contadores',
    'maqMechCounterRecDin':'Mecánico',
    'maqElecCounterRecDin':'Electrónico',
    'maqPartCounterRecDin':'Parcial',
    'dinAdded'            :'Dinero Recaudación',
    'diffCoins'           :'Diferencia Tolvas',
    'diffDaysStart'       :'Dif.Rec.Dia.Ini',
    'diffDaysEnd'         :'Dif.Rec.Dia.Fin',
    'diffDays'            :'Dif.Dia Inicial-Final',
    'dinEnd'              :'Dinero Final',
    'hdDate'              :'Día',
    'hdName'              :'Máquina',
    'hdAmount'            :'Recaudado',
  };
  public  maqRecForm              :FormGroup;
  private subsMecCntInStart       :Subscription;
  private subsMecCntOutStart      :Subscription;
  private subsMecCntDrawStart     :Subscription;
  private subsMecCntInEnd         :Subscription;
  private subsMecCntOutEnd        :Subscription;
  private subsMecCntDrawEnd       :Subscription;
  private subsElectrCntInStart    :Subscription;
  private subsElectrCntOutStart   :Subscription;
  private subsElectrCntDrawStart  :Subscription;
  private subsElectrCntInEnd      :Subscription;
  private subsElectrCntOutEnd     :Subscription;
  private subsElectrCntDrawEnd    :Subscription;
  private subsCoin1Start          :Subscription;
  private subsCoin2Start          :Subscription;
  private subsCoin3Start          :Subscription;
  private subsCoin4Start          :Subscription;
  private subsCoin5Start          :Subscription;
  private subsCoin6Start          :Subscription;
  private subsCoin1End            :Subscription;
  private subsCoin2End            :Subscription;
  private subsCoin3End            :Subscription;
  private subsCoin4End            :Subscription;
  private subsCoin5End            :Subscription;
  private subsCoin6End            :Subscription;
  private subsCoin1DinStart       :Subscription;
  private subsCoin2DinStart       :Subscription;
  private subsCoin3DinStart       :Subscription;
  private subsCoin4DinStart       :Subscription;
  private subsCoin5DinStart       :Subscription;
  private subsCoin6DinStart       :Subscription;
  private subsCoin1DinEnd         :Subscription;
  private subsCoin2DinEnd         :Subscription;
  private subsCoin3DinEnd         :Subscription;
  private subsCoin4DinEnd         :Subscription;
  private subsCoin5DinEnd         :Subscription;
  private subsCoin6DinEnd         :Subscription;
  private subsDiffDaysStart       :Subscription;
  private subsDiffDaysEnd         :Subscription;
  
  private logToConsole            :boolean;
  public  showMaqsList            :boolean;
  public  coinsInputDinFlag       :boolean;
  private maqSummList             :Maq[];
  private prevMaqSummList         :Maq[];
  private idSumm                  :number;
  private idCntMec                :number;
  private idCntEle                :number;
  private idCntPar                :number;
  private idCoinSt                :number;
  private idCoinEn                :number;
  public  difDinCntMecDinEnd      :number;
  
 /*---------------------------------------ARTS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService, private orderPipe: OrderPipe, 
                private datepipe: DatePipe, private globalVar: GlobalVarService, private _maqsService: MaqsService) { }
/*---------------------------------------ARTS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;   
    this.coinsInputDinFlag = false;
    this.difDinCntMecDinEnd = 0;

    this.maqRecForm = this.fb.group({
      maqId                     :[{value:'',disabled:true}],
      maqMaqBName               :[{value:this.newMaq.maqMaqBName ,disabled:true}],
      maqStartDate              :[{value:'',disabled:true}],
      maqEndDate                :[{value:'',disabled:true}],

      maqMechCounterInStart     :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterOutStart    :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterDrawStart   :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterInEnd       :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterOutEnd      :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterDrawEnd     :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterInDiff      :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterOutDiff     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterDrawDiff    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterInDin       :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterOutDin      :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterDrawDin     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqMechCounterRecDin      :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],

      maqEletrCounterInStart    :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterOutStart   :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterDrawStart  :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterInEnd      :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterOutEnd     :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterDrawEnd    :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterInDiff     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterOutDiff    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterDrawDiff   :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterInDin      :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterOutDin     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterDrawDin    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqEletrCounterRecDin     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],

      maqPartCounterInStart   :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterOutStart  :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterDrawStart :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterInEnd     :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterOutEnd    :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterDrawEnd   :[{value:0,disabled:false},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterInDiff    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterOutDiff   :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterDrawDiff  :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterInDin     :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterOutDin    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterDrawDin   :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqPartCounterRecDin    :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],

      maqCoin1Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin2Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin3Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin4Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin5Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin6Start           :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoinTotStart         :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqCoin1DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin2DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin3DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin4DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin5DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin6DinStart        :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoinDinTotStart      :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],

      maqCoin1End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin2End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin3End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin4End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin5End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoin6End             :[{value:0,disabled:!this.coinsInputDinFlag},[Validators.required,ValidationService.numberValidator]],
      maqCoinTotEnd           :[{value:0,disabled:true},[Validators.required,ValidationService.numberValidator]],
      maqCoin1DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin2DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin3DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin4DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin5DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoin6DinEnd          :[{value:0,disabled:this.coinsInputDinFlag},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqCoinDinTotEnd        :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],

      maqCoinDiffDin          :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],

      maqDayStartDin          :[{value:0,disabled:false},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqDayEndDin            :[{value:0,disabled:false},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqDaysDiffDin          :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],

      maqDinAdded             :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqDinEnd               :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      maqStatus               :[{value:true,disabled:true},],
    });

    var startDate = this.datepipe.transform(new Date(this.newMaq.maqStartDate),'dd-MM-yyyy');
    var endDate   = this.datepipe.transform(new Date(this.newMaq.maqEndDate),'dd-MM-yyyy');
    this.formTitles.maqsName = this.newMaq.maqMaqBName;
    this.formTitles.maqsDate = 'Del..'+startDate+'..Al..'+endDate;
    this.maqRecForm.get('maqStartDate').patchValue(startDate);
    this.maqRecForm.get('maqEndDate').patchValue(endDate);
    this.getMaqDinAdded();
    this.showMaqsList = false;
    this.subsCountersMech();
    this.subsCountersElectr();
    this.subsCountersPart();
    this.subsDiffDaysDin();
    this.subsCoinsDinStart();
    this.subsCoinsDinEnd();
    if ((this.createMaqFlag === true)) {
      this.getPrevMaqData(this.newMaq);
    } else {
      this.getOneMaq(this.newMaq);
    }
  }
  /*---------------------------------------ARTS CREATE -- FORMS-------------------------------------------------------------*/
  getMaqDinAdded(){
    var dinAdded = 0;
    if ( (this.maqRecHdsList !== null) || (this.maqRecHdsList.length !== 0) )
      for (let i=0; i<this.maqRecHdsList.length ; i++){
        dinAdded = this.maqRecHdsList[i].hdAmount + dinAdded;
      }
    this.maqRecForm.get('maqDinAdded').patchValue(dinAdded.toFixed(2));
    this.updateDinEnd();
  }
  updateDinEnd(){
    let dinAdded    = Number(this.maqRecForm.get('maqDinAdded').value);
    let coinDiffDin = Number(this.maqRecForm.get('maqCoinDiffDin').value);
    let daysDiffDin = Number(this.maqRecForm.get('maqDaysDiffDin').value);
    let dinEnd = dinAdded + coinDiffDin + daysDiffDin;
    this.maqRecForm.get('maqDinEnd').patchValue(dinEnd.toFixed(2));
    let dinCntMec = Number(this.maqRecForm.get('maqMechCounterRecDin').value);
    this.difDinCntMecDinEnd = dinEnd - dinCntMec;
    this.maqRecForm.updateValueAndValidity(); 
  }
  subsDiffDaysDin(){
    this.subsDiffDaysStart = this.maqRecForm.get('maqDayStartDin').valueChanges.subscribe((data:any) => {this.updateDiffDaysDin();});
    this.subsDiffDaysEnd   = this.maqRecForm.get('maqDayEndDin').valueChanges.subscribe((data:any) => {this.updateDiffDaysDin();});
  }
  updateDiffDaysDin(){
    let diffDinDaysStart = Number(this.maqRecForm.get('maqDayStartDin').value);
    let diffDinDaysEnd   = Number(this.maqRecForm.get('maqDayEndDin').value);
    let diffDinDays = diffDinDaysEnd - diffDinDaysStart;
    this.maqRecForm.get('maqDaysDiffDin').patchValue(diffDinDays.toFixed(2));
    this.maqRecForm.updateValueAndValidity(); 
  }
  subsCountersMech(){
    this.subsMecCntInStart   = this.maqRecForm.get('maqMechCounterInStart').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
    this.subsMecCntOutStart  = this.maqRecForm.get('maqMechCounterOutStart').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
    this.subsMecCntDrawStart = this.maqRecForm.get('maqMechCounterDrawStart').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
    this.subsMecCntInEnd     = this.maqRecForm.get('maqMechCounterInEnd').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
    this.subsMecCntOutEnd    = this.maqRecForm.get('maqMechCounterOutEnd').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
    this.subsMecCntDrawEnd   = this.maqRecForm.get('maqMechCounterDrawEnd').valueChanges.subscribe((data:any) => {this.updateCountersMech();});
  }
  updateCountersMech(){
    let inStart   = Number(this.maqRecForm.get('maqMechCounterInStart').value);
    let outStart  = Number(this.maqRecForm.get('maqMechCounterOutStart').value);
    let drawStart = Number(this.maqRecForm.get('maqMechCounterDrawStart').value);
    let inEnd     = Number(this.maqRecForm.get('maqMechCounterInEnd').value);
    let outEnd    = Number(this.maqRecForm.get('maqMechCounterOutEnd').value);
    let drawEnd   = Number(this.maqRecForm.get('maqMechCounterDrawEnd').value);
    let diffIn    = inEnd -inStart;
    let diffOut   = outEnd - outStart;
    let diffDraw  = drawEnd - drawStart;
    let dinIn   = diffIn * 0.20;
    let dinOut  = diffOut * 0.20;
    let dinDraw = diffDraw * 0.20;
    let recDin  = (diffIn - diffOut) * 0.20;
    this.maqRecForm.get('maqMechCounterInDiff').patchValue(diffIn.toFixed(0));
    this.maqRecForm.get('maqMechCounterOutDiff').patchValue(diffOut.toFixed(0));
    this.maqRecForm.get('maqMechCounterDrawDiff').patchValue(diffDraw.toFixed(0));
    this.maqRecForm.get('maqMechCounterInDin').patchValue(dinIn.toFixed(2));
    this.maqRecForm.get('maqMechCounterOutDin').patchValue(dinOut.toFixed(2));
    this.maqRecForm.get('maqMechCounterDrawDin').patchValue(dinDraw.toFixed(2));
    this.maqRecForm.get('maqMechCounterRecDin').patchValue(recDin.toFixed(2));
    let dinEnd = Number(this.maqRecForm.get('maqDinEnd').value);
    this.difDinCntMecDinEnd = dinEnd - recDin;
    this.maqRecForm.updateValueAndValidity(); 
  }
  subsCountersElectr(){
    this.subsElectrCntInStart   = this.maqRecForm.get('maqEletrCounterInStart').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
    this.subsElectrCntOutStart  = this.maqRecForm.get('maqEletrCounterOutStart').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
    this.subsElectrCntDrawStart = this.maqRecForm.get('maqEletrCounterDrawStart').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
    this.subsElectrCntInEnd     = this.maqRecForm.get('maqEletrCounterInEnd').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
    this.subsElectrCntOutEnd    = this.maqRecForm.get('maqEletrCounterOutEnd').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
    this.subsElectrCntDrawEnd   = this.maqRecForm.get('maqEletrCounterDrawEnd').valueChanges.subscribe((data:any) => {this.updateCountersElectr();});
  }
  updateCountersElectr(){
    let inStart   = Number(this.maqRecForm.get('maqEletrCounterInStart').value);
    let outStart  = Number(this.maqRecForm.get('maqEletrCounterOutStart').value);
    let drawStart = Number(this.maqRecForm.get('maqEletrCounterDrawStart').value);
    let inEnd     = Number(this.maqRecForm.get('maqEletrCounterInEnd').value);
    let outEnd    = Number(this.maqRecForm.get('maqEletrCounterOutEnd').value);
    let drawEnd   = Number(this.maqRecForm.get('maqEletrCounterDrawEnd').value);
    let diffIn    = inEnd -inStart;
    let diffOut   = outEnd - outStart;
    let diffDraw  = drawEnd - drawStart;
    let dinIn = diffIn * 0.20;
    let dinOut= diffOut * 0.20;
    let dinDraw = diffDraw * 0.20;
    let recDin  = (diffIn - diffOut) * 0.20;
    this.maqRecForm.get('maqEletrCounterInDiff').patchValue(diffIn.toFixed(0));
    this.maqRecForm.get('maqEletrCounterOutDiff').patchValue(diffOut.toFixed(0));
    this.maqRecForm.get('maqEletrCounterDrawDiff').patchValue(diffDraw.toFixed(0));
    this.maqRecForm.get('maqEletrCounterInDin').patchValue(dinIn.toFixed(2));
    this.maqRecForm.get('maqEletrCounterOutDin').patchValue(dinOut.toFixed(2));
    this.maqRecForm.get('maqEletrCounterDrawDin').patchValue(dinDraw.toFixed(2));
    this.maqRecForm.get('maqEletrCounterRecDin').patchValue(recDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity(); 
  }   
  subsCountersPart(){
    this.subsElectrCntInStart   = this.maqRecForm.get('maqPartCounterInStart').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
    this.subsElectrCntOutStart  = this.maqRecForm.get('maqPartCounterOutStart').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
    this.subsElectrCntDrawStart = this.maqRecForm.get('maqPartCounterDrawStart').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
    this.subsElectrCntInEnd     = this.maqRecForm.get('maqPartCounterInEnd').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
    this.subsElectrCntOutEnd    = this.maqRecForm.get('maqPartCounterOutEnd').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
    this.subsElectrCntDrawEnd   = this.maqRecForm.get('maqPartCounterDrawEnd').valueChanges.subscribe((data:any) => {this.updateCountersPart();});
  }
  updateCountersPart(){
    let inStart   = Number(this.maqRecForm.get('maqPartCounterInStart').value);
    let outStart  = Number(this.maqRecForm.get('maqPartCounterOutStart').value);
    let drawStart = Number(this.maqRecForm.get('maqPartCounterDrawStart').value);
    let inEnd     = Number(this.maqRecForm.get('maqPartCounterInEnd').value);
    let outEnd    = Number(this.maqRecForm.get('maqPartCounterOutEnd').value);
    let drawEnd   = Number(this.maqRecForm.get('maqPartCounterDrawEnd').value);
    let diffIn    = inEnd -inStart;
    let diffOut   = outEnd - outStart;
    let diffDraw  = drawEnd - drawStart;
    let dinIn = diffIn * 0.20;
    let dinOut= diffOut * 0.20;
    let dinDraw = diffDraw * 0.20;
    let recDin  = (diffIn - diffOut) * 0.20;
    this.maqRecForm.get('maqPartCounterInDiff').patchValue(diffIn.toFixed(0));
    this.maqRecForm.get('maqPartCounterOutDiff').patchValue(diffOut.toFixed(0));
    this.maqRecForm.get('maqPartCounterDrawDiff').patchValue(diffDraw.toFixed(0));
    this.maqRecForm.get('maqPartCounterInDin').patchValue(dinIn.toFixed(2));
    this.maqRecForm.get('maqPartCounterOutDin').patchValue(dinOut.toFixed(2));
    this.maqRecForm.get('maqPartCounterDrawDin').patchValue(dinDraw.toFixed(2));
    this.maqRecForm.get('maqPartCounterRecDin').patchValue(recDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity(); 
  }  
  subsCoinsStart(){
    this.subsCoin1Start = this.maqRecForm.get('maqCoin1Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
    this.subsCoin2Start = this.maqRecForm.get('maqCoin2Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
    this.subsCoin3Start = this.maqRecForm.get('maqCoin3Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
    this.subsCoin4Start = this.maqRecForm.get('maqCoin4Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
    this.subsCoin5Start = this.maqRecForm.get('maqCoin5Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
    this.subsCoin6Start = this.maqRecForm.get('maqCoin6Start').valueChanges.subscribe((data:any) => {this.updateCoinsStartDin();});
  }
  unsubsCoinsStart(){
    this.subsCoin1Start.unsubscribe();
    this.subsCoin2Start.unsubscribe();
    this.subsCoin3Start.unsubscribe();
    this.subsCoin4Start.unsubscribe();
    this.subsCoin5Start.unsubscribe();
    this.subsCoin6Start.unsubscribe();
  }
  updateCoinsStartDin(){
    let coin1Start = Number(this.maqRecForm.get('maqCoin1Start').value);
    let coin2Start = Number(this.maqRecForm.get('maqCoin2Start').value);
    let coin3Start = Number(this.maqRecForm.get('maqCoin3Start').value);
    let coin4Start = Number(this.maqRecForm.get('maqCoin4Start').value);
    let coin5Start = Number(this.maqRecForm.get('maqCoin5Start').value);
    let coin6Start = Number(this.maqRecForm.get('maqCoin6Start').value);
    let coin1StartDin = coin1Start * 0.2;
    let coin2StartDin = coin2Start * 1;
    let coin3StartDin = coin3Start * 2;
    let coin4StartDin = coin4Start * 5;
    let coin5StartDin = coin5Start * 10;
    let coin6StartDin = coin6Start * 20;
    let coinsStartTot = coin1StartDin + coin2StartDin + coin3StartDin + coin4StartDin + coin5StartDin + coin6StartDin;    
    this.maqRecForm.get('maqCoin1DinStart').patchValue(coin1StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin2DinStart').patchValue(coin2StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin3DinStart').patchValue(coin3StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin4DinStart').patchValue(coin4StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin5DinStart').patchValue(coin5StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin6DinStart').patchValue(coin6StartDin.toFixed(2));
    this.maqRecForm.get('maqCoin6DinStart').patchValue(coin6StartDin.toFixed(2));
    this.maqRecForm.get('maqCoinTotStart').patchValue(coinsStartTot.toFixed(2));
    this.maqRecForm.get('maqCoinDinTotStart').patchValue(coinsStartTot.toFixed(2));
    let coinsEndTot = Number(this.maqRecForm.get('maqCoinDinTotEnd').value);
    let coinDiffDin = coinsEndTot - coinsStartTot;
    this.maqRecForm.get('maqCoinDiffDin').patchValue(coinDiffDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity();
    this.updateDinEnd();
  }  
  subsCoinsDinStart(){
    this.subsCoin1DinStart = this.maqRecForm.get('maqCoin1DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
    this.subsCoin2DinStart = this.maqRecForm.get('maqCoin2DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
    this.subsCoin3DinStart = this.maqRecForm.get('maqCoin3DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
    this.subsCoin4DinStart = this.maqRecForm.get('maqCoin4DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
    this.subsCoin5DinStart = this.maqRecForm.get('maqCoin5DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
    this.subsCoin6DinStart = this.maqRecForm.get('maqCoin6DinStart').valueChanges.subscribe((data:any) => {this.updateCoinsStartTot();});
  }
  unsubsCoinsDinStart(){
    this.subsCoin1DinStart.unsubscribe();
    this.subsCoin2DinStart.unsubscribe();
    this.subsCoin3DinStart.unsubscribe();
    this.subsCoin4DinStart.unsubscribe();
    this.subsCoin5DinStart.unsubscribe();
    this.subsCoin6DinStart.unsubscribe();
  }
  updateCoinsStartTot(){
    let coin1StartDin = Number(this.maqRecForm.get('maqCoin1DinStart').value);
    let coin2StartDin = Number(this.maqRecForm.get('maqCoin2DinStart').value);
    let coin3StartDin = Number(this.maqRecForm.get('maqCoin3DinStart').value);
    let coin4StartDin = Number(this.maqRecForm.get('maqCoin4DinStart').value);
    let coin5StartDin = Number(this.maqRecForm.get('maqCoin5DinStart').value);
    let coin6StartDin = Number(this.maqRecForm.get('maqCoin6DinStart').value);
    let coinsStartTot = coin1StartDin + coin2StartDin + coin3StartDin + coin4StartDin + coin5StartDin + coin6StartDin;    
    this.maqRecForm.get('maqCoinDinTotStart').patchValue(coinsStartTot.toFixed(2));
    let coinsEndTot = Number(this.maqRecForm.get('maqCoinDinTotEnd').value);
    let coinDiffDin = coinsEndTot - coinsStartTot;
    this.maqRecForm.get('maqCoinDiffDin').patchValue(coinDiffDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity();
    this.updateDinEnd();
  }
  subsCoinsEnd(){
    this.subsCoin1End = this.maqRecForm.get('maqCoin1End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
    this.subsCoin2End = this.maqRecForm.get('maqCoin2End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
    this.subsCoin3End = this.maqRecForm.get('maqCoin3End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
    this.subsCoin4End = this.maqRecForm.get('maqCoin4End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
    this.subsCoin5End = this.maqRecForm.get('maqCoin5End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
    this.subsCoin6End = this.maqRecForm.get('maqCoin6End').valueChanges.subscribe((data:any) => {this.updateCoinsEndDin();});
  }
  unsubsCoinsEnd(){
    this.subsCoin1End.unsubscribe();
    this.subsCoin2End.unsubscribe();
    this.subsCoin3End.unsubscribe();
    this.subsCoin4End.unsubscribe();
    this.subsCoin5End.unsubscribe();
    this.subsCoin6End.unsubscribe();
  }
  updateCoinsEndDin(){
    let coin1End = Number(this.maqRecForm.get('maqCoin1End').value);
    let coin2End = Number(this.maqRecForm.get('maqCoin2End').value);
    let coin3End = Number(this.maqRecForm.get('maqCoin3End').value);
    let coin4End = Number(this.maqRecForm.get('maqCoin4End').value);
    let coin5End = Number(this.maqRecForm.get('maqCoin5End').value);
    let coin6End = Number(this.maqRecForm.get('maqCoin6End').value);
    let coin1EndDin = coin1End * 0.2;
    let coin2EndDin = coin2End * 1;
    let coin3EndDin = coin3End * 2;
    let coin4EndDin = coin4End * 5;
    let coin5EndDin = coin5End * 10;
    let coin6EndDin = coin6End * 20;
    let coinsEndTot = coin1EndDin + coin2EndDin + coin3EndDin + coin4EndDin + coin5EndDin + coin6EndDin;    
    this.maqRecForm.get('maqCoin1DinEnd').patchValue(coin1EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin2DinEnd').patchValue(coin2EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin3DinEnd').patchValue(coin3EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin4DinEnd').patchValue(coin4EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin5DinEnd').patchValue(coin5EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin6DinEnd').patchValue(coin6EndDin.toFixed(2));
    this.maqRecForm.get('maqCoin6DinEnd').patchValue(coin6EndDin.toFixed(2));
    this.maqRecForm.get('maqCoinTotEnd').patchValue(coinsEndTot.toFixed(2));
    this.maqRecForm.get('maqCoinDinTotEnd').patchValue(coinsEndTot.toFixed(2));
    let coinsStartTot = Number(this.maqRecForm.get('maqCoinDinTotStart').value);
    let coinDiffDin = coinsEndTot - coinsStartTot;
    this.maqRecForm.get('maqCoinDiffDin').patchValue(coinDiffDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity();
    this.updateDinEnd();
  }  
  subsCoinsDinEnd(){
    this.subsCoin1DinEnd = this.maqRecForm.get('maqCoin1DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
    this.subsCoin2DinEnd = this.maqRecForm.get('maqCoin2DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
    this.subsCoin3DinEnd = this.maqRecForm.get('maqCoin3DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
    this.subsCoin4DinEnd = this.maqRecForm.get('maqCoin4DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
    this.subsCoin5DinEnd = this.maqRecForm.get('maqCoin5DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
    this.subsCoin6DinEnd = this.maqRecForm.get('maqCoin6DinEnd').valueChanges.subscribe((data:any) => {this.updateCoinsEndTot();});
  }
  unsubsCoinsDinEnd(){
    this.subsCoin1DinEnd.unsubscribe();
    this.subsCoin2DinEnd.unsubscribe();
    this.subsCoin3DinEnd.unsubscribe();
    this.subsCoin4DinEnd.unsubscribe();
    this.subsCoin5DinEnd.unsubscribe();
    this.subsCoin6DinEnd.unsubscribe();
  }
  updateCoinsEndTot(){
    let coin1EndDin = Number(this.maqRecForm.get('maqCoin1DinEnd').value);
    let coin2EndDin = Number(this.maqRecForm.get('maqCoin2DinEnd').value);
    let coin3EndDin = Number(this.maqRecForm.get('maqCoin3DinEnd').value);
    let coin4EndDin = Number(this.maqRecForm.get('maqCoin4DinEnd').value);
    let coin5EndDin = Number(this.maqRecForm.get('maqCoin5DinEnd').value);
    let coin6EndDin = Number(this.maqRecForm.get('maqCoin6DinEnd').value);
    let coinsEndTot = coin1EndDin + coin2EndDin + coin3EndDin + coin4EndDin + coin5EndDin + coin6EndDin;    
    this.maqRecForm.get('maqCoinDinTotEnd').patchValue(coinsEndTot.toFixed(2));
    let coinsStartTot = Number(this.maqRecForm.get('maqCoinDinTotStart').value);
    let coinDiffDin = coinsEndTot - coinsStartTot;
    this.maqRecForm.get('maqCoinDiffDin').patchValue(coinDiffDin.toFixed(2));
    this.maqRecForm.updateValueAndValidity();
    this.updateDinEnd();
  }
  copyMaqFormDataToNewMaq(update:boolean){
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->copyMaqFormDataToMaq->creating->', null);
    this.maqSummList = new Array();
    let data1 = Number(this.maqRecForm.get('maqDinAdded').value);
    let data2 = Number(this.maqRecForm.get('maqDaysDiffDin').value);
    let data3 = Number(this.maqRecForm.get('maqCoinDiffDin').value);
    let data4 = Number(this.maqRecForm.get('maqDinEnd').value);
    let data5 = Number(this.maqRecForm.get('maqMechCounterRecDin').value);
    let data6 = Number(this.maqRecForm.get('maqDayEndDin').value);  //maqPartCounterRecDin maqEletrCounterRecDin maqDayEndDin
    this.maqSummList[0] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','SUMM',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[0].maqId = this.idSumm;
    data1 = Number(this.maqRecForm.get('maqMechCounterInStart').value);
    data2 = Number(this.maqRecForm.get('maqMechCounterOutStart').value);
    data3 = Number(this.maqRecForm.get('maqMechCounterDrawStart').value);
    data4 = Number(this.maqRecForm.get('maqMechCounterInEnd').value);
    data5 = Number(this.maqRecForm.get('maqMechCounterOutEnd').value);
    data6 = Number(this.maqRecForm.get('maqMechCounterDrawEnd').value); 
    this.maqSummList[1] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','CNT-MEC',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[1].maqId = this.idCntMec;
    data1 = Number(this.maqRecForm.get('maqEletrCounterInStart').value);
    data2 = Number(this.maqRecForm.get('maqEletrCounterOutStart').value);
    data3 = Number(this.maqRecForm.get('maqEletrCounterDrawStart').value);
    data4 = Number(this.maqRecForm.get('maqEletrCounterInEnd').value);
    data5 = Number(this.maqRecForm.get('maqEletrCounterOutEnd').value);
    data6 = Number(this.maqRecForm.get('maqEletrCounterDrawEnd').value); 
    this.maqSummList[2] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','CNT-ELE',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[2].maqId = this.idCntEle;
    data1 = Number(this.maqRecForm.get('maqPartCounterInStart').value);
    data2 = Number(this.maqRecForm.get('maqPartCounterOutStart').value);
    data3 = Number(this.maqRecForm.get('maqPartCounterDrawStart').value);
    data4 = Number(this.maqRecForm.get('maqPartCounterInEnd').value);
    data5 = Number(this.maqRecForm.get('maqPartCounterOutEnd').value);
    data6 = Number(this.maqRecForm.get('maqPartCounterDrawEnd').value); 
    this.maqSummList[3] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','CNT-PAR',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[3].maqId = this.idCntPar;
    data1 = Number(this.maqRecForm.get('maqCoin1DinStart').value);
    data2 = Number(this.maqRecForm.get('maqCoin2DinStart').value);
    data3 = Number(this.maqRecForm.get('maqCoin3DinStart').value);
    data4 = Number(this.maqRecForm.get('maqCoin4DinStart').value);
    data5 = Number(this.maqRecForm.get('maqCoin5DinStart').value);
    data6 = Number(this.maqRecForm.get('maqCoin6DinStart').value); 
    this.maqSummList[4] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','COIN-ST',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[4].maqId = this.idCoinSt;
    data1 = Number(this.maqRecForm.get('maqCoin1DinEnd').value);
    data2 = Number(this.maqRecForm.get('maqCoin2DinEnd').value);
    data3 = Number(this.maqRecForm.get('maqCoin3DinEnd').value);
    data4 = Number(this.maqRecForm.get('maqCoin4DinEnd').value);
    data5 = Number(this.maqRecForm.get('maqCoin5DinEnd').value);
    data6 = Number(this.maqRecForm.get('maqCoin6DinEnd').value); 
    this.maqSummList[5] = this.createMaqSummListItem(this.newMaq.maqMaqBName, this.newMaq.maqStartDate, this.newMaq.maqEndDate,
                                  'MAQB','COIN-EN',data1,data2,data3,data4,data5,data6,);
    if (update === true) this.maqSummList[5].maqId = this.idCoinEn;
    //this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->copyMaqFormDataToMaq->creating->', this.maqSummList);
  }
  copyNewMaqDataToMaqForm(){
    for (let i=0; i<this.maqSummList.length ; i++){
      switch (this.maqSummList[i].maqSumDataType) {
        case 'SUMM': 
          this.idSumm = this.maqSummList[i].maqId;
          let dayDiffDin = Number(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(0));
          let dayEndDin  = Number(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(0));
          let dayStartDin = dayEndDin - dayDiffDin;
          this.maqRecForm.get('maqId').patchValue(this.idSumm);
          this.maqRecForm.get('maqDayEndDin').patchValue(dayEndDin);
          this.maqRecForm.get('maqDayStartDin').patchValue(dayStartDin);
          break;
        case 'CNT-MEC': 
          this.idCntMec = this.maqSummList[i].maqId;
          this.maqRecForm.get('maqMechCounterInStart').patchValue(this.maqSummList[i].maqCoin1StartInputDinAdded.toFixed(0));
          this.maqRecForm.get('maqMechCounterOutStart').patchValue(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(0));
          this.maqRecForm.get('maqMechCounterDrawStart').patchValue(this.maqSummList[i].maqCoin3StartDrawerDinDifCoins.toFixed(0));
          this.maqRecForm.get('maqMechCounterInEnd').patchValue(this.maqSummList[i].maqCoin4EndInputDinEnd.toFixed(0));
          this.maqRecForm.get('maqMechCounterOutEnd').patchValue(this.maqSummList[i].maqCoin5EndOutputDinMechCntr.toFixed(0));
          this.maqRecForm.get('maqMechCounterDrawEnd').patchValue(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(0));
          break;
        case 'CNT-ELE':
          this.idCntEle = this.maqSummList[i].maqId;
          this.maqRecForm.get('maqEletrCounterInStart').patchValue(this.maqSummList[i].maqCoin1StartInputDinAdded.toFixed(0));
          this.maqRecForm.get('maqEletrCounterOutStart').patchValue(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(0));
          this.maqRecForm.get('maqEletrCounterDrawStart').patchValue(this.maqSummList[i].maqCoin3StartDrawerDinDifCoins.toFixed(0));
          this.maqRecForm.get('maqEletrCounterInEnd').patchValue(this.maqSummList[i].maqCoin4EndInputDinEnd.toFixed(0));
          this.maqRecForm.get('maqEletrCounterOutEnd').patchValue(this.maqSummList[i].maqCoin5EndOutputDinMechCntr.toFixed(0));
          this.maqRecForm.get('maqEletrCounterDrawEnd').patchValue(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(0));
          break;        
        case 'CNT-PAR': 
          this.idCntPar = this.maqSummList[i].maqId;
          this.maqRecForm.get('maqPartCounterInStart').patchValue(this.maqSummList[i].maqCoin1StartInputDinAdded.toFixed(0));
          this.maqRecForm.get('maqPartCounterOutStart').patchValue(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(0));
          this.maqRecForm.get('maqPartCounterDrawStart').patchValue(this.maqSummList[i].maqCoin3StartDrawerDinDifCoins.toFixed(0));
          this.maqRecForm.get('maqPartCounterInEnd').patchValue(this.maqSummList[i].maqCoin4EndInputDinEnd.toFixed(0));
          this.maqRecForm.get('maqPartCounterOutEnd').patchValue(this.maqSummList[i].maqCoin5EndOutputDinMechCntr.toFixed(0));
          this.maqRecForm.get('maqPartCounterDrawEnd').patchValue(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(0));
          break;        
        case 'COIN-ST':
          this.idCoinSt = this.maqSummList[i].maqId; 
          this.maqRecForm.get('maqCoin1DinStart').patchValue(this.maqSummList[i].maqCoin1StartInputDinAdded.toFixed(2));
          this.maqRecForm.get('maqCoin2DinStart').patchValue(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(2));
          this.maqRecForm.get('maqCoin3DinStart').patchValue(this.maqSummList[i].maqCoin3StartDrawerDinDifCoins.toFixed(2));
          this.maqRecForm.get('maqCoin4DinStart').patchValue(this.maqSummList[i].maqCoin4EndInputDinEnd.toFixed(2));
          this.maqRecForm.get('maqCoin5DinStart').patchValue(this.maqSummList[i].maqCoin5EndOutputDinMechCntr.toFixed(2));
          this.maqRecForm.get('maqCoin6DinStart').patchValue(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(2));
          break;  
        case 'COIN-EN':
          this.idCoinEn = this.maqSummList[i].maqId;
          this.maqRecForm.get('maqCoin1DinEnd').patchValue(this.maqSummList[i].maqCoin1StartInputDinAdded.toFixed(2));
          this.maqRecForm.get('maqCoin2DinEnd').patchValue(this.maqSummList[i].maqCoin2StartOutputDinDifDays.toFixed(2));
          this.maqRecForm.get('maqCoin3DinEnd').patchValue(this.maqSummList[i].maqCoin3StartDrawerDinDifCoins.toFixed(2));
          this.maqRecForm.get('maqCoin4DinEnd').patchValue(this.maqSummList[i].maqCoin4EndInputDinEnd.toFixed(2));
          this.maqRecForm.get('maqCoin5DinEnd').patchValue(this.maqSummList[i].maqCoin5EndOutputDinMechCntr.toFixed(2));
          this.maqRecForm.get('maqCoin6DinEnd').patchValue(this.maqSummList[i].maqCoin6EndDrawerDinPartCntr.toFixed(2)); 
          break;        
        default: 
          break;                  
      };
      this.updateCoinsEndTot();
      this.updateCoinsStartTot();
      this.updateCountersElectr();
      this.updateCountersMech();
      this.updateCountersPart();
      this.updateDinEnd();
      this.updateDiffDaysDin();
    }
  }
  createMaqSummListItem(maqName :string, startDate :Date, endDate :Date, type :string, dataType :string,
                        data1 :number,data2 :number,data3 :number,data4 :number,data5 :number,data6 :number, ):Maq {
    let newMaqSummListItem = new Maq();
    newMaqSummListItem.maqMaqBName     = maqName;
    newMaqSummListItem.maqStartDate    = startDate;
    newMaqSummListItem.maqEndDate      = endDate;
    newMaqSummListItem.maqSumType      = type;
    newMaqSummListItem.maqSumDataType  = dataType;
    newMaqSummListItem.maqCoin1StartInputDinAdded      = data1;
    newMaqSummListItem.maqCoin2StartOutputDinDifDays   = data2;
    newMaqSummListItem.maqCoin3StartDrawerDinDifCoins  = data3;
    newMaqSummListItem.maqCoin4EndInputDinEnd          = data4;
    newMaqSummListItem.maqCoin5EndOutputDinMechCntr    = data5;
    newMaqSummListItem.maqCoin6EndDrawerDinPartCntr    = data6;
    newMaqSummListItem.maqStatus = true;
    return newMaqSummListItem;
  }
  copyPrevMaqDataToMaqForm(prevMaqDataSummList :Maq[]){
    if (prevMaqDataSummList === null) return;
    if (prevMaqDataSummList.length === 0) return;
    for (let i=0; i<prevMaqDataSummList.length ; i++){
      switch (prevMaqDataSummList[i].maqSumDataType) {
        case 'SUMM':        // copy prev difDayEnd to new difDayStart
              let dayStartDin = Number(prevMaqDataSummList[i].maqCoin6EndDrawerDinPartCntr);
              this.maqRecForm.get('maqDayStartDin').patchValue(dayStartDin.toFixed(2));
              break;
        case 'CNT-MEC':     // copy prev counter End to new counter Start
              let cntMStartIn   = Number(prevMaqDataSummList[i].maqCoin4EndInputDinEnd);
              let cntMStartOut  = Number(prevMaqDataSummList[i].maqCoin5EndOutputDinMechCntr);
              let cntMStartDraw = Number(prevMaqDataSummList[i].maqCoin6EndDrawerDinPartCntr);
              this.maqRecForm.get('maqMechCounterInStart').patchValue(cntMStartIn.toFixed(0));
              this.maqRecForm.get('maqMechCounterOutStart').patchValue(cntMStartOut.toFixed(0));
              this.maqRecForm.get('maqMechCounterDrawStart').patchValue(cntMStartDraw.toFixed(0));      
              break;
        case 'CNT-ELE':     // copy prev counter End to new counter Start
              let cntEStartIn   = Number(prevMaqDataSummList[i].maqCoin4EndInputDinEnd);
              let cntEStartOut  = Number(prevMaqDataSummList[i].maqCoin5EndOutputDinMechCntr);
              let cntEStartDraw = Number(prevMaqDataSummList[i].maqCoin6EndDrawerDinPartCntr);
              this.maqRecForm.get('maqEletrCounterInStart').patchValue(cntEStartIn.toFixed(0));
              this.maqRecForm.get('maqEletrCounterOutStart').patchValue(cntEStartOut.toFixed(0));
              this.maqRecForm.get('maqEletrCounterDrawStart').patchValue(cntEStartDraw.toFixed(0)); 
              break;        
        case 'CNT-PAR':     // copy prev counter End to new counter Start
              let cntPStartIn   = Number(prevMaqDataSummList[i].maqCoin4EndInputDinEnd);
              let cntPStartOut  = Number(prevMaqDataSummList[i].maqCoin5EndOutputDinMechCntr);
              let cntPStartDraw = Number(prevMaqDataSummList[i].maqCoin6EndDrawerDinPartCntr);
              this.maqRecForm.get('maqPartCounterInStart').patchValue(cntPStartIn.toFixed(0));
              this.maqRecForm.get('maqPartCounterOutStart').patchValue(cntPStartOut.toFixed(0));
              this.maqRecForm.get('maqPartCounterDrawStart').patchValue(cntPStartDraw.toFixed(0)); 
              break;        
        case 'COIN-ST':     //Nothing to do
              break;  
        case 'COIN-EN':     // copy prev coin din End to new coin din Start
              let coint1Start = Number(prevMaqDataSummList[i].maqCoin1StartInputDinAdded);
              let coint2Start = Number(prevMaqDataSummList[i].maqCoin2StartOutputDinDifDays);
              let coint3Start = Number(prevMaqDataSummList[i].maqCoin3StartDrawerDinDifCoins);
              let coint4Start = Number(prevMaqDataSummList[i].maqCoin4EndInputDinEnd);
              let coint5Start = Number(prevMaqDataSummList[i].maqCoin5EndOutputDinMechCntr);
              let coint6Start = Number(prevMaqDataSummList[i].maqCoin6EndDrawerDinPartCntr);
              this.maqRecForm.get('maqCoin1DinStart').patchValue(coint1Start.toFixed(0));
              this.maqRecForm.get('maqCoin2DinStart').patchValue(coint2Start.toFixed(0));
              this.maqRecForm.get('maqCoin3DinStart').patchValue(coint3Start.toFixed(0));
              this.maqRecForm.get('maqCoin4DinStart').patchValue(coint4Start.toFixed(0));
              this.maqRecForm.get('maqCoin5DinStart').patchValue(coint5Start.toFixed(0));
              this.maqRecForm.get('maqCoin6DinStart').patchValue(coint6Start.toFixed(0));
              break;        
        default:            //Nothing to do
              break;                  
      };
    }
  }
  cleanMaqFormData(){
   
  }
  /*---------------------------------------ARTS CREATE  -- GENERAL -----------------------------------------------------------*/
  public setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
  }  
  public setFourNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(4);
  } 
  public setSixNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(6);
  } 

  /* ---------------------------------------ARTS CREATE BTN CLICK-----------------------------------------------------------*/
  changeCoinsInputBtnClick(){
    if (this.coinsInputDinFlag === true) {
      this.coinsInputDinFlag = false;
      this.maqRecForm.get('maqCoin1Start').disable(); 
      this.maqRecForm.get('maqCoin2Start').disable(); 
      this.maqRecForm.get('maqCoin3Start').disable(); 
      this.maqRecForm.get('maqCoin4Start').disable(); 
      this.maqRecForm.get('maqCoin5Start').disable(); 
      this.maqRecForm.get('maqCoin6Start').disable(); 
      this.maqRecForm.get('maqCoin1End').disable(); 
      this.maqRecForm.get('maqCoin2End').disable(); 
      this.maqRecForm.get('maqCoin3End').disable(); 
      this.maqRecForm.get('maqCoin4End').disable(); 
      this.maqRecForm.get('maqCoin5End').disable(); 
      this.maqRecForm.get('maqCoin6End').disable(); 
      this.maqRecForm.get('maqCoin1DinStart').enable();
      this.maqRecForm.get('maqCoin2DinStart').enable();
      this.maqRecForm.get('maqCoin3DinStart').enable();
      this.maqRecForm.get('maqCoin4DinStart').enable();
      this.maqRecForm.get('maqCoin5DinStart').enable();
      this.maqRecForm.get('maqCoin6DinStart').enable();
      this.maqRecForm.get('maqCoin1DinEnd').enable();
      this.maqRecForm.get('maqCoin2DinEnd').enable();
      this.maqRecForm.get('maqCoin3DinEnd').enable();
      this.maqRecForm.get('maqCoin4DinEnd').enable();
      this.maqRecForm.get('maqCoin5DinEnd').enable();
      this.maqRecForm.get('maqCoin6DinEnd').enable();
      this.unsubsCoinsStart();
      this.unsubsCoinsEnd();
      this.subsCoinsDinStart();
      this.subsCoinsDinEnd();
    } else {
      this.coinsInputDinFlag = true;
      this.maqRecForm.get('maqCoin1Start').enable(); 
      this.maqRecForm.get('maqCoin2Start').enable(); 
      this.maqRecForm.get('maqCoin3Start').enable(); 
      this.maqRecForm.get('maqCoin4Start').enable(); 
      this.maqRecForm.get('maqCoin5Start').enable(); 
      this.maqRecForm.get('maqCoin6Start').enable(); 
      this.maqRecForm.get('maqCoin1End').enable(); 
      this.maqRecForm.get('maqCoin2End').enable(); 
      this.maqRecForm.get('maqCoin3End').enable(); 
      this.maqRecForm.get('maqCoin4End').enable(); 
      this.maqRecForm.get('maqCoin5End').enable(); 
      this.maqRecForm.get('maqCoin6End').enable(); 
      this.maqRecForm.get('maqCoin1DinStart').disable();
      this.maqRecForm.get('maqCoin2DinStart').disable();
      this.maqRecForm.get('maqCoin3DinStart').disable();
      this.maqRecForm.get('maqCoin4DinStart').disable();
      this.maqRecForm.get('maqCoin5DinStart').disable();
      this.maqRecForm.get('maqCoin6DinStart').disable();
      this.maqRecForm.get('maqCoin1DinEnd').disable();
      this.maqRecForm.get('maqCoin2DinEnd').disable();
      this.maqRecForm.get('maqCoin3DinEnd').disable();
      this.maqRecForm.get('maqCoin4DinEnd').disable();
      this.maqRecForm.get('maqCoin5DinEnd').disable();
      this.maqRecForm.get('maqCoin6DinEnd').disable();
      this.subsCoinsStart();
      this.subsCoinsEnd();
      this.unsubsCoinsDinStart();
      this.unsubsCoinsDinEnd();
    }
  }
  changeShowMaqsListBtnClick(){
    if (this.showMaqsList === true) this.showMaqsList = false;
    else this.showMaqsList = true;
  }
  ClosedMaqCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(this.maqSummList);
        this.result.next(this.maqSummList);
        this.bsModalRef.hide();
      }
  }
  cleanFormMaqBtnClick(){
    this.createMaqFlag = true;
    this.cleanMaqFormData();
  }
  createMaqBtnClick(maqRecForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->createMaqBtnClick->creating->', null);
    this.copyMaqFormDataToNewMaq(false);
    this.createOneMaq(this.maqSummList);
  }
  updateMaqBtnClick(maqRecForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->updateMaqBtnClick->updating->', null);
    this.copyMaqFormDataToNewMaq(true);   
    this.updateOneMaqSumm(this.maqSummList);
  }
  /* ---------------------------------------ARTS CREATE --GET ARTS ---------------------------------------------------------*/
  createOneMaq(newMaqSummList: Maq[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->createOneMaq->creating->', null);
    this._maqsService.createOneMaq(newMaqSummList)
      .subscribe({next:(data) => { this.maqSummList = data;
                           this.ClosedMaqCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getPrevMaqData(modMaq: Maq): void {
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->getPrevMaqData->getting->', null);
    this._maqsService.getPrevMaqData(modMaq)
      .subscribe({next:(data) => { this.prevMaqSummList = data;
                           this.copyPrevMaqDataToMaqForm(this.prevMaqSummList);                       
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getOneMaq(modMaq: Maq): void {
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->getOneMaq->getting->', null);
    this._maqsService.getOneMaq(modMaq)
      .subscribe({next:(data) => { this.maqSummList = data;
                           this.copyNewMaqDataToMaqForm();                       
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneMaqSumm(updMaqSummList: Maq[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->MAQS CREATE ->updateOneMaqSumm->updating->', null);
    this._maqsService.updateOneMaqSumm(updMaqSummList)
      .subscribe({next:(data) => { this.maqSummList = data;
                           this.ClosedMaqCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}