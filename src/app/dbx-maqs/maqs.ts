export class Maq {
    maqId                           :number;
    maqMaqBName                     :string;
    maqStartDate                    :Date;
    maqEndDate                      :Date;
    maqSumType                      :string; //(MAQB, MAQA, TABACO, etc.)
    maqSumDataType                  :string; //(MECH CNT, ELECTR CNT, PART CNT, COIN START, COIN END, SUMMARY)
    maqCoin1StartInputDinAdded      :number; //(Coin1 - StartInput  - DinAdded   )
    maqCoin2StartOutputDinDifDays   :number; //(Coin2 - StartOutput - DinDifDays )
    maqCoin3StartDrawerDinDifCoins  :number; //(Coin3 - StartDrawer - DinDifCoins)
    maqCoin4EndInputDinEnd          :number; //(Coin4 - EndInput    - DinEnd     )
    maqCoin5EndOutputDinMechCntr    :number; //(Coin5 - EndOutput   - DinMechCntr)
    maqCoin6EndDrawerDinPartCntr    :number; //(Coin6 - EndDrawer   - DinPartCntr)
    maqStatus                       :boolean;
}
export class MaqString {
    maqId                           :number;
    maqMaqBName                     :string;
    maqStartDate                    :string;
    maqEndDate                      :string;
    maqSumType                      :string; //(MAQB, MAQA, TABACO, etc.)
    maqSumDataType                  :string; //(MECH CNT, ELECTR CNT, PART CNT, COIN START, COIN END, SUMMARY)
    maqCoin1StartInputDinAdded      :number; //(Coin1 - StartInput  - DinAdded   )
    maqCoin2StartOutputDinDifDays   :number; //(Coin2 - StartOutput - DinDifDays )
    maqCoin3StartDrawerDinDifCoins  :number; //(Coin3 - StartDrawer - DinDifCoins)
    maqCoin4EndInputDinEnd          :number; //(Coin4 - EndInput    - DinEnd     )
    maqCoin5EndOutputDinMechCntr    :number; //(Coin5 - EndOutput   - DinMechCntr)
    maqCoin6EndDrawerDinPartCntr    :number; //(Coin6 - EndDrawer   - DinPartCntr)
    maqStatus                       :boolean;
}
