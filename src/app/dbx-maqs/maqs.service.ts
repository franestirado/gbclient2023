import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Maq } from './maqs';
import { Hd } from '../dbx-hd/hd';
import { ValueRecord } from '../dbc-fields/field';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type'    :'application/json',
      'Authorization'   :'my-auth-token'
    })
  };
@Injectable({  providedIn: 'root'})
export class MaqsService {
  constructor(private http: HttpClient) { }
    private json    :string;
    private params  :string;
    private headers :HttpHeaders;

  /*---------------------------------------- MONTHS ----------------------------------------------------------*/
  getMaqsLits(firstAndLastDayOfYear: Date[]): Observable<Maq[]> {
    return this.http.post<Maq[]>('/maqs/getAll',firstAndLastDayOfYear)
  }
  checkIfMaqsIsInDB(dateRangeMaqBNameVRList: ValueRecord[]): Observable<Maq[]> {
    return this.http.post<Maq[]>('/maqs/check',dateRangeMaqBNameVRList)
  }
  getHdsMaqs(dateRangeMaqBNameVRList: ValueRecord[]): Observable<Hd[]> {
    return this.http.post<Hd[]>('/maqs/hdsMaq',dateRangeMaqBNameVRList)
  }
  createOneMaq(newMaqSummList: Maq[]): Observable<Maq[]>  {
    return this.http.post<Maq[]>('/maqs/creatOne', newMaqSummList)
  }
  getPrevMaqData(newMaq: Maq): Observable<Maq[]>  {
    return this.http.post<Maq[]>('/maqs/getPrev', newMaq)
  }
  getOneMaq(updMaq: Maq): Observable<Maq[]>  {
    return this.http.post<Maq[]>('/maqs/getOne', updMaq)
  }
  updateOneMaq(updMaq: Maq): Observable<Maq[]>  {
    return this.http.put<Maq[]>('/maqs/updOne', updMaq)
  }
  updateOneMaqSumm(updMaqSummList: Maq[]): Observable<Maq[]>  {
    return this.http.post<Maq[]>('/maqs/updOneSumm', updMaqSummList)
  }
  deleteOneMaq(delMaq: Maq): Observable<any> {
    return this.http.post('/maqs/delOne', delMaq)
  }
  deleteAllMaqs(firstAndLastDayOfYear: Date[]): Observable<any> {
    return this.http.post('/maqs/delAll',firstAndLastDayOfYear)
  }

}