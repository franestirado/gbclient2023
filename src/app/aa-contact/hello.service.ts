
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class HelloService {

  constructor(private http: HttpClient) { }

  getHello(): Observable<any>  {
    return this.http.get('/api/hi')                   
                    .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Hello...Server Error UNKNOWN...');
  }

}
