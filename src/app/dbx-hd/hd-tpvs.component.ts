import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';

import { HdTpv } from './hd';
import { HdService } from './hd.service';

@Component({
  selector      :'app-hd-tpvs',
  templateUrl   :'./hd-tpvs.component.html',
  styleUrls     :['./hd.component.css'] 
})
export class HdTpvsComponent implements OnInit {
  public receivedHdTpvs   :HdTpv[];
  public workingDate      :Date;
  callback                :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'hdTpvTitle'    :'Datos Ventas TPVs',
    'hdDate'        :'',
    'hdTpvSales'    :'Ventas Caja',
  };
  public formLabels = {
    'hdTpvName'     :'Caja',    
    'hdTpvNumber'   :'Arqueo',
    'hdTpvTickets'  :'Nº Tickets',
    'hdTpvTicket'   :'Ticket',
    'hdTpvMoney'    :'Dinero',
  };
  public hdTpvsForm           :FormGroup;
  public hdTpvsFormArray      :FormArray;
  private sortedCollection    :any[];

  private logToConsole        :boolean;
  /*---------------------------------------HD-TPVS EDIT -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private fb:FormBuilder, private orderPipe: OrderPipe, 
                  private globalVar: GlobalVarService, private _hdService: HdService, ) { }
  /*---------------------------------------HD-TPVS EDIT-- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.hdDate = this.workingDate.toLocaleDateString();
    this.hdTpvsFormArray= new FormArray([]);   
    this.hdTpvsForm = new FormGroup({
      hdTpvsFormArray: this.hdTpvsFormArray
    });
    this.prepareDateHdTpvsFormArray();    
  }
  /*---------------------------------------HD TPVS EDIT -- FORMS -----------------------------------------------------------*/
  addTpvFormGroup(id:number,date:Date,name:String,type:String,order:number,number:number,tickets:number,ticket:number,money:number,disable:boolean): FormGroup {
    return this.fb.group({  
      tpvId:      [ {value:id,       disabled:true} ],
      tpvDate:    [ {value:date,     disabled:true} ],
      tpvName:    [ {value:name,     disabled:true},],
      tpvType:    [ {value:type,     disabled:true} ],
      tpvOrder:   [ {value:order,    disabled:true} ],
      tpvNumber:  [ {value:((number===0)?'' :number), disabled:disable},[Validators.required, ValidationService.numberValidator] ],
	    tpvTickets: [ {value:((tickets===0)?'':tickets),disabled:disable},[Validators.required, ValidationService.numberValidator] ],
      tpvTicket:  [ {value:((ticket===0)?'' :ticket.toFixed(2)),disabled:disable},[Validators.required, ValidationService.decimalValidation] ],
      tpvMoney:   [ {value:((money===0)?''  :money.toFixed(2)), disabled:disable},[Validators.required, ValidationService.decimalValidation] ]
    });
  }
  addTpvTFormGroup(id:number,date:Date,name:String,type:String,order:number,number:number,tickets:number,ticket:number,money:number,disable:boolean): FormGroup {
    return this.fb.group({  
      tpvId:      [ {value: id,       disabled:true} ],
      tpvDate:    [ {value: date,     disabled:true} ],
      tpvName:    [ {value: name,     disabled:true},],
      tpvType:    [ {value: type,     disabled:true} ],
      tpvOrder:   [ {value: order,    disabled:true} ],
      tpvNumber:  [ {value: number,   disabled:disable},[Validators.required, ValidationService.numberValidator] ],
	    tpvTickets: [ {value: tickets,  disabled:disable},[Validators.required, ValidationService.numberValidator] ],
      tpvTicket:  [ {value: ticket.toFixed(2),   disabled:disable},[Validators.required, ValidationService.decimalValidation] ],
      tpvMoney:   [ {value: money.toFixed(2),    disabled:disable},[Validators.required, ValidationService.decimalValidation] ]
    });
  }
  /** value: questionsList[key].answer.length>0?  questionsList[key].answer[0].answerValue : null ,**/

  prepareDateHdTpvsFormArray(){ 
    this.globalVar.consoleLog(this.logToConsole, 'HD TPVS->prepareDateHdTpvsFormArray->Starting->', null);
    this.formTitles.hdDate = this.workingDate.toLocaleDateString();
    this.sortedCollection = this.orderPipe.transform(this.receivedHdTpvs, 'hdTpvOrder');
    this.receivedHdTpvs = this.sortedCollection;                          
    this.hdTpvsFormArray= new FormArray([]);
    this.hdTpvsForm = new FormGroup({
      hdTpvsFormArray: this.hdTpvsFormArray
    });

    for (let i = 0 ; i < this.receivedHdTpvs.length ; i++ ) {      
      if (  (this.receivedHdTpvs[i].hdTpvType === "TPV1T") || (this.receivedHdTpvs[i].hdTpvType === "TPV2T") || 
            (this.receivedHdTpvs[i].hdTpvType === "TPV3T") || (this.receivedHdTpvs[i].hdTpvType === "TPVT") ) {              
        this.hdTpvsFormArray.push(this.addTpvTFormGroup (this.receivedHdTpvs[i].hdTpvId,     this.receivedHdTpvs[i].hdTpvDate,
                                                        this.receivedHdTpvs[i].hdTpvName,   this.receivedHdTpvs[i].hdTpvType,
                                                        this.receivedHdTpvs[i].hdTpvOrder,  this.receivedHdTpvs[i].hdTpvNumber, 
                                                        this.receivedHdTpvs[i].hdTpvTickets,this.receivedHdTpvs[i].hdTpvTicket, 
                                                        this.receivedHdTpvs[i].hdTpvMoney,  true)); 
      } else {
        this.hdTpvsFormArray.push(this.addTpvFormGroup (this.receivedHdTpvs[i].hdTpvId,     this.receivedHdTpvs[i].hdTpvDate,
                                                        this.receivedHdTpvs[i].hdTpvName,   this.receivedHdTpvs[i].hdTpvType,
                                                        this.receivedHdTpvs[i].hdTpvOrder,  this.receivedHdTpvs[i].hdTpvNumber, 
                                                        this.receivedHdTpvs[i].hdTpvTickets,this.receivedHdTpvs[i].hdTpvTicket, 
                                                        this.receivedHdTpvs[i].hdTpvMoney,  false));  
      }
    }    
    
    this.hdTpvsFormArray.controls.forEach(
      control => {
          if ( (control.get('tpvType').value === 'TPV1') || (control.get('tpvType').value === 'TPV2') || (control.get('tpvType').value === 'TPV3') ) {
              control.get('tpvTickets').valueChanges.subscribe( () => { this.setHdTpvsTotals(); } )
              control.get('tpvTicket').valueChanges.subscribe ( () => { this.setHdTpvsTotals(); } )
              control.get('tpvMoney').valueChanges.subscribe  ( () => { this.setHdTpvsTotals(); } )
          } 
      }
    )    
  }
  setHdTpvsTotals(){
    this.globalVar.consoleLog(this.logToConsole, '->HD TPVS EDIT->setHdTpvsTotals->->', 'starting');  
    let tpvType = '';
    let tpvTotal1R = 0;  let tpvTotal2R = 0; let tpvTotal3R = 0; let tpvTotalTR = 0;
    let tpvTotal1K = 0;  let tpvTotal2K = 0; let tpvTotal3K = 0; let tpvTotalTK = 0;
    let tpvTotal1D = 0;  let tpvTotal2D = 0; let tpvTotal3D = 0; let tpvTotalTD = 0;
    this.hdTpvsFormArray = <FormArray> this.hdTpvsForm.get('hdTpvsFormArray');
    for (let i = 0 ; i < this.hdTpvsFormArray.length ; i++ ) {        
      tpvType = this.hdTpvsFormArray.at(i).get('tpvType').value;
      switch (tpvType) {
        case 'TPV1':{
          tpvTotal1R = tpvTotal1R + Number(this.hdTpvsFormArray.at(i).get('tpvTickets').value);
          tpvTotal1K = tpvTotal1K + Number(this.hdTpvsFormArray.at(i).get('tpvTicket').value);
          tpvTotal1D = tpvTotal1D + Number(this.hdTpvsFormArray.at(i).get('tpvMoney').value);
          break;
        }
        case 'TPV2':{
          tpvTotal2R = tpvTotal2R + Number(this.hdTpvsFormArray.at(i).get('tpvTickets').value);
          tpvTotal2K = tpvTotal2K + Number(this.hdTpvsFormArray.at(i).get('tpvTicket').value);
          tpvTotal2D = tpvTotal2D + Number(this.hdTpvsFormArray.at(i).get('tpvMoney').value);
          break;
        }
        case 'TPV3': {
          tpvTotal3R = tpvTotal3R + Number(this.hdTpvsFormArray.at(i).get('tpvTickets').value);
          tpvTotal3K = tpvTotal3K + Number(this.hdTpvsFormArray.at(i).get('tpvTicket').value);
          tpvTotal3D = tpvTotal3D + Number(this.hdTpvsFormArray.at(i).get('tpvMoney').value);
          break;
        } 
        default: { 
          break;
        }          
      }   
    }
    tpvTotalTR = tpvTotal1R + tpvTotal2R + tpvTotal3R;
    tpvTotalTK = tpvTotal1K + tpvTotal2K + tpvTotal3K;
    tpvTotalTD =  tpvTotal1D + tpvTotal2D + tpvTotal3D;
    for (let i = 0 ; i < this.hdTpvsFormArray.length ; i++ ) {        
      tpvType = this.hdTpvsFormArray.at(i).get('tpvType').value;
      switch (tpvType) {
        case 'TPV1T':{
          this.hdTpvsFormArray.at(i).get('tpvTickets').setValue(tpvTotal1R);
          this.hdTpvsFormArray.at(i).get('tpvTicket').setValue(tpvTotal1K.toFixed(2));
          this.hdTpvsFormArray.at(i).get('tpvMoney').setValue(tpvTotal1D.toFixed(2));
          break;
        }
        case 'TPV2T':{
          this.hdTpvsFormArray.at(i).get('tpvTickets').setValue(tpvTotal2R);
          this.hdTpvsFormArray.at(i).get('tpvTicket').setValue(tpvTotal2K.toFixed(2));
          this.hdTpvsFormArray.at(i).get('tpvMoney').setValue(tpvTotal2D.toFixed(2));
          break;
        }
        case 'TPV3T': {
          this.hdTpvsFormArray.at(i).get('tpvTickets').setValue(tpvTotal3R);
          this.hdTpvsFormArray.at(i).get('tpvTicket').setValue(tpvTotal3K.toFixed(2));
          this.hdTpvsFormArray.at(i).get('tpvMoney').setValue(tpvTotal3D.toFixed(2));
          break;
        } 
        case 'TPVT': {
          this.hdTpvsFormArray.at(i).get('tpvTickets').setValue(tpvTotalTR);
          this.hdTpvsFormArray.at(i).get('tpvTicket').setValue(tpvTotalTK.toFixed(2));
          this.hdTpvsFormArray.at(i).get('tpvMoney').setValue(tpvTotalTD.toFixed(2));
          break;
        } 
        default: { 
          break;
        }          
      }   
    }    
  }
  /*---------------------------------------HD TPVS EDIT -- GENERAL -----------------------------------------------------------*/
  public setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
  }  
  closeHdTpvsModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();
      }
  }
  cleanHdTvpsFormBtnClick(hdTpvsForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole, '->HD TPVS EDIT->cleanHdTvpsFormBtnClick->->', 'starting');  
    for (let i = 0 ; i < this.hdTpvsFormArray.length ; i++ ) {  
      this.hdTpvsFormArray.at(i).get('tpvNumber').setValue(0);            
      this.hdTpvsFormArray.at(i).get('tpvTickets').setValue(0);
      this.hdTpvsFormArray.at(i).get('tpvTicket').setValue(0,0);
      this.hdTpvsFormArray.at(i).get('tpvMoney').setValue(0.0);            
    }    
  }
  /*---------------------------------------HD TPVS EDIT -- BTNCLICK-----------------------------------------------------------*/
  saveHdTpvBtnClick(hdTpvsForm: FormGroup){  
    this.globalVar.consoleLog(this.logToConsole, '->HD TPVS EDIT->saveHdTpvBtnClick->->', 'starting');      
    for (let i = 0 ; i < this.receivedHdTpvs.length ; i++ ) {        
      this.receivedHdTpvs[i].hdTpvNumber  = this.hdTpvsFormArray.at(i).get('tpvNumber').value;
      this.receivedHdTpvs[i].hdTpvTickets = this.hdTpvsFormArray.at(i).get('tpvTickets').value;
      this.receivedHdTpvs[i].hdTpvTicket  = this.hdTpvsFormArray.at(i).get('tpvTicket').value;
      this.receivedHdTpvs[i].hdTpvMoney   = this.hdTpvsFormArray.at(i).get('tpvMoney').value;  
    }
    this.updateDateHdTpvs(this.workingDate,this.receivedHdTpvs);
  }
  /*---------------------------------------HD TPVS EDIT -- DATABASE-----------------------------------------------------------*/
  updateDateHdTpvs(workingDate: Date, dateHdTpvs: HdTpv[]): void {
    this.globalVar.consoleLog(this.logToConsole, '->HD TPVS EDIT->updateDateHdTpvs->updating->', null);
    this._hdService.updateDateHdTpvs(dateHdTpvs)
      .subscribe({next:(data) => { this.closeHdTpvsModalBtnClick(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}