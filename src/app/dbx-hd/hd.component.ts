import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { EnterDateComponent } from '../aa-common/enter-date.component';
import { EnterMonthComponent } from '../aa-common/enter-month.component';
import { ErrorService } from '../aa-errors/error.service';

import { Hd,HdString,HdType,HdTypeString,HdTpv } from './hd'; 
import { HdTypeCreateComponent } from './hdType-create.component';
import { HdTpvsComponent } from './hd-tpvs.component';
import { HdSumsComponent } from './hd-sums.component';
import { HdService } from './hd.service';

import { Fact } from '../dbx-facts/facts';
import { FactsHdComponent } from '../dbx-facts/facts-hd.component';
import { FactsService } from '../dbx-facts/facts.service';

import { CardPayout, CardTpv } from '../dbx-cards/cards';
import { CardsHdComponent } from '../dbx-cards/cards-hd.component';
import { CardsService } from '../dbx-cards/cards.service';

@Component({
  selector      :'app-hd',
  templateUrl   :'./hd.component.html',
  styleUrls     :['./hd.component.css']
})
export class HdComponent implements OnInit {
  public  formTitles = { 
    'hdDate'            :'',
    'hds'               :'Hojas del Día',
    'hdDeleteOne'       :'¿ Seguro que quiere BORRAR la hoja de este DÍA ?',
    'hdDeleteAll'       :'¿ Seguro que quiere BORRAR TODAS las hojas de este MES ?',
    'hdListYearString'  :'',
    'hdListMonth'       :'Mes ',
    'hdListMonthString' :'',
    'hdListStart'       :'',
    'hdListEnd'         :'',
    'hdEdit'            :'Modificar',
    'hdList'            :'Lista Hojas del Día',
    'hdEditList'        :'Editar',
    'hdDelete'          :'Borrar',
    'hdType'            :'Tipos Registros Hojas del Día',
    'hdTypeDeleteOne'   :'¿ Seguro que quiere BORRAR este Tipo de Registro Hojas del Día ?',
    'hdTypeDeleteAll'   :'¿ Seguro que quiere BORRAR TODOS los Tipos de Registro Hojas del Día ?',
    'hdTypesListString' :'',
    'hdTypesList'       :'Lista Tipos Registros Hojas del Día',
    'hdTypeEditList'    :'Editar',
    'hdTypeDelete'      :'Borrar',
  };
  public  formLabels = { 
    'id'                :'#####',
    'hdTpvName'         :'Caja',
    'hdTpvTickets'      :'Tickets',
    'hdTpvTicket'       :'Ticket',
    'hdTpvDiff'         :'Dif.Din-Ticket',
    'hdTpvMoney'        :'Dinero',
    'hdConcepts'        :'Concepto',
    'hdSigns'           :'+/-',    
    'hdAmount'          :'Cantidad',
    'factsProv'         :'Proveedor',
    'factSigns'         :'+/-',
    'factAmount'        :'Total',
    'cardsTpv'          :'TPV',
    'cardsSigns'        :'+/-',
    'cardsAmount'       :'Total',
    'hdId'              :'Id',
    'hdDate'            :'Fecha',
    'hdName'            :'Nombre',
    'hdType'            :'Tipo',
    'hdOrder'           :'Orden',
    'hdNotes'           :'Notas',    
    'hdStatus'          :'Estado',
    'hdTypeId'          :'Id Tipo',
    'hdTypeDate'        :'Fecha',
    'hdTypeName'        :'Nombre Tipo',
    'hdTypeType'        :'Tipo Registro',
    'hdTypePlace'       :'Lugar en Hoja Día',
    'hdTypeOrder'       :'Posición en Hoja Día',
    'hdTypeStatus'      :'Estado',
  };
// 
  private selectHdTypesRecordList     :ValueRecord [] = new Array();
  private hdsFirstAndLastDayOfMonth   :Date[];
  public  showCreateHd                :boolean;
  public  showEditHd                  :boolean;  
  public  showHd                      :boolean;
  public  showHdList                  :boolean;
  private createHds                   :Hd[] = new Array();  
  private receivedHds                 :Hd[];    
  public  listHds                     :Hd[] = new Array();;
  public  wSumHds                     :Hd[]; 
  public  wResHds                     :Hd[]; 
  public  wDStartHds                  :Hd[]; 
  public  wDEndHds                    :Hd[]; 
  public  wDContHds                   :Hd[]; 
  public  wDDifHds                    :Hd[]; 
  public  wDinTHds                    :Hd[]; 
  public  wDinDHds                    :Hd[];  // hd status 
  public  wDinFHds                    :Hd[];  
  public  wFacts                      :Hd[];
  public  wCardPayouts                :Hd[];
  public  wCardTPVs                   :Hd[];
  public  hdFacts                     :Fact[];
  private cardTPVs                    :CardTpv[];
  private cardPayouts                 :CardPayout[];
  public  searchHd                    :HdString;
  public  showSearchHds               :boolean;
  public  orderHdsList                :string;
  public  reverseHdsList              :boolean;

  private createHdTpvs                :HdTpv[] = new Array();
  private receivedHdTpvs              :HdTpv[];
  private previousHdTpvs              :HdTpv[];
  public  wHdTpv1                     :HdTpv[];
  public  wHdTpv1T                    :HdTpv[];
  public  wHdTpv2                     :HdTpv[];
  public  wHdTpv2T                    :HdTpv[];
  public  wHdTpv3                     :HdTpv[];
  public  wHdTpv3T                    :HdTpv[];
  public  wHdTpvTT                    :HdTpv[];
  public  hdsTotalString              :string;

  private hdIndexDStart               :number;
  private hdIndexDEnd                 :number;
  private hdIndexDCont                :number;
  private hdIndexDDif                 :number;
  private hdIndexDayT                 :number;
  private hdIndexDayD                 :number;
  private hdIndexDayF                 :number;
  private hdIndexFacts                :number;
  private hdIndexCards                :number;
  private totalFacts                  :number;
  private totalCards                  :number;

  private index                       :number;
  private numberOfTPV1                :number;
  private numberOfTPV2                :number;
  private numberOfTPV3                :number;

  public  showHdTypesList             :boolean;
  public  hdTypes                     :HdType[];
  private sumHdTypes                  :HdType[]= new Array();
  public  searchHdType                :HdTypeString;
  public  showSearchHdTypes           :boolean;
  public  orderHdTypesList            :string;
  public  reverseHdTypesList          :boolean;
  private orderFactsList              :string;
  private reverseFactsList            :boolean;
  private orderCardPayoutsList        :string;
  private reverseCardPayoutsList      :boolean;
  
  private orderCardTpvsList           :string;
  private reverseCardTpvsList         :boolean;

  public  caseInsensitive             :boolean;
  public  sortedCollection            :any[];
  private logToConsole                :boolean;
  //--------------------------------------- HDs -- CONSTRUCTOR--------------------------------------------------------------
  constructor(  private orderPipe: OrderPipe, private modalService: BsModalService, public globalVar: GlobalVarService,  
                private _errorService: ErrorService, private _hdService: HdService, private _factsService: FactsService,
                private _cardsService: CardsService ) { 
                //, private _gbViewsService: GbViewsService
    //   this.getGbViews('gbhdTypes');
  }
  //--------------------------------------- HDs -- NG ON INIT-----------------------------------------------------------...
  ngOnInit() {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;   
    this.searchHdType = new HdTypeString();
    this.searchHd = new HdString();
    this.hdsTotalString = '';

    this.resetShowHdFlags();
    this.caseInsensitive= true;
    this.hdFacts = new Array();
    this.cardPayouts = new Array();
    this.hdsFirstAndLastDayOfMonth = this.globalVar.workingFirstAndLastDayOfMonth;
    this.getAllHdTypes(true);
  }
  //--------------------------------------- HDs -- FORMS --------------------------------------------------------------  
  //--------------------------------------- HDs -- GENERAL --------------------------------------------------------------
  private resetShowHdFlags(){
    this.showHdList       = false;
    this.showCreateHd     = false;
    this.showHd           = false; 
    this.showEditHd       = false;
  }
  private returnPreviousDateNumber(typeToFind:string) : number {
    for (let i = 0 ; i < this.previousHdTpvs.length ; i++ ) {
      if (this.previousHdTpvs[i].hdTpvName === typeToFind) {
        return (this.previousHdTpvs[i].hdTpvNumber);        
      }
    }
    return 0;
  }
  private returnPreviousDateAmount(typeToFind:string) : number {
    for (let i = 0 ; i < this.previousHdTpvs.length ; i++ ) {
      if (this.previousHdTpvs[i].hdTpvName === typeToFind) {
        return (this.previousHdTpvs[i].hdTpvMoney);        
      }
    }
    return 0;
  }
  private calculateListHdsTotal(){
    let hdsTotal = 0;
    for (let i = 0 ; i < this.listHds.length ; i++ ) {
      hdsTotal = hdsTotal + this.listHds[i].hdAmount;
    }
    let hdDia = hdsTotal / this.listHds.length ;
    this.hdsTotalString = this.listHds.length + '..Hds..Total->' + hdsTotal.toFixed(2) + '..Dia->' + hdDia.toFixed(2);
  }
  private createHd(hdDate: Date){  
    this.globalVar.changeWorkingDates(hdDate); 
    this.hdFacts = new Array();  
    this.cardPayouts = new Array();    
    this.index = 0;
    this.numberOfTPV1 = 0;
    this.numberOfTPV2 = 0;
    this.numberOfTPV3 = 0;
    this.createHdTpvs = new Array();
    for (let i = 0 ; i < this.hdTypes.length ; i++ ) {
      if (this.hdTypes[i].hdTypeStatus === true) {
        switch (this.hdTypes[i].hdTypeType) {
          case 'TPV1': {
            this.createHdTpvs[this.index] = new HdTpv (hdDate,this.hdTypes[i].hdTypeName,this.hdTypes[i].hdTypeType,
                    this.hdTypes[i].hdTypeOrder,this.returnPreviousDateNumber(this.hdTypes[i].hdTypeName)+1,0,0.0,0.0);
            this.index = this.index + 1;
            this.numberOfTPV1 = this.numberOfTPV1 + 1;                                    
            break;
          }
          case 'TPV2': {
            this.createHdTpvs[this.index] = new HdTpv (hdDate,this.hdTypes[i].hdTypeName,this.hdTypes[i].hdTypeType,
              this.hdTypes[i].hdTypeOrder,this.returnPreviousDateNumber(this.hdTypes[i].hdTypeName)+1,0,0.0,0.0);
            this.index = this.index + 1;
            this.numberOfTPV2 = this.numberOfTPV2 + 1;                                    
            break;
          }
          case 'TPV3': {
            this.createHdTpvs[this.index] = new HdTpv (hdDate,this.hdTypes[i].hdTypeName,this.hdTypes[i].hdTypeType,
              this.hdTypes[i].hdTypeOrder,this.returnPreviousDateNumber(this.hdTypes[i].hdTypeName)+1,0,0.0,0.0);
            this.index = this.index + 1;
            this.numberOfTPV3 = this.numberOfTPV3 + 1;                                    
            break;
          }
          default:
                  break;
        }        
      }
    }  
    if ( this.numberOfTPV1 > 1) {
      this.createHdTpvs[this.index] = new HdTpv (hdDate,'Total TPV1','TPV1T',19,0,0,0,0);        
      this.index = this.index + 1;
    }
    if ( this.numberOfTPV2 > 1) {
      this.createHdTpvs[this.index] = new HdTpv (hdDate, 'Total TPV2','TPV2T',29,0,0,0,0);
      this.index = this.index + 1;
    }
    if ( this.numberOfTPV3 > 1) {
      this.createHdTpvs[this.index] = new HdTpv (hdDate, 'Total TPV3','TPV3T',39,0,0,0,0);
      this.index = this.index + 1;
    }
    this.createHdTpvs[this.index] = new HdTpv (hdDate, 'Total TPVs','TPVT',49,0,0,0,0);
    this.index = this.index + 1; 
    this.createHds = new Array();
    this.createHds[0] = new Hd (hdDate,'D.Empezar'   ,  'DSTART' ,'FIX',10,this.returnPreviousDateAmount("DCONT"),true);
    this.createHds[1] = new Hd (hdDate,'D.Terminar'  ,  'DEND'   ,'FIX',90,0,true);   
    this.createHds[2] = new Hd (hdDate,'D.T.Contado' ,  'DCONT'  ,'FIX',91,0,true);   
    this.createHds[3] = new Hd (hdDate,'Dif.Dinero'  ,  'DDIF'   ,'FIX',92,0,true);     
    this.createHds[4] = new Hd (hdDate,'Venta.Ticket',  'DAY-T'  ,'FIX',97,0,true);
    this.createHds[5] = new Hd (hdDate,'Venta.Din'   ,  'DAY-D'  ,'FIX',98,0,true);
    this.createHds[6] = new Hd (hdDate,'Dif.Tickets' ,  'DAY-F'  ,'FIX',99,0,true);
    this.createHds[7] = new Hd (hdDate,'Total.Facturas','FACT'   ,'FIX',99,0,true);
    this.createHds[8] = new Hd (hdDate,'Total.Tarjetas','CARDS'  ,'FIX',99,0,true);
    this.showHd = true; 
    this.showCreateHd = false;
    this.showEditHd = true;
    this.createDateHds(hdDate, this.createHdTpvs, this.createHds);
    return;
  }
  private prepareDateHdTpvs(updateReceivedHdsFlag :boolean){ 
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> prepareDateHdTpvs ->...Starting...->',null);
    this.formTitles.hdDate = this.globalVar.workingDate.toLocaleDateString();
    this.sortedCollection = this.orderPipe.transform(this.receivedHdTpvs, 'hdTpvOrder');
    this.receivedHdTpvs = this.sortedCollection;                          
    this.wHdTpv1 = new Array();
    this.wHdTpv1T = new Array();
    this.wHdTpv2 = new Array();
    this.wHdTpv2T = new Array();
    this.wHdTpv3 = new Array();
    this.wHdTpv3T = new Array();
    this.wHdTpvTT = new Array();  
    let indexHdTpv1 = 0; let indexHdTpv1T = 0; let indexHdTpv2 = 0; let indexHdTpv2T = 0; 
    let indexHdTpv3 = 0; let indexHdTpv3T = 0; let indexHdTpvTT = 0; 
    for (let i = 0 ; i < this.receivedHdTpvs.length ; i++ ) {      
      switch (this.receivedHdTpvs[i].hdTpvType){
        case 'TPV1': {
          this.wHdTpv1[indexHdTpv1] = this.receivedHdTpvs[i];
          indexHdTpv1 = indexHdTpv1 + 1;          
          break;
        }
        case 'TPV1T': {
          this.wHdTpv1T[indexHdTpv1T] = this.receivedHdTpvs[i];
          indexHdTpv1T = indexHdTpv1T + 1;          
          break;
        }
        case 'TPV2': {
          this.wHdTpv2[indexHdTpv2] = this.receivedHdTpvs[i];
          indexHdTpv2 = indexHdTpv2 + 1;          
          break;
        }
        case 'TPV2T': {
          this.wHdTpv2T[indexHdTpv2T] = this.receivedHdTpvs[i];
          indexHdTpv2T = indexHdTpv2T + 1;          
          break;
        }
        case 'TPV3': {
          this.wHdTpv3[indexHdTpv3] = this.receivedHdTpvs[i];
          indexHdTpv3 = indexHdTpv3 + 1;          
          break;
        }
        case 'TPV3T': {
          this.wHdTpv3T[indexHdTpv3T] = this.receivedHdTpvs[i];
          indexHdTpv3T = indexHdTpv3T + 1;          
          break;
        }
        case 'TPVT': { 
          this.wHdTpvTT[indexHdTpvTT] = this.receivedHdTpvs[i];
          indexHdTpvTT = indexHdTpvTT + 1;          
          break;
        }
        default:
          break;
      }
    }    
    this.prepareDateHds(updateReceivedHdsFlag);
    return;
  }
  private prepareDateHds(updateReceivedHdsFlag :boolean){ 
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> prepareDateHds ->...Starting...->',null);
    this.orderHdsList = 'hdOrder';
    this.reverseHdsList = false;
    this.sortedCollection = this.orderPipe.transform(this.receivedHds, 'hdOrder');
    this.receivedHds = this.sortedCollection;                          
    this.showSearchHds = false;
    this.hdIndexDStart = 0;
    this.hdIndexDEnd = 0;
    this.hdIndexDCont = 0;
    this.hdIndexDDif = 0;
    this.hdIndexDayT = 0;
    this.hdIndexDayD = 0;
    this.hdIndexDayF = 0;
    this.hdIndexFacts = 0;
    this.hdIndexCards = 0;
    let indexSumHds = 0; let indexResHds = 0;
    let totalSumHds = 0; let totalResHds = 0;
    this.wSumHds    = new Array();
    this.wResHds    = new Array();
    this.wDStartHds = new Array();
    this.wDEndHds   = new Array();
    this.wDContHds  = new Array();
    this.wDDifHds   = new Array();
    this.wDinTHds   = new Array();
    this.wDinDHds   = new Array();
    this.wDinFHds   = new Array();
    this.wFacts     = new Array();
    this.wCardTPVs  = new Array();
    this.wCardPayouts  = new Array();   
    for (let i = 0 ; i < this.receivedHds.length ; i++ ) {
      switch (this.receivedHds[i].hdPlace){        
        case 'SUM': {
          this.wSumHds[indexSumHds] = this.receivedHds[i];
          indexSumHds = indexSumHds + 1;
          totalSumHds = totalSumHds + this.receivedHds[i].hdAmount;
          break;
        }
        case 'RES': {
          this.wResHds[indexResHds] = this.receivedHds[i];
          indexResHds = indexResHds + 1;
          totalResHds = totalResHds + this.receivedHds[i].hdAmount;
          break;          
        }
        case 'FIX': {
          switch (this.receivedHds[i].hdType){
            case 'DSTART':  { this.hdIndexDStart  = i; this.wDStartHds[0] = this.receivedHds[i]; break; }
            case 'DEND':    { this.hdIndexDEnd    = i; this.wDEndHds[0]   = this.receivedHds[i]; break; }
            case 'DCONT':   { this.hdIndexDCont   = i; this.wDContHds[0]  = this.receivedHds[i]; break; }
            case 'DDIF':    { this.hdIndexDDif    = i; this.wDDifHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-T':   { this.hdIndexDayT    = i; this.wDinTHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-D':   { this.hdIndexDayD    = i; this.wDinDHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-F':   { this.hdIndexDayF    = i; this.wDinFHds[0]   = this.receivedHds[i]; break; }
            case 'FACT':    { this.totalFacts = 0;
                              if (this.hdFacts != null) { 
                                for (let j=0; j<this.hdFacts.length; j++) { 
                                  this.totalFacts = this.totalFacts + this.hdFacts[j].factTotalCost 
                                }
                              }
                              this.wFacts[0] = this.receivedHds[i];
                              this.hdIndexFacts = i;
                              this.wFacts[0].hdAmount = this.totalFacts;
                              break;
                            } 
            case 'CARDS':   { this.totalCards = 0;
                              if (this.cardPayouts != null) {
                                for (let j=0; j<this.cardPayouts.length; j++) { 
                                  this.totalCards = this.totalCards + (this.cardPayouts[j].cardPayoutMulti * this.cardPayouts[j].cardPayoutTotal) 
                                }
                              }
                              this.wCardPayouts[0] = this.receivedHds[i];
                              this.hdIndexCards = i;
                              this.wCardPayouts[0].hdAmount = this.totalCards;
                              if (this.cardTPVs != null) {
                                var indexCardTpvsNoZero = 0;
                                for (let h=0; h<this.cardTPVs.length; h++) { 
                                  let totalTPV = 0;
                                  if (this.cardPayouts != null) {
                                    for (let k=0; k<this.cardPayouts.length; k++) {
                                      if (this.cardPayouts[k].cardPayoutTpvId === this.cardTPVs[h].cardTpvId) {
                                        totalTPV = totalTPV + (this.cardPayouts[k].cardPayoutMulti * this.cardPayouts[k].cardPayoutTotal)
                                      }                                  
                                    }
                                    if (totalTPV != 0) {
                                      this.wCardTPVs[indexCardTpvsNoZero] = new Hd(this.wCardPayouts[0].hdDate,this.cardTPVs[h].cardTpvName,"","",0,0,true);
                                      this.wCardTPVs[indexCardTpvsNoZero].hdAmount = totalTPV; 
                                      indexCardTpvsNoZero= indexCardTpvsNoZero + 1; 
                                    }  
                                  }                            
                                }
                              }
                              break;
                            }  
            default:break;
            }            
        }              
        default:
          break;
      }      
    }
    //--- Din fin HD = Din empezar HD + total sumas HD - total restas HD - total tarjetas HD ---
    this.receivedHds[this.hdIndexDEnd].hdAmount = this.receivedHds[this.hdIndexDStart].hdAmount + 
                                                    totalSumHds - totalResHds - this.totalCards;
    this.wDEndHds[0] = this.receivedHds[this.hdIndexDEnd];   
    //--- Din HD = Din contado + total facts HD - Din fin HD ---
    this.receivedHds[this.hdIndexDayD].hdAmount = this.receivedHds[this.hdIndexDCont].hdAmount + this.totalFacts -
                                                  this.receivedHds[this.hdIndexDEnd].hdAmount;
    this.wDinDHds[0] = this.receivedHds[this.hdIndexDayD];
    
    //--- Din segun tickets HD = Suma Din segun tickets HD TPVs ---    
    this.receivedHds[this.hdIndexDayT].hdAmount = this.wHdTpvTT[0].hdTpvTicket;
    this.wDinTHds[0] = this.receivedHds[this.hdIndexDayT];
    //--- Dif Din HD - venta segun tickets  ---
    this.receivedHds[this.hdIndexDayF].hdAmount = this.receivedHds[this.hdIndexDayD].hdAmount - 
                                                  this.receivedHds[this.hdIndexDayT].hdAmount ;
    this.wDDifHds[0] = this.receivedHds[this.hdIndexDayF];
    this.receivedHds[this.hdIndexDayD].hdNotes = this.receivedHds[this.hdIndexDayF].hdAmount.toFixed(2).toString();
    //--- Dif Din TPVs - tickets TPVs ---
    this.receivedHds[this.hdIndexDDif].hdAmount = this.wHdTpvTT[0].hdTpvTicket - this.wHdTpvTT[0].hdTpvMoney;

    if (updateReceivedHdsFlag===true){
      this.updateDateHds(this.globalVar.workingDate,this.receivedHds);
    }
  }
  private updateFactsTotal(){
    if (this.hdFacts === null) { 
    } else {      
      this.totalFacts = 0;
      for (let j=0; j<this.hdFacts.length; j++) { 
        this.totalFacts = this.totalFacts + this.hdFacts[j].factTotalCost;
      }
    }
    this.hdIndexFacts = this.findIndexReceivedHds(false,'FACT');
    this.wFacts[0] = this.receivedHds[this.hdIndexFacts];  
    this.wFacts[0].hdAmount = this.totalFacts;    
  }
  private findIndexReceivedHds(hdPlaceFlag:boolean, hdStringToFind:String):number{
    var indexToFind = -1;
    if (this.receivedHds.length < 1) return indexToFind;
    if (hdPlaceFlag === true) {                  // Look for string in receivedHds[i].hdPlace
      for (let i = 0 ; i < this.receivedHds.length ; i++ ) {
        if (this.receivedHds[i].hdPlace === hdStringToFind) return i;
      }
    } else {                                     // Look for string in receivedHds[i].hdType
      for (let i = 0 ; i < this.receivedHds.length ; i++ ) {
        if (this.receivedHds[i].hdType === hdStringToFind) return i;
      }
    } 
    return indexToFind;
  }
  //--------------------------------------- HDs -- BTN CLICK --------------------------------------------------------------
  changeShowHdListBtnClick(){
    if (this.showHdList === true) {
      this.showHdList = false;
    } else {
      this.resetShowHdTypesFlags();
      this.resetShowHdFlags();
      this.showHdList = true;
      this.getListHds(this.hdsFirstAndLastDayOfMonth);
    }
  }
  createBarHdTypesBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> createBarHdTypesBtnClick ->...Starting...->',null);
    this.showHdTypesList = false;                       
    var currentBar = this.globalVar.currentBar;
    this.createBarHdTypes(currentBar);
  }
  enterHdDateBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> enterHdDateBtnClick ->...Starting...->',null);
    let inputDate = new Date();
    let minDate = new Date();
    let maxDate = new Date();
    inputDate = this.globalVar.workingDate;
    minDate = this.globalVar.workingFirstDayOfMonth;
    maxDate = this.globalVar.workingLastDayOfMonth;
    const modalInitialState = {
      dateTitle           :"Crear HOJA DIA -> Fecha",
      dateMessage         :"Introduzca la fecha para la Hoja del Día",
      dateLabel           :"Fecha Hoja Día:",       
      dateFooter          :"Crear HOJA DIA -> Fecha",
      dateContainerClass  :'theme-dark-blue',
      inputDate           :inputDate,
      minDate             :minDate,      
      maxDate             :maxDate,
      dateInputFormat     :'DD-MMM-YYYY',
      callback            :'OK',   
    };
    this.globalVar.openModal(EnterDateComponent, modalInitialState, 'modalLgTop0Left0').
    then((inputDate:any)=>{
      if (inputDate != null) { 
        this.showHdList = false;
        this.readDataPreviousDate(inputDate);
      }    
    })
  }
  showEditHdTpvsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> showEditHdTpvsBtnClick ->...Starting...->',null);
    const modalInitialState = {
      receivedHdTpvs        :this.receivedHdTpvs,
      workingDate           :this.globalVar.workingDate,
      callback              :'OK',   
    };
    this.globalVar.openModal(HdTpvsComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.receivedHdTpvs = result;  
      this.getDateHdTpvs(this.globalVar.workingDate,false); 
    })
  }
  showEditHdSumBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> showEditHdSumBtnClick ->...Starting...->',null);
    const modalInitialState = {
      hdTypes       :this.hdTypes,
      receivedHds   :this.receivedHds,
      workingDate   :this.globalVar.workingDate,
      wDStartHds    :this.wDStartHds,
      wSumHds       :this.wSumHds,
      wResHds       :this.wResHds,
      wCardPayouts  :this.wCardPayouts,
      wDEndHds      :this.wDEndHds,
      wDContHds     :this.wDContHds,
      wFacts        :this.wFacts,
      wDinDHds      :this.wDinDHds,
      wDDifHds      :this.wDDifHds,
      wDinTHds      :this.wDinTHds,
      wDinFHds      :this.wDinFHds,
      wHdTpvTT      :this.wHdTpvTT,
      totalFacts    :this.totalFacts,
      totalCards    :this.totalCards,
      callback      :'OK',   
    };
    this.globalVar.openModal(HdSumsComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.receivedHds = result;  
      this.getDateHds(this.globalVar.workingDate,false,false);  
    })
  }
  showEditHdVisasBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> showEditHdVisasBtnClick ->...Starting...->',null);
    const modalInitialState = {
      workingDate         :this.globalVar.workingDate,
      receivedHdsCards    :this.receivedHds[this.hdIndexCards],
      callback            :'OK',   
    };
    this.globalVar.openModal(CardsHdComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllCardPayoutsHd(this.globalVar.workingDate,true);                     
    })
  }
  showEditHdFactsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> showEditHdFactsBtnClick ->...Starting...->',null);
    let arrayWorkingDate = new Array();
    arrayWorkingDate[0] = this.globalVar.workingDate;
    arrayWorkingDate[1] = this.globalVar.workingDate;
    const modalInitialState = {
      arrayWorkingDates   :arrayWorkingDate,
      hdFlag              :true,
      monthFlag           :false,
      receivedHdsFacts    :this.receivedHds[this.hdIndexFacts],
      callback            :'OK',   
    };
    this.globalVar.openModal(FactsHdComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllFactsHd(this.globalVar.workingDate,true);        
    })
  } 
  enterHdsMonthBtnClick(){ 
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> enterHdsMonthBtnClick ->...Starting...->',null);
    const modalInitialState = {
      dateTitle     :"HOJAS del DÍA",
      callback      :'OK',   
    };
    this.globalVar.openModal(EnterMonthComponent, modalInitialState, 'modalLgTop0Left0').
    then((selectedDate:any)=>{
      if (selectedDate != null) { 
        this.globalVar.changeWorkingDates(selectedDate); 
        this.hdsFirstAndLastDayOfMonth = this.globalVar.workingFirstAndLastDayOfMonth;
        this.resetShowHdTypesFlags();
        this.resetShowHdFlags();
        this.showHdList = true;              
        this.getListHds(this.hdsFirstAndLastDayOfMonth);
      }
    })
  }
  private isHdNotBlocked(selecteHd: Hd):boolean{
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> isHdNotBlocked ->...Starting...->',null);
    if (selecteHd.hdStatus === false){
      this._errorService.showErrorModal( "MODIFICAR HOJAS DEL DÍA", "MODIFICAR HOJAS DEL DÍA", "Hoja del Día BLOQUEADA"+"->", 
                      selecteHd.hdDate+"->"+selecteHd.hdName+"->"+selecteHd.hdType+"->"+selecteHd.hdPlace, 
                      "", "", "", "", "" )
      return false;
    } else return true;
  }
  lockHdBtnClick(hd: Hd){
    this.globalVar.changeWorkingDates(hd.hdDate);
    if (hd.hdStatus === true) hd.hdStatus = false;
    else  hd.hdStatus = true;
    this.wDinDHds   = new Array();
    this.wDinDHds[0] = hd;
    this.updateDateHds(this.globalVar.workingDate,this.wDinDHds);
  }
  editHdBtnClick(selecteHd: Hd){
    if (this.isHdNotBlocked(selecteHd)){
      this.resetShowHdFlags();
      this.showHd = true;
      this.showEditHd = true;
      this.globalVar.changeWorkingDates(selecteHd.hdDate);      
      this.getDateHds(this.globalVar.workingDate, true, false);
    }
  }
  searchHdsBtnClick() {
    if (this.showSearchHds === true) {
      this.showSearchHds = false;
    } else {
      this.searchHd.hdDate = '';
      this.searchHd.hdNotes = '';
      this.showSearchHds = true;
    }
    return;
  }  
  setHdsOrderBtnClick(value: string) {
    if (this.orderHdsList === value) {
      this.reverseHdsList = !this.reverseHdsList;
    }
    this.orderHdsList = value;   
  }  
  //---------------------------------------HDs DELETE ----------------------------------------------------------------  
  confirmDeleteOneHdBtnClick(hd: Hd){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> confirmDeleteOneHdBtnClick ->...Starting...->',null);
    this.globalVar.changeWorkingDates(hd.hdDate);    
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.hds,this.formTitles.hds,
                            hd,this.formTitles.hdDeleteOne,this.globalVar.workingDate.toLocaleDateString(),
                            "","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.deleteDateHds(this.globalVar.workingDate); 
          }
        })
  } 
  confirmDeleteAllHdBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> confirmDeleteAllHdBtnClick ->...Starting...->',null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.hds,this.formTitles.hds,
                            null,this.formTitles.hdDeleteAll,this.globalVar.workingFirstDayOfMonth.toLocaleDateString(),
                            this.globalVar.workingLastDayOfMonth.toLocaleDateString(),"","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.deleteMonthHds(this.globalVar.workingFirstAndLastDayOfMonth);            
          }
        })
  } 
  //------------------------------------------ HDs DATABASE -------------------------------------------------------------
  private getListHds(firstAndLastDayOfMonth: Date[]) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getListHds ->...Starting...->',null);
    this._hdService.getListHds(firstAndLastDayOfMonth)
      .subscribe({next:(data) => {
                          this.listHds = data;
                          this.orderHdsList = 'hdDate';
                          this.reverseHdsList = true;
                          this.sortedCollection = this.orderPipe.transform(this.listHds, 'hdDate');
                          this.listHds = this.sortedCollection;                          
                          this.showSearchHds = false; 
                          this.showHdList = true;
                          this.formTitles.hdListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
                          let workingYear = this.globalVar.workingDate.getFullYear();
                          this.formTitles.hdListYearString = workingYear.toString();
                          this.formTitles.hdListStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
                          this.formTitles.hdListEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
                          this.calculateListHdsTotal();                                                      
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };  }});
  }
  private getDateHds(workingDate: Date, getHdTpvsFlag:boolean, updateReceivedHdsFlag:boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getDateHds ->...Starting...->',null);
    this._hdService.getDateHds(workingDate)
      .subscribe({next:(data) => {
                          this.receivedHds = new Array();
                          this.receivedHds = data;
                          this.orderHdsList = 'hdType';
                          this.reverseHdsList = false;
                          this.sortedCollection = this.orderPipe.transform(this.receivedHds, 'hdTypeName');
                          this.receivedHds = this.sortedCollection;                          
                          this.showSearchHds = false;
                          if (getHdTpvsFlag === true) { this.getDateHdTpvs(workingDate,updateReceivedHdsFlag);}
                          else { this.prepareDateHds(updateReceivedHdsFlag)}
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getDateHdTpvs(workingDate: Date, updateReceivedHdsFlag:boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getDateHdTpvs ->...Starting...->',null);
    this._hdService.getDateHdTpvs(workingDate)
      .subscribe({next:(data) => {
                          this.receivedHdTpvs = new Array();
                          this.receivedHdTpvs = data;
                          this.getAllFactsHd(workingDate,updateReceivedHdsFlag);
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private readDataPreviousDate(workingDate: Date) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> readDataPreviousDate ->...Starting...->',null);
    this._hdService.readDataPreviousDate(workingDate)
      .subscribe({next:(data) => {  this.previousHdTpvs = new Array();
                            this.previousHdTpvs = data;
                            this.createHd(workingDate);
                          },
                error:(error) => {  if (error.status === 500 ) {
                            this.previousHdTpvs = new Array();                            
                            this.createHd(workingDate);
                            } else {
                              if (this.globalVar.handleError(error)) { throwError(() => error) };
                            }                          
                          }});
  }
  //-----------------------------------------FACTS HD DATABSE --------------------------------------------------------------
  private getAllFactsHd(workingDate: Date, updateReceivedHdsFlag:boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getAllFactsHd ->...Starting...->',null);
    let factsTypeValueRecordList = this.globalVar.prepareStringDatesValueRecordList('HojaDia',workingDate,workingDate);
    this._factsService.getAllFactsTypeMonth(factsTypeValueRecordList)
      .subscribe({next:(data) => {
                          this.hdFacts = data;
                          this.orderFactsList = 'factProvName';
                          this.reverseFactsList = false;
                          this.sortedCollection = this.orderPipe.transform(this.hdFacts,this.orderFactsList,this.reverseFactsList,this.caseInsensitive);
                          this.hdFacts = this.sortedCollection;
                          this.getAllCardTpvs(workingDate,updateReceivedHdsFlag);
                        },
                error:(error) => {  if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //-----------------------------------------CARDS HD DATABSE --------------------------------------------------------------
  private getAllCardTpvs(workingDate: Date, updateReceivedHdsFlag:boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getAllCardTpvs ->...Starting...->',null);
    this._cardsService.getAllCardTpvs()
        .subscribe({next:(data) => {
                            this.cardTPVs = data;
                            this.orderCardTpvsList = 'cardTpvName';
                            this.reverseCardTpvsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.cardTPVs,this.orderCardTpvsList,this.reverseCardTpvsList,this.caseInsensitive);
                            this.cardTPVs = this.sortedCollection;
                            this.getAllCardPayoutsHd(workingDate,updateReceivedHdsFlag);
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getAllCardPayoutsHd(workingDate: Date, updateReceivedHdsFlag:boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getAllCardPayoutsHd ->...Starting...->',null);
    let cardPayoutVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',workingDate,workingDate);
    this._cardsService.getAllCardPayouts(cardPayoutVRList)
        .subscribe({next:(data) => {
                            this.cardPayouts = data;
                            this.orderCardPayoutsList = 'cardPayoutTpvId';
                            this.reverseCardPayoutsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.cardPayouts,this.orderCardPayoutsList,this.reverseCardPayoutsList, this.caseInsensitive);
                            this.cardPayouts = this.sortedCollection;
                            this.prepareDateHdTpvs(updateReceivedHdsFlag);
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private createDateHds(workingDate: Date, dateHdTpvs: HdTpv[], dateHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> createDateHds ->...Starting...->',null);
    this._hdService.createDateHds(dateHds)
      .subscribe({next:(data) => {
                          this.receivedHds = new Array();
                          this.receivedHds = data;                          
                          this.createDateHdTpvs(workingDate,dateHdTpvs,dateHds);
                        },
                error:(error) => {                        
                          this.resetShowHdFlags();
                          this.showHdList = true;
                          if (this.globalVar.handleError(error)) { throwError(() => error) };
                          }});
  }
  private createDateHdTpvs(workingDate: Date, dateHdTpvs: HdTpv[], dateHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> createDateHdTpvs ->...Starting...->',null);
    this._hdService.createDateHdTpvs(dateHdTpvs)
      .subscribe({next:(data) => {
                          this.receivedHdTpvs = new Array();
                          this.receivedHdTpvs = data;                          
                          this.prepareDateHdTpvs(false);
                        },
                error:(error) => {
                          this.resetShowHdFlags();
                          this.showHdList = true;
                          this.deleteDateHds(workingDate);
                          if (this.globalVar.handleError(error)) { throwError(() => error) };
                          }});
  }
  private updateDateHds(workingDate: Date, dateHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> updateDateHds ->...Starting...->',null);
    this._hdService.updateDateHds(dateHds)
      .subscribe({next:(data) => { this.getDateHds(workingDate,false,false);   },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

  private deleteDateHds(deleteDate: Date) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> deleteDateHds ->...Starting...->',null);
    this._hdService.deleteDateHds(deleteDate)
      .subscribe({next:(data) => { this.getListHds(this.hdsFirstAndLastDayOfMonth);},
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private deleteMonthHds(firstAndLastDayOfMonth: Date[]) {    
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> deleteMonthHds ->...Starting...->',null);
    this._hdService.deleteMonthHds(firstAndLastDayOfMonth)
      .subscribe({next:(data) => { this.getListHds(this.hdsFirstAndLastDayOfMonth); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //-------------------------------------Hd Types BtnClick()------------------------------------------------------------------
  private resetShowHdTypesFlags(){        
    this.showHdTypesList  = false;
  }
  changeHdTypeListBtnClick(){
    if (this.showHdTypesList === true) {
      this.resetShowHdTypesFlags();
    } else {
      this.resetShowHdFlags();
      this.resetShowHdTypesFlags();
      this.showHdTypesList = true;
      this.getAllHdTypes(false);
    }
  }
  createOneHdTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> createOneHdTypeBtnClick ->...Starting...->',null);
    const modalInitialState = {
      createHdType    :true,
      editHdType      :false,
      callback        :'OK',   
    };
    this.globalVar.openModal(HdTypeCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.getAllHdTypes(false);                                        
    })
  }
  editHdTypeBtnClick(modHdType: HdType){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> editHdTypeBtnClick ->...Starting...->',null);
    const modalInitialState = {
      createHdType:       false,
      editHdType:         true,
      newHdType:          modHdType,
      callback              :'OK',   
    };
    this.globalVar.openModal(HdTypeCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.getAllHdTypes(false);                               
    })
  }
  searchHdTypesBtnClick() {
    if (this.showSearchHdTypes === true) {
      this.showSearchHdTypes = false;
    } else {
      this.searchHdType.hdTypeName  = "";
      this.searchHdType.hdTypeType  = "";
      this.searchHdType.hdTypePlace = "";
      this.showSearchHdTypes = true;
    }
    return;
  }
  setHdTypesOrderBtnClick(value: string) {
    if (this.orderHdTypesList === value) {
      this.reverseHdTypesList = !this.reverseHdTypesList;
    }
    this.orderHdTypesList = value;   
  }
  private prepareSelectHdTypesRecordList() { 
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> prepareSelectHdTypesRecordList ->...Starting...->',null);
    let sumIndex = 0;
    this.selectHdTypesRecordList[0] = new ValueRecord();
    this.selectHdTypesRecordList[0].id = 0;
    this.selectHdTypesRecordList[0].value = 'Select...';
    sumIndex = sumIndex + 1;
    for (let i = 0 ; i < this.hdTypes.length ; i++ ) {
      if ( (this.hdTypes[i].hdTypePlace =='SUM') || (this.hdTypes[i].hdTypePlace =='RES') ){
        this.sumHdTypes[sumIndex] = this.hdTypes[i];
        this.selectHdTypesRecordList[sumIndex] = new ValueRecord();
        this.selectHdTypesRecordList[sumIndex].id = sumIndex;
        this.selectHdTypesRecordList[sumIndex].value = this.hdTypes[i].hdTypeName;
        sumIndex = sumIndex + 1;
      }
    }
  }                
  //-------------------------------------Hd Types DELETE------------------------------------------------------------------
  confirmDeleteOneHdTypeBtnClick(hdType: HdType){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.hdType,this.formTitles.hdType,
                            hdType,this.formTitles.hdTypeDeleteOne,hdType.hdTypeName,
                            hdType.hdTypeType,hdType.hdTypePlace,"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.resetShowHdFlags();
            this.resetShowHdTypesFlags();   
            this.showHdTypesList = true;        
            this.deleteOneHdType(hdType); 
          }
        })
  }
  confirmDeleteAllHdTypesBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.hdType,this.formTitles.hdType,
                            null,this.formTitles.hdTypeDeleteAll,"","","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.resetShowHdFlags();
            this.resetShowHdTypesFlags(); 
            this.showHdTypesList = true;
            this.deleteAllHdTypes();  
          }
        })
  }
  //----------------------------------------------- HDTYPES DATABASE ------------------------------------------------------
  private getAllHdTypes(getHdsFlag: boolean) {
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> getAllHdTypes ->...Starting...->',null);
    this._hdService.getAllHdTypes()
      .subscribe({next:(data) => { this.hdTypes = data;
                           this.orderHdTypesList = 'hdTypeName';
                           this.reverseHdTypesList = false;
                           this.sortedCollection = this.orderPipe.transform(this.hdTypes,this.orderHdTypesList,this.reverseHdTypesList,this.caseInsensitive);
                           this.hdTypes = this.sortedCollection;                          
                           this.showSearchHdTypes = false;
                           this.formTitles.hdTypesListString = this.hdTypes.length + '...Tipos';
                           this.prepareSelectHdTypesRecordList();
                           if (getHdsFlag === true) { this.getListHds(this.hdsFirstAndLastDayOfMonth); } 
                           else { this.showHdTypesList = true; }                         
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }  
  private createBarHdTypes(currentBar :string){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> createBarHdTypes ->...Starting...->',null);
    this._hdService.createBarHdTypes(currentBar)
      .subscribe({next:(data) => { this.hdTypes = data;
                           this.orderHdTypesList = 'hdTypeName';
                           this.reverseHdTypesList = false;
                           this.sortedCollection = this.orderPipe.transform(this.hdTypes,this.orderHdTypesList,this.reverseHdTypesList,this.caseInsensitive);
                           this.hdTypes = this.sortedCollection;                          
                           this.showSearchHdTypes = false;
                           this.formTitles.hdTypesListString = this.hdTypes.length + '...Tipos';
                           this.prepareSelectHdTypesRecordList();  
                           this.showHdTypesList = true;                       
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private deleteAllHdTypes(){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> deleteAllHdTypes ->...Starting...->',null);
    this._hdService.delAllHdTypes()
    .subscribe({next:(data) => { this.getAllHdTypes(false); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});  
  }
  private deleteOneHdType(delHdType: HdType){
    this.globalVar.consoleLog(this.logToConsole, '-> HDs -> deleteOneHdType ->...Starting...->',null);
    this._hdService.delOneHdType(delHdType)
    .subscribe({next:(data) => { this.getAllHdTypes(false); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}
