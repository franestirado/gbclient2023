import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { EnterDecimalNumbersComponent } from '../aa-common/enter-decimal-numbers.component';
import { EnterSelectDecimalsComponent } from '../aa-common/enter-select-decimals.component';

import { Hd,HdType, HdTpv } from './hd';
import { HdService } from './hd.service';

@Component({
  selector      :'app-hd-sums',
  templateUrl   :'./hd-sums.component.html',
  styleUrls     :['./hd.component.css'] 
})
export class HdSumsComponent implements OnInit {  
  public hdTypes        :HdType[];
  public receivedHds    :Hd[];  
  public workingDate    :Date;
  public wDStartHds     :Hd[];  
  public wSumHds        :Hd[];  
  public wResHds        :Hd[];  
  public wCardPayouts   :Hd[];  
  public wDEndHds       :Hd[];  
  public wDContHds      :Hd[];
  public wFacts         :Hd[];
  public wDinDHds       :Hd[];
  public wDDifHds       :Hd[];
  public wDinTHds       :Hd[];
  public wDinFHds       :Hd[];
  public wHdTpvTT       :HdTpv[];
  public totalFacts     :number;
  public totalCards     :number;
  public callback       :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'hdSumsTitle'     :'Sumas/Restas',
    'hdDate'          :'',
  };
  public formLabels = {
    'hdSumAdd'        :'Añadir',
    'hdSumEdit'       :'Modificar',
    'hdSumName'       :'Nombre',
    'hdSigns'         :' + / - ',   
    'hdPlus'          :' + ', 
    'hdMinus'         :' - ',  
    'hdSumAmount'     :'Cantidad',
    'hdEditList'      :'Editar',
    'hdDelete'        :'Borrar',
  };
  public formErrors = {
    'hdSumSelected'   :'',
    'hdSumAmount'     :'',
  };
  public selectHdTypesRecordList  :ValueRecord [] = new Array();
  public hdSumForm                :FormGroup;
  public hdEditForm               :FormGroup;
  private sumHdTypes              :HdType[]= new Array();  
  private hdIndexDStart           :number;
  private hdIndexDEnd             :number;
  private hdIndexDCont            :number;
  private hdIndexDDif             :number;
  private hdIndexDayT             :number;
  private hdIndexDayD             :number;
  private hdIndexDayF             :number;
  private createHdSum             :Hd;
  private selectedSumHds          :Hd[];
  public showEditHdSum            :boolean;
  public showAddHdSum             :boolean;
  private sortedCollection        :any[];
  private logToConsole            :boolean;
  /*---------------------------------------HD-SUMS EDIT -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private modalService: BsModalService, 
                  private fb:FormBuilder, private orderPipe: OrderPipe, 
                  private globalVar: GlobalVarService, private _hdService: HdService, ) { }
  /*---------------------------------------HD-SUMS EDIT-- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.hdDate = this.workingDate.toLocaleDateString();
    this.hdSumForm = this.fb.group({
        hdSumSelected:  ['', Validators.required ],
        hdSumAmount:    ['', Validators.required ],
      })
      this.hdEditForm = this.fb.group({
        hdSumSelected:  ['', Validators.required ],
        hdSumAmount:    ['', Validators.required ],
      })

    this.showAddHdSum = true; 
    this.showEditHdSum = false;
    this.prepareSelectHdTypesRecordList();
    this.getDateHds(this.workingDate,false,false); 
  }
  /*---------------------------------------HD SUMS EDIT -- FORMS -----------------------------------------------------------*/

  /*---------------------------------------HD SUMS EDIT -- GENERAL -----------------------------------------------------------*/
  prepareSelectHdTypesRecordList() { 
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->prepareSelectHdTypesRecordList->starting->', null);   
    let sumIndex = 0;
    this.selectHdTypesRecordList[0] = new ValueRecord();
    this.selectHdTypesRecordList[0].id = 0;
    this.selectHdTypesRecordList[0].value = 'Select...';
    sumIndex = sumIndex + 1;
    for (let i = 0 ; i < this.hdTypes.length ; i++ ) {
      if ( (this.hdTypes[i].hdTypePlace =='SUM') || (this.hdTypes[i].hdTypePlace =='RES') ){
        this.sumHdTypes[sumIndex] = this.hdTypes[i];
        this.selectHdTypesRecordList[sumIndex] = new ValueRecord();
        this.selectHdTypesRecordList[sumIndex].id = sumIndex;
        this.selectHdTypesRecordList[sumIndex].value = this.hdTypes[i].hdTypeName;
        sumIndex = sumIndex + 1;
      }
    }                
  }
  getSelectedSumHdTypeIndex(hdSumType: String): number {
    let hdSumIndex = 0;
    for (let i = 0 ; i < this.selectHdTypesRecordList.length ; i++ ) {
      if (this.selectHdTypesRecordList[i].value == hdSumType ){
        hdSumIndex = i;
        break;
      }
    }
    return hdSumIndex;
  }
  prepareDateHds(updateReceivedHdsFlag :boolean, exitModal:boolean){ 
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->prepareDateHds->', '...Starting...');
    this.sortedCollection = this.orderPipe.transform(this.receivedHds, 'hdOrder');
    this.receivedHds = this.sortedCollection;                          
    this.hdIndexDStart = 0;
    this.hdIndexDEnd = 0;
    this.hdIndexDCont = 0;
    this.hdIndexDDif = 0;
    this.hdIndexDayT = 0;
    this.hdIndexDayD = 0;
    this.hdIndexDayF = 0;
    let indexSumHds = 0; let indexResHds = 0;
    let totalSumHds = 0; let totalResHds = 0;
    this.wSumHds    = new Array();
    this.wResHds    = new Array();
    this.wDStartHds = new Array();
    this.wDEndHds   = new Array();
    this.wDContHds  = new Array();
    this.wDDifHds   = new Array();
    this.wDinTHds   = new Array();
    this.wDinDHds   = new Array();
    this.wDinFHds   = new Array();

    for (let i = 0 ; i < this.receivedHds.length ; i++ ) {
      switch (this.receivedHds[i].hdPlace){        
        case 'SUM': {
          this.wSumHds[indexSumHds] = this.receivedHds[i];
          indexSumHds = indexSumHds + 1;
          totalSumHds = totalSumHds + this.receivedHds[i].hdAmount;
          break;
        }
        case 'RES': {
          this.wResHds[indexResHds] = this.receivedHds[i];
          indexResHds = indexResHds + 1;
          totalResHds = totalResHds + this.receivedHds[i].hdAmount;
          break;          
        }
        case 'FIX': {
          switch (this.receivedHds[i].hdType){
            case 'DSTART':  { this.hdIndexDStart  = i; this.wDStartHds[0] = this.receivedHds[i]; break; }
            case 'DEND':    { this.hdIndexDEnd    = i; this.wDEndHds[0]   = this.receivedHds[i]; break; }
            case 'DCONT':   { this.hdIndexDCont   = i; this.wDContHds[0]  = this.receivedHds[i]; break; }
            case 'DDIF':    { this.hdIndexDDif    = i; this.wDDifHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-T':   { this.hdIndexDayT    = i; this.wDinTHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-D':   { this.hdIndexDayD    = i; this.wDinDHds[0]   = this.receivedHds[i]; break; }
            case 'DAY-F':   { this.hdIndexDayF    = i; this.wDinFHds[0]   = this.receivedHds[i]; break; }
            default:break;
            }            
        }              
        default:
          break;
      }      
    }
    /* Din fin HD = Din empezar HD + total sumas HD - total restas HD - total tarjetas HD */
    this.receivedHds[this.hdIndexDEnd].hdAmount = this.receivedHds[this.hdIndexDStart].hdAmount + 
                                                    totalSumHds - totalResHds - this.totalCards;
    this.wDEndHds[0] = this.receivedHds[this.hdIndexDEnd];   
    /* Din HD = Din contado + total facts HD - Din fin HD */
    this.receivedHds[this.hdIndexDayD].hdAmount = this.receivedHds[this.hdIndexDCont].hdAmount + this.totalFacts -
                                                  this.receivedHds[this.hdIndexDEnd].hdAmount;
    this.wDinDHds[0] = this.receivedHds[this.hdIndexDayD];
    /* Din segun tickets HD = Suma Din segun tickets HD TPVs */    
    this.receivedHds[this.hdIndexDayT].hdAmount = this.wHdTpvTT[0].hdTpvTicket;
    this.wDinTHds[0] = this.receivedHds[this.hdIndexDayT];
    /* Dif Din HD - venta segun tickets  */
    this.receivedHds[this.hdIndexDayF].hdAmount = this.receivedHds[this.hdIndexDayD].hdAmount - 
                                                  this.receivedHds[this.hdIndexDayT].hdAmount ;
    this.wDDifHds[0] = this.receivedHds[this.hdIndexDayF];
    this.receivedHds[this.hdIndexDayD].hdNotes = this.receivedHds[this.hdIndexDayF].hdAmount.toFixed(2).toString();
    /* Dif Din TPVs - tickets TPVs */
    this.receivedHds[this.hdIndexDDif].hdAmount = this.wHdTpvTT[0].hdTpvTicket - this.wHdTpvTT[0].hdTpvMoney;

    this.hdSumForm.patchValue( {hdSumSelected: this.selectHdTypesRecordList[0].value, hdSumAmount: 0} );
    if (updateReceivedHdsFlag===true){
      this.updateDateHds(this.globalVar.workingDate,this.receivedHds, exitModal);
    }
  }  
  /*---------------------------------------HD SUMS EDIT -- BTNCLICK-----------------------------------------------------------*/
  closeHdSumsModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();
      }
  }
  saveAllHdSumsBtnClick(){
    this.prepareDateHds(true,true);
  }
  deleteAllHdSumsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->deleteAllHdSumsBtnClick->starting->', null);
    let delSumHds = new Array();
    for (let i=0; i<this.wSumHds.length; i++) { delSumHds[i] = this.wSumHds[i]; }
    for (let j=0; j<this.wResHds.length; j++) { delSumHds[j+this.wSumHds.length] = this.wResHds[j]; }
    this.deleteAllSumHds(this.workingDate,delSumHds);
  }
  addHdSumsModalBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->HD SUMS->addHdSumsModalBtnClick->starting->', null);
    this.selectedSumHds = new Array();       
    this.modalService.show(EnterSelectDecimalsComponent, {
      initialState: {
         displayMessage1:   this.workingDate.toLocaleDateString(),
         displayMessage2:   this.formLabels.hdSumAdd,
         displayMessage3:   this.formTitles.hdSumsTitle,
         selectHdTypesRecordList: this.selectHdTypesRecordList,
         displayLabel:      this.selectHdTypesRecordList[0].value,  
         displayNumber:     0,
        callback: (result1, result2) => {
          if ( result1 != null){             
            let hdSumIndex = 0;
            hdSumIndex = this.getSelectedSumHdTypeIndex(result1); 
            if (hdSumIndex === 0)  return;
            if (result2 === 0)  return;
            this.createHdSum = new Hd(this.globalVar.workingDate,this.sumHdTypes[hdSumIndex].hdTypeName,
                                      this.sumHdTypes[hdSumIndex].hdTypeType,this.sumHdTypes[hdSumIndex].hdTypePlace,
                                      this.sumHdTypes[hdSumIndex].hdTypeOrder,result2,true);                
            this.createSumHd(this.globalVar.workingDate,this.createHdSum);
          }                           
        }
      },
      animated: true,
      keyboard: false,
      backdrop: false,
      ignoreBackdropClick: true,
      class: 'modal-lm modalHdSumsAdd'             
    });     
    return;
  }
  editSumHdsBtnClick(hd: Hd){
    this.globalVar.consoleLog(this.logToConsole,'->HD SUMS->editSumHdsBtnClick->starting->', null);
    this.selectedSumHds = new Array();
    this.selectedSumHds[0] = hd;
    let displayNumber = hd.hdAmount;
    const modalInitialState = {
      displayMessage1     :this.workingDate.toLocaleDateString(),
      displayMessage2     :this.formLabels.hdSumEdit,
      displayMessage3     :this.formTitles.hdSumsTitle,
      displayLabel        :hd.hdName,  
      displayNumber       :displayNumber,
      callback            :'OK',   
    };
    this.globalVar.openModal(EnterDecimalNumbersComponent, modalInitialState, 'modalLmTop0Left0').
    then((result:any)=>{
      if ( result != null){             
        this.selectedSumHds[0].hdAmount = result;            
        this.updateDateHds(this.globalVar.workingDate,this.selectedSumHds,false);
      }   
    })
    return;
  }
  delSumHdsBtnClick(hd: Hd){
    this.deleteOneSumHd(this.globalVar.workingDate,hd);
    return;
  }
  delResHdsBtnClick(hd: Hd){
    this.deleteOneSumHd(this.globalVar.workingDate,hd);
    return;
  }
  /*---------------------------------------HD SUMS EDIT -- DATABASE-----------------------------------------------------------*/
  getDateHds(workingDate: Date, updateReceivedHdsFlag:boolean, exitModal: boolean) {
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->getDateHds->starting->', null);
    this._hdService.getDateHds(workingDate)
      .subscribe({next:(data) => { this.receivedHds = data;
                           this.prepareDateHds(updateReceivedHdsFlag,false);
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  createSumHd(workingDate: Date, sumHd: Hd): void {
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->createSumHd->creating->', null);
    this._hdService.createSumHd(sumHd)
      .subscribe({next:(data) => { this.getDateHds(workingDate,true,false); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateDateHds(workingDate: Date, dateHds: Hd[], exit:boolean): void {
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->updateDateHds->updating->', null);
    this._hdService.updateDateHds(dateHds)
      .subscribe({next:(data) => { if (exit === false) {
                             this.getDateHds(workingDate,false,false);  
                           } else {
                             if (this.bsModalRef.content.callback != null){
                               //this.bsModalRef.content.callback('OK');
                               this.result.next('OK');
                               this.bsModalRef.hide();
                             }
                           }                          
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }  
  deleteOneSumHd(workingDate: Date, delSumHd: Hd) {
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->deleteOneSumHd->starting->', null);
    this._hdService.deleteOneSumHd(delSumHd)
      .subscribe({next:(data) => { this.getDateHds(workingDate,true,false); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }  
  deleteAllSumHds(workingDate: Date, sumHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '->HD SUMS->deleteAllSumHds->deleting->', null);
    this._hdService.deleteAllSumHds(sumHds)
      .subscribe({next:(data) => { this.getDateHds(workingDate,false,false);   },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}