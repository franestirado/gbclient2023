import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';

import { HdType } from './hd';
import { HdService } from './hd.service';

@Component({
  selector: 'app-hdType-create',
  templateUrl: './hdType-create.component.html',
  styleUrls: ['./hd.component.css']   
})
export class HdTypeCreateComponent implements OnInit {
  public  createHdType    :boolean;
  public  editHdType      :boolean;
  public  newHdType       :HdType;
  callback: any;
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'hdTypeTitle'       :'Otras Sumas/Restas HOJAS DÍA',
    'hdTypeCreate'      :'Crear Tipo Registros Hojas del Día',
    'hdTypeEdit'        :'Modificar Tipo Registros Hojas del Día',
  };
  public  formLabels = {
    'hdTypeId'          :'Id Tipo',
    'hdTypeDate'        :'Fecha',
    'hdTypeName'        :'Nombre Tipo',
    'hdTypeType'        :'Tipo Registro',
    'hdTypePlace'       :'Lugar en Hoja Día',
    'hdTypeOrder'       :'Posición en Hoja Día',
    'hdTypeStatus'      :'Activo'
  };
  public  hdTypeForm                    : FormGroup;
  public  localHdTypeTypesRecordList    : ValueRecord []; 
  public  localHdTypePlacesRecordList   : ValueRecord [];   
  private logToConsole                  : boolean;
  public  currentDate                   :Date;
  public  hdTypesDatePickerConfig       :Partial <BsDatepickerConfig>;
  /*---------------------------------------HD TYPES CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor (   private bsModalRef: BsModalRef, private fb:FormBuilder,
                  private globalVar: GlobalVarService, private _hdService: HdService, ) { }
  /*---------------------------------------HD TYPES CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.currentDate = this.globalVar.currentDate;
    this.hdTypeForm = this.fb.group({
        hdTypeId        :[''],
        hdTypeDate      :[''],
        hdTypeName      :['', Validators.required ],
        hdTypeType      :['', Validators.required ],
        hdTypePlace     :['', Validators.required ],
        hdTypeOrder     :['', Validators.required ],
        hdTypeStatus    :['']
      })
    this.localHdTypeTypesRecordList = this.globalVar.hdTypeTypesRecordList;
    this.localHdTypePlacesRecordList = this.globalVar.hdTypePlacesRecordList;
    if (this.createHdType === true) { 
        this.cleanHdTypeFormData(this.hdTypeForm);
        this.hdTypeForm.get('hdTypeDate').patchValue(new Date(this.currentDate));
    } else {
        this.getOneHdType(this.newHdType);
    }
  }
  /*---------------------------------------HD TYPES CREATE -- FORMS -----------------------------------------------------------*/
  copyFormDataToHdType(updateFlag: boolean){
    if (updateFlag===true) { this.newHdType.hdTypeId = this.hdTypeForm.controls.hdTypeId.value; } 
    this.newHdType.hdTypeDate   = this.hdTypeForm.controls.hdTypeDate.value;   
    this.newHdType.hdTypeName   = this.hdTypeForm.controls.hdTypeName.value;
    this.newHdType.hdTypeType   = this.hdTypeForm.controls.hdTypeType.value;
    this.newHdType.hdTypePlace  = this.hdTypeForm.controls.hdTypePlace.value;
    this.newHdType.hdTypeOrder  = this.hdTypeForm.controls.hdTypeOrder.value;
    this.newHdType.hdTypeStatus = this.hdTypeForm.controls.hdTypeStatus.value;
  }
  copyNewHdTypeDataToForm(){
    this.hdTypeForm.patchValue( { hdTypeId: this.newHdType.hdTypeId, hdTypeName: this.newHdType.hdTypeName, hdTypeType: this.newHdType.hdTypeType, 
                                  hdTypePlace: this.newHdType.hdTypePlace, hdTypeOrder: this.newHdType.hdTypeOrder, 
                                  hdTypeStatus: this.newHdType.hdTypeStatus} );
    this.currentDate = this.newHdType.hdTypeDate;
    this.hdTypeForm.get('hdTypeDate').patchValue(new Date(this.currentDate));
  }
  cleanHdTypeFormData(hdTypeForm: FormGroup){
    hdTypeForm.patchValue( {hdTypeId:null, hdTypeName:'', hdTypeType: this.localHdTypeTypesRecordList[0].value, 
                                hdTypePlace: this.localHdTypePlacesRecordList[0].value, hdTypeOrder: 0, hdTypeStatus: false} );
  }  
  /*---------------------------------------HD TYPES CREATE -- GENERAL -----------------------------------------------------------*/
  
  /*---------------------------------------HD TYPES CREATE -- BTNCLICK-----------------------------------------------------------*/
  closeHdTypeCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();
      }
  }
  cleanFormHdTypeBtnClick(hdTypeForm: FormGroup){
    this.editHdType = false;
    this.createHdType = true;
    this.cleanHdTypeFormData(hdTypeForm);
    return;
  }
  createHdTypeBtnClick(hdTypeForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole, '->HDS->createHdTypeBtnClick->starting->', null);
    this.newHdType = new HdType();
    this.copyFormDataToHdType(false);    
    this.createOneHdType(this.newHdType);
    return;
  }
  editHdTypeBtnClick(modHdType: HdType){
    this.globalVar.consoleLog(this.logToConsole, '->HDS->editHdTypeBtnClick->starting->', null);
    this.editHdType = true;       
    this.getOneHdType(modHdType);
  }
  updateHdTypeBtnClick(hdTypeForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole, '->HDS->updateHdTypeBtnClick->starting->', null);
    this.copyFormDataToHdType(true);
    this.updateOneHdType(this.newHdType);
  }
  /*---------------------------------------HD TYPES CREATE -- DATABASE-----------------------------------------------------------*/
  createOneHdType(newHdType: HdType): void {
    this.globalVar.consoleLog(this.logToConsole, '->createOneHdType->creating->', null);
    this._hdService.createOneHdType(newHdType)
      .subscribe({next:(data) => {
                          this.newHdType = data;
                          this.cleanHdTypeFormData(this.hdTypeForm);
                          this.closeHdTypeCreateModalBtnClick();
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getOneHdType(modHdType: HdType): void {
    this.globalVar.consoleLog(this.logToConsole, '->getOneHdType->creating->', null);
    this._hdService.getOneHdType(modHdType)
      .subscribe({next:(data) => {
                          this.newHdType = data;
                          this.copyNewHdTypeDataToForm();                                                
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneHdType(modHdType: HdType): void {
    this.globalVar.consoleLog(this.logToConsole, '->updateOneHdType->creating->', null);
    this._hdService.updateOneHdType(modHdType)
      .subscribe({next:(data) => {
                          this.newHdType = data;
                          this.copyNewHdTypeDataToForm();      
                          this.closeHdTypeCreateModalBtnClick();
                        },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}