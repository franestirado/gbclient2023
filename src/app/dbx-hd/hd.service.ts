import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HdType,Hd, HdTpv } from './hd';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
@Injectable({  providedIn: 'root'})
export class HdService {
  constructor(private http: HttpClient) { }
    private json: string;
    private params: string;
    private headers: HttpHeaders;

  /* ---------------------------------------- HDS ----------------------------------------------------------*/
  getListHds(firstAndLastDayOfMonth: Date[]): Observable<Hd[]>  {
    return this.http.post<Hd[]>('/hds/getListHds',firstAndLastDayOfMonth)
  }
  getHdsMonth(firstAndLastDayOfMonth: Date[]): Observable<Hd[]>  {
    return this.http.post<Hd[]>('/hds/getHdsMonth',firstAndLastDayOfMonth)
  }
  getDateHds(workingDate: Date): Observable<Hd[]>  {
    return this.http.post<Hd[]>('/hds/getDateHds',workingDate)
  }
  createSumHd(sumHd: Hd): Observable<any>  {
    return this.http.post('/hds/createSumHd', sumHd)
  }
  createDateHds(dateHds: Hd[]): Observable<any>  {
    return this.http.post('/hds/createDateHds', dateHds)
  }
  updateDateHds(dateHds: Hd[]): Observable<any>  {
    return this.http.post('/hds/updateDateHds', dateHds)
  }
  deleteOneSumHd(delSumHd: Hd): Observable<any> {
    return this.http.post<number>('/hds/deleteOneSumHd',delSumHd)
  }
  deleteAllSumHds(sumHds: Hd[]): Observable<any>  {
    return this.http.post('/hds/deleteAllSumHd', sumHds)
  }
  deleteDateHds(workingDate: Date): Observable<any> {
    return this.http.post<number>('/hds/deleteDateHds',workingDate)
  }
  deleteMonthHds(firstAndLastDayOfMonth: Date[]): Observable<any> {
    return this.http.post<number>('/hds/deleteMonthHds',firstAndLastDayOfMonth)     
  }
  /* ---------------------------------------- HD TPVS ----------------------------------------------------------*/
  getDateHdTpvs(workingDate: Date): Observable<HdTpv[]>  {
    return this.http.post<HdTpv[]>('/hds/getDateHdTpvs',workingDate)
  }
  getHdTpvsMonth(firstAndLastDayOfMonth: Date[]): Observable<HdTpv[]>  {
    return this.http.post<HdTpv[]>('/hds/getHdTpvsMonth',firstAndLastDayOfMonth)
  }
  createDateHdTpvs(dateHdTpvs: HdTpv[]): Observable<any>  {
    return this.http.post('/hds/createDateHdTpvs', dateHdTpvs)
  }
  updateDateHdTpvs(dateHdTpvs: HdTpv[]): Observable<any>  {
    return this.http.post('/hds/updateDateHdTpvs', dateHdTpvs)
  }
  readDataPreviousDate(workingDate: Date): Observable<HdTpv[]>  {
    return this.http.post<HdTpv[]>('/hds/previousDateHdTpvs',workingDate)
  }
  /* ----------------------------------------------- HDTYPES ---------------------------------------------------*/
  getAllHdTypes(): Observable<HdType[]>  {
    return this.http.get<HdType[]>('/hds/getAllHdTypes')
  }
  createBarHdTypes(currentBar :string): Observable<HdType[]>  {
    return this.http.post<HdType[]>('/hds/createBarHdTypes', currentBar)
  }
  getOneHdType(editHdType: HdType): Observable<HdType>  {
    return this.http.post<HdType>('/hds/getOneHdType', editHdType)
  }
  createOneHdType(newHdType: HdType): Observable<any>  {
    return this.http.post('/hds/createOneHdType', newHdType)
  }
  updateOneHdType(newHdType: HdType): Observable<any>  {
    return this.http.put('/hds/updOneHdType', newHdType)
  }
  delOneHdType(delHdType: HdType): Observable<any> {
    return this.http.post('/hds/delOneHdType', delHdType)
  }
  delAllHdTypes(): Observable<any> {
      return this.http.post('/hds/delAllHdTypes','')
  }

}
