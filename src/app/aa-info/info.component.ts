import { Component, OnInit } from '@angular/core';
import { GlobalVarService } from '../aa-common/global-var.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(public globalVar: GlobalVarService) { }

  ngOnInit() {
  }

}
