import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { OrderPipe } from 'ngx-order-pipe';

import { Art } from './art';
import { ArtService } from './art.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';
import { ValueRecord } from '../dbc-fields/field';

@Component({
  selector      :'app-arts-create',
  templateUrl   :'./arts-create.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class ArtsCreateComponent implements OnInit {
  public newArt      :Art;
  public artsTitle   :string;
  public artsDate    :Date;
  public createArt   :boolean;
  callback           :any;
  result: Subject<Art> = new Subject<Art>();

  public  formTitles = {
    'artsTitle'         :'',   
    'artsDate'          :'',
    'arts'              :'Artículos',
    'artCreate'         :'Crear Artículo',
    'artEdit'           :'Modificar Artículo',
  };
  public  formLabels = {
    'id'                :'#####',
    'artDate'           :'Fecha',
    'artId'             :'Id',
    'artReference'      :'Referencia',
    'artName'           :'Artículo',
    'artLongName'       :'Descripción',
    'artType'           :'Tipo',
    'artSize'           :'Tamaño',
    'artMeasurement'    :'Lt/Kg/...',
    'artUnitPrice'      :'€/Ud.',
    'artPackageUnits'   :'Uds/Pk.',
    'artPackagePrice'   :'€/Pk.',
    'artFlag'           :'IVA incl',
    'artTax'            :'% IVA',
    'artTaxP'           :'IVA',
    'artPackagePriceP'  :'€/Pk inlc.IVA',
    'artStatus'         :'Estado',    
  };
  public  artForm                       :FormGroup;
  public  localArtUnitsRecordList       :ValueRecord [];
  public  localArtTaxesRecordList       :ValueRecord [];
  public  localArtTypesNamesRecordList  :ValueRecord [];
  private logToConsole                  :boolean;
  public  showArt                       :boolean;
  public  editArt                       :boolean;
  public  artTotalCostFlag              :boolean;
  public  disableArtNameArtType         :boolean;
  private artUnitPriceSubs              :Subscription;
  private artPackageUnitsSubs           :Subscription;
  private artTaxSubs                    :Subscription;
  private artPackagePricePSubs          :Subscription;
  private currentDate                   :Date;
  public  artsDatePickerConfig          :Partial <BsDatepickerConfig>;
  public  orderArtTypesList             :string;
  public  reverseArtTypesList           :boolean;
  public  caseInsensitive               :boolean;

 /*---------------------------------------ARTS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService, private orderPipe: OrderPipe, 
                private globalVar: GlobalVarService, private _artsService: ArtService) { 
  this.artsDatePickerConfig = Object.assign ( {}, {
    containerClass    :'theme-dark-blue',
    showWeekNumbers   :true,
    minDate           :this.globalVar.workingPreviousYear,
    maxDate           :this.globalVar.workingNextYear,
    dateInputFormat   :'DD-MM-YYYY'
    } );
}
/*---------------------------------------ARTS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;   
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE ->  ngOnInit ->...Starting...->', null);

    if (this.artsDate === null) {
      this.currentDate = this.globalVar.currentDate;
    } else {
      this.currentDate = new Date(this.artsDate);
    }
    this.formTitles.artsDate = this.currentDate.toLocaleDateString();
    this.formTitles.artsTitle = this.artsTitle;
    this.artTotalCostFlag = true;
    this.disableArtNameArtType = false;
    if (this.createArt === false) {
      this.disableArtNameArtType = true;
    }
    this.artForm = this.fb.group({
      artId             :[{value:'',disabled:true}],
      artDate           :[{value:this.currentDate,disabled:true},[Validators.required]],
      artName           :[{value:'',disabled:this.disableArtNameArtType},[Validators.required,Validators.minLength(3),Validators.maxLength(32)]],
      artReference      :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(16)]],
      artLongName       :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      artType           :[{value:'',disabled:this.disableArtNameArtType},[Validators.required]],
      artSize           :[{value:'',disabled:false},[Validators.required,ValidationService.sixDecimalsValidation]],
      artMeasurement    :[{value:'',disabled:false},[Validators.required]],      
      artUnitPrice      :[{value:'',disabled:false},[Validators.required,ValidationService.sixDecimalsValidation]],
      artPackageUnits   :[{value:'',disabled:false},[Validators.required,ValidationService.sixDecimalsValidation]],
      artPackagePrice   :[{value:'',disabled:true},[Validators.required,ValidationService.sixDecimalsValidation]],
      artTax            :[{value:'',disabled:false},[Validators.required,ValidationService.sixDecimalsValidation]],
      artTaxP           :[{value:'',disabled:true},[Validators.required,ValidationService.sixDecimalsValidation]],
      artPackagePriceP  :[{value:'',disabled:true},[Validators.required,ValidationService.sixDecimalsValidation]],
      artStatus         :[{value:true,disabled:false},[Validators.required]],
    })
    this.artUnitPriceSubs = this.artForm.get('artUnitPrice').valueChanges.subscribe((data:any) => {
      this.setArtTotalPackagePrice();
    });
    this.artPackageUnitsSubs = this.artForm.get('artPackageUnits').valueChanges.subscribe((data:any) => {
      this.setArtTotalPackagePrice();
    });
    this.artTaxSubs = this.artForm.get('artTax').valueChanges.subscribe((data:any) => {
      this.setArtTotalPackagePrice();
    });
    this.localArtUnitsRecordList = this.globalVar.artUnitsRecordList;
    this.localArtTaxesRecordList = this.globalVar.artTaxesRecordList;
    this.localArtTypesNamesRecordList = this.globalVar.artTypesNamesRecordList;
    this.artForm.patchValue( {artUnit: this.localArtUnitsRecordList[0].value} );
    this.artForm.patchValue( {artTax:  this.localArtTaxesRecordList[0].value} );
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE ->  ngOnInit ->...Starting...->', this.createArt);

    if (this.createArt === true) {
      this.showArt = true;        
      this.editArt = false;
      this.newArt = new Art();      
      this.cleanArtFormData();
      this.artForm.get('artSize').patchValue('');
      this.artForm.get('artPackagePriceP').patchValue('');
      this.artForm.get('artTaxP').patchValue('');
      this.artForm.get('artPackagePrice').patchValue('');
      this.artForm.get('artUnitPrice').patchValue('');
      this.artForm.get('artPackageUnits').patchValue('');
      this.artForm.get('artDate').patchValue(new Date(this.currentDate));
      this.artForm.patchValue( {artUnit: this.localArtUnitsRecordList[0].value} );
      this.artForm.patchValue( {artTax:  this.localArtTaxesRecordList[0].value} );
      this.artForm.patchValue( {artType: this.localArtTypesNamesRecordList[0].value} );                       
    } else {
      //this.createArt = false;
      this.showArt = true;        
      this.editArt = true;
      this.getOneArt(this.newArt);
    }
  }
  /*---------------------------------------ARTS CREATE -- FORMS-----------------------------------------------------------*/
  public  changeArtFlagBtnClick(){
    if (this.artTotalCostFlag === true) {
      this.artTotalCostFlag = false;
      this.artForm.get('artUnitPrice').disable(); 
      this.artForm.get('artPackagePriceP').enable();
      this.artUnitPriceSubs.unsubscribe();
      this.artPackageUnitsSubs.unsubscribe();
      this.artTaxSubs.unsubscribe();
      this.artPackageUnitsSubs = this.artForm.get('artPackageUnits').valueChanges.subscribe((data:any) => {
        this.setArtNetPackagePrice();
      });
      this.artTaxSubs = this.artForm.get('artTax').valueChanges.subscribe((data:any) => {
        this.setArtNetPackagePrice();
      });
      this.artPackagePricePSubs = this.artForm.get('artPackagePriceP').valueChanges.subscribe((data:any) => {
        this.setArtNetPackagePrice();
      });
    } else {
      this.artTotalCostFlag = true;
      this.artForm.get('artUnitPrice').enable(); 
      this.artForm.get('artPackagePriceP').disable();
      this.artPackageUnitsSubs.unsubscribe();
      this.artTaxSubs.unsubscribe();
      this.artPackagePricePSubs.unsubscribe();
      this.artUnitPriceSubs = this.artForm.get('artUnitPrice').valueChanges.subscribe((data:any) => {
        this.setArtTotalPackagePrice();
      });
      this.artPackageUnitsSubs = this.artForm.get('artPackageUnits').valueChanges.subscribe((data:any) => {
        this.setArtTotalPackagePrice();
      });
      this.artTaxSubs = this.artForm.get('artTax').valueChanges.subscribe((data:any) => {
        this.setArtTotalPackagePrice();
      });
    }
  }
  private setArtNetPackagePrice(){
    const artUnitPrice = this.artForm.get('artUnitPrice');
    const artPackagePriceP = this.artForm.get('artPackagePriceP');
    const artPackageUnits  = this.artForm.get('artPackageUnits');
    const artTax = this.artForm.get('artTax');   
    let newArtPackagePriceP = Number(artPackagePriceP.value); 
    if (isNaN(Number(artPackagePriceP.value)) === true) { return; }
    let newArtPackagePrice = 0;
    let newArtTax = 0; 
    let newArtTaxP = 0; 
    let newArtUnitPrice = 0; 
    if (isNaN(Number(artTax.value)) === true) {
      newArtTaxP = 0;     
    } else {
      newArtTax = Number(artTax.value);
      newArtPackagePrice = Number(newArtPackagePriceP) / ( 1 + (Number(newArtTax)/100) ) ; 
      newArtTaxP = Number(newArtPackagePriceP) - newArtPackagePrice;  
    }     
    if ( (isNaN(Number(artPackageUnits.value)) === true) || (Number(artPackageUnits.value) === 0) ) {
      newArtUnitPrice = 0;
    } else {
      newArtUnitPrice = newArtPackagePrice / (Number(artPackageUnits.value));   
    }    
    this.artForm.get('artTaxP').patchValue(newArtTaxP.toFixed(6));
    this.artForm.get('artPackagePrice').patchValue(newArtPackagePrice.toFixed(6));
    this.artForm.get('artUnitPrice').patchValue(newArtUnitPrice.toFixed(6));
    this.artForm.updateValueAndValidity();  
  }
  private setArtTotalPackagePrice(){
    const artUnitPrice = this.artForm.get('artUnitPrice');
    const artPackageUnits = this.artForm.get('artPackageUnits');
    let totalArtPackagePrice =   Number(artUnitPrice.value) * Number(artPackageUnits.value); 
    this.artForm.get('artPackagePrice').patchValue(totalArtPackagePrice.toFixed(6)); 
    let artTaxP = 0;
    const artTax = this.artForm.get('artTax');
    if (isNaN(Number(artTax.value)) === true) {
      artTaxP = 0;
    } else { 
      artTaxP = totalArtPackagePrice * Number(artTax.value) / 100;
    } 
    this.artForm.get('artTaxP').patchValue(artTaxP.toFixed(6));
    let totalArtPackagePriceP =  totalArtPackagePrice + artTaxP;
    this.artForm.get('artPackagePriceP').patchValue(totalArtPackagePriceP.toFixed(6));
    this.artForm.updateValueAndValidity();  
  }   
  private copyArtFormDataToArt(){
    this.newArt.artDate         = this.artForm.controls.artDate.value;
    this.newArt.artReference    = this.artForm.controls.artReference.value;
    this.newArt.artName         = this.artForm.controls.artName.value;
    this.newArt.artLongName     = this.artForm.controls.artLongName.value;
    this.newArt.artType         = this.artForm.controls.artType.value;
    this.newArt.artSize         = this.artForm.controls.artSize.value;
    this.newArt.artMeasurement  = this.artForm.controls.artMeasurement.value;    
    this.newArt.artUnitPrice    = this.artForm.get('artUnitPrice').value;
    this.newArt.artPackageUnits = this.artForm.get('artPackageUnits').value;
    this.newArt.artPackagePrice  = this.artForm.controls.artPackagePrice.value;
    this.newArt.artTax          = this.artForm.controls.artTax.value;
    this.newArt.artStatus       = this.artForm.controls.artStatus.value;

    var selectedArtTypeTag = this.globalVar.getArtTypeTagFromArtTypeName(this.newArt.artType);
    if (selectedArtTypeTag != null) this.newArt.artType= selectedArtTypeTag;
    else this.newArt.artType = "NO ArtType Tag";
  }
  private copyNewArtDataToArtForm(){
    this.artForm.patchValue( {artId:this.newArt.artId,artDate:this.newArt.artDate,artReference:this.newArt.artReference,artName:this.newArt.artName, 
                              artLongName:this.newArt.artLongName,artType:this.newArt.artType,  
                              artMeasurement:this.newArt.artMeasurement, artTax:this.newArt.artTax, 
                              artStatus:this.newArt.artStatus} );
    this.artForm.get('artDate').patchValue(new Date(this.newArt.artDate));
    this.artForm.get('artSize').patchValue(this.newArt.artSize.toFixed(6));
    this.artForm.get('artPackagePrice').patchValue(this.newArt.artPackagePrice.toFixed(6));
    this.artForm.get('artUnitPrice').patchValue(this.newArt.artUnitPrice.toFixed(6));
    this.artForm.get('artPackageUnits').patchValue(this.newArt.artPackageUnits.toFixed(6));

    
    var selectedArtTypeName = this.globalVar.getArtTypeNameFromArtTypeTag(this.newArt.artType);
    if (selectedArtTypeName != null) this.artForm.get('artType').patchValue(selectedArtTypeName);
    else this.artForm.get('artType').patchValue(this.localArtTypesNamesRecordList[0].value);
  }
  private cleanArtFormData(){
    this.artForm.patchValue( {artReference:'', artName:'',  artLongName:'',  artType:this.localArtTypesNamesRecordList[0].value,
                              artSize:0, artMeasurement:this.localArtUnitsRecordList[0].value, artUnitPrice:0, artPackageUnits:0, 
                              artPackagePrice:0, artTax:this.localArtTaxesRecordList[0].value, artStatus:true} );
  }
  /*---------------------------------------ARTS CREATE  -- GENERAL -----------------------------------------------------------*/
  public setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
  }  
  public setFourNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(4);
  } 
  public setSixNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(6);
  } 

  /* ---------------------------------------ARTS CREATE BTN CLICK-----------------------------------------------------------*/
  public  ClosedArstCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(this.newArt);
        this.result.next(this.newArt);
        this.bsModalRef.hide();
      }
  }
  public  changeDisableArtNameArtTypeBtnClick(){
    if (this.disableArtNameArtType === false) {
      this.artForm.get('artName').disable(); 
      this.artForm.get('artType').disable();
      this.disableArtNameArtType = true;
    } else {
      this.artForm.get('artName').enable(); 
      this.artForm.get('artType').enable();
      this.disableArtNameArtType = false;
    }
  }
  public  createArtTypeBtnClick(){

    
  }
  public  cleanFormArtBtnClick(){
    this.createArt = true;
    this.editArt = false;
    this.cleanArtFormData();
  }
  public  createArtBtnClick(artForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> createArtBtnClick ->...Starting...->', null);
    this.copyArtFormDataToArt();
    this.createOneArt(this.newArt);
  }
  public  updateArtBtnClick(artForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> updateArtBtnClick ->...Starting...->', null);
    this.copyArtFormDataToArt();   
    if ((this.editArt === true) && (this.disableArtNameArtType === true)) {
      this.updateOneArt(this.newArt);
    } 
    if ((this.editArt === true) && (this.disableArtNameArtType === false)) {
      this.updateOneArtSpecial(this.newArt);
    } 
  }
  /* ---------------------------------------ARTS CREATE --GET ARTS ---------------------------------------------------------*/
  private createOneArt(newArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> createOneArt ->...Starting...->', null);
    this._artsService.createOneArt(newArt)
      .subscribe({next:(data) => { this.newArt = data;
                           this.ClosedArstCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getOneArt(modArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> getOneArt ->...Starting...->', null);
    this._artsService.getOneArt(modArt)
      .subscribe({next:(data) => { this.newArt = data;
                           this.copyNewArtDataToArtForm();                       
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneArt(modArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> updateOneArt ->...Starting...->', null);
    this._artsService.updateOneArt(modArt)
      .subscribe({next:(data) => { this.newArt = data;
                           this.ClosedArstCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneArtSpecial(modArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS CREATE -> updateOneArtSpecial ->...Starting...->', null);
    this._artsService.updateOneArtSpecial(modArt)
      .subscribe({next:(data) => { this.newArt = data;
                           this.ClosedArstCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}