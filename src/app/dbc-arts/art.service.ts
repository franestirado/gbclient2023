import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Art,Dep,SubDep,ArtType } from './art';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()

export class ArtService {

  constructor(private http: HttpClient) { }

  private json: string;
  private params: string;
  private headers: HttpHeaders;

  /* ---------------------------------------ARTS-----------------------------------------------------------*/
  getAllArts(): Observable<Art[]>  {
    return this.http.get<Art[]>('/arts/getAll')
  }
  getAllArtsInArtType(selArtType: ArtType): Observable<Art[]>  {
    return this.http.post<Art[]>('/arts/getAllInArtType', selArtType)
  }
  getAllArtsWithArtTypeTag(selArtTypeTag: string): Observable<Art[]>  {
    return this.http.post<Art[]>('/arts/getAllWithArtTypeTag', selArtTypeTag)
  }
  createOneArt(newArt: Art): Observable<any>  {    
    return this.http.post('/arts/createOne', newArt)  
  }
  getOneArt(editArt: Art): Observable<Art>  {
    return this.http.post<Art>('/arts/getOne', editArt)
  }
  getOneArtById(artId :number): Observable<Art>  {
    return this.http.post<Art>('/arts/getOneById', artId)
  }
  updateOneArt(newArt: Art): Observable<any>  {
    return this.http.post('/arts/updOne', newArt)
  }
  updateOneArtSpecial(newArt: Art): Observable<any>  {
    return this.http.post('/arts/updOneSpecial', newArt)
  }
  updateArtWithArtFactData(selectedArtFact: Art): Observable<any>  {
    return this.http.post('/arts/updWithArtFact', selectedArtFact)
  }
  delOneArt(delArt: Art): Observable<any> {
    return this.http.post('/arts/delOne', delArt)
  }
  delAllArts(): Observable<any> {
      return this.http.delete('/arts/')
  }
  /* ---------------------------------------DEPS-----------------------------------------------------------*/
  getAllDeps(): Observable<Dep[]>  {
    return this.http.get<Dep[]>('/arts/getDeps')
  }
  createOneDep(newDep: Dep): Observable<any>  {    
    return this.http.post('/arts/createDep', newDep)  
  }
  getOneDep(editDep: Dep): Observable<Dep>  {
    return this.http.post<Dep>('/arts/getDep', editDep)
  }
  updateOneDep(newDep: Dep): Observable<any>  {
    return this.http.post('/arts/updDep', newDep)
  }
  updateOneDepSpecial(newDep: Dep): Observable<any>  {
    return this.http.post('/arts/updDepSpecial', newDep)
  }
  delOneDep(delDep: Dep): Observable<any> {
    return this.http.post('/arts/delDep', delDep)
  }
  delAllDeps(delDep: Dep): Observable<any> {
    return this.http.post('/arts/delDeps', delDep)
  }
  /* ---------------------------------------SUBDEPS-----------------------------------------------------------*/
  getAllSubDeps(): Observable<SubDep[]>  {
    return this.http.get<SubDep[]>('/arts/getSubDeps')
  }
  createOneSubDep(newSubDep: SubDep): Observable<any>  {    
    return this.http.post('/arts/createSubDep', newSubDep)  
  }
  getOneSubDep(editSubDep: SubDep): Observable<SubDep>  {
    return this.http.post<SubDep>('/arts/getSubDep', editSubDep)
  }
  updateOneSubDep(newSubDep: SubDep): Observable<any>  {
    return this.http.post('/arts/updSubDep', newSubDep)
  }
  updateOneSubDepSpecial(newSubDep: SubDep): Observable<any>  {
    return this.http.post('/arts/updSubDepSpecial', newSubDep)
  }
  delOneSubDep(delSubDep: SubDep): Observable<any> {
    return this.http.post('/arts/delSubDep', delSubDep)
  }
  delAllSubDeps(delSubDep: SubDep): Observable<any> {
    return this.http.post('/arts/delSubDeps', delSubDep)
  }
  getAllSubDepsInDep(selectedDep: Dep) :Observable<SubDep[]>  {
    return this.http.post<SubDep[]>('/arts/getSubDepsIn', selectedDep)
  }
  /* ---------------------------------------ARTTYPES-----------------------------------------------------------*/
  getAllArtTypes(): Observable<ArtType[]>  {
    return this.http.get<ArtType[]>('/arts/getArtTypes')
  }
  createOneArtType(newArtType: ArtType): Observable<any>  {    
    return this.http.post('/arts/createArtType', newArtType)  
  }
  getOneArtType(editArtType: ArtType): Observable<ArtType>  {
    return this.http.post<ArtType>('/arts/getArtType', editArtType)
  }
  updateOneArtType(newArtType: ArtType): Observable<any>  {
    return this.http.post('/arts/updArtType', newArtType)
  }
  updateOneArtTypeSpecial(newArtType: ArtType): Observable<any>  {
    return this.http.post('/arts/updArtTypeSpecial', newArtType)
  }
  delOneArtType(delArtType: ArtType): Observable<any> {
    return this.http.post('/arts/delArtType', delArtType)
  }
  delAllArtTypes(delArtType: ArtType): Observable<any> {
    return this.http.post('/arts/delArtTypes', delArtType)
  }
  getAllArtTypesInSubDep(selectedSubDep: SubDep) :Observable<ArtType[]>  {
    return this.http.post<ArtType[]>('/arts/getArtTypesIn', selectedSubDep)
  }
  checkGenericArtTypes() :Observable<ArtType[]>   {
    return this.http.get<ArtType[]>('/arts/checkArtTypesIn', )
  }
}
