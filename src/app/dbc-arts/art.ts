export class Art {
    artId               :number;
    artDate             :Date;
    artReference        :string;
    artName             :string;
    artLongName         :string;
    artType             :string;   
    artSize             :number;
    artMeasurement      :string;    
    artUnitPrice        :number;    
    artPackageUnits     :number;
    artPackagePrice     :number;
    artTax              :string; 
    artStatus           :boolean;
}
export class ArtString {
    artId               :number;
    artDate             :string;
    artReference        :string;
    artName             :string;
    artLongName         :string;
    artType             :string;   
    artSize             :number;
    artMeasurement      :string;    
    artUnitPrice        :number;    
    artPackageUnits     :number;
    artPackagePrice     :number;
    artTax              :string; 
    artStatus           :boolean;
}
export class Dep {
    depId               :number;
    depDate             :Date;
    depName             :string;
    depDescription      :string;
    depTag              :string;
    depStatus           :boolean;
}
export class DepString {
    depId               :number;
    depDate             :string;
    depName             :string;
    depDescription      :string;
    depTag              :string;
    depStatus           :boolean;
}
export class SubDep {
    subDepId            :number;
    subDepDate          :Date;
    subDepName          :string;
    subDepDescription   :string;
    subDepTag           :string;
    subDepDepId         :number;
    subDepDepName       :string;
    subDepSequence      :number;
    subDepStatus        :boolean;
}
export class SubDepString {
    subDepId            :number;
    subDepDate          :string;
    subDepName          :string;
    subDepDescription   :string;
    subDepTag           :string;
    subDepDepId         :number;
    subDepDepName       :string;
    subDepSequence      :number;
    subDepStatus        :boolean;
}
export class ArtType {
    artTypeId           :number;
    artTypeDate         :Date;
    artTypeName         :string;
    artTypeDescription  :string;
    artTypeTag          :string;
    artTypeSubDepId     :number;
    artTypeSubDepName   :string;
    artTypeSequence     :number;
    artTypeStatus       :boolean;
}
export class ArtTypeString {
    artTypeId           :number;
    artTypeDate         :string;
    artTypeName         :string;
    artTypeDescription  :string;
    artTypeTag          :string;
    artTypeSubDepId     :number;
    artTypeSubDepName   :string;
    artTypeSequence     :number;
    artTypeStatus       :boolean;
}