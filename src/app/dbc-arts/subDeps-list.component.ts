import { Component, OnInit } from '@angular/core';
import { throwError, Subject} from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { Dep, SubDep, SubDepString } from './art';
import { ArtService } from './art.service';
import { SubDepsCreateComponent } from './subDeps-create.component';
import { ArtTypesListComponent } from './artTypes-list.component';

@Component({
  selector      :'app-subDeps-list',
  templateUrl   :'./subDeps-list.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class SubDepsListComponent implements OnInit {
  public subDepsTitle   :string;
  public subDepsDate    :Date;
  public selectedDep    :Dep;
  callback              :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'edit'              :'Editar',
    'delete'            :'Borrar',
    'subDepsList'       :'Lista de SubDepartamentos',
    'subDeps'           :'SubDepartamentos',
    'selectedDep'       :'Departamento Seleccionado',
    'dep'               :'Departamento ',    
    'addArtTypes'       :'TiposArts',
    'subDepsTotalString':'SubDeps',
    'subDepDeleteOne'   :'¿ Seguro que quiere BORRAR este SUBDEPARTAMENTO ?',
    'subDepDeleteAll'   :'¿ Seguro que quiere BORRAR TODOS los SUBDEPARTAMENTOS ?',
  };
  public formLabels = {
    'subDepId'          :'SubDpId', 
    'subDepDate'        :'Fecha',
    'subDepDepId'       :'DepId',
    'subDepDepName'     :'Nombre.Dep.',
    'subDepName'        :'Nombre.SubDp',
    'subDepDescription' :'Descripción',
    'subDepTag'         :'Tag',
    'subDepSequence'    :'Sequence',
    'subDepStatus'      :'Estado',
  };
  private createSubDep      :boolean;
  public  showSubDepsList   :boolean;
  public  subDeps           :SubDep[];
  private newSubDep         :SubDep;
  public  searchSubDep      :SubDepString;
  private selectedSubDep    :SubDep;
  public  showSelectedSubDep:boolean;
  public  showSearchSubDeps :boolean;
  public  orderSubDepsList  :string;
  public  reverseSubDepsList:boolean;
  public  showSelectedDep   :boolean;

  public  caseInsensitive       :boolean;
  private sortedCollection      :any[];
  private logToConsole          :boolean;
  //--------------------------------------- DEPS LIST -- CONSTRUCTOR -----------------------------------------------------------
  constructor ( private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _errorService: ErrorService, private _artService: ArtService) { 

  }
  //--------------------------------------- DEPS LIST -- NG ON INIT -----------------------------------------------------------
  ngOnInit(): void {
    this.searchSubDep = new SubDepString();
    this.orderSubDepsList = 'subDepName';
    this.reverseSubDepsList = false;
    this.caseInsensitive = true;
    this.showSearchSubDeps = false;
    this.showSelectedDep = false;
    this.getSubDepsList();
  }
  //--------------------------------------- SUBDEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- SUBDEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- SUBDEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- SUBDEPS LIST -- FORMS --------------------------------------------------------------
  //--------------------------------------- SUBDEPS LIST -- GENERAL ------------------------------------------------------------
  //--------------------------------------- SUBDEPS LIST -- BTN CLICK -----------------------------------------------------------
  public  listArtTypesInSubDepBtnClick(selectedSubDep: SubDep){
    const modalInitialState = {
      artTypesTitle       :'Subdepartamento, Lista Tipos Artículos',
      artTypesDate        :this.subDepsDate,
      selectedSubDep      :selectedSubDep,
      callback            :'OK',   
    };
    this.globalVar.openModal(ArtTypesListComponent, modalInitialState, 'modalXlTop0Left6')
    .then((result:any)=>{ this.getSubDepsList(); }) 
  }
  public  createSubDepBtnClick(){
    this.newSubDep = new SubDep();
    this.createSubDep = true;
    this.createSubDepModal();
  }
  public  editSubDepBtnClick(editSubDep: SubDep){
    if (this.isSubDepNotBlocked(editSubDep)){ 
      this.newSubDep = editSubDep;
      this.createSubDep = false;
      this.createSubDepModal();
    }
  }
  private createSubDepModal(){
    const modalInitialState = {
      newSubDep     :this.newSubDep,
      subDepsTitle  :'SubDepartamentos',
      subDepsDate   :this.globalVar.currentDate,
      createSubDep  :this.createSubDep,
      selectedDep   :this.selectedDep,
      callback      :'OK',   
    };
    this.globalVar.openModal(SubDepsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{ this.getSubDepsList(); })
  }
  private isSubDepNotBlocked(selectedSubDep: SubDep):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> isSubDepNotBlocked ->...Starting...->', null);
    if (selectedSubDep.subDepStatus === false){
      this._errorService.showErrorModal( "SUBDEPARTAMENTOS", "SUBDEPARTAMENTOS", "SUBDEPARTAMENTO BLOQUEADO"+"->",
                        "SubDepartamento->"+selectedSubDep.subDepName, "Descripión->"+selectedSubDep.subDepDescription, 
                        "Id->"+selectedSubDep.subDepId, "Estado->"+selectedSubDep.subDepStatus,
                        "Fecha->"+selectedSubDep.subDepDate, "");
      return false;
    } else return true;
  }
  public  lockSubDepBtnClick(modSubDep: SubDep){
    if (modSubDep.subDepStatus === true) modSubDep.subDepStatus = false;
    else  modSubDep.subDepStatus = true;
    this.updateOneSubDep(modSubDep);
  }
  public  searchSubDepsBtnClick() {
    if (this.showSearchSubDeps === true) {
      this.showSearchSubDeps = false;
    } else {
      this.showSearchSubDeps = true;
    }
  }
  public  setSubDepOrderBtnClick(value: string) {
    if (this.orderSubDepsList === value) {
      this.reverseSubDepsList = !this.reverseSubDepsList;
    }
    this.orderSubDepsList = value;
  }
  public closedSubDepsListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.newDep);
      this.result.next('OK');
      this.bsModalRef.hide();
      }
  }
  //--------------------------------------- SUBDEPS LIST -- DATABASE -------------------------------------------------------------------
  private getSubDepsList(){
    if (this.selectedDep === null) this.getAllSubDeps();
    else this.getAllSubDepsInDep(this.selectedDep);
  }
  private getAllSubDeps() {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllSubDeps ->...Starting...->', null);
    this._artService.getAllSubDeps()
      .subscribe({next:(data) => { this.subDeps = data;
                           this.sortedCollection = this.orderPipe.transform(this.subDeps,this.orderSubDepsList,this.reverseSubDepsList,this.caseInsensitive);
                           this.subDeps = this.sortedCollection;
                           this.formTitles.subDepsList ='Lista de TODOS los Subdepartamentos';
                           this.formTitles.subDepsTotalString =this.subDeps.length+'..SubDeps';
                           this.showSubDepsList = true;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getAllSubDepsInDep(selectedDep: Dep){
    this.globalVar.consoleLog(this.logToConsole,'->ARTS ->getAllSubDepsInDep ->...Starting...->',null);
    this._artService.getAllSubDepsInDep(selectedDep)
      .subscribe({next:(data) => { this.subDeps = data;
                           this.sortedCollection = this.orderPipe.transform(this.subDeps,this.orderSubDepsList,this.reverseSubDepsList,this.caseInsensitive);
                           this.subDeps = this.sortedCollection;
                           this.formTitles.subDepsList ='Lista Subdepartamentos en el Departamento ==> '+selectedDep.depName;
                           this.formTitles.dep ='Departamento ... '+selectedDep.depName;
                           this.formTitles.selectedDep ='->Nombre: '+selectedDep.depName+'->Descripcion: '+selectedDep.depDescription+'->DepTag: '+selectedDep.depTag+
                                                        '->DepId: '+selectedDep.depId+'->Fecha: '+selectedDep.depDate;
                           this.formTitles.subDepsTotalString =this.subDeps.length+'..SubDeps';
                           this.showSubDepsList = true;
                           this.showSelectedDep = true;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneSubDep(modSubDep: SubDep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> updateOneSubDep ->...Starting...->', modSubDep);
    this._artService.updateOneSubDep(modSubDep)
      .subscribe({next:(data) => { this.getSubDepsList(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- SUBDEPS LIST -- DELETE -------------------------------------------------------------------
  public  confirmDeleteOneSubDepBtnClick(subDep: SubDep){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.subDeps,this.formTitles.subDeps,
      subDep,this.formTitles.subDepDeleteOne,subDep.subDepName,subDep.subDepDescription,subDep.subDepTag,subDep.subDepDate.toString(),"","",)
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteOneSubDep(subDep);  } })  
  }
  public  confirmDeleteAllSubDepsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.subDeps,this.formTitles.subDeps,null,this.formTitles.subDepDeleteAll,"","","","","","")
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteAllSubDeps(this.subDeps[0]); } }) 
  } 
  private deleteAllSubDeps(delSubDep: SubDep){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteAllSubDeps ->...Starting...->', null);
    this._artService.delAllSubDeps(delSubDep)
      .subscribe({next:(data) => { this.getSubDepsList(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };}});
  }
  private deleteOneSubDep(delSubDep: SubDep){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteOneSubDep ->...Starting...->', null);
    this._artService.delOneSubDep(delSubDep)
      .subscribe({next:(data) => { this.getSubDepsList(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}