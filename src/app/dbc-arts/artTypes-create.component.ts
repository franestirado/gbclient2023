import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { ArtType, SubDep } from './art';
import { ArtService } from './art.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';

@Component({
  selector      :'app-artTypes-create',
  templateUrl   :'./artTypes-create.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class ArtTypesCreateComponent implements OnInit {
  public newArtType       :ArtType;
  public subDeps          :SubDep[];
  public artTypesTitle    :string;
  public artTypesDate     :Date;
  public createArtType    :boolean;
  public selectedSubDep   :SubDep;
  callback                :any;
  result: Subject<ArtType> = new Subject<ArtType>();

  public formTitles = {
    'artTypesTitle'      :'',   
    'artTypesDate'       :'',
    'artTypes'           :'Departamento',
    'artTypeCreate'      :'Crear',
    'artTypeEdit'        :'Modificar',
  };
  public formLabels = {
    'id'                :'#####',
    'artTypeId'          :'Id',
    'artTypeDate'        :'Fecha',
    'artTypeName'        :'Nombre',
    'artTypeDescription' :'Descripción',
    'artTypeTag'         :'Tag',
    'artTypeSequence'    :'Sequence',
    'artTypeSubDepName'  :'SubDepartamento',
    'artTypeSubDepId'    :'SubDep.Id',
    'artTypeStatus'      :'Estado',   
  };
  public  artTypeForm        :FormGroup;
  public  artTypes           :ArtType[];
  public  currentArtTypeDate :Date;
  public  selectSubDep       :boolean;
  private logToConsole       :boolean;
  public  editArtType        :boolean;
  private disableArtTypeTAG  :boolean;
  public  localFactTypesSubDepsRecordList :ValueRecord [];
  public  artTypesDatePickerConfig        :Partial <BsDatepickerConfig>;

 /*---------------------------------------ARTTYPES CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _artsService: ArtService) { 
    this.artTypesDatePickerConfig = Object.assign ( {}, {
      containerClass:   'theme-dark-blue',
      showWeekNumbers:  true,
      minDate:          this.globalVar.workingPreviousYear,
      maxDate:          this.globalVar.workingNextYear,
      dateInputFormat:  'DD-MM-YYYY'
      } );
  }
/*---------------------------------------ARTTYPES CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.currentArtTypeDate = this.artTypesDate;
    if (this.artTypesDate === null) {
      this.formTitles.artTypesDate = '';
    } else {
      this.formTitles.artTypesDate = this.artTypesDate.toLocaleDateString();
    }
    this.formTitles.artTypesTitle = this.artTypesTitle;
    if (this.selectedSubDep === null) this.selectSubDep = true;
    else this.selectSubDep = false;
    this.disableArtTypeTAG  = false;
    if (this.createArtType === false) {
      this.disableArtTypeTAG = true;
    }
    this.artTypeForm = this.fb.group ({
      artTypeId             :[{value:'',disabled:true}],
      artTypeDate           :[{value:this.currentArtTypeDate,disabled:true},[Validators.required]],
      artTypeName           :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(32)]],
      artTypeDescription    :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      artTypeTag            :[{value:'',disabled:this.disableArtTypeTAG},[Validators.required,Validators.minLength(3),Validators.maxLength(16)]],
      artTypeSequence       :[{value:0,disabled:true},[Validators.required,Validators.minLength(3),Validators.maxLength(8)]],
      artTypeSubDepName     :[{value:'',disabled:false},[Validators.required]],
      artTypeSubDepId       :[{value:'',disabled:false},[Validators.required]],
      artTypeStatus         :[{value:true,disabled:false},[Validators.required]],
    })
    this.artTypeForm.get('artTypeDate').patchValue(new Date(this.currentArtTypeDate));
    this.localFactTypesSubDepsRecordList = this.globalVar.factTypesSubDepsRecordList;
    if (this.selectedSubDep === null) {
      this.selectSubDep = true;
      this.artTypeForm.get('artTypeSubDepName').patchValue(this.localFactTypesSubDepsRecordList[0].value); 
      this.artTypeForm.get('artTypeSubDepId').patchValue(0); 
    } else {
      this.selectSubDep = false;
      this.artTypeForm.get('artTypeSubDepName').patchValue(this.selectedSubDep.subDepName); 
      this.artTypeForm.get('artTypeSubDepId').patchValue(this.selectedSubDep.subDepId); 
    }
    if (this.createArtType === true) {
        this.editArtType = false;
        this.newArtType = new ArtType();   
    } else {
      this.createArtType = false;
      this.editArtType = true;
      this.getOneArtType(this.newArtType);
    }
  }
  /*---------------------------------------ARTTYPES CREATE -- FORMS-----------------------------------------------------------*/    
  private copyArtTypeFormDataToArtType(){
    this.newArtType.artTypeDate        = this.artTypeForm.controls.artTypeDate.value;
    this.newArtType.artTypeName        = this.artTypeForm.controls.artTypeName.value;
    this.newArtType.artTypeDescription = this.artTypeForm.controls.artTypeDescription.value;
    this.newArtType.artTypeTag         = this.artTypeForm.controls.artTypeTag.value;
    this.newArtType.artTypeSequence    = this.artTypeForm.controls.artTypeSequence.value;
    this.newArtType.artTypeSubDepName  = this.artTypeForm.controls.artTypeSubDepName.value;
    this.newArtType.artTypeStatus      = this.artTypeForm.controls.artTypeStatus.value;
    
    let subDepId = this.globalVar.getSubDepIdFromSubDepName(this.newArtType.artTypeSubDepName);
    this.newArtType.artTypeSubDepId    = subDepId;
  }
  private copyNewArtTypeDataToDepForm(){
    this.artTypeForm.patchValue( { artTypeId:this.newArtType.artTypeId,artTypeDate:this.newArtType.artTypeDate,artTypeName:this.newArtType.artTypeName, 
                                  artTypeDescription:this.newArtType.artTypeDescription,artTypeTag:this.newArtType.artTypeTag,
                                  artTypeSequence:this.newArtType.artTypeSequence,artTypeSubDepName:this.newArtType.artTypeSubDepName,
                                  artTypeSubDepId:this.newArtType.artTypeSubDepId,artTypeStatus:this.newArtType.artTypeStatus} );
    this.artTypeForm.get('artTypeDate').patchValue(new Date(this.newArtType.artTypeDate));
  }
  private cleanArtTypeFormData(){
    this.artTypeForm.patchValue( {  artTypeName:'',artTypeDescription:'',artTypeTag:'',artTypeSubDepName:this.localFactTypesSubDepsRecordList[0].value,
                                    artTypeSubDepId:'',artTypeStatus:true} );
  }
  /*---------------------------------------ARTTYPES CREATE  -- GENERAL -----------------------------------------------------------*/ 
  /* ---------------------------------------ARTTYPES CREATE BTN CLICK-----------------------------------------------------------*/
  public closeArtTypesCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.globalVar.getAllArtTypes();
      //this.bsModalRef.content.callback(this.newArtType);
      this.result.next(this.newArtType);
      this.bsModalRef.hide();
    }
  }
  public cleanFormArtTypeBtnClick(){
    this.createArtType = true;
    this.editArtType = false;
    this.cleanArtTypeFormData();
  }
  public changeDisableArtTypeTagBtnClick(){
    if (this.disableArtTypeTAG === true) {
      this.artTypeForm.get('artTypeTag').enable(); 
      this.disableArtTypeTAG = false;
    } else {
      this.artTypeForm.get('artTypeTag').disable(); 
      this.disableArtTypeTAG = true;
    }
  }
  public createArtTypeBtnClick(artTypeForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> createArtTypeBtnClick ->...Starting...->', null);
    this.copyArtTypeFormDataToArtType();
    this.createOneArtType(this.newArtType);
  }
  public updateArtTypeBtnClick(artTypeForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> updateArtTypeBtnClick ->...Starting...->', null);
    this.copyArtTypeFormDataToArtType();    
    if ((this.editArtType === true) && (this.disableArtTypeTAG === true)) {
      this.updateOneArtType(this.newArtType);
    } 
    if ((this.editArtType === true) && (this.disableArtTypeTAG === false)) {
      this.updateOneArtTypeSpecial(this.newArtType);
    } 
  }
  /* ---------------------------------------ARTTYPES CREATE --GET ARTTYPES ---------------------------------------------------------*/
  private createOneArtType(newArtType: ArtType): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> createOneArtType ->...Starting...->',null);
    this._artsService.createOneArtType(newArtType)
      .subscribe({next:(data) => { this.newArtType = data;
                           this.closeArtTypesCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getOneArtType(modArtType: ArtType): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> getOneArtType ->...Starting...->',null);
    this._artsService.getOneArtType(modArtType)
      .subscribe({next:(data) => { this.newArtType = data;
                           this.copyNewArtTypeDataToDepForm();                          
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneArtType(modArtType: ArtType): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> updateOneArtType ->...Starting...->',null);
    this._artsService.updateOneArtType(modArtType)
      .subscribe({next:(data) => { this.newArtType = data;
                           this.closeArtTypesCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneArtTypeSpecial(modArtType: ArtType): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ART TYPES CREATE -> updateOneArtTypeSpecial ->...Starting...->',null);
    this._artsService.updateOneArtTypeSpecial(modArtType)
      .subscribe({next:(data) => { this.newArtType = data;
                           this.closeArtTypesCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}