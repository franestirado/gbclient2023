import { Component, OnInit } from '@angular/core';
import { throwError, Subject} from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { SubDep, Art, ArtType, ArtString, Dep } from './art';
import { ArtService } from './art.service';
import { ArtsCreateComponent } from './arts-create.component';

@Component({
  selector      :'app-arts-list',
  templateUrl   :'./arts-list.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class ArtsListComponent implements OnInit {
  public artsTitle          :string;
  public artsDate           :Date;
  public selectedArtType    :ArtType;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'edit'              :'Editar',
    'delete'            :'Borrar',
    'arts'              :'Artículos',
    'artsList'          :'Lista de Artículos',
    'addArts'           :'Arts',
    'artsTotalString'   :'Arts',
    'addArtTypes'       :'TiposArts',
    'dep'               :'Departamento',
    'selectedDep'       :'Departamento Seleccionado',
    'subDep'            :'Subdepartamento',
    'selectedSubDep'    :'Subdepartamento Seleccionado',
    'artType'           :'Tipo de Artículo',
    'selectedArtType'   :'Tipo de Artículo Seleccionado',
    'artDeleteOne'      :'¿ Seguro que quiere BORRAR este ARTÍCULO ?',
    'artDeleteAll'      :'¿ Seguro que quiere BORRAR TODOS los ARTÍCULOS ?',
    'listArtFacts'      :'InFacts',
    'listArtFactModels' :'InModels',
  };
  public formLabels = {
    'id'                :'#####',
    'artId'             :'Id',
    'artDate'           :'Fecha',
    'artType'           :'Tipo.Art.',
    'artReference'      :'Referencia',
    'artName'           :'Artículo',
    'artLongName'       :'Descripción',
    'artUnitPrice'      :'€/Ud.',
    'artPackageUnits'   :'Uds/Pk.',
    'artPackagePrice'   :'€/Pk.',
    'artTax'            :'IVA',
  };
  private createArt             :boolean;
  public  showArtsList          :boolean;
  public  arts                  :Art[];
  private newArt                :Art;
  public  showArtTypesList      :boolean;
  public  searchArt             :ArtString;
  public  showSelectedArtType   :boolean;
  public  showSearchArts        :boolean;
  public  orderArtsList         :string;
  public  reverseArtsList       :boolean;
  public  selectedDep           :Dep;
  public  selectedSubDep        :SubDep;
  public  caseInsensitive       :boolean;
  private sortedCollection      :any[];
  private logToConsole          :boolean;
  //--------------------------------------- ARTS LIST -- CONSTRUCTOR -----------------------------------------------------------
  constructor ( private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _errorService: ErrorService, private _artService: ArtService) { 

  }
  //--------------------------------------- ARTS LIST -- NG ON INIT -----------------------------------------------------------
  ngOnInit(): void {
    this.searchArt = new ArtString();
    this.orderArtsList = 'artName';
    this.reverseArtsList = false;
    this.caseInsensitive = true;
    this.showSearchArts = false;
    this.showSelectedArtType = false;
    this.showArtsList    = false;   
    this.getArtsList();
  }
  //--------------------------------------- ARTS LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTS LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTS LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTS LIST -- FORMS --------------------------------------------------------------
  //--------------------------------------- ARTS LIST -- GENERAL ------------------------------------------------------------
  //--------------------------------------- ARTS LIST -- BTN CLICK -----------------------------------------------------------
  public  editArtBtnClick(editArt: Art){
    if (this.isArtNotBlocked(editArt)){ 
      this.newArt = editArt;
      this.createArt = false;
      this.createArtModal(); 
    }
  } 
  public  createArtBtnClick(){
    this.createArt = true;
    this.newArt = new Art()
    this.createArtModal(); 
  } 
  private createArtModal(){
    const modalInitialState = {
      newArt      :this.newArt,
      artsTitle   :'Artículos',
      artsDate    :this.artsDate,
      createArt   :this.createArt,
      callback    :'OK', 
    }
    this.globalVar.openModal(ArtsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{ this.getArtsList(); }) 
  }
  private isArtNotBlocked(selectedArt: Art):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> isArtNotBlocked ->...Starting...->', null);
    if (selectedArt.artStatus === false){
      this._errorService.showErrorModal( "ARTÍCULOS", "ARTÍCULOS", "ARTÍCULO BLOQUEADO"+"->",
                        "Artículo->"+selectedArt.artName, "Descripión->"+selectedArt.artLongName, 
                        "Tipo->"+selectedArt.artType, "Referencia->"+selectedArt.artReference,
                        "Id->"+selectedArt.artId, "Tamaño->"+selectedArt.artSize);
      return false;
    } else return true;
  }
  public  lockArtBtnClick(modArt: Art){
    if (modArt.artStatus === true) modArt.artStatus = false;
    else  modArt.artStatus = true;
    this.updateOneArt(modArt);
  }
  public  searchArtsBtnClick() {
    if (this.showSearchArts === true) {
      this.showSearchArts = false;
    } else {
      this.showSearchArts = true;
    }
  }
  public  setOrderBtnClick(value: string) {
    if (this.orderArtsList === value) {
      this.reverseArtsList = !this.reverseArtsList;
    }
    this.orderArtsList = value;
  }
  public closedArtsListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.newDep);
      this.result.next('OK');
      this.bsModalRef.hide();
      }
  } 
  //--------------------------------------- ARTS LIST -- DATABASE -------------------------------------------------------------------
  private getArtsList(){
    if (this.selectedArtType === null) this.getAllArts();
    else {
      this.showSelectedArtType = true;
      this.selectedSubDep = this.globalVar.getSubDepObjectFromSubDepId(this.selectedArtType.artTypeSubDepId);
      this.selectedDep = this.globalVar.getDepObjectFromDepId(this.selectedSubDep.subDepDepId);
      this.getAllArtsInArtType(this.selectedArtType);
    }
  }
  private getAllArts() {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArts ->...Starting...->', null);
    this._artService.getAllArts()
      .subscribe({next:(data) => { this.arts = data;
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                            this.arts = this.sortedCollection;
                            this.formTitles.artsTotalString =this.arts.length+'..Arts'; 
                            this.showArtsList    = true;   
                            },
                 error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getAllArtsWithArtTypeTag(selArtTypeTag :string){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArtsWithArtTypeTag ->...Starting...->', null);
    this._artService.getAllArtsWithArtTypeTag(selArtTypeTag)
      .subscribe({next:(data) => { this.arts = data;
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                            this.arts = this.sortedCollection;
                            this.formTitles.artsTotalString =this.arts.length+'..Arts'; 
                            this.showArtsList    = true;   
                            },
                 error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getAllArtsInArtType(selArtType :ArtType){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArtsInArtType ->...Starting...->', null);
    this._artService.getAllArtsInArtType(selArtType)
      .subscribe({next:(data) => { this.arts = data;
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                            this.arts = this.sortedCollection;
                            this.formTitles.artsTotalString =this.arts.length+'..Arts'; 
                            this.formTitles.artsList ='Lista Artículos del Tipo de Artículo ==> '+this.selectedArtType.artTypeName;
                            this.formTitles.dep ='Departamento ... '+this.selectedDep.depName;
                            this.formTitles.selectedDep ='->Nombre: '+this.selectedDep.depName+'->Descripcion: '+this.selectedDep.depDescription+
                                                         '->DepTag: '+this.selectedDep.depTag+'->DepId: '+this.selectedDep.depId+
                                                         '->Fecha: '+this.selectedDep.depDate;
                            this.formTitles.subDep ='Subdepartamento ... '+this.selectedSubDep.subDepName;
                            this.formTitles.selectedSubDep = '->Nombre: '+this.selectedSubDep.subDepName+'->Descripcion: '+this.selectedSubDep.subDepDescription+
                                                             '->DepTag: '+this.selectedSubDep.subDepTag+'->DepId: '+this.selectedSubDep.subDepId+
                                                             '->Fecha: '+this.selectedSubDep.subDepDate;
                            this.formTitles.artType ='Tipo de Artículo ... '+this.selectedArtType.artTypeName;
                            this.formTitles.selectedArtType = '->Nombre: '+this.selectedArtType.artTypeName+'->Descripcion: '+this.selectedArtType.artTypeDescription+
                                                              '->DepTag: '+this.selectedArtType.artTypeTag+'->DepId: '+this.selectedArtType.artTypeId+
                                                              '->Fecha: '+this.selectedArtType.artTypeDate;                                                        
                            this.showArtsList    = true;   
                            },
                 error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneArt(modArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> updateOneArt ->...Starting...->', modArt);
    this._artService.updateOneArt(modArt)
      .subscribe({next:(data) => { this.getArtsList();},
                 error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- ARTS LIST -- DELETE ------------------------------------------------------------
  public  confirmDeleteOneArtBtnClick(art: Art){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.arts,this.formTitles.arts,
      art,this.formTitles.artDeleteOne,art.artName,art.artLongName,art.artType,art.artMeasurement,"","",)
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteOneArt(art); } })  
  }
  public  confirmDeleteAllArtsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.arts,this.formTitles.arts,null,this.formTitles.artDeleteAll,"","","","","","")
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteAllArts(); } })  
  } 
  private deleteAllArts(){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteAllArts ->...Starting...->', null);
    this._artService.delAllArts()
    .subscribe({next:(data) => { this.getArtsList();},
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };}});
  }
  private deleteOneArt(delArt: Art){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteOneArt ->...Starting...->', null);
    this._artService.delOneArt(delArt)
    .subscribe({next:(data) => {this.getArtsList(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}