import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

class DateObject {
    dateRange : Date[];    
  }
@Component({
  selector: 'app-enterRange-date',
  template: `
  <div class="card bg-enterDate text-primary rounded border border-primary">
      <nav class="navbar navbar-expand-lg navbar-light text-primary bg-enterDate rounded border border-primary">      
        <a class="navbar-brand text-primary bg-enterDate rounded ">
            {{dateTitle1}} <i class="fas fa-arrow-alt-circle-right"></i>  
        </a> 
        <a class="navbar-brand text-primary  mx-auto bg-enterDate rounded ">
            {{dateTitle2}}  
        </a>         
        <div class="btn-group ml-auto" role="group">
            <button class="btn btn-outline-primary rounded mr-2" (click)="closeEnterDateRangeModalBtnClick()"><i class="fas fa-reply-all"></i></button>
        </div>  
      </nav>
      <div class="card-body">
        <div class="text-center" ng-if="!dateMessage">{{dateMessage}}</div>
        <div [formGroup]="dateForm">
            <div class="form-row ">
                <div class="form-group row">
                    <label for="dateRange" class="col-5 mx-auto col-form-label">{{dateLabel}}</label>
                    <input type="text" class="col-5 form-control" placeholder="Datepicker" bsDaterangepicker [bsConfig]="datePickerConfig"                    
                            value="{{ dateObjet.dateRange }}" formControlName="dateRange" style="width:400px" />
                </div>
                <div class="mx-auto" >     
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" (click)="saveDateBtnClick()"><span><i class="far fa-check-circle"></i></span></button> 
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="card-footer text-center">   
        <div ng-if="!dateFooter"> <p>{{dateFooter}}</p> </div>        
      </div>       
  </div>
  `
})

export class EnterDateRangeComponent implements OnInit {
  public datePickerConfig       :Partial <BsDatepickerConfig>;  
  public dateForm               :FormGroup;
  public dateObjet              :DateObject;
  public dateTitle1             :string;
  public dateTitle2             :string;
  public dateMessage            :string;
  public dateLabel              :string;  
  public dateFooter             :string;
  public dateRange              :Date[];
  public minDate                :Date;
  public maxDate                :Date;
  public dateContainerClass     :string;
  public dateInputFormat        :string;
  public rangeInputFormat       :string;
  callback: any;
  result: Subject<any> = new Subject<any>();

  dateErrorRequired:    'Hay que introducir una fecha';

  constructor ( public bsModalRef: BsModalRef, private fb: FormBuilder ) {  }
 
  ngOnInit(): void {
    this.dateForm = this.fb.group({
        dateRange: null,
      });    
    this.dateForm.get('dateRange').setValue(this.dateRange);
    this.dateObjet = new DateObject();
    this.datePickerConfig = Object.assign ( {}, {
        containerClass:     this.dateContainerClass,
        showWeekNumbers:    true,
        minDate:            this.minDate,      
        maxDate:            this.maxDate,
        dateInputFormat:    this.dateInputFormat,
        rangeInputFormat:    this.rangeInputFormat
        } );
    this.dateObjet.dateRange = this.dateRange;    
  }
  closeEnterDateRangeModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      } 
  }
  saveDateBtnClick() {
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.dateForm.get('dateRange').value);
      this.result.next(this.dateForm.get('dateRange').value);
      this.bsModalRef.hide();
    }
  }
}