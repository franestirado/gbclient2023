import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';
import { GlobalVarService } from './global-var.service';

@Component({
  selector:     'app-select-month',
  templateUrl:  './select-month.component.html',
  styleUrls:    ['./common.component.css']
})

export class SelectMonthComponent implements OnInit {
  public dateHeader         :string;
  public dateTitle          :string;
  public changeDateFlag     :boolean;
  public changeDayFlag      :boolean;
  public changeMonthFlag    :boolean;
  public changeYearFlag     :boolean;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public tLabels = { 
    'header':           'CAMBIO FECHA TRABAJO',
    'currentDate':      'Fecha Actual',
    'workingDate':      'Fecha Trabajo',  
    'workingDay':       'Día de Trabajo',  
    'workingMonth':     'Mes de Trabajo',  
    'workingYear':      'Año de Trabajo',
    'changeDate':       'Cambiar Fecha Trabajo',  
    'changeDay':        'Cambiar Día Trabajo',  
    'changeMonth':      'Cambiar Mes Trabajo',  
    'changeYear':       'Cambiar Año Trabajo',   
  }
  public currentDate:       Date;
  public localWorkingDate:  Date;
  public localWorkingDay:   number;
  public localWorkingMonthString: string;
  public localWorkingYear:  number;

  public datePickerConfig:    Partial <BsDatepickerConfig>; 
  public dayPickerConfig:     Partial <BsDatepickerConfig>; 
  public monthPickerConfig:   Partial <BsDatepickerConfig>; 
  public yearPickerConfig:    Partial <BsDatepickerConfig>; 
  private localMinMode:       BsDatepickerViewMode;
  private dateContainerClass: string;
  private dateInputFormat:    string;

  constructor ( public bsModalRef: BsModalRef, public globalVar: GlobalVarService ) {  }
 
  ngOnInit(): void { 
    this.currentDate = new Date();
    this.localWorkingDate = this.globalVar.workingDate;   
    this.dateInputFormat = 'DD-MMM-YYYY';
    this.dateContainerClass = 'theme-dark-blue';    
    this.setDatesAndPickers(this.localWorkingDate); 
  }
  getClass(showFlag){
    var classList='';
    if (showFlag === true) { classList = 'bg-highlight text-dark font-weight-bold';  }
    return classList;
  }
  setDatesAndPickers(selectedDate: Date){
    this.changeAllDates(selectedDate);
    if (this.changeDateFlag === true) this.setDatePickerConfig(selectedDate); 
    if (this.changeDayFlag === true) this.setDayPickerConfig(selectedDate); 
    if (this.changeMonthFlag === true) this.setMonthPickerConfig(selectedDate);   
    if (this.changeYearFlag === true) this.setYearPickerConfig(selectedDate);
  }
  changeAllDates(selectedDate: Date){    
    this.localWorkingDay          = selectedDate.getDate();
    this.localWorkingMonthString  = selectedDate.toLocaleString('default', { month: 'long' });
    this.localWorkingYear         = selectedDate.getFullYear();
  }
  onDateChanged(changedDate: Date){
    this.localWorkingDate  = changedDate;
    this.setDatesAndPickers(this.localWorkingDate);
  }
  onDayChanged(changedDate: Date){
    this.localWorkingDate  = changedDate;
    this.setDatesAndPickers(this.localWorkingDate);
  }
  onMonthChanged(changedDate: Date){
    var month = changedDate.getMonth();
    this.localWorkingDate  = new Date(this.localWorkingDate.setMonth(month));
    this.setDatesAndPickers(this.localWorkingDate);
  }
  onYearChanged(changedDate: Date){
    var year = changedDate.getFullYear();
    this.localWorkingDate  = new Date(this.localWorkingDate.setFullYear(year));
    this.setDatesAndPickers(this.localWorkingDate);
  }
  saveDateBtnClick() {
    if (this.bsModalRef.content.callback != null){
      this.globalVar.changeWorkingDates(this.localWorkingDate);
      //this.bsModalRef.content.callback(this.localWorkingDate);
      this.result.next(this.localWorkingDate);
      this.bsModalRef.hide();
    }
  }
  closeModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(null);
      this.result.next(null);
      this.bsModalRef.hide();
    }
  }
  setDatePickerConfig(selectedDate: Date){
    var year = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var day = selectedDate.getDate();
    var fiveYearsBefore = new Date(year - 5, month, day);
    var fiveYearsAfter = new Date(year + 5, month, day);
    this.localMinMode = "day";
    this.datePickerConfig = Object.assign ( {}, {
        containerClass:     this.dateContainerClass,
        showWeekNumbers:    true, 
        minDate:            fiveYearsBefore,      
        maxDate:            fiveYearsAfter,
        dateInputFormat:    this.dateInputFormat,
        minMode:            this.localMinMode,
        adaptivePosition:   true,
        isAnimated:         true,
        } ); 
  }
  setDayPickerConfig(selectedDate: Date){
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var firstDayOfMonth = new Date(year,month,1);
    var lastDayOfMonth  = new Date(year,month+1,0); 
    this.localMinMode =  "day";
    this.dayPickerConfig = Object.assign ( {}, {
      containerClass:     this.dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            firstDayOfMonth,      
      maxDate:            lastDayOfMonth,
      dateInputFormat:    this.dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } );
  }
  setMonthPickerConfig(selectedDate: Date){
    var year = selectedDate.getFullYear();
    var firstDayOfYear = new Date(year,0,1);
    var lastDayOfYear  = new Date(year,11,31); 
    this.localMinMode = "month";
    this.monthPickerConfig = Object.assign ( {}, {
      containerClass:     this.dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            firstDayOfYear,     
      maxDate:            lastDayOfYear,
      dateInputFormat:    this.dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } );
  }
  setYearPickerConfig(selectedDate: Date){
    var selectedDate = new Date();
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var day   = selectedDate.getDate();
    var fiveYearsBefore = new Date(year - 5, month, day);
    var fiveYearsAfter  = new Date(year + 5, month, day);
    this.localMinMode = "year";
    this.yearPickerConfig = Object.assign ( {}, {
      containerClass:     this.dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            fiveYearsBefore,      
      maxDate:            fiveYearsAfter,
      dateInputFormat:    this.dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } ); 
  }
}
