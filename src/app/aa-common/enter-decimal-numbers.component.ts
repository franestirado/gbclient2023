import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { ValidationService } from './validation.service';
@Component({
  selector: 'app-enter-decimal-numbers',
  templateUrl: './enter-decimal-numbers.component.html',
  styleUrls: ['./common.component.css']
})
export class EnterDecimalNumbersComponent implements OnInit {
  /*-------------------------------------- ENTER DECIMAL NUMBER -- INPUT PARAMETERS-----------------------------------------------------------*/
  public displayMessage1    :string;
  public displayMessage2    :string;
  public displayMessage3    :string;
  public displayLabel       :string;  
  public displayNumber      :number;
  public callback           :any;
  result: Subject<any> = new Subject<any>();

  public displayForm : FormGroup;
  /*--------------------------------------- ENTER DECIMAL NUMBER -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(public bsModalRef: BsModalRef, private fb:FormBuilder,) { }
  /*--------------------------------------- ENTER DECIMAL NUMBER -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {    
    this.displayForm = this.fb.group({
      updateLabel:  [ {value: this.displayLabel, disabled:true} ],
      updateNumber: [ {value: this.displayNumber}, [Validators.required, ValidationService.decimalValidation] ],
    }) 
    if (this.displayNumber === 0) {
      this.displayForm.get('updateNumber').setValue('');       
    } else {
      this.displayForm.get('updateNumber').setValue(this.displayNumber.toFixed(2));   
    }
  }
  returnNumber() {
    if (this.bsModalRef.content.callback != null){
      this.displayNumber = this.displayForm.get('updateNumber').value;
      //this.bsModalRef.content.callback(this.displayNumber);
      this.result.next(this.displayNumber);
      this.bsModalRef.hide();
    }
  }
  closeModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      }
  }
}