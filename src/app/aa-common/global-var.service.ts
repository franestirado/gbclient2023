import { Injectable } from '@angular/core';
import { Inject, LOCALE_ID,TemplateRef } from '@angular/core';
import { formatDate } from '@angular/common';

import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService,BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord } from '../dbc-fields/field';
import { FieldsService } from '../dbc-fields/fields.service';
import { EnterDateInformationComponent } from './enter-date-information.component';
import { Dep,SubDep,ArtType } from '../dbc-arts/art';
import { ArtService } from '../dbc-arts/art.service';

import { HdService } from '../dbx-hd/hd.service';
import { HdType } from '../dbx-hd/hd';

import { CardTpv } from '../dbx-cards/cards';
import { CardsService } from '../dbx-cards/cards.service';

import { Bares } from '../cfg-config/config';
import { ConfigService } from '../cfg-config/config.service';

import { Prov } from '../dbc-provs/prov';

@Injectable()
export class GlobalVarService {
  private localEnterDateModalRef                 :BsModalRef;

  public topView                        :number;
  public topData                        :number;
  public nextView                       :number;
 
  public currentUser                    :string;
  public authenticated                  :boolean;
  public showCurrentUser                :boolean;
  public currentBar                     :string;

  public errorMessage                   :string;
  public errorMsgType                   :string;
  public errorStatus                    :string;
  public errorError                     :string;
  public errorMsg1                      :string;
  public errorMsg2                      :string;
  public showErrorMsg1                  :boolean;
  public showErrorMsg2                  :boolean;
  public logToConsole                   :boolean;
  private localModalRef                 :BsModalRef;
  private localModalConfig              :ModalOptions;


  public valueRecordList                :ValueRecord [][];
  public screensRecordList              :ValueRecord [];
  public languagesRecordList            :ValueRecord [];
  public cifTypesRecordList             :ValueRecord [];
  public hdTypeTypesRecordList          :ValueRecord [];
  public hdTypePlacesRecordList         :ValueRecord [];
  public hdTypesRecordList              :ValueRecord [];
  public mProvTypesRecordList           :ValueRecord [];
  public artTaxesRecordList             :ValueRecord [];
  public artUnitsRecordList             :ValueRecord [];
  
  public artTypes                       :ArtType[];
  public artTypesNamesRecordList        :ValueRecord [];
  public orderArtTypesList              :string;
  public reverseArtTypesList            :boolean;

  public deps                           :Dep[];
  public depNamesRecordList             :ValueRecord[];
  public orderDepsList                  :string;
  public reverseDepsList                :boolean;

  public subDeps                        :SubDep[];
  public orderSubDepsList               :string;
  public reverseSubDepsList             :boolean;
  public factTypesSubDepsRecordList     :ValueRecord[];

  public factOrNoteRecordList           :ValueRecord[];

  public hdTypes                        :HdType[];
  public factPayTypesRecordList         :ValueRecord[];
  public bankAccountsRecordList         :ValueRecord[];
  public maqBRecordList                 :ValueRecord[];

  public  cardTpvs                      :CardTpv[];
  private orderCardTpvsList             :string;
  private reverseCardTpvsList           :boolean;
  public  cardTpvsRecordList            :ValueRecord[];

  public fieldNamesRecordList           :ValueRecord[];
  public tpvTypesRecordList             :ValueRecord[];
  public provsRecordList                :ValueRecord[];

  public dbUrlsRecordList               :ValueRecord[];
  public dbUserNamesRecordList          :ValueRecord[];
  public dbDriverClassNamesRecordList   :ValueRecord[];
  public dbDdlAutosRecordList           :ValueRecord[];
  public dbPhysNStrategysRecordList     :ValueRecord[]; 
  
  public  bares                         :Bares[];
  private newBar                        :Bares;
  public  orderBaresList                :string;
  public  reverseBaresList              :boolean;
  public  baresRecordList               :ValueRecord[];
  private selectedBar                   :Bares;
  public workingDate                    :Date;
  public currentDate                    :Date;
  public workingFirstDayOfMonth         :Date;
  public workingLastDayOfMonth          :Date;
  public workingFirstAndLastDayOfMonth  :Date[];
  public workingFirstDayOfYear          :Date;
  public workingLastDayOfYear           :Date;
  public workingFirstAndLastDayOfYear   :Date[];
  public workingMonth                   :number;
  public workingYear                    :number;
  public workingPreviousYear            :Date;
  public workingNextYear                :Date;
  public  caseInsensitive               :boolean;
  private sortedCollection              :any[];
  /*---------------------------------------GLOBAL VAR  -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  @Inject(LOCALE_ID) private locale: string, private modalService: BsModalService,
                private _errorService: ErrorService, private orderPipe: OrderPipe, private _configService: ConfigService, 
                private _cardService: CardsService,
                private _fieldsService: FieldsService, private _hdService: HdService, private _artService: ArtService ) {
    this.topView = 60;
    this.topData = 120;
    this.nextView = 180;
    this.showErrorMsg1 = false;
    this.showErrorMsg2 = false;
    this.currentUser = 'No User yet';
    this.currentBar = 'No Bar yet';
    this.authenticated = false;
    this.showCurrentUser = false;
    this.logToConsole = true;
    this.workingDate = new Date();
    this.currentDate = new Date(this.workingDate);
  }
  /* ----------------------------------------------- CONSOLE LOG ---------------------------------------------------*/
  consoleLog(logging: boolean, message: String , data: any) {
    if (logging === false) {return; }
    console.log(message);
    console.log(JSON.stringify(data, null, '    ') );
    return;
  }
  /* ----------------------------------------------- SET CURRENT BAR ---------------------------------------------------*/
  setCurrentBar(newCurrentBar: string) {
    this.currentBar = newCurrentBar;
    return;
  }
  /* ----------------------------------------------- GET CURRENT BAR ---------------------------------------------------*/
  getCurrentBar():string { 
    return this.currentBar;
  }
  /* ----------------------------------------------- WORKING DATES ---------------------------------------------------*/
  stringToDate(dateStr) {
    var parts = dateStr.split("-")
    return new Date(parts[2], parts[1] - 1, parts[0])
  }
  transformDate(date) {
    return formatDate(date, 'dd-MM-yyyy', this.locale);
  }
  prepareStringDatesValueRecordList(nameString: string, startDate: Date, endDate: Date): ValueRecord[] {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> prepareValueRecordList ->...Starting...->', null);
    let ValueRecordList = new Array();
    var options = {day: '2-digit', month: '2-digit', year: 'numeric'};
    let index = 0;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = nameString;
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(startDate); 
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(endDate);  
    return ValueRecordList; 
  }
  prepareDateRangeStringValueRecordList(startDate: Date, endDate: Date, nameString: string): ValueRecord[] {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> prepareValueRecordList ->...Starting...->', null);
    let ValueRecordList = new Array();
    var options = {day: '2-digit', month: '2-digit', year: 'numeric'};
    let index = 0;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(startDate); 
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(endDate);
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = nameString;
    return ValueRecordList; 
  }
  prepareFilterFactsValueRecordList(startDate :Date,endDate :Date, provName :String, factType :String, factPayType :String, factNote :String): ValueRecord[] {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> prepareFilterFactsValueRecordList->...Starting...->', null);
    let ValueRecordList = new Array();
    var options = {day: '2-digit', month: '2-digit', year: 'numeric'};
    let index = 0;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(startDate); 
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = this.transformDate(endDate);
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = provName;
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = factType;
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = factPayType;
    index = index + 1;
    ValueRecordList[index] = new ValueRecord();
    ValueRecordList[index].id = index;
    ValueRecordList[index].value = factNote;
    return ValueRecordList; 
  }
  changeWorkingDates(newWorkingDate :Date) {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> changeWorkingDates ->...Starting...->', null);
    this.workingDate = new Date(newWorkingDate);  
    this.workingYear = this.workingDate.getFullYear();
    this.workingMonth = this.workingDate.getMonth();

    this.workingFirstDayOfMonth = new Date(this.workingYear,this.workingMonth,1);
    this.workingLastDayOfMonth  = new Date(this.workingYear,this.workingMonth+1,0); 
    this.workingFirstAndLastDayOfMonth = [];
    this.workingFirstAndLastDayOfMonth.push(this.workingFirstDayOfMonth);
    this.workingFirstAndLastDayOfMonth.push(this.workingLastDayOfMonth);
      
    this.workingFirstDayOfYear = new Date(this.workingYear,0,1);
    this.workingLastDayOfYear  = new Date(this.workingYear,11,31);   
    this.workingFirstAndLastDayOfYear = [];
    this.workingFirstAndLastDayOfYear.push(this.workingFirstDayOfYear);
    this.workingFirstAndLastDayOfYear.push(this.workingLastDayOfYear); 

    this.workingPreviousYear = new Date(this.workingYear-1,0,1);
    this.workingNextYear = new Date(this.workingYear+1,11,31); ;
    /*
    if (this.logToConsole){
      console.log(JSON.stringify('workingDate......2........'+this.workingDate));
      console.log(JSON.stringify('workingFirstDayOfMonth...'+this.workingFirstDayOfMonth));
      console.log(JSON.stringify('workingLastDayOfMonth....'+this.workingLastDayOfMonth));
      console.log(JSON.stringify('workingYear..............'+this.workingYear));
      console.log(JSON.stringify('workingMonth.............'+this.workingMonth));
      console.log(JSON.stringify('workingFirstDayOfYear...'+this.workingFirstDayOfYear));
      console.log(JSON.stringify('workingPreviousYear....'+this.workingPreviousYear)); 
      console.log(JSON.stringify('workingNextYear....'+this.workingNextYear)); 
    }
    */
    return;
  }
  //----------------------------------------------- OPEN MODAL -- NGX BOOTSTRAP ---------------------------------------------------
  openModal(modalTemplate :any, modalInitialState :any, modalMode :string){
    var isAnimated = true;
    var isKeyboard = false;
    var isBackdrop = false;
    var isIgnoreBackdropClick = true;
    var isClass = 'modal-lg'
    switch (modalMode){
      case 'modalLmTop0Left0':  isClass = 'modal-lm modalTop-0-Left-0'; break;
      case 'modalLgTop0Left0':  isClass = 'modal-lg modalTop-0-Left-0'; break;
      case 'modalXlTop0Left0':  isClass = 'modal-xl modalTop-0-Left-0'; break;
      case 'modalLmTop0Left3':  isClass = 'modal-lm modalTop-0-Left-3'; break;
      case 'modalLgTop0Left3':  isClass = 'modal-lg modalTop-0-Left-3'; break;
      case 'modalXlTop0Left3':  isClass = 'modal-xl modalTop-0-Left-3'; break;
      case 'modalLmTop0Left6':  isClass = 'modal-lm modalTop-0-Left-6'; break;
      case 'modalLgTop0Left6':  isClass = 'modal-lg modalTop-0-Left-6'; break;
      case 'modalXlTop0Left6':  isClass = 'modal-xl modalTop-0-Left-6'; break;
      case 'modalLmTop0Left9':  isClass = 'modal-lm modalTop-0-Left-9'; break;
      case 'modalLgTop0Left9':  isClass = 'modal-lg modalTop-0-Left-9'; break;
      case 'modalXlTop0Left9':  isClass = 'modal-xl modalTop-0-Left-9'; break;
      default:                isClass = 'modal-lg'; break;
    }
    this.localModalRef = this.modalService.show( modalTemplate, {initialState : modalInitialState,
      animated              :isAnimated,
      keyboard              :isKeyboard,
      backdrop              :isBackdrop,
      ignoreBackdropClick   :isIgnoreBackdropClick,
      class                 :isClass,
      });
    return new Promise<any>((resolve, reject) => this.localModalRef.content.result.subscribe((result) => {
      resolve(result)
    } ));
  }
  //----------------------------------------------- ENTER ONE DAY OR DATES RANGE ---------------------------------------------------
  enterInformation( color: string, title: string, footer:string, dates :Date[], datesMode :string,
                        message1: string, message2: string, message3: string, message4: string, 
                        message5: string, message6: string, message7: string ) : Promise<any[]> {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> enterInformation ->...Starting...->', null);
    this.localEnterDateModalRef = this.modalService.show(EnterDateInformationComponent, {
      initialState: {        
        msgColor      :color,
        msgHeader     :title,
        msgFooter     :footer,
        msgDates      :dates,
        msgDatesMode  :datesMode,
        msg1          :message1,        
        msg2          :message2, 
        msg3          :message3, 
        msg4          :message4, 
        msg5          :message5, 
        msg6          :message6, 
        msg7          :message7, 
        callback: (resultDateRange:any[]) => {
          this.consoleLog(this.logToConsole, '---> GlobalVar ---> enterInformation ->...callback...->',resultDateRange); 
        }
      },
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: false,
      class: 'modal-lm'  //modal-dialog-centered
      });    
      return new Promise<any[]>((resolve, reject) => this.localEnterDateModalRef.content.resultSubjectDatesRange.subscribe((resultDateRange) => {
        resolve(resultDateRange)
      } ));
    }
  /* ----------------------------------------------- USERS ---------------------------------------------------*/
  changeShowCurrentUser() {
    if (this.showCurrentUser === true) {this.showCurrentUser = false; } else {this.showCurrentUser = true; }
    return (this.showCurrentUser);
  }
  /* ----------------------------------------------- ERRORS ---------------------------------------------------*/
  changeShowErrorMsg1() {
    if (this.showErrorMsg1 === true) {this.showErrorMsg1 = false; } else {this.showErrorMsg1 = true; }
    return (this.showErrorMsg1);
  }
  changeShowErrorMsg2() {
    if (this.showErrorMsg2 === true) {this.showErrorMsg2 = false; } else {this.showErrorMsg2 = true; }
    return (this.showErrorMsg1);
  }

  handleError(error: HttpErrorResponse) {
    // return an ErrorObservable with a user-facing error message
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred.
      this.errorMessage = 'Client Error Happened'
      this.errorMsgType = 'Client site';
      this.errorStatus =  error.error.type;
      this.errorMsg1 = error.error.message;
      this.errorError = error.error.error;
      this.showErrorMsg1 = true;
      if (this.logToConsole) {
        console.log(JSON.stringify('---> GlobalVar--->  handleError ->......Client Error.....error.error.message...->'));
        console.log(error.error.message); 
      }
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      this.errorMessage = 'Server Error Happened'
      this.errorMsgType = 'Server site';
      this.errorStatus = error.status.toString();
      this.errorMsg1 = error.message;
      this.errorError = error.name;
      this.showErrorMsg1 = true;
      if (this.logToConsole) {
        console.log(JSON.stringify('---> GlobalVar --->  handleError ->......Server Error.....error.status...->'));
        console.log(error.status); 
        console.log(JSON.stringify('---> GlobalVar --->  handleError ->......Server Error.....error.message...->'));
        console.log(error.message); 
      }      
      switch (error.status) {
        case 400 : {
                  this.errorMsg2 = 'S->Petición Erronea';                        
                  this.showErrorMsg2 = true;
                  this.showErrorMsg1 = false;
                  break;
                  }
        case 409 : {
                  this.errorMsg2 = 'S->Ya exite';                        
                  this.showErrorMsg2 = true;
                  this.showErrorMsg1 = false;
                  break;
                  }
        case 422 : {
                  this.errorMsg2 = 'S->Parámetros Invalidos';                       
                  this.showErrorMsg2 = false;
                  break;
                  }
        case 424 : {
                  this.errorMsg2 = 'S->Borrado NO Permitido, Hay Depedencias';                       
                  this.showErrorMsg2 = false;
                  break;
                  }
        default:  {
                  this.errorMsg2 = 'S->Something bad happened; please try again later';                        
                  this.showErrorMsg2 = false;
                  }
      }
    }
    this._errorService.showMsgObject( "errorMsg","... Ha ocurrido un ERROR ...","... Ha ocurrido un ERROR ...",
                                      error.error,"... Notifiquelo para que se corrija ...",this.errorMsg2,this.errorStatus,
                                      this.errorMessage,this.errorMsgType,this.errorMsg1,"");
/*
    this._errorService.showErrorModal('... Ha ocurrido un ERROR ...','... Notifiquelo para que se corrija ...',
                this.errorMsg1, this.errorMsg2,this.errorMessage, this.errorMsgType, this.errorStatus, this.errorError, 
                "GlobalVarService --> handleError");*/

    if (this.showErrorMsg2) { return false; }
    else {return throwError(() => error); }    
  }  
  /*----------- PREPARE VALUE RECORD LIST ----> CARDTPVS - BARES ---------------------------------------------------*/
  prepareProvsValueRecordList(provs: Prov[]): ValueRecord[]{
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareProvsValueRecordList ->...Starting...->',null);  
    this.provsRecordList = new Array();    
    let index = 0;
    this.provsRecordList[index] = new ValueRecord();
    this.provsRecordList[index].id = 0;
    this.provsRecordList[index].value = 'Select...Proveedor';
    index = index + 1;
    for (let i = 0 ; i < (provs.length) ; i++ ) {
        if (provs[i].provStatus === true) {
          this.provsRecordList[index] = new ValueRecord();
          this.provsRecordList[index].id = index;
          this.provsRecordList[index].value = provs[i].provName;
          index = index + 1;
        }      
    }
    return this.provsRecordList;
  }
  prepareRecordListFromArrayOfStrings(selectedStrings: string[]): ValueRecord[]{
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareRecordListFromArrayOfStrings ->...Starting...->',null);  
    this.factOrNoteRecordList = new Array();    
    let index = 0;
    this.factOrNoteRecordList[index] = new ValueRecord();
    this.factOrNoteRecordList[index].id = 0;
    this.factOrNoteRecordList[index].value = 'Select...Factura o Nota';
    index = index + 1;
    for (let i = 0 ; i < (selectedStrings.length) ; i++ ) {
          this.factOrNoteRecordList[index] = new ValueRecord();
          this.factOrNoteRecordList[index].id = index;
          this.factOrNoteRecordList[index].value = selectedStrings[i];
          index = index + 1;             
    }
    return this.factOrNoteRecordList;
  }
  /*--------------------------------------- GET ALL BARES AND PREPARE BARES VALUE RECORD LIST------------------------------------------------------------*/
  getAllBaresAndSetBar(barName :string) {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> getAllBares ->...Statring...->', null);
    this._configService.getAllBares()
      .subscribe({next:(data) => {
                          this.bares = data;
                          this.orderBaresList = 'barName';
                          this.reverseBaresList = false;
                          this.sortedCollection = this.orderPipe.transform(this.bares,this.orderBaresList,this.reverseBaresList,this.caseInsensitive);
                          this.bares = this.sortedCollection;
                          this.prepareBaresValueRecordList(this.bares);
                          this.changeWorkingBarByBarName(barName);
                         },
                error:(error) => { if (this.handleError(error)) { throwError(() => error) }; }});
  }
  prepareBaresValueRecordList(bares: Bares[]): ValueRecord[]{
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareBaresValueRecordList ->...Starting...->',null);  
    this.baresRecordList = new Array();    
    let index = 0;
    this.baresRecordList[index] = new ValueRecord();
    this.baresRecordList[index].id = 0;
    this.baresRecordList[index].value = 'Select...BAR de trabajo';
    index = index + 1;
    for (let i = 0 ; i < (bares.length) ; i++ ) {
        if (bares[i].barStatus === true) {
          this.baresRecordList[index] = new ValueRecord();
          this.baresRecordList[index].id = index;
          this.baresRecordList[index].value = bares[i].barName;
          index = index + 1;
        }      
    }
    return this.baresRecordList;
  }
  getBarObjectfromBarName(barName:string):Bares{
    for(let j=0; j<this.bares.length; j++){
      if (this.bares[j].barName === barName) return this.bares[j];
    }
    return null;    
  }
  changeWorkingBarByBarName(selectedBar: string): Bares {
    this.consoleLog(this.logToConsole,'---> GlobalVar ---> changeWorkingBarByBarName ->...Starting...->', selectedBar);
    this.selectedBar = this .getBarObjectfromBarName(selectedBar);
    this._configService.changeWorkingBar(this.selectedBar)
      .subscribe({next:(data) => {
                          this.newBar = data;
                          this.currentBar = this.newBar.barName;
                          this.getFactPayTypesAndBankAccountsFromHdTypes();
                          this.getAllCardTpvs();                          
                        },
                error :(error)=> { if (this.handleError(error)) { throwError(() => error) }; }});
    return this.newBar;
  }
  /* ----------------------- GET ALL CARD TPVS AND PREPARE CARD TPVS VALUE RECORD LIST ---------------------------------------------*/
  getAllCardTpvs() {
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> getAllCardTpvs ->...Starting...->', null);
    this._cardService.getAllCardTpvs()
      .subscribe({next:(data) => {
                          this.cardTpvs = data;
                          if ((data == null) || (data.length == 0)){ 
                            this.cardTpvsRecordList = null;
                            return;
                          } else {
                            this.orderCardTpvsList = 'cardTpvType';
                            this.reverseCardTpvsList = true;
                            this.sortedCollection = this.orderPipe.transform(this.cardTpvs,this.orderCardTpvsList,this.reverseCardTpvsList,this.caseInsensitive);
                            this.cardTpvs = this.sortedCollection;                          
                            this.prepareCardTpvsRecordList(this.cardTpvs);
                          }                                             
                         },
                error :(error)=> { if (this.handleError(error)) { throwError(() => error) }; }});
  } 
  prepareCardTpvsRecordList(selCardTpvs: CardTpv[]): ValueRecord[]{
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareCardTpvsRecordList ->...Starting...->', null);
    var orderSelCardTpvs :CardTpv[];
    orderSelCardTpvs = new Array();
    // Adding cardTvpType to cardTpvName
    for (let i = 0 ; i < (selCardTpvs.length) ; i++ ) {
      orderSelCardTpvs[i] = new CardTpv;
      orderSelCardTpvs[i].cardTpvStatus = selCardTpvs[i].cardTpvStatus;
      orderSelCardTpvs[i].cardTpvName = selCardTpvs[i].cardTpvType.substring(0,4)+"->"+selCardTpvs[i].cardTpvName;       
    }
    // Ordering cardTpvs by cardTpvName to have Visa first in selection
    this.orderCardTpvsList = 'cardTpvName';
    this.reverseCardTpvsList = true;
    this.sortedCollection = this.orderPipe.transform(orderSelCardTpvs,this.orderCardTpvsList,this.reverseCardTpvsList,this.caseInsensitive);
    orderSelCardTpvs = this.sortedCollection; 
    // Creating CardTpvs value record list
    this.cardTpvsRecordList = new Array();    
    let index = 0;
    this.cardTpvsRecordList[index] = new ValueRecord();
    this.cardTpvsRecordList[index].id = 0;
    this.cardTpvsRecordList[index].value = 'Select...Card TPV';
    index = index + 1;
    for (let i = 0 ; i < (orderSelCardTpvs.length) ; i++ ) {
        if (orderSelCardTpvs[i].cardTpvStatus === true) {
          this.cardTpvsRecordList[index] = new ValueRecord();
          this.cardTpvsRecordList[index].id = index;
          this.cardTpvsRecordList[index].value = orderSelCardTpvs[i].cardTpvName;
          index = index + 1;
        }      
    }
    return this.cardTpvsRecordList;
  }
  getCardTpvName(cardTpvId: number): string{
    var tpvName = 'TpvName not Found';
    if (this.cardTpvs === null)  return tpvName;
    if (this.cardTpvs.length < 1)  return tpvName;    
    for (var i=0; i<(this.cardTpvs.length); i++) {
      if (this.cardTpvs[i].cardTpvId === cardTpvId) tpvName = this.cardTpvs[i].cardTpvName;
    }
    return tpvName;
  }
  getCardTpvObjectFromTpvName(tpvName: string) : CardTpv{
    for(let j=0; j<this.cardTpvs.length; j++){
      if (this.cardTpvs[j].cardTpvName === tpvName) return this.cardTpvs[j];
    }
    return null;
  }
  /* -------------- GET ALL HD TYPES AND PREPARE FACTPAYTYPES AND BANKACCOUNTS RECORDS LIST ------------------------------*/
  getFactPayTypesAndBankAccountsFromHdTypes() {
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> getFactPayTypesAndBankAccountsFromHdTypes ->...Starting...->', null);
    this._hdService.getAllHdTypes()
      .subscribe({next:(data) => {
                          this.hdTypes = data;
                          this.prepareFactPayTypesAndBankAccountsRecordsList();                        
                          },
                error :(error)=> {
                          if (this.handleError(error)) { throwError(() => error) };
                          }});
  }
  checkDefaultFactPayTypeIsInCurrentBar(defaultFactPayType :string):boolean{
    for (let i = 0 ; i < this.factPayTypesRecordList.length ; i++ ) {
      if (this.factPayTypesRecordList[i].value === defaultFactPayType) return true;
    }
    return false;
  }
  prepareFactPayTypesAndBankAccountsRecordsList() {  
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareFactPayTypesAndBankAccountsRecordsList ->...Starting...->', null);

    this.factPayTypesRecordList = new Array();
    let sumIndex = 0;
    this.factPayTypesRecordList[sumIndex] = new ValueRecord();
    this.factPayTypesRecordList[sumIndex].id = 0;
    this.factPayTypesRecordList[sumIndex].value = 'Select...Forma Pago';
    sumIndex = sumIndex + 1;
    this.factPayTypesRecordList[sumIndex] = new ValueRecord();
    this.factPayTypesRecordList[sumIndex].id = 1;
    this.factPayTypesRecordList[sumIndex].value = 'HojaDia';
    sumIndex = sumIndex + 1;

    this.bankAccountsRecordList = new Array();
    let bankIndex = 0;
    this.bankAccountsRecordList[bankIndex] = new ValueRecord();
    this.bankAccountsRecordList[bankIndex].id = 0;
    this.bankAccountsRecordList[bankIndex].value = 'Select...Cuenta Banco';
    bankIndex = bankIndex + 1;

    this.maqBRecordList = new Array();
    let maqBIndex = 0;
    this.maqBRecordList[maqBIndex] = new ValueRecord();
    this.maqBRecordList[maqBIndex].id = 0;
    this.maqBRecordList[maqBIndex].value = 'Select...Máquina';
    maqBIndex = maqBIndex + 1;

    for (let i = 0 ; i < this.hdTypes.length ; i++ ) {
      if (this.hdTypes[i].hdTypePlace =='HD') {
        this.factPayTypesRecordList[1].value = this.hdTypes[i].hdTypeName;
      }
      if ( (this.hdTypes[i].hdTypePlace =='SUM') || (this.hdTypes[i].hdTypePlace =='RES') ){
        if ( (this.hdTypes[i].hdTypeType =='BANK') || (this.hdTypes[i].hdTypeType =='CAJA') ){
          this.factPayTypesRecordList[sumIndex] = new ValueRecord();
          this.factPayTypesRecordList[sumIndex].id = sumIndex;
          this.factPayTypesRecordList[sumIndex].value = this.hdTypes[i].hdTypeName;
          sumIndex = sumIndex + 1;
        }
        if (this.hdTypes[i].hdTypeType =='BANK') {
          this.bankAccountsRecordList[bankIndex] = new ValueRecord();
          this.bankAccountsRecordList[bankIndex].id = bankIndex;
          this.bankAccountsRecordList[bankIndex].value = this.hdTypes[i].hdTypeName;
          bankIndex = bankIndex + 1;
        }
        if (this.hdTypes[i].hdTypeType =='MAQB') {
          this.maqBRecordList[maqBIndex] = new ValueRecord();
          this.maqBRecordList[maqBIndex].id = maqBIndex;
          this.maqBRecordList[maqBIndex].value = this.hdTypes[i].hdTypeName;
          maqBIndex = maqBIndex + 1;
        }
      }
    }
  }

  /*------------ GET ALL DEPS AND PREPARE DEP NAMES RECORD LIST ---------------------------------------------------*/
  getAllDeps() {
    this.consoleLog(this.logToConsole,'----> GlobalVar ---> getAllDep s->...Starting...->', null);
    this._artService.getAllDeps()
      .subscribe({next:(data) => {
                          this.deps = data;
                          this.orderDepsList = 'depName';
                          this.reverseDepsList = false;
                          this.sortedCollection = this.orderPipe.transform(this.deps,this.orderDepsList,this.reverseDepsList,this.caseInsensitive);
                          this.deps = this.sortedCollection;
                          this.prepareDepNamesRecordList();
                          },
                error :(error)=> { if (this.handleError(error)) { throwError(() => error) }; }});
  }
  prepareDepNamesRecordList(){
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareDepNamesRecordList ->...Starting...->', null);
    this.depNamesRecordList = new Array();
    this.depNamesRecordList[0] = new ValueRecord();
    this.depNamesRecordList[0].id = 0;
    this.depNamesRecordList[0].value = 'Select...Dep';
    for (let i=0; i<this.deps.length; i++) {
      this.depNamesRecordList[i+1] = new ValueRecord();
      this.depNamesRecordList[i+1].id = i+1;
      this.depNamesRecordList[i+1].value = this.deps[i].depName;
    }
  }
  getDepNameFromDepId(selectedDepId:number):string{    
    let index = -1;
    for (let i=0; i<this.deps.length; i++) {
      if (this.deps[i].depId === selectedDepId) index = i;
    }
    if (index === -1) return null;
    else return (this.deps[index].depName);
  }
  getDepObjectFromDepId(selectedDepId:number):Dep{    
    let index = -1;
    for (let i=0; i<this.deps.length; i++) {
      if (this.deps[i].depId === selectedDepId) index = i;
    }
    if (index === -1) return null;
    else return (this.deps[index]);
  }
  getDepIdFromDepName(selecteDepName:string):number{    
    let index = -1;
    for (let i=0; i<this.deps.length; i++) {
      if (this.deps[i].depName === selecteDepName) index = i;
    }
    if (index === -1) return -1;
    else return (this.deps[index].depId);
  }
  /* -------------- GET ALL ARTYPES AND PREPARE ART TYPES NAMES RECORD LIST ------------------------------*/
  getAllArtTypes() {
    this.consoleLog(this.logToConsole,'-> GlobalVar -> getAllArtTypes ->...Starting...->', null);
    this._artService.getAllArtTypes()
      .subscribe({next:(data) => {
                          this.artTypes = data;
                          this.orderArtTypesList = 'artTypeName';
                          this.reverseArtTypesList = false;
                          this.sortedCollection = this.orderPipe.transform(this.artTypes,this.orderArtTypesList,this.reverseArtTypesList,this.caseInsensitive);
                          this.artTypes = this.sortedCollection;
                          this.prepareArtTypesNamesRecordList();
                          },
                error :(error)=> { if (this.handleError(error)) { throwError(() => error) }; }});
  }
  prepareArtTypesNamesRecordList(){
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareArtTypesNamesRecordList ->...Starting...->', null);
    this.artTypesNamesRecordList = new Array();
    this.artTypesNamesRecordList[0] = new ValueRecord();
    this.artTypesNamesRecordList[0].id = 0;
    this.artTypesNamesRecordList[0].value = 'Select...Tipo Art';
    for (let i=0; i<this.artTypes.length; i++) {
      this.artTypesNamesRecordList[i+1] = new ValueRecord();
      this.artTypesNamesRecordList[i+1].id = i+1;
      this.artTypesNamesRecordList[i+1].value = this.artTypes[i].artTypeName;
    }
  }
  getArtTypeNameFromArtTypeTag(artTypeTag:string):string{    
    let index = -1;
    for (let i=0; i<this.artTypes.length; i++) {
      if (this.artTypes[i].artTypeTag === artTypeTag) index = i;
    }
    if (index === -1) return null;
    else return (this.artTypes[index].artTypeName);
  }
  getArtTypeTagFromArtTypeName(artTypeName:string):string{    
    let index = -1;
    for (let i=0; i<this.artTypes.length; i++) {
      if (this.artTypes[i].artTypeName === artTypeName) index = i;
    }
    if (index === -1) return null;
    else return (this.artTypes[index].artTypeTag);
  }
  getArtTypeObjectFromArtTypeName(artTypeName:string):ArtType{    
    let index = -1;
    for (let i=0; i<this.artTypes.length; i++) {
      if (this.artTypes[i].artTypeName === artTypeName) index = i;
    }
    if (index === -1) return null;
    else return (this.artTypes[index]);
  }
  /* -------------- GET ALL SUBDEPS AND PREPARE FACTTYPES RECORD LIST WITH SUBDEP NAME ------------------------------*/
  getFactTypesFromSubDeps() {
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> getFactTypesFromSubDeps ->...Starting...->', null);
    this._artService.getAllSubDeps()
      .subscribe({next:(data) => {
                          this.subDeps = data;
                          this.orderSubDepsList = 'subDepName';
                          this.reverseSubDepsList = false;
                          this.sortedCollection = this.orderPipe.transform(this.subDeps,this.orderSubDepsList,this.reverseSubDepsList,this.caseInsensitive);
                          this.subDeps = this.sortedCollection;
                          this.prepareFactTypesRecordList();                        
                          },
                error :(error)=> {
                          if (this.handleError(error)) { throwError(() => error) };
                          }});
  }
  prepareFactTypesRecordList() {  
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareFactTypesRecordList ->...Starting...->', null);
    this.factTypesSubDepsRecordList = new Array();
    this.factTypesSubDepsRecordList[0] = new ValueRecord();
    this.factTypesSubDepsRecordList[0].id = 0;
    this.factTypesSubDepsRecordList[0].value = 'Select...Tipo Fact';
    for (let i=0; i<this.subDeps.length; i++) {
      this.factTypesSubDepsRecordList[i+1] = new ValueRecord();
      this.factTypesSubDepsRecordList[i+1].id = i+1;
      this.factTypesSubDepsRecordList[i+1].value = this.subDeps[i].subDepName;
    }
  }
  getSubDepNameFromSubDepTag(subDepTag:string):string{    
    let index = -1;
    for (let i=0; i<this.subDeps.length; i++) {
      if (this.subDeps[i].subDepTag === subDepTag) index = i;
    }
    if (index === -1) return null;
    else return (this.subDeps[index].subDepName);
  }
  getSubDepTagFromSubDepName(subDepName:string):string{    
    let index = -1;
    for (let i=0; i<this.subDeps.length; i++) {
      if (this.subDeps[i].subDepName === subDepName) index = i;
    }
    if (index === -1) return null;
    else return (this.subDeps[index].subDepTag);
  }
  getSubDepIdFromSubDepName(subDepName:string):number{    
    let index = -1;
    for (let i=0; i<this.subDeps.length; i++) {
      if (this.subDeps[i].subDepName === subDepName) index = i;
    }
    if (index === -1) return -1;
    else return (this.subDeps[index].subDepId);
  }
  getSubDepObjectFromSubDepId(subDepId:number):SubDep{    
    let index = -1;
    for (let i=0; i<this.subDeps.length; i++) {
      if (this.subDeps[i].subDepId === subDepId) index = i;
    }
    if (index === -1) return null;
    else return (this.subDeps[index]);
  }
  getIndexArtTaxesRecordlist(taxValue : string):number{
    let index = -1;
    for (let i=0; i<this.artTaxesRecordList.length; i++) {
      if (this.artTaxesRecordList[i].value === taxValue) index = i;
    }
    return index;
  }
  /* ----------------------------------------------- FIELDS AND VALUES ---------------------------------------------------*/
  getFieldsAndValues() {
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> getFieldsAndValues->...Starting...->', null);
    this._fieldsService.getFieldsAndValues()
          .subscribe({next:(data) => {
                          // this.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->data->', data);
                          this.valueRecordList = data;
                          this.prepareFieldsAndValuesRecordsList();       
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->valueRecordList->', this.globalVar.valueRecordList); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->screensRecordList->', this.globalVar.screensRecordList);                  
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->languagesRecordList->', this.globalVar.languagesRecordList);                                      
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->cifTypesRecordList->', this.globalVar.cifTypesRecordList);            
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->hdTypesRecordList->', this.globalVar.hdTypesRecordList); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->hdTypePlacesRecordList->', this.globalVar.hdTypePlacesRecordList); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->hdTypeTypesRecordList->', this.globalVar.hdTypeTypesRecordList); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->mProvTypesRecordList->', this.globalVar.mProvTypesRecordList ); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldsAndValues->factTypesRecordList->', this.globalVar.factTypesRecordList ); 
                          // this.globalVar.consoleLog(this.logToConsole, '--->GlobalVar---> getFieldAndValues->fieldNamesRecordList->', this.globalVar.fieldNamesRecordList ); 

                          },
                error :(error)=> {
                          this.consoleLog(this.logToConsole, '---> GlobalVar ---> getFieldsAndValues -> error ->', error);
                          this.showErrorMsg2 = true;
                          this.errorMsg2 = error;
                          }});
  }
  prepareFieldsAndValuesRecordsList() {
    this.consoleLog(this.logToConsole, '---> GlobalVar ---> prepareFieldsAndValuesRecordsList ->...Starting...->', null);
    this.currentBar                   = null;
    this.screensRecordList            = null;
    this.languagesRecordList          = null;
    this.cifTypesRecordList           = null;
    this.hdTypeTypesRecordList        = null;
    this.hdTypePlacesRecordList       = null;
    this.mProvTypesRecordList         = null;
    this.artTaxesRecordList           = null;
    this.artUnitsRecordList           = null;
    this.fieldNamesRecordList         = null;
    this.tpvTypesRecordList           = null;
    this.dbUrlsRecordList             = null;
    this.dbUserNamesRecordList        = null;
    this.dbDriverClassNamesRecordList = null;
    this.dbDdlAutosRecordList         = null;
    this.dbPhysNStrategysRecordList   = null;
    for (let i = 0 ; i < this.valueRecordList.length ; i++ ) {
      switch (this.valueRecordList[i][0].value) {
        case 'currentBar': {
          if (this.valueRecordList[i].length > 1) {
            this.currentBar = this.valueRecordList[i][1].value;
          } else {
            this.currentBar = 'NO current BAR defined yet';
          }
          
          break;
          }
        case 'screens': {
          this.screensRecordList = this.valueRecordList[i];
          this.screensRecordList[0].value = 'Select...' + this.screensRecordList[0].value;
          if ( (this.screensRecordList === null) || (this.screensRecordList.length === 1) ) {
            this.screensRecordList = [];
            this.screensRecordList[0] = new ValueRecord();
            this.screensRecordList[0].value = 'NO screens defined yet';
            }
          break;
          }
        case 'languages': {
          this.languagesRecordList = this.valueRecordList[i];
          this.languagesRecordList[0].value = 'Select...' + this.languagesRecordList[0].value;         
          if ( (this.languagesRecordList === null) || (this.languagesRecordList.length === 1) ) {
            this.languagesRecordList = [];
            this.languagesRecordList[0] = new ValueRecord();
            this.languagesRecordList[0].value = 'NO languages defined yet';            
            }
          break;
          }
        case 'cifTypes': {
          this.cifTypesRecordList = this.valueRecordList[i];
          this.cifTypesRecordList[0].value = 'Select...' + this.cifTypesRecordList[0].value;         
          if ( (this.cifTypesRecordList === null) || (this.cifTypesRecordList.length === 1) ) {
            this.cifTypesRecordList = [];
            this.cifTypesRecordList[0] = new ValueRecord();
            this.cifTypesRecordList[0].value = 'NO CIF Types defined yet';            
            }
          break;
          }
        case 'hdTypes': {
          this.hdTypeTypesRecordList = this.valueRecordList[i];
          this.hdTypeTypesRecordList[0].value = 'Select...' + this.hdTypeTypesRecordList[0].value;         
          if ( (this.hdTypeTypesRecordList === null) || (this.hdTypeTypesRecordList.length === 1) ) {
            this.hdTypeTypesRecordList = [];
            this.hdTypeTypesRecordList[0] = new ValueRecord();
            this.hdTypeTypesRecordList[0].value = 'NO HD Types defined yet';            
            }
          break;
        }
        case 'hdPlaces': {
          this.hdTypePlacesRecordList = this.valueRecordList[i];
          this.hdTypePlacesRecordList[0].value = 'Select...' + this.hdTypePlacesRecordList[0].value;         
          if ( (this.hdTypePlacesRecordList === null) || (this.hdTypePlacesRecordList.length === 1) ) {
            this.hdTypePlacesRecordList = [];
            this.hdTypePlacesRecordList[0] = new ValueRecord();
            this.hdTypePlacesRecordList[0].value = 'NO HD Types defined yet';            
            }
          break;
        }
        case 'mProvTypes': {
          this.mProvTypesRecordList = this.valueRecordList[i];
          this.mProvTypesRecordList[0].value = 'Select...' + this.mProvTypesRecordList[0].value;         
          if ( (this.mProvTypesRecordList === null) || (this.mProvTypesRecordList.length === 1) ) {
            this.mProvTypesRecordList = [];
            this.mProvTypesRecordList[0] = new ValueRecord();
            this.mProvTypesRecordList[0].value = 'NO More Provs Types defined yet';            
            }
          break;
        }
        case 'artTaxes': {
          this.artTaxesRecordList = this.valueRecordList[i];
          this.artTaxesRecordList[0].value = 'Select...' + this.artTaxesRecordList[0].value;         
          if ( (this.artTaxesRecordList === null) || (this.artTaxesRecordList.length === 1) ) {
            this.artTaxesRecordList = [];
            this.artTaxesRecordList[0] = new ValueRecord();
            this.artTaxesRecordList[0].value = 'NO Articles Taxes defined yet';            
            }
          break;
        }
        case 'artUnits': {
          this.artUnitsRecordList = this.valueRecordList[i];
          this.artUnitsRecordList[0].value = 'Select...' + this.artUnitsRecordList[0].value;         
          if ( (this.artUnitsRecordList === null) || (this.artUnitsRecordList.length === 1) ) {
            this.artUnitsRecordList = [];
            this.artUnitsRecordList[0] = new ValueRecord();
            this.artUnitsRecordList[0].value = 'NO Articles Units defined yet';            
            }
          break;
        }
        case 'fieldNames': {
          this.fieldNamesRecordList = this.valueRecordList[i];
          this.fieldNamesRecordList[0].value = 'Select...' + this.fieldNamesRecordList[0].value;         
          if ( (this.fieldNamesRecordList === null) || (this.fieldNamesRecordList.length === 1) ) {
            this.fieldNamesRecordList = [];
            this.fieldNamesRecordList[0] = new ValueRecord();
            this.fieldNamesRecordList[0].value = 'NO Field Names defined yet';            
            }
          break;
        }
        case 'tpvTypes': {
          this.tpvTypesRecordList = this.valueRecordList[i];
          this.tpvTypesRecordList[0].value = 'Select...' + this.tpvTypesRecordList[0].value;         
          if ( (this.tpvTypesRecordList === null) || (this.tpvTypesRecordList.length === 1) ) {
            this.tpvTypesRecordList = [];
            this.tpvTypesRecordList[0] = new ValueRecord();
            this.tpvTypesRecordList[0].value = 'NO TPV Types defined yet';            
            }
          break;
        }   
        case 'dbUrl': {
          this.dbUrlsRecordList = this.valueRecordList[i];
          this.dbUrlsRecordList[0].value = 'Select...' + this.dbUrlsRecordList[0].value;         
          if ( (this.dbUrlsRecordList === null) || (this.dbUrlsRecordList.length === 1) ) {
            this.dbUrlsRecordList = [];
            this.dbUrlsRecordList[0] = new ValueRecord();
            this.dbUrlsRecordList[0].value = 'NO DataBase URLs defined yet';            
            }
          break;
        }   
        case 'dbUserName': {
          this.dbUserNamesRecordList = this.valueRecordList[i];
          this.dbUserNamesRecordList[0].value = 'Select...' + this.dbUserNamesRecordList[0].value;         
          if ( (this.dbUserNamesRecordList === null) || (this.dbUserNamesRecordList.length === 1) ) {
            this.dbUserNamesRecordList = [];
            this.dbUserNamesRecordList[0] = new ValueRecord();
            this.dbUserNamesRecordList[0].value = 'NO DataBase UserNames defined yet';            
            }
          break;
        } 
        case 'dbDriverClassName': {
          this.dbDriverClassNamesRecordList = this.valueRecordList[i];
          this.dbDriverClassNamesRecordList[0].value = 'Select...' + this.dbDriverClassNamesRecordList[0].value;         
          if ( (this.dbDriverClassNamesRecordList === null) || (this.dbDriverClassNamesRecordList.length === 1) ) {
            this.dbDriverClassNamesRecordList = [];
            this.dbDriverClassNamesRecordList[0] = new ValueRecord();
            this.dbDriverClassNamesRecordList[0].value = 'NO DabaBase Drivers defined yet';            
            }
          break;
        } 
        case 'dbDdlAuto': {
          this.dbDdlAutosRecordList = this.valueRecordList[i];
          this.dbDdlAutosRecordList[0].value = 'Select...' + this.dbDdlAutosRecordList[0].value;         
          if ( (this.dbDdlAutosRecordList === null) || (this.dbDdlAutosRecordList.length === 1) ) {
            this.dbDdlAutosRecordList = [];
            this.dbDdlAutosRecordList[0] = new ValueRecord();
            this.dbDdlAutosRecordList[0].value = 'NO DataBase DdlAutos defined yet';            
            }
          break;
        } 
        case 'dbPhysNStrategy': {
          this.dbPhysNStrategysRecordList = this.valueRecordList[i];
          this.dbPhysNStrategysRecordList[0].value = 'Select...' + this.dbPhysNStrategysRecordList[0].value;         
          if ( (this.dbPhysNStrategysRecordList === null) || (this.dbPhysNStrategysRecordList.length === 1) ) {
            this.dbPhysNStrategysRecordList = [];
            this.dbPhysNStrategysRecordList[0] = new ValueRecord();
            this.dbPhysNStrategysRecordList[0].value = 'NO DataBase Physical Naming Startegies defined yet';            
            }
          break;
        }     
        default : {
          break;
        }
      }
    }
    this.checkReceivedFieldsValuesRecordsList();
  }
  checkReceivedFieldsValuesRecordsList() {
    this.consoleLog(this.logToConsole, '--->GlobalVar---> checkReceivedValuesRecordsList->...Starting...->', null);
    if (this.currentBar === null) { this.currentBar = "NO Bar from DB"; }
    if (this.screensRecordList === null) {
      this.screensRecordList = new Array();
      this.screensRecordList[0] = new ValueRecord();
      this.screensRecordList[0].id = 0;
      this.screensRecordList[0].value = 'NO Screens from DB';            
    }
    if (this.languagesRecordList === null) {
      this.languagesRecordList = new Array();
      this.languagesRecordList[0] = new ValueRecord();
      this.languagesRecordList[0].id = 0;
      this.languagesRecordList[0].value = 'NO Languages from DB';            
    }
    if (this.cifTypesRecordList === null) {
      this.cifTypesRecordList = new Array();
      this.cifTypesRecordList[0] = new ValueRecord();
      this.cifTypesRecordList[0].id = 0;
      this.cifTypesRecordList[0].value = 'NO Cif Types from DB';            
    }
    if (this.hdTypeTypesRecordList === null) {
      this.hdTypeTypesRecordList = new Array();
      this.hdTypeTypesRecordList[0] = new ValueRecord();
      this.hdTypeTypesRecordList[0].id = 0;
      this.hdTypeTypesRecordList[0].value = 'NO Hd Types from DB';            
    }
    if (this.hdTypePlacesRecordList === null) {
      this.hdTypePlacesRecordList = new Array();
      this.hdTypePlacesRecordList[0] = new ValueRecord();
      this.hdTypePlacesRecordList[0].id = 0;
      this.hdTypePlacesRecordList[0].value = 'NO Hd Type Places from DB';            
    }
    if (this.mProvTypesRecordList === null) {
      this.mProvTypesRecordList = new Array();
      this.mProvTypesRecordList[0] = new ValueRecord();
      this.mProvTypesRecordList[0].id = 0;
      this.mProvTypesRecordList[0].value = 'NO More Prov Types from DB';            
    }
    if (this.artTaxesRecordList === null) {
      this.artTaxesRecordList = new Array();
      this.artTaxesRecordList[0] = new ValueRecord();
      this.artTaxesRecordList[0].id = 0;
      this.artTaxesRecordList[0].value = 'NO Art Taxes from DB';            
    }
    if (this.artUnitsRecordList === null) {
      this.artUnitsRecordList = new Array();
      this.artUnitsRecordList[0] = new ValueRecord();
      this.artUnitsRecordList[0].id = 0;
      this.artUnitsRecordList[0].value = 'NO Art Units from DB';            
    }
    if (this.fieldNamesRecordList === null) {
      this.fieldNamesRecordList = new Array();
      this.fieldNamesRecordList[0] = new ValueRecord();
      this.fieldNamesRecordList[0].id = 0;
      this.fieldNamesRecordList[0].value = 'NO Field Names from DB';            
    }
    if (this.tpvTypesRecordList === null) {
      this.tpvTypesRecordList = new Array();
      this.tpvTypesRecordList[0] = new ValueRecord();
      this.tpvTypesRecordList[0].id = 0;
      this.tpvTypesRecordList[0].value = 'NO Tpv Types from DB';            
    }
    if (this.dbUrlsRecordList === null) {
      this.dbUrlsRecordList = new Array();
      this.dbUrlsRecordList[0] = new ValueRecord();
      this.dbUrlsRecordList[0].id = 0;
      this.dbUrlsRecordList[0].value = 'NO Db Urls from DB';            
    }
    if (this.dbUserNamesRecordList === null) {
      this.dbUserNamesRecordList = new Array();
      this.dbUserNamesRecordList[0] = new ValueRecord();
      this.dbUserNamesRecordList[0].id = 0;
      this.dbUserNamesRecordList[0].value = 'NO Db User Names from DB';            
    }
    if (this.dbDriverClassNamesRecordList === null) {
      this.dbDriverClassNamesRecordList = new Array();
      this.dbDriverClassNamesRecordList[0] = new ValueRecord();
      this.dbDriverClassNamesRecordList[0].id = 0;
      this.dbDriverClassNamesRecordList[0].value = 'NO Db Drivers from DB';            
    }
    if (this.dbDdlAutosRecordList === null) {
      this.dbDdlAutosRecordList = new Array();
      this.dbDdlAutosRecordList[0] = new ValueRecord();
      this.dbDdlAutosRecordList[0].id = 0;
      this.dbDdlAutosRecordList[0].value = 'NO Db Ddl Autos Types from DB';            
    }
    if (this.dbPhysNStrategysRecordList === null) {
      this.dbPhysNStrategysRecordList = new Array();
      this.dbPhysNStrategysRecordList[0] = new ValueRecord();
      this.dbPhysNStrategysRecordList[0].id = 0;
      this.dbPhysNStrategysRecordList[0].value = 'NO Db PhysNStrategy from DB';            
    }
  }

}