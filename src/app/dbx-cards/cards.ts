export class CardPayout {
    cardPayoutId            :number;
    cardPayoutDate          :Date;
    cardPayoutOperationNr   :number;
    cardPayoutMulti         :number;
    cardPayoutTotal         :number;
    cardPayoutTpvId         :number;
    cardPayoutTotalId       :number;
    cardPayoutTotalDate     :Date;
}
export class CardPayoutSelect {
    cardPayoutId            :number;
    cardPayoutDate          :Date;
    cardPayoutOperationNr   :number;
    cardPayoutMulti         :number;
    cardPayoutTotal         :number;
    cardPayoutTpvId         :number;
    cardPayoutTotalId       :number;
    cardPayoutTotalDate     :Date;
    cardPayoutSelected      :boolean;
}
export class CardPayoutGroup {
    cardPayoutId            :number;
    cardPayoutDate          :Date;
    cardPayoutOperationNr   :string;
    cardPayoutMulti         :number;
    cardPayoutTotal         :number;
    cardPayoutTTotal        :number;
    cardPayoutTpvId         :number;
    cardPayoutTotalId       :number;
    cardPayoutTotalDate     :Date;
    cardPayoutSelected      :boolean;
}
export class CardPayoutString {
    cardPayoutId            :number;
    cardPayoutDate          :string;
    cardPayoutOperationNr   :number;
    cardPayoutMulti         :number;
    cardPayoutTotal         :number;
    cardPayoutTpvId         :number;
    cardPayoutTotalId       :number;
    cardPayoutTotalDate     :Date;
}
export class CardTpv {
    cardTpvId           :number;
    cardTpvDate         :Date;
    cardTpvName         :string;
    cardTpvShopNr       :string;
    cardTpvTpvNr        :string;
    cardTpvSerialNr     :string;
    cardTpvBank         :string;
    cardTpvType         :string;
    cardTpvContact      :string;
    cardTpvMulti        :boolean;
    cardTpvComis1       :number;
    cardTpvComis2       :number;
    cardTpvComis3       :number;
    cardTpvComis4       :number;
    cardTpvComisTax     :number;
    cardTpvProvId       :number;
    cardTpvProvName     :string;
    cardTpvArtId        :number;
    cardTpvArtName      :string;
    cardTpvStatus       :boolean;
}
export class CardTotal {
    cardTotalId             :number;
    cardTotalDate           :Date;
    cardTotalDateStart      :Date;
    cardTotalDateEnd        :Date;
    cardTotalCardTpvId      :number;
    cardTotalCardTpvName    :string;
    cardTotalCardTpvBank    :string;
    cardTotalTotalOper      :number;
    cardTotalTotalMoney     :number;
    cardTotalExp1Perc       :number;
    cardTotalExp1Money      :number;
    cardTotalExp2Perc       :number;
    cardTotalExp2Money      :number;    
    cardTotalExp3Perc       :number;
    cardTotalExp3Money      :number;
    cardTotalExp4Perc       :number;
    cardTotalExp4Money      :number;
    cardTotalExpTotal       :number;
    cardTotalExpTaxesPerc   :number;
    cardTotalExpTaxesMoney  :number;
    cardTotalBankMoney      :number;
    cardTotalFactId         :number;
    cardTotalStatus         :boolean;
}
export class CardTotalString {
    cardTotalId             :number;
    cardTotalDate           :string;
    cardTotalDateStart      :string;
    cardTotalDateEnd        :string;
    cardTotalCardTpvId      :number;
    cardTotalCardTpvName    :string;
    cardTotalCardTpvBank    :string;
    cardTotalTotalMoney     :number;
    cardTotalExp1Perc       :number;
    cardTotalExp1Money      :number;
    cardTotalExp2Perc       :number;
    cardTotalExp2Money      :number;    
    cardTotalExp3Perc       :number;
    cardTotalExp3Money      :number;
    cardTotalExp4Perc       :number;
    cardTotalExp4Money      :number;
    cardTotalExpTotal       :number;
    cardTotalExpTaxesPerc   :number;
    cardTotalExpTaxesMoney  :number;
    cardTotalBankMoney      :number;
    cardTotalFactId         :number;
    cardTotalStatus         :boolean;
}