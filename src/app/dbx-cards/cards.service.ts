import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ValueRecord } from '../dbc-fields/field';

import { CardPayout, CardTpv, CardTotal } from './cards';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()

export class CardsService {

  constructor(private http: HttpClient) { }

  private json: string;
  private params: string;
  private headers: HttpHeaders;

  /* ---------------------------------------CARDS-----------------------------------------------------------*/
  getAllCardPayouts(cardPayoutVRList: ValueRecord[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/getAllPayouts',cardPayoutVRList)
  }
  getPrevCardPayouts(cardPayoutVRList: ValueRecord[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/getAllPayouts',cardPayoutVRList)
  }
  createCardPayoutsList(newCardPayoutsList: CardPayout[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/createPayouts',newCardPayoutsList)
  }
  updateCardPayoutsList(updateCardPayoutsList: CardPayout[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/updatePayouts',updateCardPayoutsList)
  }
  updateBeforeAfterCardPayoutsListTotalId(updateCardPayoutsList: CardPayout[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/updPayoutsTotalId',updateCardPayoutsList)
  }
  delOneCardPayout(delCard: CardPayout): Observable<any> {
    return this.http.post('/cards/delOnePayout', delCard)
  }
  deleteAllCardPayoutsList(delCardPayoutsList: CardPayout[]): Observable<any>  {
    return this.http.post<CardPayout[]>('/cards/delAllPayouts',delCardPayoutsList)
  }
  getAllCardPayoutsNotTotal(cardPayoutVRList: ValueRecord[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/getPayoutsNoTot',cardPayoutVRList)
  }
  getAllCardPayoutsWithCardTotalId(cardPayoutVRList: ValueRecord[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/getPayoutsTotalId',cardPayoutVRList)
  }
  getCardPayoutsNoTotal(cardPayoutVRList: ValueRecord[]): Observable<CardPayout[]>  {
    return this.http.post<CardPayout[]>('/cards/getPayoutsNoTotal',cardPayoutVRList)
  }
  /* ---------------------------------------CARD TPVS-----------------------------------------------------------*/
  getAllCardTpvs(): Observable<CardTpv[]>  {
    return this.http.get<CardTpv[]>('/cards/getAllTpvs')
  }
  createOneCardTpv(newCardTpv: CardTpv): Observable<any>  {    
    return this.http.post('/cards/createOneTpv', newCardTpv)  
  }
  getOneCardTpv(editCardTpv: CardTpv): Observable<CardTpv>  {
    return this.http.post<CardTpv>('/cards/getOneTpv', editCardTpv)
  }
  updateOneCardTpv(newCardTpv: CardTpv): Observable<any>  {
    return this.http.post('/cards/updOneTpv', newCardTpv)
  }
  delOneCardTpv(delCardTpv: CardTpv): Observable<any> {
    return this.http.post('/cards/delOneTpv', delCardTpv)
  }
  delAllCardTpvs(delCardTpv: CardTpv): Observable<any> {
    return this.http.post('/cards/delAllTpvs', delCardTpv)    
  }
  /* ---------------------------------------CARDS TOTALS-----------------------------------------------------------*/
  getAllCardsTotals(localCardsTotalsVRList: ValueRecord[]): Observable<CardTotal[]>  {
    return this.http.post<CardTotal[]>('/cards/getAllTotals',localCardsTotalsVRList)
  }
  createOneCardTotal(newCardTotal: CardTotal): Observable<CardTotal>  {    
    return this.http.post<CardTotal>('/cards/createOneTotal', newCardTotal)  
  }
  updateOneCardTotal(newCardTotal: CardTotal): Observable<CardTotal>  {    
    return this.http.post<CardTotal>('/cards/updateOneTotal', newCardTotal)  
  }
  getOneCardTotal(newCardTotal: CardTotal): Observable<CardTotal>  {    
    return this.http.post<CardTotal>('/cards/getOneTotal', newCardTotal)  
  }
  deleteOneCardTotal(delCardTotal: CardTotal): Observable<any> {
    return this.http.post('/cards/delOneTotal', delCardTotal)
  }
  deleteAllCardsTotals(localCardsTotalsVRList: ValueRecord[]): Observable<any> {
    return this.http.post('/cards/delAllTotals', localCardsTotalsVRList)    
  }
}
