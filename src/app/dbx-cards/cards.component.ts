import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ErrorService } from '../aa-errors/error.service';
import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { EnterDateRangeComponent } from '../aa-common/enter-dateRange.component';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { ValueRecord } from '../dbc-fields/field';

import { CardPayout, CardTpv } from './cards';
import { CardTpvsCreateComponent } from './cardTpvs-create.component';
import { CardPayoutsCreateComponent } from './cardPayouts-create.component';
import { CardsTotalsComponent } from './cards-totals.component';
import { CardsService } from './cards.service';

@Component({
  selector      :'app-cards',
  templateUrl   :'./cards.component.html',
  styleUrls     :['./cards.component.css']
})
export class CardsComponent implements OnInit {
  formTitles = {
    'cardPayouts'           :'Pagos con Tarjetas',
    'cardsListYearString'   :'',
    'cardsListMonth'        :'Mes ',
    'cardsListMonthString'  :'',
    'cardsListStart'        :'',
    'cardsListEnd'          :'',
    'cardPayoutsList'       :'TPV',
    'cardDeleteOne'         :'¿ Seguro que quiere BORRAR este PAGO con TARJETA ?',
    'cardDeleteAll'         :'¿ Seguro que quiere BORRAR TODOS PAGOS con TARJETAS ?',   
    'cardEditList'          :'Editar',
    'cardDelete'            :'Borrar',
    'tpvName'               :'',
    'startDate'             :'',
    'endDate'               :'',
    'cardTpv'               :'Tpvs Tarjetas',
    'cardTpvsList'          :'Lista TPVS para Tarjetas',
    'cardTpvDeleteOne'      :'¿ Seguro que quiere BORRAR este TPV para TARJETAS ?',
    'cardTpvDeleteAll'      :'¿ Seguro que quiere BORRAR TODOS los TPVS para TARJETAS ?',
    'cardTpvEditList'       :'Editar',
    'cardTpvDelete'         :'Borrar',
  };
  formLabels = { 
    'id'                    :'#####',
    'cardPayoutId'          :'Id',
    'cardPayoutDate'        :'Fecha',
    'cardPayoutOperationNr' :'NºOper.',
    'cardPayoutMulti'       :'Cantidad',
    'cardPayoutTotal'       :'Total',
    'cardPayoutTpvId'       :'Tpv.Id',
    'cardPayoutTotalId'     :'Total.Id',
    'cardPayoutTotalDate'   :'Fecha.Total.',
    'cardTpvId'             :'Id Tpv',
    'cardTpvDate'           :'Fecha',
    'cardTpvName'           :'Nombre Tpv',
    'cardTpvShopNr'         :'Nº Tienda',
    'cardTpvTpvNr'          :'Nº Tpv',
    'cardTpvSerialNr'       :'Nº Serie',
    'cardTpvBank'           :'CCC Banco',
    'cardTpvType'           :'Tipo TPV',
    'cardTpvContact'        :'Contacto',
    'cardTpvComis'          :'Comision',
    'cardTpvMulti'          :'Vales',
    'cardTpvStatus'         :'Estado',
  };  
  public  localCardTpvsRecordList   :ValueRecord [];
  private createCard                :boolean;
  public  showCardPayoutsList       :boolean;
  public  cardPayouts               :CardPayout[];
  public  cardPayoutsList           :CardPayout[];
  private newCardPayoutList         :CardPayout;
  private nextCardPayoutOperationNr :number;
  public  cardPayoutsTotalString    :string;

  public  searchCard                :CardPayout;
  private selectedTpvName           :string;
  private selectedStartDate         :Date;
  private selectedEndDate           :Date;
  public  showSearchCard            :boolean;
  public  orderCardsList            :string;
  public  reverseCardsList          :boolean;
  
  private createCardTpv             :boolean;
  public  localCardTpvs             :CardTpv[];
  public  showCardTpvsList          :boolean;
  private newCardTpv                :CardTpv;

  public  searchCardTpv             :CardTpv;
  private selectedCardTpv           :CardTpv;
  public  showSearchCardTpvs        :boolean;
  public  orderCardTpvsList         :string;
  public  reverseCardTpvsList       :boolean;
  public  caseInsensitive           :boolean;
  private sortedCollection          :any[];
  private logToConsole              :boolean;
  //--------------------------------------- CARDS -- CONSTRUCTOR ---------------------------------------------------------------
  constructor(  private orderPipe: OrderPipe, private viewContainer: ViewContainerRef, private modalService: BsModalService,
                public globalVar: GlobalVarService, private _gbViewsService: GbViewsService, private _errorService: ErrorService,                
                private _cardService: CardsService ) {
                    //   this.getGbViews('gbCards'); 
              }
  //--------------------------------------- CARDS -- NG ON INIT ---------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.cardPayoutsTotalString ='Total... 0.00';

    this.caseInsensitive = true;
    this.newCardPayoutList = new CardPayout();
    this.searchCard = new CardPayout();
    this.newCardTpv = new CardTpv();
    this.selectedCardTpv = new CardTpv();
    this.searchCardTpv = new CardTpv();
    this.selectedTpvName = '';
    this.selectedStartDate = this.globalVar.workingFirstDayOfMonth;
    this.selectedEndDate = this.globalVar.workingLastDayOfMonth;
    this.formTitles.startDate = this.globalVar.workingFirstDayOfMonth.toLocaleDateString();;
    this.formTitles.endDate = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
    this.formTitles.tpvName = '';
    this.resetShowCardFlags();
    this.localCardTpvsRecordList = this.globalVar.cardTpvsRecordList;
    this.localCardTpvs = this.globalVar.cardTpvs;
    if ((this.localCardTpvsRecordList == null) || (this.localCardTpvsRecordList.length == 0)){ 
      this.showCardPayoutsList = false;
      this.localCardTpvs = null;
      this.selectedTpvName = '';
      this.cardPayoutsList = null;
      this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
      "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
    } else {
      this.selectedCardTpv = this.localCardTpvs[0];                         
      this.selectedTpvName =  this.localCardTpvs[0].cardTpvName;
      this.formTitles.tpvName = this.selectedTpvName;
      this.getAllCardPayouts();  
    }
  }
  //--------------------------------------- CARDS -- FORMS ---------------------------------------------------------------
  
  //--------------------------------------- CARDS -- GENERAL ---------------------------------------------------------------  
  private calculateCardPayoutsTotal(){
    let cardPayoutsTotal = 0;
    for (let i = 0 ; i < this.cardPayoutsList.length ; i++ ) {
      cardPayoutsTotal = cardPayoutsTotal + ( this.cardPayoutsList[i].cardPayoutTotal * this.cardPayoutsList[i].cardPayoutMulti );
    }
    this.cardPayoutsTotalString =this.cardPayoutsList.length+'..'+this.formTitles.tpvName.substring(0, 10)+'..Total->'+ cardPayoutsTotal.toFixed(2);
  }
  private resetShowCardFlags(){
    this.showCardPayoutsList    = false;
    this.createCard             = false;
    this.showCardTpvsList       = false;
    this.createCardTpv          = false;
  }
  private maxOperationNrCardPayoutList():number{
    let maxOperationNr = 0;
    for(let j=0; j<this.cardPayoutsList.length; j++){
      maxOperationNr = Math.max ( maxOperationNr, this.cardPayoutsList[j].cardPayoutOperationNr );
    }
    return maxOperationNr;
  }
  //---------------------------------------- CARDS -- BTN CLICK ---------------------------------------------------------------
  public changeShowCardsListBtnClick(){  
    if (this.localCardTpvs === null) { return; }  
    this.resetShowCardFlags();           
    this.getAllCardPayouts();        
  }
  public showCardTotalListBtnClick(){
    var workingDateRange = new Array();
    workingDateRange[0] = new Date(this.selectedStartDate);
    workingDateRange[1] = new Date(this.selectedEndDate);
    const modalInitialState = {
      workingDateRange          :workingDateRange, // first and last date of current month
      cardTpvs                  :this.localCardTpvs,
      cardTpvsRecordList        :this.localCardTpvsRecordList,
      callback                  :'OK',   
    };
    this.globalVar.openModal(CardsTotalsComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllCardPayouts();
    })
  }
  public changeShowCardTpvsListBtnClick(){
    this.resetShowCardFlags();      
    this.getAllCardTpvs();
  }
  public enterCardsPeriodBtnClick(){
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS -> enterCardsPeriodBtnClick ->...Starting...->', null);
    let dateRange = new Array();
    let minDate = new Date();
    let maxDate = new Date();
    this.globalVar.workingFirstDayOfYear
    dateRange = this.globalVar.workingFirstAndLastDayOfMonth;
    minDate = this.globalVar.workingFirstDayOfYear;
    maxDate = this.globalVar.workingLastDayOfYear;
    const modalInitialState = {
      dateTitle1          :"Pagos TARJETAS",
      dateTitle2          :"Desde ..... Hasta",
      dateMessage         :"Introduzca las fechas entre las que quiere ver los pagos con TARJETAS",
      dateLabel           :"Periodo pago Tarjetas:",       
      dateFooter          :"Pagos TARJETAS -> Desde ..... Hasta",
      dateContainerClass  :'theme-dark-blue', //theme-dark-blue theme-blue
      dateRange           :dateRange,
      minDate             :minDate,      
      maxDate             :maxDate,
      dateInputFormat     :'DD-MMM-YYYY',
      rangeInputFormat    :'DD-MMM-YYYY',
      callback            :'OK',   
    };
    this.globalVar.openModal(EnterDateRangeComponent, modalInitialState, 'modalLgTop0Left0').
    then((dateRange:any)=>{
      if (dateRange != null) { 
        this.selectedStartDate = dateRange[0];
        this.selectedEndDate = dateRange[1];
        this.formTitles.startDate = this.selectedStartDate.toLocaleDateString();;
        this.formTitles.endDate = this.selectedEndDate.toLocaleDateString();
       }    
    })
  }
  public selectCardTpvBtnClick(){   
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS -> selectCardTpvBtnClick ->...Starting...->', null);
    if (this.localCardTpvs == null) {
      this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
                        "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
      return;
    }        
    this.localCardTpvsRecordList = this.globalVar.prepareCardTpvsRecordList(this.localCardTpvs);
    const modalInitialState = {
      localValueRecordList  :this.localCardTpvsRecordList,
      selectTitle1          :"TPVS para TARJETAS",
      selectTitle2          :"Seleccionar TPV",
      selectMessage         :"Seleccione el TPV del que quiere ver los pagos de TARJETAS",
      selectLabel           :"TPV Tarjetas:",       
      selectFooter          :"TPVS para TARJETAS",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){      
        var selTpvTypeTpvname :string;
        selTpvTypeTpvname = result;                         
        this.selectedTpvName = selTpvTypeTpvname.substring(6);
        this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedTpvName);  
        this.formTitles.tpvName = this.selectedTpvName;
        this.getAllCardPayouts();           
      }
    })
  }
  public createCardPayoutsListBtnClick(){
    this.newCardPayoutList = new CardPayout();
    this.createCard = true;
    this.createCardPayoutListModal();
  }  
  public editCardPayoutBtnClick(editCard: CardPayout){
    this.newCardPayoutList = editCard;
    this.createCard = false;
    this.createCardPayoutListModal();
  } 
  private createCardPayoutListModal(){    
    const modalInitialState = {
      createCardPayout        :this.createCard,
      cardPayoutId            :null,
      cardPayoutDate          :this.selectedStartDate,
      cardPayoutMinDate       :this.selectedStartDate,
      cardPayoutMaxDate       :this.selectedEndDate,
      cardPayoutOperationNr   :this.nextCardPayoutOperationNr,
      cardPayoutList          :this.newCardPayoutList,
      selectedCardTpv         :this.selectedCardTpv,
      changeCardsDate         :true,
      cardPayoutTotalId       :null,
      callback                :'OK',   
    };
    this.globalVar.openModal(CardPayoutsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null) this.newCardPayoutList = result;           
      this.getAllCardPayouts();
    })
  }  
  public searchCardPayoutsBtnClick() {
    if (this.showSearchCard === true) {
      this.showSearchCard = false;
    } else {
      this.showSearchCard = true;
    }
  }
  public setOrderBtnClick(value: string) {
    if (this.orderCardsList === value) {
      this.reverseCardsList = !this.reverseCardsList;
    }
    this.orderCardsList = value;
    console.log(this.orderCardsList);
  }
  //--------------------------------------- CARDS -- DATABASE ---------------------------------------------------------------
  private getAllCardPayouts() {
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS -> getAllCardPayouts ->...Starting...->', null);
    if ( (this.localCardTpvs === null) || (this.localCardTpvs.length === 0) ) {
      this.cardPayoutsList = null;
      return;
    }   
    let cardPayoutVRList = this.globalVar.prepareStringDatesValueRecordList( this.selectedTpvName,this.selectedStartDate,this.selectedEndDate);    
    this._cardService.getAllCardPayouts(cardPayoutVRList)
      .subscribe({next:(data) => {  this.cardPayoutsList = data;
                            this.orderCardsList = 'cardPayoutOperationNr';
                            this.reverseCardsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.cardPayoutsList,this.orderCardsList,this.reverseCardsList,this.caseInsensitive);
                            this.cardPayoutsList = this.sortedCollection;
                            this.nextCardPayoutOperationNr = this.maxOperationNrCardPayoutList() + 1;
                            this.formTitles.cardsListMonthString = this.selectedStartDate.toLocaleString('default',{month:'long'});
                            let workingYear = this.selectedStartDate.getFullYear();
                            this.formTitles.cardsListYearString = workingYear.toString();
                            this.formTitles.cardsListStart = this.selectedStartDate.toLocaleDateString(); 
                            this.formTitles.cardsListEnd = this.selectedEndDate.toLocaleDateString();
                            this.calculateCardPayoutsTotal();
                            this.showCardPayoutsList = true;    
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- CARDS -- DELETE ---------------------------------------------------------------
  public confirmDeleteOneCardPayoutBtnClick(card: CardPayout){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardPayouts,this.formTitles.cardPayouts,
                            card,this.formTitles.cardDeleteOne,card.cardPayoutDate.toLocaleString(),
                            card.cardPayoutOperationNr.toString(),card.cardPayoutTotal.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneCardPayout(card); }
        }) 
  }
  public confirmDeleteAllCardPayoutsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardPayouts,this.formTitles.cardPayouts,
                            this.selectedCardTpv,this.formTitles.cardDeleteAll,this.selectedStartDate.toString(),
                            this.selectedEndDate.toString(),this.nextCardPayoutOperationNr.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllCardPayoutsList(this.cardPayoutsList); }
        }) 
  } 
  private deleteAllCardPayoutsList(delCardPayoutsList: CardPayout[]){
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS -> deleteAllCardPayoutsList ->...Starting...->', null);
    this._cardService.deleteAllCardPayoutsList(delCardPayoutsList)
    .subscribe({next:(data) => { this.getAllCardPayouts(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };}});
  }
  private deleteOneCardPayout(delCard: CardPayout){
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS -> deleteOneCardPayout ->...Starting...->', null);
    this._cardService.delOneCardPayout(delCard)
    .subscribe({next:(data) => { this.getAllCardPayouts(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //------------------------------------ CARDS TPVS -- GENERAL -------------------------------------------------------------------
  //------------------------------------ CARDS TPVS -- BTNCLICK -------------------------------------------------------------------
  public createOneCardTpvBtnClick(){
    this.newCardTpv = new CardTpv();
    this.createCardTpv = true;
    this.createCardTpvModal();
  }  
  public editCardTpvBtnClick(editCardTpv: CardTpv){
    if (this.isCardTpvNotBlocked(editCardTpv)){ 
      this.newCardTpv = editCardTpv;
      this.createCardTpv = false;
      this.createCardTpvModal();
    }
  }   
  private createCardTpvModal(){    
    const modalInitialState = {
      newCardTpv      :this.newCardTpv,
      createCardTpv   :this.createCardTpv,
      callback        :'OK',   
    };
    this.globalVar.openModal(CardTpvsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.getAllCardTpvs();          
    })
  } 
  public isCardTpvNotBlocked(selectedCardTpv: CardTpv):boolean{
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS TPVS -> deleteAllCardTpvs ->...Starting...->', null);
    if (selectedCardTpv.cardTpvStatus === false){
      this._errorService.showErrorModal( "ARTÍCULOS", "ARTÍCULOS", "ARTÍCULO BLOQUEADO"+"->",
                        "Cardículo->"+selectedCardTpv.cardTpvName, "Banco->"+selectedCardTpv.cardTpvBank, 
                        "Tipo->"+selectedCardTpv.cardTpvType, "Contacto->"+selectedCardTpv.cardTpvContact,
                        "Id->"+selectedCardTpv.cardTpvId, "Tienda->"+selectedCardTpv.cardTpvShopNr);
      return false;
    } else return true;
  }
  public lockCardTpvBtnClick(modCardTpv: CardTpv){
    if (modCardTpv.cardTpvStatus === true) modCardTpv.cardTpvStatus = false;
    else  modCardTpv.cardTpvStatus = true;
    this.updateOneCardTpv(modCardTpv);
  }
  public searchCardTpvsBtnClick() {
    if (this.showSearchCardTpvs === true) {
      this.showSearchCardTpvs = false;
    } else {
      this.searchCardTpv.cardTpvName  = "";
      this.searchCardTpv.cardTpvType  = "";
      this.searchCardTpv.cardTpvBank = "";
      this.showSearchCardTpvs = true;
    }
    return;
  }
  public setCardTpvsOrderBtnClick(value: string) {
    if (this.orderCardTpvsList === value) {
      this.reverseCardTpvsList = !this.reverseCardTpvsList;
    }
    this.orderCardTpvsList = value;   
  }              
  //------------------------------------ CARDS TPVS -- DELETE -------------------------------------------------------------------
  public confirmDeleteOneCardTpvBtnClick(cardTpv: CardTpv){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardTpv,this.formTitles.cardTpv,
                            cardTpv,this.formTitles.cardTpvDeleteOne,cardTpv.cardTpvName,
                            cardTpv.cardTpvType,cardTpv.cardTpvBank,"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.resetShowCardFlags();
            this.showCardTpvsList = true;        
            this.deleteOneCardTpv(cardTpv);   
          }
        })
  }
  public confirmDeleteAllCardTpvsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardTpv,this.formTitles.cardTpv,
                            null,this.formTitles.cardTpvDeleteAll,"","","","","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.resetShowCardFlags();
            this.showCardTpvsList = true;
            this.deleteAllCardTpvs(this.localCardTpvs[0]); 
          }
        })
  }
  //------------------------------------------------ CARD TPVS -- DATABASE -------------------------------------------------------
  private getAllCardTpvs() {
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS TPVS -> deleteAllCardTpvs ->...Starting...->', null);
    this._cardService.getAllCardTpvs()
      .subscribe({next:(data) => {  if ((data == null) || (data.length == 0)){ 
                              this.showCardPayoutsList = false;
                              this.localCardTpvs = null;
                              this.selectedTpvName = '';
                              this.cardPayoutsList = null;
                              this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
                              "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
                            } else {
                              this.localCardTpvs = data;
                              this.orderCardTpvsList = 'cardTpvType';
                              this.reverseCardTpvsList = false;
                              this.sortedCollection = this.orderPipe.transform(this.localCardTpvs,this.orderCardTpvsList,this.reverseCardTpvsList,this.caseInsensitive);
                              this.localCardTpvs = this.sortedCollection;                          
                              this.showSearchCardTpvs = false;
                              this.localCardTpvsRecordList = this.globalVar.prepareCardTpvsRecordList(this.localCardTpvs);
                              this.selectedCardTpv = this.localCardTpvs[0];                         
                              this.selectedTpvName =  this.localCardTpvs[0].cardTpvName;
                              this.formTitles.tpvName = this.selectedTpvName;
                              this.showCardTpvsList = true;
                            }                                             
                         },
                error:(error) => {  if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  } 
  private updateOneCardTpv(modCard: CardTpv): void {
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS TPVS -> updateOneCardTpv ->...Starting...->', null);
    this._cardService.updateOneCardTpv(modCard)
      .subscribe({next:(data) => { this.newCardTpv = data;
                           this.getAllCardTpvs();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private deleteAllCardTpvs(delCardTpv: CardTpv){
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS TPVS -> deleteAllCardTpvs ->...Starting...->', null);
    this._cardService.delAllCardTpvs(delCardTpv)
    .subscribe({next:(data) => { this.getAllCardTpvs(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});  
  }
  private deleteOneCardTpv(delCardTpv: CardTpv){
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS TPVS -> deleteOneCardTpv ->...Starting...->', null);
    this._cardService.delOneCardTpv(delCardTpv)
    .subscribe({next:(data) => { this.getAllCardTpvs(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}

