import { Component, OnInit } from '@angular/core';
import { throwError, forkJoin, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ErrorService } from '../aa-errors/error.service';
import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { CardTpv, CardTotal, CardTotalString } from './cards';
import { CardsTotalsCreateComponent } from './cards-totals-create.component';
import { CardsService } from './cards.service';
import { FactsService } from '../dbx-facts/facts.service';

@Component({
  selector      :'app-cards-totals',
  templateUrl   :'./cards-totals.component.html',
  styleUrls     :['./cards.component.css'] 
})

export class CardsTotalsComponent implements OnInit {
  public workingDateRange         :Date[];
  public cardTpvs                 :CardTpv[];
  public cardTpvsRecordList       :ValueRecord[];
  callback                        :any;
  result: Subject<any> = new Subject<any>();

  formTitles = {
    'edit'                  :'Editar',
    'delete'                :'Borrar',
    'cardsTotals'           :'Totalizaciones',
    'cardsTotalsList'       :'Desde...Hasta',
    'cardsTotalsMoney'      :'Total Venta',
    'cardsTotalsDeleteOne'  :'¿ Seguro que quiere BORRAR está TOTALIZACIÓN ?',
    'cardsTotalsDeleteAll'  :'¿ Seguro que quiere BORRAR TODAS las TOTALIZACIONES ?',
  };
  cardTotalStatus         :boolean;
  formLabels = { 
    'cardTotalId'             :'Id',
    'cardTotalDate'           :'Fecha',
    'cardTotalDateStart'      :'Desde',
    'cardTotalDateEnd'        :'Hasta',
    'cardTotalCardTpvName'    :'Tpv',
    'cardTotalCardTpvBank'    :'Banco',
    'cardTotalTotalMoney'     :'Total',
    'cardTotalExpTaxesMoney'  :'Comis.',
    'cardTotalBankMoney'      :'Din.',
    'cardTotalStatus'         :'Estado',
  };  

  public  cardsTotalsVRList             :ValueRecord[];
  public  showCardsTotalsList           :boolean;
  public  cardsTotalsList               :CardTotal[];
  public  newCardTotal                  :CardTotal;
  public  createCardTotal               :boolean;
  private selectedCardTpv               :CardTpv;
  private selectedCardTpvName           :string;

  public  showSearchCardsTotals         :boolean;
  public  searchCardsTotals             :CardTotalString;
  public  orderCardsTotalsList          :string;
  public  reverseCardsTotalsList        :boolean;
  public  caseInsensitive               :boolean;
  private sortedCollection              :any[];
  private logToConsole                  :boolean;
  //--------------------------------------- CARDS TOTALS -- CONSTRUCTOR -------------------------------------------------------------
  constructor(private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,                     
              private globalVar: GlobalVarService, private _gbViewsService: GbViewsService, private _errorService: ErrorService, 
              private _cardService: CardsService, private _factService: FactsService ) {
                    //   this.getGbViews('gbCards'); 
              }
  //--------------------------------------- CARDS TOTALS -- NG ON INIT -------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.caseInsensitive = true;
    this.searchCardsTotals = new CardTotalString();
    this.selectedCardTpv = null;
    this.selectedCardTpvName = 'ALL';
    this.formTitles.cardsTotalsList = 'TPV...'+ this.selectedCardTpvName.slice(0,15)+'...Desde...'+this.workingDateRange[0].toLocaleDateString()+'...Hasta...'+this.workingDateRange[1].toLocaleDateString();
    this.showCardsTotalsList = false; 
    this.cardsTotalsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpvName,this.workingDateRange[0],this.workingDateRange[1]);    
    this.getAllCardsTotals(this.cardsTotalsVRList);  
  }
  //--------------------------------------- CARDS TOTALS -- FORMS -------------------------------------------------------------
  
  //--------------------------------------- CARDS TOTALS -- GENERAL -------------------------------------------------------------
  private calculateCardsTotalsTotal(localCardsTotalsList :CardTotal[]){
    this.formTitles.cardsTotalsList = 'TPV...'+ this.selectedCardTpvName+'...Desde...'+this.workingDateRange[0].toLocaleDateString()+'...Hasta...'+this.workingDateRange[1].toLocaleDateString();
    this.formTitles.cardsTotalsMoney= 'Total Venta';
  }

  public closeCardsTotalsModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback('OK');
      this.result.next('OK');
      this.bsModalRef.hide();
    }
  }
  //--------------------------------------- CARDS TOTALS -- BTN CLICK -------------------------------------------------------------
  public changeShowCardTotalListBtnClick(){
    if (this.showCardsTotalsList === true) {
      this.showCardsTotalsList = false;
    } else {
      this.getAllCardsTotals(this.cardsTotalsVRList);  
    }
  }
  public selectCardTpvBtnClick(){   
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-TOTALS-> selectCardTpvBtnClick ->...Starting...->', null); 
    if (this.cardTpvsRecordList === null) {
      this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
                        "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
      return;
    }          
    //this.localCardTpvsRecordList = this.globalVar.prepareCardTpvsValueRecordList(this.localCardTpvs);
    const modalInitialState = {
      localValueRecordList  :this.cardTpvsRecordList,
      selectTitle1          :"TPVS para TARJETAS",
      selectTitle2          :"Seleccionar TPV",
      selectMessage         :"Seleccione el TPV del que quiere ver los pagos de TARJETAS",
      selectLabel           :"TPV Tarjetas:",       
      selectFooter          :"TPVS para TARJETAS",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){
        var selTpvTypeTpvname :string;
        selTpvTypeTpvname = result;                         
        var selectedTpvName = selTpvTypeTpvname.substring(6);
        this.selectedCardTpvName = selectedTpvName;
        this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedCardTpvName);  
        this.cardsTotalsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpvName,this.workingDateRange[0],this.workingDateRange[1]);    
        this.getAllCardsTotals(this.cardsTotalsVRList);           
      }
    })
  }
  public createCardTotalBtnClick(){
    this.newCardTotal = new CardTotal();
    this.createCardTotal = true;
    this.createCardTotalModal(this.newCardTotal);
  }  
  public editCardTotalBtnClick(editCardTotal: CardTotal){
    this.newCardTotal = editCardTotal;
    this.createCardTotal = false;
    this.createCardTotalModal(editCardTotal);
  }  
  private createCardTotalModal(localCardTotal :CardTotal){    
    const modalInitialState = {
      createCardTotal           :this.createCardTotal,
      newCardTotal              :this.newCardTotal,
      workingDateRange          :this.workingDateRange,
      cardTpvs                  :this.cardTpvs,
      cardTpvsRecordList        :this.cardTpvsRecordList,
      selectedCardTpv           :this.selectedCardTpv,
      callback                  :'OK',   
    };
    this.globalVar.openModal(CardsTotalsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllCardsTotals(this.cardsTotalsVRList);
    })        
  }  
  public searchCardsTotalsBtnClick() {
    if (this.showSearchCardsTotals === true) {
      this.showSearchCardsTotals = false;
    } else {
      this.showSearchCardsTotals = true;
    }
  }
  public setCarsdTotalsOrderBtnClick(value: string) {
    if (this.orderCardsTotalsList === value) {
      this.reverseCardsTotalsList = !this.reverseCardsTotalsList;
    }
    this.orderCardsTotalsList = value;
    console.log(this.orderCardsTotalsList);
  }
  //--------------------------------------- CARDS TOTALS -- DATABASE-------------------------------------------------------------
  private getAllCardsTotals(localCardsTotalsVRList :ValueRecord[]) {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-TOTALS > getAllCardsTotal ->...Starting...->', null);
    this._cardService.getAllCardsTotals(localCardsTotalsVRList)
      .subscribe({next:(data) => {  this.cardsTotalsList = data;
                            this.orderCardsTotalsList = 'cardTotalCardTpvName';
                            this.reverseCardsTotalsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.cardsTotalsList,this.orderCardsTotalsList,this.reverseCardsTotalsList,this.caseInsensitive);
                            this.cardsTotalsList = this.sortedCollection;
                            this.calculateCardsTotalsTotal(this.cardsTotalsList);
                            this.showCardsTotalsList = true;    
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- CARDS TOTALS -- DELETE-------------------------------------------------------------
  public confirmDeleteOneCardTotalBtnClick(delCardTotal :CardTotal){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardsTotals,this.formTitles.cardsTotals,
                            delCardTotal,this.formTitles.cardsTotalsDeleteOne,delCardTotal.cardTotalDateStart.toLocaleString(),
                            delCardTotal.cardTotalDateEnd.toString(),delCardTotal.cardTotalBankMoney.toFixed(2),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneCardTotal(delCardTotal); }
        }) 
  }
  public confirmDeleteAllCardsTotalsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardsTotals,this.formTitles.cardsTotals,
                          this.selectedCardTpv,this.formTitles.cardsTotalsDeleteAll,this.workingDateRange[0].toString(),
                          this.workingDateRange[1].toString(),"","","","",).
      then((deleteOK:boolean)=>{
        if (deleteOK === true){ this.deleteAllCardsTotals(this.cardsTotalsVRList); }
    }) 
  } 
  private deleteAllCardsTotals(localCardsTotalsVRList: ValueRecord[]){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-TOTALS-> deleteAllCardsTotals ->...Starting...->', null);
    this._cardService.deleteAllCardsTotals(localCardsTotalsVRList)
    .subscribe({next:(data) => { this.getAllCardsTotals(this.cardsTotalsVRList); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };}});
  }
  private deleteOneCardTotal(delCardTotal: CardTotal){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-TOTALS-> deleteOneCardTotal ->...Starting...->', null);
    var delFactId = delCardTotal.cardTotalFactId;
    forkJoin({  delTotalResult    :this._cardService.deleteOneCardTotal(delCardTotal),
                delFactResult     :this._factService.deleteOneFactById(delFactId),
      })
  .subscribe({next:(data)=> { 
    this.getAllCardsTotals(this.cardsTotalsVRList);
    },
    error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}