import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { CardTpv } from './cards';
import { CardsService } from './cards.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';
import { ValueRecord } from '../dbc-fields/field';
import { ArtsSelectComponent } from '../dbc-arts/arts-select.component';
import { ProvsSelectComponent } from '../dbc-provs/provs-select.component';
import { Prov } from '../dbc-provs/prov';
import { Art } from '../dbc-arts/art';

@Component({
  selector      :'app-cardTpvs-create',
  templateUrl   :'./cardTpvs-create.component.html',
  styleUrls     :['./cards.component.css'] 
})
export class CardTpvsCreateComponent implements OnInit {
  public newCardTpv     :CardTpv;
  public createCardTpv  :boolean;
  callback              :any;
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'cardTpvs'          :'TPVs para Tarjetas',
    'cardTpvCreate'     :'Crear TPV para Tarjetas',
    'cardTpvEdit'       :'Modificar TPV para Tarjetas',
  };
  public  formLabels = {
    'cardTpvId'         :'Id',
    'cardTpvDate'       :'Fecha',
    'cardTpvName'       :'Nombre Tpv',
    'cardTpvShopNr'     :'Nº Tienda',
    'cardTpvTpvNr'      :'Nº Tpv',
    'cardTpvSerialNr'   :'Nº Serie',
    'cardTpvBank'       :'CCC Banco',
    'cardTpvType'       :'Tipo Tpv',
    'cardTpvContact'    :'Contacto',
    'cardTpvComis1'     :'Comis.Fija',
    'cardTpvComis2'     :'Comis.X.Operacion',
    'cardTpvComis3'     :'% Comisión 1',
    'cardTpvComis4'     :'% Comisión 2',
    'cardTpvComisTax'   :'Impuesto Comisión',
    'cardTpvMulti'      :'Tarjeta / Vales',
    'cardTpvProvId'     :'Id.Prov',
    'cardTpvProvName'   :'Proveedor',
    'cardTpvArtId'      :'Id.Art',
    'cardTpvArtName'    :'Artículo',
    'cardTpvStatus'     :'Estado',  
  };
  public  cardTpvForm                 : FormGroup;
  public  localTpvTypesRecordList     : ValueRecord [];
  public  localBankAccountsRecordList : ValueRecord [];
  private logToConsole                : boolean;
  public  showCardTpv                 : boolean;  
  public  editCardTpv                 : boolean;
  private currentDate                 :Date;
  private selectedProv                :Prov;
  private selectedArt                 :Art;
  public  cardTpvsDatePickerConfig    :Partial <BsDatepickerConfig>;

  //--------------------------------------- CARDTPVS CREATE -- CONSTRUCTOR --------------------------------------------------------------
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _cardsService: CardsService) { 
    this.cardTpvsDatePickerConfig = Object.assign ( {}, {
      containerClass    :'theme-dark-blue',
      showWeekNumbers   :true,
      minDate           :this.globalVar.workingPreviousYear,
      maxDate           :this.globalVar.workingNextYear,
      dateInputFormat   :'DD-MM-YYYY'
      } );
  }
  //--------------------------------------- CARDTPVS CREATE -- NG ON INIT --------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.currentDate = this.globalVar.currentDate;
    this.cardTpvForm = this.fb.group({
      cardTpvId           :[''],
      cardTpvDate         :[{value:this.currentDate,disabled:true},[Validators.required]],
      cardTpvName         :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvShopNr       :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvTpvNr        :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvSerialNr     :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvBank         :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvType         :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ], 
      cardTpvContact      :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvComis1       :['', [Validators.required, ValidationService.twoDecimalsValidation] ],
      cardTpvComis2       :['', [Validators.required, ValidationService.twoDecimalsValidation] ],
      cardTpvComis3       :['', [Validators.required, ValidationService.percentageValidation] ],
      cardTpvComis4       :['', [Validators.required, ValidationService.percentageValidation] ],
      cardTpvComisTax     :['', [Validators.required, ValidationService.percentageValidation] ],
      cardTpvMulti        :['', [Validators.required] ],  
      cardTpvProvId       :['', [ValidationService.numberValidator] ],
      cardTpvProvName     :['', [Validators.minLength(1), Validators.maxLength(64)] ],
      cardTpvArtId        :['', [ValidationService.numberValidator] ],    
      cardTpvArtName      :['', [Validators.minLength(1), Validators.maxLength(32)] ],
      cardTpvStatus       :['', [Validators.required] ]
    })
    this.localTpvTypesRecordList = this.globalVar.tpvTypesRecordList;
    this.localBankAccountsRecordList = this.globalVar.bankAccountsRecordList;
    if (this.createCardTpv === true) {
      this.showCardTpv = true;        
      this.editCardTpv = false;
      this.newCardTpv = new CardTpv();
      this.cleanCardTpvFormData();
      this.cardTpvForm.patchValue( { cardTpvBank: this.localBankAccountsRecordList[0].value, 
                                      cardTpvType: this.localTpvTypesRecordList[0].value} );
      this.cardTpvForm.get('cardTpvDate').patchValue(new Date(this.currentDate));

    } else {
      this.createCardTpv = false;
      this.showCardTpv = true;        
      this.editCardTpv = true;
      this.getOneCardTpv(this.newCardTpv);
    }
  }
  //--------------------------------------- CARDTPVS CREATE -- FORMS --------------------------------------------------------------
  private copyCardTpvFormDataToCardTpv(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> createCardTpvBtnClick ->...Starting...->', null);
    this.newCardTpv.cardTpvDate       = this.cardTpvForm.controls.cardTpvDate.value;
    this.newCardTpv.cardTpvName       = this.cardTpvForm.controls.cardTpvName.value;
    this.newCardTpv.cardTpvShopNr     = this.cardTpvForm.controls.cardTpvShopNr.value;
    this.newCardTpv.cardTpvTpvNr      = this.cardTpvForm.controls.cardTpvTpvNr.value;
    this.newCardTpv.cardTpvSerialNr   = this.cardTpvForm.controls.cardTpvSerialNr.value;
    this.newCardTpv.cardTpvBank       = this.cardTpvForm.controls.cardTpvBank.value;    
    this.newCardTpv.cardTpvType       = this.cardTpvForm.controls.cardTpvType.value;
    this.newCardTpv.cardTpvContact    = this.cardTpvForm.controls.cardTpvContact.value;
    this.newCardTpv.cardTpvComis1     = Number(this.cardTpvForm.get('cardTpvComis1').value);
    this.newCardTpv.cardTpvComis2     = Number(this.cardTpvForm.get('cardTpvComis2').value);
    this.newCardTpv.cardTpvComis3     = Number(this.cardTpvForm.get('cardTpvComis3').value);
    this.newCardTpv.cardTpvComis4     = Number(this.cardTpvForm.get('cardTpvComis4').value);
    this.newCardTpv.cardTpvComisTax   = Number(this.cardTpvForm.get('cardTpvComisTax').value);
    this.newCardTpv.cardTpvMulti      = this.cardTpvForm.controls.cardTpvMulti.value;
    this.newCardTpv.cardTpvStatus     = this.cardTpvForm.controls.cardTpvStatus.value;
  }
  private copyNewCardTpvDataToCardTpvForm(){
    this.cardTpvForm.patchValue( { cardTpvId:this.newCardTpv.cardTpvId, cardTpvName:this.newCardTpv.cardTpvName,  
                                    cardTpvShopNr:this.newCardTpv.cardTpvShopNr, cardTpvTpvNr:this.newCardTpv.cardTpvTpvNr,  
                                    cardTpvSerialNr:this.newCardTpv.cardTpvSerialNr, cardTpvBank:this.newCardTpv.cardTpvBank,  
                                    cardTpvType:this.newCardTpv.cardTpvType, cardTpvContact:this.newCardTpv.cardTpvContact, 
                                    cardTpvMulti:this.newCardTpv.cardTpvMulti, cardTpvStatus:this.newCardTpv.cardTpvStatus} );
    this.currentDate = this.newCardTpv.cardTpvDate;                           
    this.cardTpvForm.get('cardTpvDate').patchValue(new Date(this.currentDate));
    this.cardTpvForm.get('cardTpvComis1').patchValue(this.newCardTpv.cardTpvComis1.toFixed(2));
    this.cardTpvForm.get('cardTpvComis2').patchValue(this.newCardTpv.cardTpvComis2.toFixed(2));
    this.cardTpvForm.get('cardTpvComis3').patchValue(this.newCardTpv.cardTpvComis3.toFixed(2));
    this.cardTpvForm.get('cardTpvComis4').patchValue(this.newCardTpv.cardTpvComis4.toFixed(2));
    this.cardTpvForm.get('cardTpvComisTax').patchValue(this.newCardTpv.cardTpvComisTax.toFixed(2));
    this.cardTpvForm.get('cardTpvProvId').patchValue(this.newCardTpv.cardTpvProvId.toFixed(0));
    this.cardTpvForm.get('cardTpvProvName').patchValue(this.newCardTpv.cardTpvProvName);
    this.cardTpvForm.get('cardTpvArtId').patchValue(this.newCardTpv.cardTpvArtId.toFixed(0));
    this.cardTpvForm.get('cardTpvArtName').patchValue(this.newCardTpv.cardTpvArtName);
  }
  private cleanCardTpvFormData(){
    this.cardTpvForm.patchValue( {  cardTpvName:'', cardTpvShopNr:'', cardTpvTpvNr:'', cardTpvSerialNr:'',
                                    cardTpvBank:this.localBankAccountsRecordList[0].value, 
                                    cardTpvType:this.localTpvTypesRecordList[0].value, cardTpvMulti:false, cardTpvStatus:true} );
    let initialValue = 0;
    this.cardTpvForm.get('cardTpvComis1').patchValue(initialValue.toFixed(2));
    this.cardTpvForm.get('cardTpvComis2').patchValue(initialValue.toFixed(2));
    this.cardTpvForm.get('cardTpvComis3').patchValue(initialValue.toFixed(2));
    this.cardTpvForm.get('cardTpvComis4').patchValue(initialValue.toFixed(2));
    this.cardTpvForm.get('cardTpvComisTax').patchValue(initialValue.toFixed(2));
    this.cardTpvForm.get('cardTpvProvId').patchValue(initialValue.toFixed(0));
    this.cardTpvForm.get('cardTpvProvName').patchValue('');
    this.cardTpvForm.get('cardTpvArtId').patchValue(initialValue.toFixed(0));
    this.cardTpvForm.get('cardTpvArtName').patchValue('');
  }
  public setTwoNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(2);
    }   
  } 
  //---------------------------------------- CARDTPVS CREATE -- BTN CLICK --------------------------------------------------------------
  public closedCardTpvsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.globalVar.getAllCardTpvs();
      //this.bsModalRef.content.callback(this.newCardTpv);
      this.result.next(this.newCardTpv);
      this.bsModalRef.hide();
    }
  }
  public cleanFormCardTpvBtnClick(){
    this.createCardTpv = true;
    this.editCardTpv = false;
    this.cleanCardTpvFormData();
  }
  public createCardTpvBtnClick(cardTpvForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> createCardTpvBtnClick ->...Starting...->', null);
    this.copyCardTpvFormDataToCardTpv();
    this.createOneCardTpv(this.newCardTpv);
  }
  public updateCardTpvBtnClick(cardTpvForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> updateCardTpvBtnClick ->...Starting...->', null);
    this.copyCardTpvFormDataToCardTpv();    
    this.updateOneCardTpv(this.newCardTpv);
  }
  public selectProvBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> selectProvBtnClick ->...Starting...->', null);
    const modalInitialState = {
      provTitle   :'TPV para tarjetas',
      provDate    :this.currentDate,
      callback    :'OK',   
    };
    this.globalVar.openModal(ProvsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){
        this.selectedProv = result;
        this.newCardTpv.cardTpvProvId = this.selectedProv.provId;
        this.newCardTpv.cardTpvProvName = this.selectedProv.provName;
        this.cardTpvForm.get('cardTpvProvId').patchValue(this.newCardTpv.cardTpvProvId.toFixed(0));
        this.cardTpvForm.get('cardTpvProvName').patchValue(this.newCardTpv.cardTpvProvName);
        this.cardTpvForm.updateValueAndValidity();
      }  
    })
  }
  public selectArtBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> selectArtBtnClick ->...Starting...->', null);
    const modalInitialState = {
      artsTitle     :'TPV para tarjetas',
      artsDate      :this.currentDate,
      artsSelMany   :false,
      callback      :'OK',   
    };
    this.globalVar.openModal(ArtsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){   
        this.selectedArt = result;
        this.newCardTpv.cardTpvArtId = this.selectedArt.artId;
        this.newCardTpv.cardTpvArtName = this.selectedArt.artName;
        this.cardTpvForm.get('cardTpvArtId').patchValue(this.newCardTpv.cardTpvArtId.toFixed(0));
        this.cardTpvForm.get('cardTpvArtName').patchValue(this.newCardTpv.cardTpvArtName);
        this.cardTpvForm.updateValueAndValidity();
      }
    })
  }
  //---------------------------------------- CARDTPVS CREATE -- GET CARDTPVS ------------------------------------------------------------
  private createOneCardTpv(newCardTpv: CardTpv): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> createOneCardTpv ->...Starting...->', null);
    this._cardsService.createOneCardTpv(newCardTpv)
      .subscribe({next:(data) => { this.newCardTpv = data;
                           this.closedCardTpvsCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getOneCardTpv(modCardTpv: CardTpv): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> getOneCardTpv ->...Starting...->', null);
    this._cardsService.getOneCardTpv(modCardTpv)
      .subscribe({next:(data) => { this.newCardTpv = data;
                           this.copyNewCardTpvDataToCardTpvForm();                          
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneCardTpv(modCardTpv: CardTpv): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDTPVS CREATE -> updateOneCardTpv ->...Starting...->', null);
    this._cardsService.updateOneCardTpv(modCardTpv)
      .subscribe({next:(data) => { this.newCardTpv = data;
                           this.closedCardTpvsCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}