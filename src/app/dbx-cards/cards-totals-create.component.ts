import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray,FormControl,AbstractControl } from '@angular/forms';
import { Subscription, throwError, forkJoin, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsDatepickerConfig,BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord,ValueRecordNumber } from '../dbc-fields/field';
import { ValidationService } from '../aa-common/validation.service';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { CardPayout, CardPayoutString, CardPayoutSelect, CardTpv, CardTotal, CardTotalString } from './cards';
import { CardsTotalsGroupComponent } from './cards-totals-group.component';
import { CardsService } from './cards.service';
import { ArtFact, Fact,FactString } from '../dbx-facts/facts';
import { FactsSeeOneComponent } from '../dbx-facts/facts-seeone.component';
import { FactsService } from '../dbx-facts/facts.service';
import { Art } from '../dbc-arts/art';
import { ArtService } from '../dbc-arts/art.service';
import { Prov } from '../dbc-provs/prov';
import { ProvsService } from '../dbc-provs/provs.service';

@Component({
  selector      :'app-cards-total-create',
  templateUrl   :'./cards-totals-create.component.html',
  styleUrls     :['./cards.component.css']   
})
export class CardsTotalsCreateComponent implements OnInit {
  public createCardTotal        :boolean;
  public newCardTotal           :CardTotal;
  public workingDateRange       :Date[];
  public cardTpvs               :CardTpv[];
  public cardTpvsRecordList     :ValueRecord[];
  public selectedCardTpv        :CardTpv;
  callback                      :any;
  result: Subject<any> = new Subject<any>();
   
  public formTitles = {
    'title'           :'Totalización Tarjetas', 
    'create'          :'Crear',  
    'edit'            :'Actualizar',  
    'select'          :'Seleccionar', 
    'dates'           :'Desde...Hasta', 
    'totals'          :'xxx,xx euros en xx Pagos',
    'selectNumber'    :'Seleccione el NÚMERO de los Pagos a Añadir', 
    'selectDay'       :'Seleccione el DÍA de los Pagos a Añadir', 
    'selectDates'     :'Seleccione los DÍAS entre los que están los Pagos a Añadir ',
    'changeDateRange' :'Seleccione el PERIODO para seleccionar los pagos a TOTALIZAR',
  };
  public formLabels = {
    'Id'                        :'####',
    'percentage'                :' % -->',
    'euros'                     :' € -->',
    'cardTotalId'               :'Id',
    'cardTotalDate'             :'Fecha',
    'cardTotalDateStart'        :'Desde',
    'cardTotalDateEnd'          :'Hasta',
    'cardTotalCardTpvId'        :'Tpv Id',
    'cardTotalCardTpvName'      :'Tpv',
    'cardTotalCardTpvBank'      :'Banco',
    'cardTotalTotalOper'        :'Nº.Oper.',
    'cardTotalTotalMoney'       :'Din.Total',
    'cardTotalExp1Perc'         :'Comis.Fija',
    'cardTotalExp1Money'        :'Comis.Fija',
    'cardTotalExp2Perc'         :'Comis.X.Oper',
    'cardTotalExp2Money'        :'Comis.X.Oper',   
    'cardTotalExp3Perc'         :'Comis.1.%',
    'cardTotalExp3Money'        :'Comis.1.%',
    'cardTotalExp4Perc'         :'Comis.2.%',
    'cardTotalExp4Money'        :'Comis.2.%',
    'cardTotalExpTotal'         :'Comis.SubTotal',
    'cardTotalExpTaxesPerc'     :'Comis.Imp.%',
    'cardTotalExpTaxesMoney'    :'Comis.Imp',
    'cardTotalExpAdded'         :'Comis.Total',
    'cardTotalBankMoney'        :'Din.Banco',
    'cardTotalFactId'           :'Fatura.Comis.',
    'cardTotalStatus'           :'Estado',
    'cardPayoutId'              :'Id',
    'cardPayoutDate'            :'Fecha',
    'cardPayoutOperationNr'     :'NºOper.',
    'cardPayoutMulti'           :'Cantidad',
    'cardPayoutTotal'           :'Total',
    'cardPayoutTpvId'           :'Tpv.Id',
    'cardPayoutTotalId'         :'Total.Id',
  };
  public  cardTotalForm                 :FormGroup;
  private selectedCardTpvName           :string;
  public  changeCardTotalDates          :boolean;

  public  cardTotalDatePickerConfig     :Partial <BsDatepickerConfig>; 
  private localMinMode                  :BsDatepickerViewMode;
  public  cardTotalDate                 :Date;
  public  cardTotalDateStart            :Date;
  public  cardTotalDateEnd              :Date;
  public  cardTotalDateRange            :Date[];
  public  cardTotalExp1Flag             :boolean;
  public  cardTotalExp2Flag             :boolean;
  public  cardTotalExp3Flag             :boolean;
  public  cardTotalExp4Flag             :boolean;
  public  cardTotalExpTaxesFlag         :boolean;

  private subsExp1Perc                  :Subscription;
  private subsExp2Perc                  :Subscription;
  private subsExp3Perc                  :Subscription;
  private subsExp4Perc                  :Subscription;
  private subsExpTaxPerc                :Subscription;
  private subsExp2Tot                   :Subscription;
  private subsExp3Tot                   :Subscription;
  private subsExp4Tot                   :Subscription;
  private subsExpTaxTot                 :Subscription;

  public  cardPayoutsSelectList         :CardPayoutSelect[];
  public  cardPayoutsList               :CardPayout[];
  private cardPayoutsListToStore        :CardPayout[];
  public  searchCardPayout              :CardPayoutString;
  public  showSearchCardPayoutsList     :boolean;
  public  showCardPayoutsList           :boolean;
  private createCardTotalFactOnEdit     :boolean;
  private newCardTotalFact              :Fact;
  private newCardTotalProv              :Prov;
  private newCardTotalArt               :Art;
  private newCardTotalArtFact           :ArtFact;
  private newCardTotalArtFactList       :ArtFact[];

  public  orderCardsPayoutsList         :string;
  public  reverseCardsPayoutsList       :boolean;
  public  caseInsensitive               :boolean;
  private sortedCollection              :any[];
  private logToConsole                  :boolean;
  //--------------------------------------- CARDS TOTALS CREATE --- CONSTRUCTOR -----------------------------------------------------------------
  constructor (   private fb:FormBuilder, private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService, 
                  private globalVar: GlobalVarService, private _errorService: ErrorService, private _cardService: CardsService,
                  private _artService: ArtService, private _provService: ProvsService, private _factService: FactsService) {  }
  //--------------------------------------- CARDS TOTALS CREATE --- NG ON INIT ------------------------------------------------------------------
  ngOnInit(): void { 
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.orderCardsPayoutsList   = 'cardPayoutOperationNr';
    this.reverseCardsPayoutsList = false;
    this.caseInsensitive         = true;
    this.searchCardPayout        = new CardPayoutString();
    this.showSearchCardPayoutsList = false;
    this.cardPayoutsList = null;
    this.cardPayoutsSelectList = null;
    this.cardTotalDate      = this.workingDateRange[0]; // first day current month
    this.cardTotalDateStart = this.workingDateRange[0];
    this.cardTotalDateEnd   = this.workingDateRange[1];
    this.cardTotalDateRange = new Array();
    this.cardTotalDateRange[0] = this.cardTotalDateStart;
    this.cardTotalDateRange[1] = this.cardTotalDateEnd;
    this.setDatePickerConfig(this.workingDateRange[0],'year');
    this.cardTotalExp1Flag = true;
    this.cardTotalExp2Flag = true;
    this.cardTotalExp3Flag = true;
    this.cardTotalExp4Flag = true;
    this.cardTotalExpTaxesFlag = true;
    this.changeCardTotalDates = true;
    this.cardTotalForm = this.fb.group({
      cardTotalId            :[{value:-1,disabled:true},[Validators.required]],
      cardTotalDate          :[{value:'',disabled:true},[Validators.required]],
      cardTotalDateStart     :[{value:'',disabled:true},[Validators.required]],
      cardTotalDateEnd       :[{value:'',disabled:true},[Validators.required]],
      cardTotalCardTpvId     :[{value:'',disabled:true},[Validators.required]],
      cardTotalCardTpvName   :[{value:'',disabled:false},[Validators.required]],
      cardTotalCardTpvBank   :[{value:'',disabled:true},[Validators.required]],
      cardTotalTotalOper     :[{value:0,disabled:true},[Validators.required,ValidationService.positiveIntegerValidation]],
      cardTotalTotalMoney    :[{value:0,disabled:false},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExp1Perc      :[{value:0,disabled:!this.cardTotalExp1Flag},[Validators.required,ValidationService.percentageValidation]],
      cardTotalExp1Money     :[{value:0,disabled:this.cardTotalExp1Flag},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExp2Perc      :[{value:0,disabled:!this.cardTotalExp2Flag},[Validators.required,ValidationService.percentageValidation]],
      cardTotalExp2Money     :[{value:0,disabled:this.cardTotalExp2Flag},[Validators.required,ValidationService.twoDecimalsValidation]],    
      cardTotalExp3Perc      :[{value:0,disabled:!this.cardTotalExp3Flag},[Validators.required,ValidationService.percentageValidation]],
      cardTotalExp3Money     :[{value:0,disabled:this.cardTotalExp3Flag},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExp4Perc      :[{value:0,disabled:!this.cardTotalExp4Flag},[Validators.required,ValidationService.percentageValidation]],
      cardTotalExp4Money     :[{value:0,disabled:this.cardTotalExp4Flag},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExpTotal      :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExpTaxesPerc  :[{value:0,disabled:false},[Validators.required,ValidationService.percentageValidation]],
      cardTotalExpTaxesMoney :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalExpAdded      :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalBankMoney     :[{value:0,disabled:true},[Validators.required,ValidationService.twoDecimalsValidation]],
      cardTotalFactId        :[{value:'',disabled:true},[]],
      cardTotalStatus        :[{value:true,disabled:false},[Validators.required]],
    }) 
    this.setTitleAndTotals(this.cardPayoutsSelectList);
    this.subcribeToPerc();
    if (this.createCardTotal === true) {
      this.showCardPayoutsList = false;
      this.cleanCardTotalFormData();
    } else {
      this.createCardTotalFactOnEdit = false;
      this.cardTotalDateStart = new Date(this.newCardTotal.cardTotalDateStart);
      this.cardTotalDateEnd   = new Date(this.newCardTotal.cardTotalDateEnd);
      this.cardTotalDateRange[0] = this.cardTotalDateStart;
      this.cardTotalDateRange[1] = this.cardTotalDateEnd;
      this.getCardTotalDataToEdit(this.newCardTotal);
    }
  }
  //--------------------------------------- CARDS TOTALS CREATE --- GENERAL ---------------------------------------------------------------------
  private setTitleAndTotals(localCardPayoutsSelectList :CardPayoutSelect[]){
    if (this.selectedCardTpv != null) this.selectedCardTpvName = this.selectedCardTpv.cardTpvName;
    else  this.selectedCardTpvName = 'ALL';    
    this.formTitles.dates = 'TPV...'+ this.selectedCardTpvName.slice(0,15)+'...Del..'+this.cardTotalDateStart.toLocaleDateString()+'..Al..'+this.cardTotalDateEnd.toLocaleDateString();
    if (localCardPayoutsSelectList === null) return;
    let localCardTotalId = Number(this.cardTotalForm.get('cardTotalId').value);
    let selectedPayoutsTotalMoney = 0;
    let numberOfSelectedPayouts = 0;
    let numberOfTotalPayouts = 0;
    if (localCardPayoutsSelectList != null){
      for (let k=0;k<localCardPayoutsSelectList.length;k++){
        numberOfTotalPayouts = numberOfTotalPayouts + localCardPayoutsSelectList[k].cardPayoutMulti;          
        if (localCardPayoutsSelectList[k].cardPayoutSelected === true) {
          localCardPayoutsSelectList[k].cardPayoutTotalId = localCardTotalId;
          localCardPayoutsSelectList[k].cardPayoutTotalDate = this.cardTotalDate;
          selectedPayoutsTotalMoney = selectedPayoutsTotalMoney + (localCardPayoutsSelectList[k].cardPayoutMulti * localCardPayoutsSelectList[k].cardPayoutTotal);
          numberOfSelectedPayouts = numberOfSelectedPayouts + localCardPayoutsSelectList[k].cardPayoutMulti;
        } else {
          localCardPayoutsSelectList[k].cardPayoutTotalId = null;
          localCardPayoutsSelectList[k].cardPayoutTotalDate = null;
        }
      }
    }
    this.formTitles.totals = selectedPayoutsTotalMoney.toFixed(2)+' € en '+numberOfSelectedPayouts.toFixed(0)+' Pagos (de '+numberOfTotalPayouts.toFixed(0)+')';
    this.updateCardTotalsInForm(numberOfSelectedPayouts,selectedPayoutsTotalMoney);
  }
  private updateStartAndEndDatesCardPayoutsList(localCardPayoutsSelectList :CardPayoutSelect[]){
    if (localCardPayoutsSelectList === null) return;
    let startDate =  this.cardTotalDateRange[0];
    let endDate   =  this.cardTotalDateRange[1];
    let firstSelected = true;
    for (let k=0;k<localCardPayoutsSelectList.length;k++){
      if (localCardPayoutsSelectList[k].cardPayoutSelected === true) {
        if (firstSelected === true){        // set start and end dates to first selected Payout
          firstSelected = false;
          startDate = new Date(localCardPayoutsSelectList[k].cardPayoutDate);
          endDate = startDate;
        }
        let localCardPayoutDate = new Date(localCardPayoutsSelectList[k].cardPayoutDate);
        if (startDate.getTime() > localCardPayoutDate.getTime()) startDate = localCardPayoutDate;
        if (endDate.getTime() < localCardPayoutDate.getTime()) endDate = localCardPayoutDate;
      } 
    }
    this.cardTotalForm.get('cardTotalDateStart').patchValue(startDate);
    this.cardTotalForm.get('cardTotalDateEnd').patchValue(endDate);   
  }
  private setDatePickerConfig(selectedDate :Date, mode :string){
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var startDate = new Date();  
    var endDate   = new Date();
    var day;  
    switch (mode){
      case 'month': startDate = new Date(year,month,1); //firstDayOfMonth
                    endDate   = new Date(year,month+1,0); //lastDayOfMonth
                    break;
      case 'year':  day = selectedDate.getDate();
                    if (month > 5) {
                      startDate = new Date(year, month-6, day); //startCurrentYear
                      endDate   = new Date(year+1, month, day); //endCurrentYear
                    } else {
                      startDate = new Date(year-1, month, day); //startCurrentYear
                      endDate   = new Date(year, month+6, day); //endCurrentYear
                    }
                    break;
      case '2year': day = selectedDate.getDate();
                    startDate = new Date(year - 1, month, day); //startCurrentYear
                    endDate   = new Date(year + 1, month, day); //endCurrentYear
                    break;
      case 'period':day = selectedDate.getDate();
                    startDate = new Date(year - 5, month, day); //fiveYearsBefore
                    endDate   = new Date(year + 5, month, day); //fiveYearsAfter
                    break;
      default:      day = selectedDate.getDate();
                    startDate = new Date(year - 5, month, day); //fiveYearsBefore
                    endDate   = new Date(year + 5, month, day); //fiveYearsAfter
                    break;
    }
    var dateContainerClass  = 'theme-dark-blue';
    var dateInputFormat     = 'DD-MMM-YYYY';
    this.localMinMode       =  "day";
    this.cardTotalDatePickerConfig = Object.assign ( {}, {
      containerClass:     dateContainerClass,
      showWeekNumbers:    true, 
      minDate:            startDate,      
      maxDate:            endDate,
      dateInputFormat:    dateInputFormat,
      minMode:            this.localMinMode,
      adaptivePosition:   true,
      isAnimated:         true,
      } );
  }
  private addSelectToCardsPayouts(localList :CardPayout[], selectedStatus :boolean) :CardPayoutSelect[]{
    if ((localList === null) || (localList.length === 0)) return null;
    var localCardPayoutsSelectList = new Array();
    var newCardPayoutSelect = new CardPayoutSelect();
    for (let k=0;k<localList.length;k++) {
      newCardPayoutSelect = new CardPayoutSelect();
      newCardPayoutSelect.cardPayoutId          = localList[k].cardPayoutId;
      newCardPayoutSelect.cardPayoutDate        = localList[k].cardPayoutDate;
      newCardPayoutSelect.cardPayoutMulti       = localList[k].cardPayoutMulti;
      newCardPayoutSelect.cardPayoutOperationNr = localList[k].cardPayoutOperationNr;
      newCardPayoutSelect.cardPayoutTotal       = localList[k].cardPayoutTotal;
      newCardPayoutSelect.cardPayoutTotalId     = localList[k].cardPayoutTotalId;
      newCardPayoutSelect.cardPayoutTotalDate   = localList[k].cardPayoutTotalDate;
      newCardPayoutSelect.cardPayoutTpvId       = localList[k].cardPayoutTpvId;
      newCardPayoutSelect.cardPayoutSelected    = selectedStatus;
      localCardPayoutsSelectList.push(newCardPayoutSelect);
    }
    return localCardPayoutsSelectList;
  }
  private copySelectedCardPayoutsToStore(localCardTotalId :number, localCardTotalDate :Date){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->copySelectedCardPayoutsToStore ->...Starting...->',null);
    this.cardPayoutsListToStore = new Array();
    for (let k=0;k<this.cardPayoutsSelectList.length;k++) {
      if ( this.cardPayoutsSelectList[k].cardPayoutSelected === true) {
        let localCardPayout = new CardPayout();        
        localCardPayout.cardPayoutId          = this.cardPayoutsSelectList[k].cardPayoutId;
        localCardPayout.cardPayoutDate        = this.cardPayoutsSelectList[k].cardPayoutDate;
        localCardPayout.cardPayoutMulti       = this.cardPayoutsSelectList[k].cardPayoutMulti;
        localCardPayout.cardPayoutOperationNr = this.cardPayoutsSelectList[k].cardPayoutOperationNr;
        localCardPayout.cardPayoutTotal       = this.cardPayoutsSelectList[k].cardPayoutTotal;
        localCardPayout.cardPayoutTotalId     = localCardTotalId;
        localCardPayout.cardPayoutTotalDate   = localCardTotalDate;
        localCardPayout.cardPayoutTpvId       = this.cardPayoutsSelectList[k].cardPayoutTpvId; 
        this.cardPayoutsListToStore.push(localCardPayout);               
      }
    }     
  }
  private checkCardTotalFact():boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->checkCardTotalFact ->...Starting...->', null);
    // There will be always one fact for comissions in cards totalization, even with these comissions are 0
    // error, prov and/or art are not defined for this tpv to create comissions related fact
    if ( (this.newCardTotalProv === null) || ( this.newCardTotalArt === null) ) {
      this._errorService.showMsgObject("errorMsg","Totalizar Tarjetas","Totalizar Tarjetas",
                        this.selectedCardTpv,"ERROR Crear Factura Comisiones","Actualice los Datos del TPV","Falta Proveedor y/o Artículo para Factura","","","","");
      return false;
    }                                       
    if (this.createCardTotal === true) {                        // creating total card
      this.newCardTotal.cardTotalFactId = null;
      this.newCardTotalFact = new Fact();
      this.newCardTotalFact.factId            = -1;
      this.newCardTotalFact.factNumber        = 'Totalizar->'+new Date(this.newCardTotal.cardTotalDate).toLocaleDateString()+
                                                '->'+this.selectedCardTpvName.slice(0,8);   //  12 + 9 + 3 + 8 = 32
      this.newCardTotalArtFact = new ArtFact();
      this.newCardTotalArtFact.artFactFactId  = -1;
      this.newCardTotalArtFact.artFactId      = -1;
      this.newCardTotalArtFactList = new Array();
    } else {                                                    // editing total card, if there are no fact and/or no artfact, they are created
      if (this.newCardTotalFact === null) {                     // there was no fact, maybe comissions were 0
        this.newCardTotal.cardTotalFactId = null;
        this.newCardTotalFact = new Fact();
        this.newCardTotalFact.factId            = -1;
        this.newCardTotalFact.factNumber        = 'Totalizar->'+new Date(this.newCardTotal.cardTotalDate).toLocaleDateString()+
                                                  '->'+this.selectedCardTpvName.slice(0,8);
      }
      if ( (this.newCardTotalArtFactList === null) || (this.newCardTotalArtFactList.length === 0) ) { // there was no artfact, maybe comissions were 0
        this.newCardTotalArtFact = new ArtFact();
        this.newCardTotalArtFact.artFactFactId  = null;
        this.newCardTotalArtFact.artFactId      = -1;
      }
    }
    this.newCardTotalFact.factNote        = this.newCardTotalProv.provFactNote;
    this.newCardTotalFact.factType        = this.newCardTotalProv.provFactType;
    //this.newCardTotalFact.factPayType     = this.newCardTotalProv.provPayType;
    this.newCardTotalFact.factPayType     = this.selectedCardTpv.cardTpvBank;
    this.newCardTotalFact.factProvId      = this.newCardTotalProv.provId;
    this.newCardTotalFact.factProvName    = this.newCardTotalProv.provName; 
    this.newCardTotalFact.factDate        = this.newCardTotal.cardTotalDate;
    this.newCardTotalFact.factNetCost     = this.newCardTotal.cardTotalExpTotal;
    this.newCardTotalFact.factTaxes       = this.newCardTotal.cardTotalExpTaxesMoney;
    this.newCardTotalFact.factTotalCost   = this.newCardTotal.cardTotalExpTaxesMoney + this.newCardTotal.cardTotalExpTotal;
    this.newCardTotalFact.factDetailed    = true;
    this.newCardTotalFact.factStatus      = true;

    this.newCardTotalArtFact.artFactArtId             = this.newCardTotalArt.artId;
    this.newCardTotalArtFact.artFactArtName           = this.newCardTotalArt.artName;
    this.newCardTotalArtFact.artFactArtType           = this.newCardTotalArt.artType;
    this.newCardTotalArtFact.artFactArtMeasurement    = this.newCardTotalArt.artMeasurement;
    this.newCardTotalArtFact.artFactArtSize           = this.newCardTotalArt.artSize;
    this.newCardTotalArtFact.artFactArtUnitPrice      = this.newCardTotalArt.artUnitPrice;
    this.newCardTotalArtFact.artFactArtPackageUnits   = this.newCardTotalArt.artPackageUnits;
    this.newCardTotalArtFact.artFactArtPackagePrice   = this.newCardTotalArt.artPackagePrice;
    this.newCardTotalArtFact.artFactArtTax            = this.newCardTotal.cardTotalExpTaxesPerc;    
    this.newCardTotalArtFact.artFactFactDate          = this.newCardTotal.cardTotalDate;
    this.newCardTotalArtFact.artFactNetCost           = this.newCardTotal.cardTotalExpTotal;
    this.newCardTotalArtFact.artFactTax               = this.newCardTotal.cardTotalExpTaxesMoney;
    this.newCardTotalArtFact.artFactTotalCost         = this.newCardTotal.cardTotalExpTaxesMoney + this.newCardTotal.cardTotalExpTotal;
    this.newCardTotalArtFact.artFactDiscount          = 0;
    this.newCardTotalArtFact.artFactQuantity          = 1;
    this.newCardTotalArtFactList = new Array();
    this.newCardTotalArtFactList.push(this.newCardTotalArtFact);
    return true;
  }  
  //--------------------------------------- CARDS TOTALS CREATE --- FORMS -----------------------------------------------------------------------
  private subcribeToPerc(){
    this.subsExp1Perc = this.cardTotalForm.get('cardTotalExp1Perc').valueChanges.subscribe((data:any) => {
      this.setTitleAndTotals(this.cardPayoutsSelectList);
    });
    this.subsExp2Perc = this.cardTotalForm.get('cardTotalExp2Perc').valueChanges.subscribe((data:any) => {
      this.setTitleAndTotals(this.cardPayoutsSelectList);
    });
    this.subsExp3Perc = this.cardTotalForm.get('cardTotalExp3Perc').valueChanges.subscribe((data:any) => {
      this.setTitleAndTotals(this.cardPayoutsSelectList);
    });
    this.subsExp4Perc = this.cardTotalForm.get('cardTotalExp4Perc').valueChanges.subscribe((data:any) => {
      this.setTitleAndTotals(this.cardPayoutsSelectList);
    });
    this.subsExpTaxPerc = this.cardTotalForm.get('cardTotalExpTaxesPerc').valueChanges.subscribe((data:any) => {
      this.setTitleAndTotals(this.cardPayoutsSelectList);
    });
  }
  private setCardTpvInCardTotalFormData(localCardTpv :CardTpv){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> setCardTpvInCardTotalFormData ->...Starting...->', null); 
    if (localCardTpv != null) {
      this.newCardTotal.cardTotalCardTpvId    = localCardTpv.cardTpvId;
      this.newCardTotal.cardTotalCardTpvName  = localCardTpv.cardTpvName;
      this.newCardTotal.cardTotalCardTpvBank  = localCardTpv.cardTpvBank;
      this.newCardTotal.cardTotalExp1Perc     = localCardTpv.cardTpvComis1;
      this.newCardTotal.cardTotalExp2Perc     = localCardTpv.cardTpvComis2;
      this.newCardTotal.cardTotalExp3Perc     = localCardTpv.cardTpvComis3;
      this.newCardTotal.cardTotalExp4Perc     = localCardTpv.cardTpvComis4;
      this.newCardTotal.cardTotalExpTaxesPerc = localCardTpv.cardTpvComisTax;
    } else {
      this.newCardTotal.cardTotalCardTpvId    = 0;
      this.newCardTotal.cardTotalCardTpvName  = '';
      this.newCardTotal.cardTotalCardTpvBank  = '';
      this.newCardTotal.cardTotalExp1Perc     = 0;
      this.newCardTotal.cardTotalExp2Perc     = 0;
      this.newCardTotal.cardTotalExp3Perc     = 0;
      this.newCardTotal.cardTotalExp4Perc     = 0;
      this.newCardTotal.cardTotalExpTaxesPerc = 0;
    }
    this.cardTotalForm.get('cardTotalCardTpvId').patchValue(this.newCardTotal.cardTotalCardTpvId);  
    this.cardTotalForm.get('cardTotalCardTpvName').patchValue(this.newCardTotal.cardTotalCardTpvName);  
    this.cardTotalForm.get('cardTotalCardTpvBank').patchValue(this.newCardTotal.cardTotalCardTpvBank);  
    this.cardTotalForm.get('cardTotalExp1Perc').patchValue(this.newCardTotal.cardTotalExp1Perc);  
    this.cardTotalForm.get('cardTotalExp2Perc').patchValue(this.newCardTotal.cardTotalExp2Perc);  
    this.cardTotalForm.get('cardTotalExp3Perc').patchValue(this.newCardTotal.cardTotalExp3Perc);  
    this.cardTotalForm.get('cardTotalExp4Perc').patchValue(this.newCardTotal.cardTotalExp4Perc);
    this.cardTotalForm.get('cardTotalExpTaxesPerc').patchValue(this.newCardTotal.cardTotalExpTaxesPerc);  
    this.cardTotalForm.updateValueAndValidity(); 
  }
  private updateCardTotalsInForm(localNumberOfSelectedPayouts :number, localSelectedPayoutsTotalMoney :number){
    this.cardTotalForm.get('cardTotalTotalOper').patchValue(localNumberOfSelectedPayouts.toFixed(0));
    this.cardTotalForm.get('cardTotalTotalMoney').patchValue(localSelectedPayoutsTotalMoney.toFixed(2));
    var exp1Perc,exp2Perc,exp3Perc,exp4Perc,expTaxesPerc :number;
    var exp1Money,exp2Money,exp3Money,exp4Money,expTotalMoney,expTaxesMoney,totalBankMoney :number;
    exp1Perc = Number(this.cardTotalForm.get('cardTotalExp1Perc').value);
    exp1Money = exp1Perc;                                 // Fixed comission
    if (this.cardTotalExp2Flag === true) {
      exp2Perc = Number(this.cardTotalForm.get('cardTotalExp2Perc').value);
      exp2Money = exp2Perc * localNumberOfSelectedPayouts;     // comission per operacion
    } else {
      exp2Money = Number(this.cardTotalForm.get('cardTotalExp2Money').value);
    }
    if (this.cardTotalExp3Flag === true) {
      exp3Perc = Number(this.cardTotalForm.get('cardTotalExp3Perc').value);
      exp3Money = exp3Perc * localSelectedPayoutsTotalMoney / 100;        // percentage of total amount
    } else {
      exp3Money = Number(this.cardTotalForm.get('cardTotalExp3Money').value);
    }
    if (this.cardTotalExp4Flag === true) {
      exp4Perc = Number(this.cardTotalForm.get('cardTotalExp4Perc').value);
      exp4Money = exp4Perc * localSelectedPayoutsTotalMoney / 100;        // percentage of total amount
    } else {
      exp4Money = Number(this.cardTotalForm.get('cardTotalExp4Money').value);
    }
    expTotalMoney = exp1Money + exp2Money + exp3Money + exp4Money;
    if (this.cardTotalExpTaxesFlag === true) {
      expTaxesPerc = Number(this.cardTotalForm.get('cardTotalExpTaxesPerc').value);
      expTaxesMoney = expTotalMoney * expTaxesPerc / 100;
    } else {
      expTaxesMoney = Number(this.cardTotalForm.get('cardTotalExpTaxesMoney').value);
    }   
    totalBankMoney = localSelectedPayoutsTotalMoney - expTotalMoney - expTaxesMoney;
    var totalExp = expTotalMoney + expTaxesMoney;
    this.newCardTotal.cardTotalExp1Money = exp1Money;
    this.newCardTotal.cardTotalExp2Money = exp2Money;
    this.newCardTotal.cardTotalExp3Money = exp3Money;
    this.newCardTotal.cardTotalExp4Money = exp4Money;
    this.newCardTotal.cardTotalExpTaxesMoney = expTaxesMoney;
    this.newCardTotal.cardTotalExpTotal = expTotalMoney;
    this.newCardTotal.cardTotalBankMoney = totalBankMoney;
    this.cardTotalForm.get('cardTotalExp1Money').patchValue(exp1Money.toFixed(2));
    if (this.cardTotalExp2Flag === true) this.cardTotalForm.get('cardTotalExp2Money').patchValue(exp2Money.toFixed(2));
    if (this.cardTotalExp3Flag === true) this.cardTotalForm.get('cardTotalExp3Money').patchValue(exp3Money.toFixed(2));
    if (this.cardTotalExp4Flag === true) this.cardTotalForm.get('cardTotalExp4Money').patchValue(exp4Money.toFixed(2));
    this.cardTotalForm.get('cardTotalExpTotal').patchValue(expTotalMoney.toFixed(2));
    if (this.cardTotalExpTaxesFlag === true) this.cardTotalForm.get('cardTotalExpTaxesMoney').patchValue(expTaxesMoney.toFixed(2));
    this.cardTotalForm.get('cardTotalBankMoney').patchValue(totalBankMoney.toFixed(2));
    this.cardTotalForm.get('cardTotalExpAdded').patchValue(totalExp.toFixed(2));
    this.cardTotalForm.updateValueAndValidity(); 
  }
  private copyCardTotalFormToNewCardTotal(){
    this.newCardTotal.cardTotalId             = Number(this.cardTotalForm.get('cardTotalId').value);
    this.newCardTotal.cardTotalDate           = this.cardTotalDate; // last day of month
    this.newCardTotal.cardTotalCardTpvId      = Number(this.cardTotalForm.get('cardTotalCardTpvId').value);
    this.newCardTotal.cardTotalCardTpvName    = this.cardTotalForm.get('cardTotalCardTpvName').value;
    this.newCardTotal.cardTotalCardTpvBank    = this.cardTotalForm.get('cardTotalCardTpvBank').value;
    this.newCardTotal.cardTotalTotalOper      = Number(this.cardTotalForm.get('cardTotalTotalOper').value);
    this.newCardTotal.cardTotalTotalMoney     = Number(this.cardTotalForm.get('cardTotalTotalMoney').value);
    this.newCardTotal.cardTotalExp1Perc       = Number(this.cardTotalForm.get('cardTotalExp1Perc').value);
    this.newCardTotal.cardTotalExp1Money      = Number(this.cardTotalForm.get('cardTotalExp1Money').value);
    this.newCardTotal.cardTotalExp2Perc       = Number(this.cardTotalForm.get('cardTotalExp2Perc').value);
    this.newCardTotal.cardTotalExp2Money      = Number(this.cardTotalForm.get('cardTotalExp2Money').value);    
    this.newCardTotal.cardTotalExp3Perc       = Number(this.cardTotalForm.get('cardTotalExp3Perc').value);
    this.newCardTotal.cardTotalExp3Money      = Number(this.cardTotalForm.get('cardTotalExp3Money').value);
    this.newCardTotal.cardTotalExp4Perc       = Number(this.cardTotalForm.get('cardTotalExp4Perc').value);
    this.newCardTotal.cardTotalExp4Money      = Number(this.cardTotalForm.get('cardTotalExp4Money').value);
    this.newCardTotal.cardTotalExpTotal       = Number(this.cardTotalForm.get('cardTotalExpTotal').value);
    this.newCardTotal.cardTotalExpTaxesPerc   = Number(this.cardTotalForm.get('cardTotalExpTaxesPerc').value);
    this.newCardTotal.cardTotalExpTaxesMoney  = Number(this.cardTotalForm.get('cardTotalExpTaxesMoney').value);
    this.newCardTotal.cardTotalBankMoney      = Number(this.cardTotalForm.get('cardTotalBankMoney').value);
    this.newCardTotal.cardTotalStatus         = this.cardTotalForm.get('cardTotalStatus').value;
    // update start and end date with only selected cardPayouts
    this.updateStartAndEndDatesCardPayoutsList(this.cardPayoutsSelectList);
    this.newCardTotal.cardTotalDateStart      = this.cardTotalForm.get('cardTotalDateStart').value;
    this.newCardTotal.cardTotalDateEnd        = this.cardTotalForm.get('cardTotalDateEnd').value;
    this.newCardTotal.cardTotalFactId         = Number(this.cardTotalForm.get('cardTotalFactId').value);

  }
  private copyNewCardTotalToForm(localNewCardTotal :CardTotal){
    this.cardTotalForm.get('cardTotalId').patchValue(this.newCardTotal.cardTotalId);
    this.cardTotalForm.get('cardTotalDate').patchValue(new Date(this.newCardTotal.cardTotalDate));
    this.cardTotalForm.get('cardTotalDateStart').patchValue(new Date(this.newCardTotal.cardTotalDateStart));
    this.cardTotalForm.get('cardTotalDateEnd').patchValue(new Date(this.newCardTotal.cardTotalDateEnd));
    this.cardTotalForm.get('cardTotalCardTpvId').patchValue(this.newCardTotal.cardTotalCardTpvId);
    this.cardTotalForm.get('cardTotalCardTpvName').patchValue(this.newCardTotal.cardTotalCardTpvName);
    this.cardTotalForm.get('cardTotalCardTpvBank').patchValue(this.newCardTotal.cardTotalCardTpvBank);
    this.cardTotalForm.get('cardTotalTotalOper').patchValue(this.newCardTotal.cardTotalTotalOper);
    this.cardTotalForm.get('cardTotalTotalMoney').patchValue(this.newCardTotal.cardTotalTotalMoney);
    this.cardTotalForm.get('cardTotalExp1Perc').patchValue(this.newCardTotal.cardTotalExp1Perc);
    this.cardTotalForm.get('cardTotalExp1Money').patchValue(this.newCardTotal.cardTotalExp1Money);
    this.cardTotalForm.get('cardTotalExp2Perc').patchValue(this.newCardTotal.cardTotalExp2Perc);
    this.cardTotalForm.get('cardTotalExp2Money').patchValue(this.newCardTotal.cardTotalExp2Money);    
    this.cardTotalForm.get('cardTotalExp3Perc').patchValue(this.newCardTotal.cardTotalExp3Perc);
    this.cardTotalForm.get('cardTotalExp3Money').patchValue(this.newCardTotal.cardTotalExp3Money);
    this.cardTotalForm.get('cardTotalExp4Perc').patchValue(this.newCardTotal.cardTotalExp4Perc);
    this.cardTotalForm.get('cardTotalExp4Money').patchValue(this.newCardTotal.cardTotalExp4Money);
    this.cardTotalForm.get('cardTotalExpTotal').patchValue(this.newCardTotal.cardTotalExpTotal);
    this.cardTotalForm.get('cardTotalExpTaxesPerc').patchValue(this.newCardTotal.cardTotalExpTaxesPerc);
    this.cardTotalForm.get('cardTotalExpTaxesMoney').patchValue(this.newCardTotal.cardTotalExpTaxesMoney);
    this.cardTotalForm.get('cardTotalBankMoney').patchValue(this.newCardTotal.cardTotalBankMoney);
    this.cardTotalForm.get('cardTotalFactId').patchValue(this.newCardTotal.cardTotalFactId);
    this.cardTotalForm.get('cardTotalStatus').patchValue(this.newCardTotal.cardTotalStatus);
  }
  private cleanCardTotalFormData(){   
    this.newCardTotal.cardTotalId           = -1;
    this.newCardTotal.cardTotalFactId       = -1;
    this.newCardTotal.cardTotalDate         = this.cardTotalDate;
    this.newCardTotal.cardTotalDateStart    = this.cardTotalDateStart;
    this.newCardTotal.cardTotalDateEnd      = this.cardTotalDateEnd;
    if (this.selectedCardTpv != null) {
      this.newCardTotal.cardTotalCardTpvId    = this.selectedCardTpv.cardTpvId;
      this.newCardTotal.cardTotalCardTpvName  = this.selectedCardTpv.cardTpvName;
      this.newCardTotal.cardTotalCardTpvBank  = this.selectedCardTpv.cardTpvBank;
    } else {
      this.newCardTotal.cardTotalCardTpvId    = -1;
      this.newCardTotal.cardTotalCardTpvName  = '';
      this.newCardTotal.cardTotalCardTpvBank  = '';
    }
    this.cardTotalForm.get('cardTotalId').patchValue(this.newCardTotal.cardTotalId);
    this.cardTotalForm.get('cardTotalCardTpvId').patchValue(this.newCardTotal.cardTotalCardTpvId);  
    this.cardTotalForm.get('cardTotalCardTpvName').patchValue(this.newCardTotal.cardTotalCardTpvName);  
    this.cardTotalForm.get('cardTotalCardTpvBank').patchValue(this.newCardTotal.cardTotalCardTpvBank);  
    this.cardTotalForm.get('cardTotalDate').patchValue(this.newCardTotal.cardTotalDate);  
    this.cardTotalForm.get('cardTotalDateStart').patchValue(this.newCardTotal.cardTotalDateStart);  
    this.cardTotalForm.get('cardTotalDateEnd').patchValue(this.newCardTotal.cardTotalDateEnd);
    this.cardTotalForm.patchValue( {cardTotalTotalMoney:0,cardTotalExp1Perc:0,cardTotalExp1Money:0,cardTotalExp2Perc:0,cardTotalExp2Money:0,
      cardTotalExp3Perc:0,cardTotalExp3Money:0, cardTotalExp4Perc:0,cardTotalExp4Money:0,cardTotalExpTotal:0, 
      cardTotalExpTaxesPerc:0,cardTotalBankMoney:0} );

    this.cardTotalForm.updateValueAndValidity();                                               
  }
  //--------------------------------------- CARDS TOTALS CREATE --- BTN CLICK -------------------------------------------------------------------
  public selectCardTpvBtnClick(){  
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> selectCardTpvBtnClick->...Starting...->', null); 
    if (this.cardTpvsRecordList === null) {
      this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
                        "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
      return;
    }          
    const modalInitialState = {
      localValueRecordList  :this.cardTpvsRecordList,
      selectTitle1          :"TPVS para TARJETAS",
      selectTitle2          :"Seleccionar TPV",
      selectMessage         :"Seleccione el TPV del que quiere TOTALIZAR",
      selectLabel           :"TPV Tarjetas:",       
      selectFooter          :"TPVS para TARJETAS",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){   
        var selTpvTypeTpvname :string;
        selTpvTypeTpvname = result;                         
        var selectedTpvName = selTpvTypeTpvname.substring(6);
        this.selectedCardTpvName = selectedTpvName;
        this.newCardTotal.cardTotalCardTpvName = this.selectedCardTpvName;
        this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedCardTpvName);  
        this.setCardTpvInCardTotalFormData(this.selectedCardTpv);
        this.getCardTotalDataToCreate(this.newCardTotal);
        //var localCardPayoutsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpvName,this.cardTotalDateStart,this.cardTotalDateEnd);    
        //this.getAllCardPayoutsNotTotal(localCardPayoutsVRList);           
      }
    })
  }
  public changeDateStartBtnClick(){
    // start date can be changed from first date of current month to any previous date one year before
    var localWorkingDateRange :Date[];
    localWorkingDateRange = new Array();
    localWorkingDateRange[0] = new Date(this.cardTotalDate);
    localWorkingDateRange[1] = new Date(this.cardTotalDate);
    this.globalVar.enterInformation("deleteMsg",this.formTitles.title,this.formTitles.title,
                                    localWorkingDateRange,"yearBefore",this.formTitles.changeDateRange,"","","","","","").
          then((result:Date[])=>{
              // update date start period in form
              this.newCardTotal.cardTotalDateStart = new Date(result[0]);
              this.cardTotalDateStart = this.newCardTotal.cardTotalDateStart;
              this.cardTotalDateRange[0] = this.cardTotalDateStart;
              this.cardTotalForm.get('cardTotalDateStart').patchValue(this.newCardTotal.cardTotalDateStart); 
              this.cardTotalForm.updateValueAndValidity();
              // If card tpv is selected, read all card payouts within new period
              if (this.selectedCardTpv != null) {
                var localCardPayoutsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpv.cardTpvName,this.cardTotalDateStart,this.cardTotalDateEnd);    
                this.getAllCardPayoutsNotTotal(localCardPayoutsVRList); 
              }

          }).catch(error => {
              this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> changeDateStartBtnClick ->...Error...->', error);
          });
  }
  public changeDateEndBtnClick(){
    this.cardTotalForm.get('cardTotalDateEnd').patchValue(this.cardTotalDateEnd); 
    this.cardTotalForm.updateValueAndValidity();
    if (this.selectedCardTpv != null) {
      var localCardPayoutsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpv.cardTpvName,this.cardTotalDateStart,this.cardTotalDateEnd);    
      this.getAllCardPayoutsNotTotal(localCardPayoutsVRList); 
    }
  }
  public showCardTotalFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->showCardTotalFactBtnClick ->...Starting...->', null);
    var isFactOK = this.checkCardTotalFact();
    if (isFactOK === true) {
      const modalInitialState = {
        factsTitle    :'Factura Comisiones TPV',
        newFact       :this.newCardTotalFact,
        artFactsList  :this.newCardTotalArtFactList,
        callback      :'OK',   
      };
      this.globalVar.openModal(FactsSeeOneComponent, modalInitialState, 'modalXlTop0Left0').
      then((result:any)=>{
        if (result != null){  }
      })
    }
  }
  public setExp2PercBtnClick(){
    if (this.cardTotalExp2Flag === false) {
      this.subsExp2Tot.unsubscribe();
      this.cardTotalForm.get('cardTotalExp2Money').disable(); 
      this.cardTotalForm.get('cardTotalExp2Perc').enable(); 
      this.subsExp2Perc = this.cardTotalForm.get('cardTotalExp2Perc').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp2Flag = true;     
    } 
  }
  public setExp2MoneyBtnClick(){
    if (this.cardTotalExp2Flag === true) {
      this.subsExp2Perc.unsubscribe();
      this.cardTotalForm.get('cardTotalExp2Perc').disable(); 
      this.cardTotalForm.get('cardTotalExp2Money').enable(); 
      this.subsExp2Tot = this.cardTotalForm.get('cardTotalExp2Money').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp2Flag = false;
    } 
  }
  public setExp3PercBtnClick(){
    if (this.cardTotalExp3Flag === false) {
      this.subsExp3Tot.unsubscribe();
      this.cardTotalForm.get('cardTotalExp3Money').disable(); 
      this.cardTotalForm.get('cardTotalExp3Perc').enable(); 
      this.subsExp3Perc = this.cardTotalForm.get('cardTotalExp3Perc').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp3Flag = true;     
    } 
  }
  public setExp3MoneyBtnClick(){
    if (this.cardTotalExp3Flag === true) {
      this.subsExp3Perc.unsubscribe();
      this.cardTotalForm.get('cardTotalExp3Perc').disable(); 
      this.cardTotalForm.get('cardTotalExp3Money').enable(); 
      this.subsExp3Tot = this.cardTotalForm.get('cardTotalExp3Money').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp3Flag = false;
    } 
  }
  public setExp4PercBtnClick(){
    if (this.cardTotalExp4Flag === false) {
      this.subsExp4Tot.unsubscribe();
      this.cardTotalForm.get('cardTotalExp4Money').disable(); 
      this.cardTotalForm.get('cardTotalExp4Perc').enable(); 
      this.subsExp4Perc = this.cardTotalForm.get('cardTotalExp4Perc').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp4Flag = true;     
    } 
  }
  public setExp4MoneyBtnClick(){
    if (this.cardTotalExp4Flag === true) {
      this.subsExp4Perc.unsubscribe();
      this.cardTotalForm.get('cardTotalExp4Perc').disable(); 
      this.cardTotalForm.get('cardTotalExp4Money').enable(); 
      this.subsExp4Tot = this.cardTotalForm.get('cardTotalExp4Money').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExp4Flag = false;
    } 
  }
  public setExpTaxPercBtnClick(){
    if (this.cardTotalExpTaxesFlag === false) {
      this.subsExpTaxTot.unsubscribe();
      this.cardTotalForm.get('cardTotalExpTaxesMoney').disable(); 
      this.cardTotalForm.get('cardTotalExpTaxesPerc').enable(); 
      this.subsExpTaxPerc = this.cardTotalForm.get('cardTotalExpTaxesPerc').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExpTaxesFlag = true;     
    } 
  }
  public setExpTaxMoneyBtnClick(){
    if (this.cardTotalExpTaxesFlag === true) {
      this.subsExpTaxPerc.unsubscribe();
      this.cardTotalForm.get('cardTotalExpTaxesPerc').disable(); 
      this.cardTotalForm.get('cardTotalExpTaxesMoney').enable(); 
      this.subsExpTaxTot = this.cardTotalForm.get('cardTotalExpTaxesMoney').valueChanges.subscribe((data:any) => {
        this.setTitleAndTotals(this.cardPayoutsSelectList);
      });
      this.cardTotalExpTaxesFlag = false;
    } 
  }
  public closeCardsTotalsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback('OK');
      this.result.next('OK');
      this.bsModalRef.hide();        
    }
  }
  public groupCardTotalBtnClick(){
    const modalInitialState = {
      newCardTotal              :this.newCardTotal,
      workingDateRange          :this.cardTotalDateRange,
      cardTpvs                  :this.cardTpvs,
      selectedCardTpv           :this.selectedCardTpv,
      cardPayoutsSelectList     :this.cardPayoutsSelectList,
      callback                  :'OK',   
    };
    this.globalVar.openModal(CardsTotalsGroupComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{ })
  }
  public cleanFormCardTotalBtnClick(){
    this.cleanCardTotalFormData();
  }
  public createCardTotalBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createCardTotalBtnClick ->...Starting...->',null);
    this.copyCardTotalFormToNewCardTotal();
    this.createOneCardTotalStep1(this.newCardTotal);
  }
  public updateCardTotalBtnClick(){
    if (this.createCardTotalFactOnEdit === false) this.updateOneCardTotalOptionA(this.newCardTotal);
    else this.updateOneCardTotalOptionBStep1(this.newCardTotal);
  }
  public unSelectAllCardPayoutsBtnClick(){
    if (this.cardPayoutsSelectList != null){
      for (let k=0;k<this.cardPayoutsSelectList.length;k++){
        this.cardPayoutsSelectList[k].cardPayoutSelected = false;
      }
    }
    this.setTitleAndTotals(this.cardPayoutsSelectList);
  }
  public selectAllCardPayoutsBtnClick(){
    if (this.cardPayoutsSelectList != null){
      for (let k=0;k<this.cardPayoutsSelectList.length;k++){
        this.cardPayoutsSelectList[k].cardPayoutSelected = true;
      }
    }
    this.setTitleAndTotals(this.cardPayoutsSelectList);
  }
  public selectNumberCardPayoutsBtnClick(){
    this.globalVar.enterInformation("deleteMsg",this.formTitles.title,this.formTitles.title,
                            this.cardTotalDateRange,"number",this.formTitles.selectNumber,"","","","","","").
        then((result:number[])=>{
          var selNumber = result[0];
          if (this.cardPayoutsSelectList != null){
            for (let k=0;k<this.cardPayoutsSelectList.length;k++){
              if (k < selNumber) this.cardPayoutsSelectList[k].cardPayoutSelected = true;
            }
            this.setTitleAndTotals(this.cardPayoutsSelectList);
          }
        }).catch(error => {
          this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> selectNumberCardPayoutsBtnClick ->...Error...->', error);
        });
  }
  public selectDayCardPayoutsBtnClick(){
    this.globalVar.enterInformation("deleteMsg",this.formTitles.title,this.formTitles.title,
                            this.cardTotalDateRange,"day",this.formTitles.selectDay,"","","","","","").
        then((result:Date[])=>{
          var selDate = new Date(result[0]);
          var selDateLocale = selDate.toLocaleDateString();
          if (this.cardPayoutsSelectList != null){
            for (let k=0;k<this.cardPayoutsSelectList.length;k++){
              var selCardPayoutDate = new Date(this.cardPayoutsSelectList[k].cardPayoutDate);
              var selCardPayoutDateLocale = selCardPayoutDate.toLocaleDateString();
              if (selCardPayoutDateLocale === selDateLocale) this.cardPayoutsSelectList[k].cardPayoutSelected = true;
            }
            this.setTitleAndTotals(this.cardPayoutsSelectList);
          }
        }).catch(error => {
          this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> selectDayCardPayoutsBtnClick ->...Error...->', error);
        });
  }
  public selectDateRangeCardPayoutsBtnClick(){
    this.globalVar.enterInformation("deleteMsg",this.formTitles.title,this.formTitles.title,
                            this.cardTotalDateRange,"range",this.formTitles.selectDates,"","","","","","").
        then((result:Date[])=>{
          var selStartDate = new Date(result[0]);
          var selStartDateToLocale = selStartDate.toLocaleDateString();
          var selEndDate = new Date(result[1]);
          var selEndDateToLocale = selEndDate.toLocaleDateString();
          if (this.cardPayoutsSelectList != null){
            for (let k=0;k<this.cardPayoutsSelectList.length;k++){
              var selCardPayoutDate = new Date(this.cardPayoutsSelectList[k].cardPayoutDate);
              var selCardPayoutDateToLocale = selCardPayoutDate.toLocaleDateString();
              if ((selCardPayoutDateToLocale >= selStartDateToLocale) && (selCardPayoutDateToLocale <= selEndDateToLocale)) {
                this.cardPayoutsSelectList[k].cardPayoutSelected = true;
              }
            }
            this.setTitleAndTotals(this.cardPayoutsSelectList);
          }
        }).catch(error => {
          this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> selectDateRangeCardPayoutsBtnClick ->...Error...->', error);
        });
  }
  public searchCardPayoutsBtnClick(){
    if (this.showSearchCardPayoutsList === true) this.showSearchCardPayoutsList = false;
    else this.showSearchCardPayoutsList = true;
  }
  public updateCardTotals(selCardPayout :CardPayout){
    this.setTitleAndTotals(this.cardPayoutsSelectList);
  }
  public setOrderCardsPayoutBtnClick(value: string){
    if (this.orderCardsPayoutsList === value) {
      this.reverseCardsPayoutsList = !this.reverseCardsPayoutsList;
    }
    this.orderCardsPayoutsList = value;
  }
  //--------------------------------------- CARDS TOTALS CREATE --- DATABASE -------------------------------------------------
  private createOneCardTotalStep1(localCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createOneCardTotalStep1 ->...Starting...->',null);
    var isCardFactNeeded = this.checkCardTotalFact();
    if (isCardFactNeeded === false) return;  
    // create cardTotal and cardTotalFact to get id's  
    this.copyCardTotalFormToNewCardTotal();
    var localCardTotalFact = this.newCardTotalFact;
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createOneCardTotalStep1 ->...localCardTotalFact...->',localCardTotalFact);

    forkJoin({cardTotal       :this._cardService.createOneCardTotal(localCardTotal),
              cardTotalFact   :this._factService.createOneFact(localCardTotalFact),
              })
      .subscribe({next:(data)=> { 
          this.newCardTotal = data.cardTotal;         
          this.newCardTotalFact = data.cardTotalFact;
          this.createOneCardTotalStep2(this.newCardTotal);
          },
      error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private createOneCardTotalStep2(localCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createOneCardTotalStep2 ->...Starting...->',null);
    // update cardTotal with factId, create ArtFactList with fact Id, and update cardPayouts with cardTotalId and Date
    this.newCardTotal.cardTotalFactId = this.newCardTotalFact.factId;
    this.newCardTotalArtFactList[0].artFactFactId = this.newCardTotalFact.factId;
    var localCardTotalArtFactList = this.newCardTotalArtFactList;
    this.copySelectedCardPayoutsToStore(this.newCardTotal.cardTotalId, this.cardTotalDate);
    forkJoin({cardTotal             :this._cardService.updateOneCardTotal(localCardTotal),
              cardPayouts           :this._cardService.updateCardPayoutsList(this.cardPayoutsListToStore),
              cardTotalArtFactList  :this._factService.createArtsFact(localCardTotalArtFactList),
              })
      .subscribe({next:(data)=> { 
          this.newCardTotal = data.cardTotal;         
          this.cardPayoutsListToStore = data.cardPayouts;
          this.newCardTotalArtFactList = data.cardTotalArtFactList;
          this.closeCardsTotalsCreateModalBtnClick();
          },
      error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneCardTotalOptionA(localCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateOneCardTotalOptionA ->...Starting...->',null);
    var isCardFactNeeded = this.checkCardTotalFact();
    if (isCardFactNeeded === false) return;  
    // update cardTotal, cardpayouts, cardTotalFact and cardTotalArtFactList
    this.copyCardTotalFormToNewCardTotal();
    this.copySelectedCardPayoutsToStore(this.newCardTotal.cardTotalId, this.cardTotalDate);
    var localCardTotalFact = this.newCardTotalFact;
    var localCardTotalArtFactList = this.newCardTotalArtFactList;
    forkJoin({cardTotal             :this._cardService.updateOneCardTotal(localCardTotal),
              cardPayouts           :this._cardService.updateBeforeAfterCardPayoutsListTotalId(this.cardPayoutsListToStore),
              cardTotalFact         :this._factService.updateOneFact(localCardTotalFact),
              cardTotalArtFactList  :this._factService.updateArtsFact(localCardTotalArtFactList),
              })
      .subscribe({next:(data)=> { 
        this.newCardTotal = data.cardTotal;         
        this.cardPayoutsListToStore = data.cardPayouts;
        this.newCardTotalArtFactList = data.cardTotalArtFactList;
        this.closeCardsTotalsCreateModalBtnClick();
        },
      error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  } 
  private updateOneCardTotalOptionBStep1(localCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateOneCardTotalOptionBStep1 ->...Starting...->',null);
    var isCardFactNeeded = this.checkCardTotalFact();
    if (isCardFactNeeded === false) return;  
    // Step 1, update cardTotal, cardpayouts, and create cardTotalFact, 
    // later on, step 2, create cardTotalArtFactList with factId and update cardTotal with factId
    this.copyCardTotalFormToNewCardTotal();
    this.copySelectedCardPayoutsToStore(this.newCardTotal.cardTotalId, this.cardTotalDate);
    var localCardTotalFact = this.newCardTotalFact;
    forkJoin({cardTotal             :this._cardService.updateOneCardTotal(localCardTotal),
              cardPayouts           :this._cardService.updateBeforeAfterCardPayoutsListTotalId(this.cardPayoutsListToStore),
              cardTotalFact         :this._factService.createOneFact(localCardTotalFact),
              })
      .subscribe({next:(data)=> { 
        this.newCardTotal = data.cardTotal;         
        this.cardPayoutsListToStore = data.cardPayouts;
        this.newCardTotalFact = data.cardTotalFact;
        this.newCardTotal.cardTotalFactId = this.newCardTotalFact.factId;        
        this.newCardTotalArtFactList[0].artFactFactId = this.newCardTotalFact.factId;
        this.updateOneCardTotalOptionBStep2(this.newCardTotal);        },
      error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneCardTotalOptionBStep2(localCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateOneCardTotalOptionBStep2 ->...Starting...->',null);
    // step 2, create cardTotalArtFactList with factId and update cardTotal with factId
    var localCardTotalArtFactList = this.newCardTotalArtFactList;
    forkJoin({cardTotal             :this._cardService.updateOneCardTotal(localCardTotal),
              cardTotalArtFactList  :this._factService.updateArtsFact(localCardTotalArtFactList),
              })
      .subscribe({next:(data)=> { 
        this.newCardTotal = data.cardTotal;         
        this.newCardTotalArtFactList = data.cardTotalArtFactList;
        this.closeCardsTotalsCreateModalBtnClick();
        },
      error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateOneCardTotal(newCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateOneCardTotal ->...Starting...->', null);
    this._cardService.updateOneCardTotal(newCardTotal)
      .subscribe({next:(data) => { this.newCardTotal = data;
                           this.copySelectedCardPayoutsToStore(this.newCardTotal.cardTotalId, this.cardTotalDate);
                           this.updateBeforeAfterSelectedCardPayoutsWithCardTotalId();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  } 
  private deleteOneCardTotal(delCardTotal: CardTotal): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->deleteOneCardTotal ->...Starting...->', null);
    this._cardService.deleteOneCardTotal(delCardTotal)
      .subscribe({next:(data) => { this.closeCardsTotalsCreateModalBtnClick(); },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  } 
  //--------------------------------------- CARDS PAYOUTS -- DATABASE---------------------------------------------------------------------
  private getAllCardPayoutsNotTotal(localCardPayoutVRList :ValueRecord[]) {
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> getAllCardPayoutsNotTotal ->...Starting...->', null); 
    if ( (localCardPayoutVRList === null) || (localCardPayoutVRList.length === 0) ) {
      this.cardPayoutsList = null;
      return;
    }   
    this._cardService.getAllCardPayoutsNotTotal(localCardPayoutVRList)
      .subscribe({next:(data) => {  this.cardPayoutsSelectList = this.addSelectToCardsPayouts(data,false);
                            this.sortedCollection = this.orderPipe.transform(this.cardPayoutsSelectList,this.orderCardsPayoutsList,this.reverseCardsPayoutsList,this.caseInsensitive);
                            this.cardPayoutsSelectList = this.sortedCollection;
                            this.setTitleAndTotals(this.cardPayoutsSelectList);
                            this.showCardPayoutsList = true;    
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateBeforeAfterSelectedCardPayoutsWithCardTotalId(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateBeforeAfterSelectedCardPayoutsWithCardTotalId ->...Starting...->',null);
    this._cardService.updateBeforeAfterCardPayoutsListTotalId(this.cardPayoutsListToStore)
    .subscribe({next:(data) => {  var isCardFactNeeded = this.checkCardTotalFact();
                          if (isCardFactNeeded === true) {
                            if (this.newCardTotalFact.factId === -1) this.createCardTotalFact(this.newCardTotalFact);
                            else this.updateCardTotalFact(this.newCardTotalFact);
                          } else { this.closeCardsTotalsCreateModalBtnClick(); }
                        },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- CARDS TOTALS CREATE --- DATABASE -- FACTS -- ARTFACTS ------------------------------------------
  private createCardTotalFact(localCardTotalFact :Fact){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createCardTotalFact ->...Starting...->',null);
    this._factService.createOneFact(localCardTotalFact)
    .subscribe({next:(data) => {  this.newCardTotalFact = data;
                          this.newCardTotal.cardTotalFactId = this.newCardTotalFact.factId;
                          this.newCardTotalArtFactList[0].artFactFactId = this.newCardTotalFact.factId;
                          this.createCardTotalArtFactlist(this.newCardTotalArtFactList); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateCardTotalFact(localCardTotalFact :Fact){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateCardTotalFact ->...Starting...->',null);
    this._factService.updateOneFact(localCardTotalFact)
    .subscribe({next:(data) => { this.updateCardTotalArtFactlist(this.newCardTotalArtFactList); },
              error:(error) => {if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private createCardTotalArtFactlist(localCardTotalArtFactList :ArtFact[]){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->createCardTotalArtFactlist ->...Starting...->',null);
    this._factService.createArtsFact(localCardTotalArtFactList)
    .subscribe({next:(data) => {  this.updatCardTotalWithFactId(this.newCardTotal);  },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});              
  }
  private updatCardTotalWithFactId(localCardTotal :CardTotal){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updatCardTotalWithFactId ->...Starting...->',null);
    this._cardService.updateOneCardTotal(localCardTotal)
    .subscribe({next:(data) => {  this.closeCardsTotalsCreateModalBtnClick();  },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});              
  }
  private updateCardTotalArtFactlist(localCardTotalArtFactList :ArtFact[]){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE ->updateCardTotalArtFactlist ->...Starting...->',null);
    this._factService.updateArtsFact(localCardTotalArtFactList)
    .subscribe({next:(data) => {  this.closeCardsTotalsCreateModalBtnClick();  },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //----------------------------- CARDS TOTALS CREATE --- DATABASE -- GET ALL DATA ON CREATE OR EDIT -------------------------------
  private getCardTotalDataToCreate(localCardTotal :CardTotal){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> getCardTotalDataToCreate ->...Starting...->', null); 
    var cardPayoutsList :CardPayout[];
    //this.selectedCardTpvName = this.newCardTotal.cardTotalCardTpvName;
    this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedCardTpvName); 
    var selectedCardTotalId = this.newCardTotal.cardTotalId;
    var selectedProvId = this.selectedCardTpv.cardTpvProvId;
    var selectedArtId  = this.selectedCardTpv.cardTpvArtId;
    var localCardPayoutsVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpvName,this.cardTotalDateStart,this.cardTotalDateEnd);    
    forkJoin({cardPayoutsList   :this._cardService.getAllCardPayoutsNotTotal(localCardPayoutsVRList),
              prov              :this._provService.getOneProvById(selectedProvId),
              art               :this._artService.getOneArtById(selectedArtId),
              })
      .subscribe({next:(data)=> { 
        cardPayoutsList = data.cardPayoutsList;
        this.newCardTotalProv = data.prov;
        this.newCardTotalArt = data.art;
        this.newCardTotalFact = null;
        this.newCardTotalArtFactList = null;
        this.cardPayoutsSelectList = this.addSelectToCardsPayouts(cardPayoutsList,true);
        this.sortedCollection = this.orderPipe.transform(this.cardPayoutsSelectList,this.orderCardsPayoutsList,this.reverseCardsPayoutsList,this.caseInsensitive);
        this.cardPayoutsSelectList = this.sortedCollection;
        this.setTitleAndTotals(this.cardPayoutsSelectList);
        this.showCardPayoutsList = true;
        },
        error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private getCardTotalDataToEdit(localCardTotal :CardTotal){
    this.globalVar.consoleLog(this.logToConsole,'-> CARD-TOTAL-CREATE -> getCardTotalDataToEdit ->...Starting...->', null); 
    var cardPayoutsList :CardPayout[];
    this.selectedCardTpvName = this.newCardTotal.cardTotalCardTpvName;
    this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedCardTpvName); 
    var selectedCardTotalId = this.newCardTotal.cardTotalId;
    var selectedProvId = this.selectedCardTpv.cardTpvProvId;
    var selectedArtId = this.selectedCardTpv.cardTpvArtId;
    var selectedFactid = this.newCardTotal.cardTotalFactId;
    var localCardPayoutsVRList = this.globalVar.prepareStringDatesValueRecordList(selectedCardTotalId.toFixed(0),this.cardTotalDateStart,this.cardTotalDateEnd);    
    forkJoin({cardTotal         :this._cardService.getOneCardTotal(localCardTotal),
              cardPayoutsList   :this._cardService.getAllCardPayoutsWithCardTotalId(localCardPayoutsVRList),
              prov              :this._provService.getOneProvById(selectedProvId),
              art               :this._artService.getOneArtById(selectedArtId),
              fact              :this._factService.getOneFactById(selectedFactid),
              artFactslist      :this._factService.getArtsFactByFactId(selectedFactid),
              })
      .subscribe({next:(data)=> { 
        this.newCardTotal = data.cardTotal;
        cardPayoutsList = data.cardPayoutsList;
        this.newCardTotalProv = data.prov;
        this.newCardTotalArt = data.art;
        this.newCardTotalFact = data.fact;
        this.newCardTotalArtFactList = data.artFactslist;
        if ( (this.newCardTotalArtFactList != null) && (this.newCardTotalArtFactList.length > 0) ) this.newCardTotalArtFact = this.newCardTotalArtFactList[0];
        else {  this.newCardTotalFact = null;
                this.newCardTotalArtFact = null;                
                this.createCardTotalFactOnEdit = true;
        }
        this.copyNewCardTotalToForm(this.newCardTotal);
        this.cardPayoutsSelectList = this.addSelectToCardsPayouts(cardPayoutsList,true);
        this.sortedCollection = this.orderPipe.transform(this.cardPayoutsSelectList,this.orderCardsPayoutsList,this.reverseCardsPayoutsList,this.caseInsensitive);
        this.cardPayoutsSelectList = this.sortedCollection;
        this.setTitleAndTotals(this.cardPayoutsSelectList);
        this.showCardPayoutsList = true;
        },
        error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}
  //--------------------------------------- CARDS TOTALS CREATE --- DELETES ---------------------------------------------------------------------


  //--------------------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- CONSTRUCTOR -----------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- NG ON INIT ------------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- FORM ARRAY FILTERING TOOLS --------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- GENERAL ---------------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- FORMS -----------------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- BTN CLICK -------------------------------------------------------------------
  //--------------------------------------- CARDS TOTALS CREATE --- DATABASE --------------------------------------------------------------------


  //--------------------------------------- CARDS TOTALS CREATE --- DELETES ---------------------------------------------------------------------
