
import { Component, OnInit } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ErrorService } from '../aa-errors/error.service';
import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { CardPayout, CardTpv } from './cards';
import { CardPayoutsCreateComponent } from './cardPayouts-create.component';
import { CardsService } from './cards.service';
import { Hd } from '../dbx-hd/hd'; 
import { HdService } from '../dbx-hd/hd.service';
@Component({
  selector      :'app-cards-hd',
  templateUrl   :'./cards-hd.component.html',
  styleUrls     :['./cards.component.css'] 
})

export class CardsHdComponent implements OnInit {
  public workingDate        :Date;
  public receivedHdsCards   :Hd;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  formTitles = {
    'cardPayouts'       :'Pagos con Tarjetas',
    'cardDeleteOne'     :'¿ Seguro que quiere BORRAR este PAGO con TARJETA ?',
    'cardDeleteAll'     :'¿ Seguro que quiere BORRAR TODOS PAGOS con TARJETAS ?',
    'cardPayoutsList'   :'Tarjetas',
    'cardEditList'      :'Editar',
    'cardDelete'        :'Borrar',
    'tpv'               :'TPV',
    'tpvName'           :'',
    'startDate'         :'',
    'endDate'           :'',
    'cardTpv'           :'Tpvs Tarjetas',
    'hdDate'            :'',
    'cardTpvsList'      :'Lista TPVS para Tarjetas',
    'cardTpvDeleteOne'  :'¿ Seguro que quiere BORRAR este TPV para TARJETAS ?',
    'cardTpvDeleteAll'  :'¿ Seguro que quiere BORRAR TODOS los TPVS para TARJETAS ?',
    'cardTpvEditList'   :'Editar',
    'cardTpvDelete'     :'Borrar',
  };
  formLabels = { 
    'cardPayoutId'          :'Id',
    'cardPayoutDate'        :'Fecha',
    'cardPayoutOperationNr' :'NºOper.',
    'cardPayoutMulti'       :'Cantidad',
    'cardPayoutTotal'       :'Total',
    'cardPayoutTpvId'       :'Tpv.Id',
    'cardPayoutTotalId'     :'Total.Id',
    'cardPayoutTotalDate'   :'Fecha.Total.',
    'cardTpvId'             :'Id Tpv',
    'cardTpvName'           :'Nombre Tpv',
    'cardTpvShopNr'         :'Nº Tienda',
    'cardTpvTpvNr'          :'Nº Tpv',
    'cardTpvSerialNr'       :'Nº Serie',
    'cardTpvBank'           :'CCC Banco',
    'cardTpvType'           :'Tipo TPV',
    'cardTpvContact'        :'Contacto',
    'cardTpvMulti'          :'Vales',
    'cardTpvStatus'         :'Estado',
  };  

  public  localCardTpvsRecordList       :ValueRecord[];

  private createCard                    :boolean;
  public  showCardPayoutsList           :boolean;
  public  cardPayouts                   :CardPayout[];
  public  cardPayoutsList               :CardPayout[];
  private newCardPayoutList             :CardPayout;
  private nextCardPayoutOperationNr     :number;
  private receivedHdsCardsList          :Hd[];

  public  searchCard                    :CardPayout;
  private selectedTpvName               :string;
  private selectedStartDate             :Date;
  private selectedEndDate               :Date;
  public  showSearchCard                :boolean;
  public  orderCardsList                :string;
  public  reverseCardsList              :boolean;
  
  public  localCardTpvs                 :CardTpv[];
  public  showCardTpvsList              :boolean;
  public  cardPayoutsTotalString        :string;

  public  searchCardTpv                 :CardTpv;
  private selectedCardTpv               :CardTpv;
  public  showSearchCardTpvs            :boolean;
  public  orderCardTpvsList             :string;
  public  reverseCardTpvsList           :boolean;  
  public  caseInsensitive               :boolean;
  private sortedCollection              :any[];
  private logToConsole                  :boolean;
  //--------------------------------------- CARDS HD  -- CONSTRUCTOR -------------------------------------------------------------
  constructor(private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,                     
              private globalVar: GlobalVarService, private _gbViewsService: GbViewsService, private _errorService: ErrorService, 
              private _cardService: CardsService, private _hdService: HdService ) {
                    //   this.getGbViews('gbCards'); 
              }
  //--------------------------------------- CARDS HD  -- NG ON INIT -------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;

    this.receivedHdsCardsList = new Array();
    this.receivedHdsCardsList[0] = this.receivedHdsCards; 

    this.formTitles.hdDate = this.workingDate.toLocaleDateString();
    this.cardPayoutsTotalString = 'Total... 0.00';
    this.caseInsensitive = true;
    this.newCardPayoutList = new CardPayout();
    this.searchCard = new CardPayout();
    this.selectedCardTpv = new CardTpv();
    this.searchCardTpv = new CardTpv();
    this.selectedTpvName = '';
    this.selectedStartDate = this.workingDate;
    this.selectedEndDate = this.workingDate;
    this.formTitles.startDate = this.workingDate.toLocaleDateString();
    this.formTitles.endDate = this.workingDate.toLocaleDateString();
    this.formTitles.tpvName = '';
    this.resetShowCardFlags();
    this.localCardTpvs = this.globalVar.cardTpvs;
    this.localCardTpvsRecordList = this.globalVar.cardTpvsRecordList;
    this.showCardPayoutsList = true;    
    this.selectedCardTpv = this.localCardTpvs[0];                         
    this.selectedTpvName =  this.localCardTpvs[0].cardTpvName;
    this.formTitles.tpvName = this.selectedTpvName;
    this.getAllCardPayouts();  
  }
  //--------------------------------------- CARDS HD -- FORMS -------------------------------------------------------------
  
  //--------------------------------------- CARDS HD  -- GENERAL -------------------------------------------------------------
  private calculateCardPayoutsTotal():number{
    let cardPayoutsTotal = 0;
    for (let i = 0 ; i < this.cardPayoutsList.length ; i++ ) {
      cardPayoutsTotal = cardPayoutsTotal + ( this.cardPayoutsList[i].cardPayoutTotal * this.cardPayoutsList[i].cardPayoutMulti );
    }
    this.cardPayoutsTotalString = this.cardPayoutsList.length+'..'+this.formTitles.tpv+'__'+this.formTitles.tpvName+'..Total->'+ + cardPayoutsTotal.toFixed(2);
    return cardPayoutsTotal;
  }
  private resetShowCardFlags(){
    this.showCardPayoutsList  = false;
    this.createCard           = false;
    this.showCardTpvsList     = false;
  }
  private maxOperationNrCardPayoutList():number{
    let maxOperationNr = 0;
    for(let j=0; j<this.cardPayoutsList.length; j++){
      maxOperationNr = Math.max ( maxOperationNr, this.cardPayoutsList[j].cardPayoutOperationNr );
    }
    return maxOperationNr;
  }
  public closeCardsHdModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.updateHdSumAllCardPayoutsAndReturn();
      }
  }
  //---------------------------------------- CARDS HD -- BTN CLICK -------------------------------------------------------------
  public changeShowCardTotalListBtnClick(){
  
  }

  public selectCardTpvBtnClick(){   
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-HD -> selectCardTpvBtnClick ->...Starting...->', null); 
    if (this.localCardTpvs == null) {
      this._errorService.showErrorModal( "TPVs TARJETAS", "TPVs TARJETAS", "Hay que CREAR TPV's para PAGOS CON TARJETA",
                        "M-1", "M-2", "M-3", "M-4", "M-5", "M-6");
      return;
    }          
    //this.localCardTpvsRecordList = this.globalVar.prepareCardTpvsValueRecordList(this.localCardTpvs);
    const modalInitialState = {
      localValueRecordList  :this.localCardTpvsRecordList,
      selectTitle1          :"TPVS para TARJETAS",
      selectTitle2          :"Seleccionar TPV",
      selectMessage         :"Seleccione el TPV del que quiere ver los pagos de TARJETAS",
      selectLabel           :"TPV Tarjetas:",       
      selectFooter          :"TPVS para TARJETAS",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){   
        var selTpvTypeTpvname :string;
        selTpvTypeTpvname = result;                         
        this.selectedTpvName = selTpvTypeTpvname.substring(6);
        this.selectedCardTpv = this.globalVar.getCardTpvObjectFromTpvName(this.selectedTpvName);  
        this.formTitles.tpvName = this.selectedTpvName;
        this.getAllCardPayouts();           
      }
    })
  }
  public createCardPayoutsListBtnClick(){
    this.newCardPayoutList = new CardPayout();
    this.createCard = true;
    this.createCardPayoutListModal();
  }  
  public editCardPayoutBtnClick(editCard: CardPayout){
    this.newCardPayoutList = editCard;
    this.createCard = false;
    this.createCardPayoutListModal();
  } 
  private createCardPayoutListModal(){    
    const modalInitialState = {
      createCardPayout      :this.createCard,
      cardPayoutId          :null,
      cardPayoutDate        :this.selectedStartDate,
      cardPayoutMinDate     :this.selectedStartDate,
      cardPayoutMaxDate     :this.selectedEndDate,
      cardPayoutOperationNr :this.nextCardPayoutOperationNr,
      cardPayoutList        :this.newCardPayoutList,
      selectedCardTpv       :this.selectedCardTpv,
      changeCardsDate       :false,
      cardPayoutTotalId     :null,
      callback              :'OK',   
    };
    this.globalVar.openModal(CardPayoutsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null) this.newCardPayoutList = result;           
      this.getAllCardPayouts();
    })   
  }  
  public searchCardPayoutsBtnClick() {
    if (this.showSearchCard === true) {
      this.showSearchCard = false;
    } else {
      this.showSearchCard = true;
    }
  }
  public setOrderBtnClick(value: string) {
    if (this.orderCardsList === value) {
      this.reverseCardsList = !this.reverseCardsList;
    }
    this.orderCardsList = value;
    console.log(this.orderCardsList);
  }
  //--------------------------------------- CARDS HD  -- DATABASE -------------------------------------------------------------
  private getAllCardPayouts() {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-HD -> getAllCardPayouts ->...Starting...->', null);
    if ( (this.localCardTpvs === null) || (this.localCardTpvs.length === 0) ) {
      this.cardPayoutsList = null;
      return;
    }
    let cardPayoutVRList = this.globalVar.prepareStringDatesValueRecordList( this.selectedTpvName,this.selectedStartDate, this.selectedEndDate);    
    this._cardService.getAllCardPayouts(cardPayoutVRList)
      .subscribe({next:(data) => { this.cardPayoutsList = data;
                            this.orderCardsList = 'cardPayoutOperationNr';
                            this.reverseCardsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.cardPayoutsList, 'cardPayoutOperationNr');
                            this.cardPayoutsList = this.sortedCollection;
                            this.nextCardPayoutOperationNr = this.maxOperationNrCardPayoutList() + 1;
                            this.calculateCardPayoutsTotal();
                            this.showCardPayoutsList = true;    
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  private updateHdSumAllCardPayoutsAndReturn() {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-HD -> sumAllCardPayoutsAndReturn ->...Starting...->', null);
    let cardPayoutVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',this.selectedStartDate, this.selectedEndDate);    
    this._cardService.getAllCardPayouts(cardPayoutVRList)
      .subscribe({next:(data) => { this.cardPayoutsList = data;
                            this.receivedHdsCardsList[0].hdAmount = this.calculateCardPayoutsTotal();
                            this.updateDateHds(this.workingDate,this.receivedHdsCardsList);
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //--------------------------------------- CARDS HD  -- DELETE -------------------------------------------------------------
  public confirmDeleteOneCardPayoutBtnClick(card :CardPayout){
  this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardPayouts,this.formTitles.cardPayouts,
                          card,this.formTitles.cardDeleteOne,card.cardPayoutDate.toLocaleString(),
                          card.cardPayoutOperationNr.toString(),card.cardPayoutTotal.toString(),"","","",).
      then((deleteOK:boolean)=>{
        if (deleteOK === true){ this.deleteOneCardPayout(card); }
      }) 
  }
  public confirmDeleteAllCardPayoutsBtnClick(){
  this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.cardPayouts,this.formTitles.cardPayouts,
                          this.selectedCardTpv,this.formTitles.cardDeleteAll,this.selectedStartDate.toString(),
                          this.selectedEndDate.toString(),this.nextCardPayoutOperationNr.toString(),"","","",).
      then((deleteOK:boolean)=>{
        if (deleteOK === true){ this.deleteAllCardPayoutsList(this.cardPayoutsList); }
      }) 
  } 
  private deleteAllCardPayoutsList(delCardPayoutsList: CardPayout[]){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-HD -> deleteAllCardPayoutsList ->...Starting...->', null);
    this._cardService.deleteAllCardPayoutsList(delCardPayoutsList)
    .subscribe({next:(data) => { this.getAllCardPayouts(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) };}});
  }
  private deleteOneCardPayout(delCard: CardPayout){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS-HD -> deleteOneCardPayout ->...Starting...->', null);
    this._cardService.delOneCardPayout(delCard)
    .subscribe({next:(data) => { this.getAllCardPayouts(); },
              error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  //-------------------------------------- CARDS HDS -- DATABASE ---------------------------------------------------------------
  private updateDateHds(workingDate: Date, dateHds: Hd[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> CARDS HD -> updateDateHds ->...Starting...->', null);
    this._hdService.updateDateHds(dateHds)
      .subscribe({next:(data) => { //this.bsModalRef.content.callback('OK');
                           this.result.next('OK');
                           this.bsModalRef.hide();
                          },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
}