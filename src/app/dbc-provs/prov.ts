
export class Prov {
    provId          :number;
    provDate        :Date;
    provCifType     :string;
    provCif         :string;
    provName        :string;
    provLongName    :string;
    provFactType    :string;
    provPayType     :string;
    provFactNote    :boolean;
    provStatus      :boolean;
}
export class ProvString {
    provId          :number;
    provDate        :string;
    provCifType     :string;
    provCif         :string;
    provName        :string;
    provLongName    :string;
    provFactType    :string;
    provPayType     :string;
    provFactNote    :string;
    provStatus      :boolean;
}
export class MProv {
    mprovId         :number;
    mprovProvId     :number;
    mprovType       :string;
    mprovData1      :string;
    mprovData2      :string;
}