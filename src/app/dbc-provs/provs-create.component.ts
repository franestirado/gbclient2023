import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';

import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord } from '../dbc-fields/field';

import { Prov,MProv } from './prov';
import { MoreProvComponent } from './moreProv.component';
import { ProvsService } from './provs.service';
//import { SubDep } from '../dbc-arts/art';
import { ArtService } from '../dbc-arts/art.service';

@Component({
  selector      :'app-provs-create',
  templateUrl   :'./provs-create.component.html',
  styleUrls     :['./provs.component.css'] 
})
export class ProvsCreateComponent implements OnInit {
  public newProv        :Prov;
  public provTitle      :string;
  public provDate       :Date;
  public showCreateProv :boolean;
  public showEditProv   :boolean;
  callback:   any;  
  result: Subject<any> = new Subject<any>();

  public formTitles = {  
    'provTitle'       :'',   
    'provDate'        :'',
    'moreProvsCreate' :'Crear Más Datos del PROVEEDOR',
    'moreProvsEdit'   :'Modificar Más Datos del PROVEEDOR', 
    'provCreate'      :'Crear Proveedor',
    'provEdit'        :'Modificar Proveedor',
    'provMoreData'    :'Mas Datos Proveedor',
    'provEditList'    :'Editar',
    'provDelete'      :'Borrar',
  };
  public formLabels = { 
    'provId'          :'Id',
    'provDate'        :'Fecha',
    'provCifType'     :'Tipo CIF',
    'provCif'         :'CIF',
    'provName'        :'Nombre',
    'provLongName'    :'Nombre Largo',
    'provFactType'    :'Tipo Factura', 
    'provPayType'     :'Forma Pago', 
    'provFactNote'    :'Factura/Nota', 
    'provStatus'      :'Estado',
    'mprovId'         :'Id',
    'mprovProvId'     :'Prov Id',
    'mprovType'       :'Tipo Datos',
    'mprovData1'      :'Texto',
    'mprovData2'      :'Mas...',
  };
  private logToConsole                    :boolean;
  public  provForm                        :FormGroup; 
  public  localCifTypesRecordList         :ValueRecord [];  
  public  localFactPayTypesRecordList     :ValueRecord [];
  public  localFactTypesSubDepsRecordList :ValueRecord [];
  public  moreProvs                       :MProv[];
  private newMoreProv                     :MProv;
  private orderMProvsList                 :string;
  private reverseMProvsList               :boolean;
  private caseInsensitive                 :boolean;
  private sortedCollection                :any[];
  public  currentDate                     :Date;
  private disableProvNameAndFactType      :boolean;
  public  provsDatePickerConfig           :Partial <BsDatepickerConfig>;

  /*---------------------------------------PROVS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService, 
                private orderPipe: OrderPipe,  public globalVar: GlobalVarService, private _errorService:ErrorService, 
                private _provsService: ProvsService, private _artService: ArtService ) {
    this.provsDatePickerConfig = Object.assign ( {}, {
      containerClass    :'theme-dark-blue',
      showWeekNumbers   :true,
      minDate           :this.globalVar.workingPreviousYear,
      maxDate           :this.globalVar.workingNextYear,
      dateInputFormat   :'DD-MM-YYYY'
      } );
  }
  /*---------------------------------------PROVS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;    
    if (this.provDate === null) {
      this.currentDate = this.globalVar.currentDate;
      this.formTitles.provDate = '';
    } else {
      this.currentDate = this.provDate;
      this.formTitles.provDate = this.provDate.toLocaleDateString();
    }
    this.formTitles.provTitle = this.provTitle;
    this.disableProvNameAndFactType = false;
    if (this.showCreateProv === false) {
      this.disableProvNameAndFactType = true;
    }    
    this.provForm = this.fb.group({
      provId          :[{value:'',disabled:false}],
      provDate        :[{value:this.currentDate,disabled:true},[Validators.required]],
      provCifType     :[{value:'',disabled:false}, [Validators.required, Validators.minLength(3), Validators.maxLength(32)] ],
      provCif         :[{value:'',disabled:false}, [Validators.required, Validators.minLength(3), Validators.maxLength(32)] ],
      provName        :[{value:'',disabled:this.disableProvNameAndFactType}, [Validators.required, Validators.minLength(3), Validators.maxLength(64)] ],
      provLongName    :[{value:'',disabled:false}, [Validators.required, Validators.minLength(3), Validators.maxLength(128)] ],
      provFactType    :[{value:'',disabled:this.disableProvNameAndFactType}, [Validators.required, Validators.minLength(3), Validators.maxLength(32)] ],
      provPayType     :[{value:'',disabled:false}, [Validators.required, Validators.minLength(3), Validators.maxLength(16)] ],
      provFactNote    :[{value:'',disabled:false}, Validators.required ],
      provStatus      :[{value:'',disabled:false}, Validators.required ],
    })    
    this.localCifTypesRecordList = this.globalVar.cifTypesRecordList;
    this.localFactPayTypesRecordList = this.globalVar.factPayTypesRecordList;
    this.localFactTypesSubDepsRecordList = this.globalVar.factTypesSubDepsRecordList;

    this.provForm.patchValue( {provCifType: this.localCifTypesRecordList[0].value} );
    this.provForm.get('provCifType').valueChanges.subscribe((data:string) => {
      this.setCifValidation(data);
    });
    this.setCifValidation(this.localCifTypesRecordList[0].value);
    if (this.showCreateProv === true) {
      this.newProv = new Prov();
      this.newMoreProv = new MProv();
      this.moreProvs = new Array();
      this.cleanProvFormData();
      this.provForm.patchValue( {provCifType: this.localCifTypesRecordList[0].value} );
      this.provForm.patchValue( {provFactType: this.localFactTypesSubDepsRecordList[0].value} );
      this.provForm.patchValue( {provPayType: this.localFactPayTypesRecordList[0].value} );
      this.provForm.get('provDate').patchValue(new Date(this.currentDate));
    } else {
      this.newMoreProv = new MProv();
      this.getOneProv(this.newProv);
    }
  }
  /*---------------------------------------PROVS CREATE -- FORMS-----------------------------------------------------------*/
  setCifValidation (selectedValue: string) {
    const provCifControl = this.provForm.get('provCif');
    selectedValue = selectedValue.toLowerCase();
    switch (selectedValue) {
      case "cif":
                provCifControl.setValidators([Validators.required,ValidationService.validCif('CIF')]);
                break;
      case "nif":
                provCifControl.setValidators([Validators.required,ValidationService.validNif('NIF')]);
                break;
      case "nie":
                provCifControl.setValidators([Validators.required,ValidationService.validNie('NIE')]);
                break;
      case "dni":
                provCifControl.setValidators([Validators.required,ValidationService.validNif('NIF')]);
                break;
      default:
                provCifControl.setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(32)]);
                break;
    }
    provCifControl.updateValueAndValidity();
  }
  copyProvFormDataToProv(){
    this.newProv.provDate = this.provForm.controls.provDate.value;
    this.newProv.provCifType = this.provForm.controls.provCifType.value;
    this.newProv.provCif = this.provForm.controls.provCif.value;
    this.newProv.provName = this.provForm.controls.provName.value;
    this.newProv.provLongName = this.provForm.controls.provLongName.value;
    this.newProv.provFactType = this.provForm.controls.provFactType.value;
    this.newProv.provPayType = this.provForm.controls.provPayType.value;
    this.newProv.provFactNote = this.provForm.controls.provFactNote.value;
    this.newProv.provStatus = this.provForm.controls.provStatus.value;

    var subDepTag = this.globalVar.getSubDepTagFromSubDepName(this.newProv.provFactType);
    if (subDepTag != null) this.newProv.provFactType = subDepTag; 
    else this.newProv.provFactType = "NO SubDep Tag";
  }
  copyNewProvDataToProvForm(){
    this.provForm.patchValue( { provId: this.newProv.provId, provDate: this.newProv.provDate, provCifType: this.newProv.provCifType, 
                                provCif: this.newProv.provCif, provName: this.newProv.provName, provLongName: this.newProv.provLongName, 
                                provFactType: this.newProv.provFactType, provPayType: this.newProv.provPayType, 
                                provFactNote: this.newProv.provFactNote, provStatus: this.newProv.provStatus} );
    this.currentDate = new Date(this.newProv.provDate);
    this.provForm.get('provDate').patchValue(this.currentDate);

    var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.newProv.provFactType);
    if (subDepName != null) this.provForm.get('provFactType').patchValue(subDepName);
    else this.provForm.get('provFactType').patchValue(this.localFactTypesSubDepsRecordList[0].value);
  }
  cleanProvFormData(){
    this.provForm.patchValue( {provCifType:this.localCifTypesRecordList[0].value, provCif:'', provName:'', provLongName:'',
                                provFactType:this.localFactTypesSubDepsRecordList[0].value, provPayType:this.localFactPayTypesRecordList[0].value,
                                provFactNote:true, provStatus:true} );
  }
  /*---------------------------------------PROVS CREATE -- GENERAL-----------------------------------------------------------*/
  changeDisableProvNameAndFactTypeBtnClick(){
    if (this.disableProvNameAndFactType === true) {
      this.provForm.get('provName').enable(); 
      this.provForm.get('provFactType').enable(); 
      this.disableProvNameAndFactType = false;
    } else {
      this.provForm.get('provName').disable(); 
      this.provForm.get('provFactType').disable(); 
      this.disableProvNameAndFactType = true;
    }
  }
  /*---------------------------------------PROVS CREATE -- BTN CLICK-----------------------------------------------------------*/  
  CloseProvsCreateModalBtnClick(selecteProv: Prov){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(selecteProv);
        this.result.next(selecteProv);
        this.bsModalRef.hide();
      }
  }
  cleanFormProvBtnClick(){
    this.showCreateProv = true;
    this.showEditProv = false;
    this.moreProvs = new Array();
    this.cleanProvFormData();    
  }
  createProvBtnClick(provForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->createProvBtnClick->creating->', null);
    this.copyProvFormDataToProv();  
    this.createOneProv(this.newProv);   
  }
  updateProvBtnClick(provForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->updateProvBtnClick->updating->', null);
    this.copyProvFormDataToProv();
    if ((this.showCreateProv === false) && (this.disableProvNameAndFactType === true)) {
      this.updateOneProv(this.newProv);    
    } 
    if ((this.showCreateProv === false) && (this.disableProvNameAndFactType === false)) {
      this.updateOneProvSpecial(this.newProv);    
    } 
  }    
  /*---------------------------------------MORE PROVS CREATE -- BTN CLICK-----------------------------------------------------------*/  
  changeShowCreateMoreProvBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->changeShowCreateMoreProvBtnClick->starting->', null);
    if (this.provForm.invalid) {
      this._errorService.showErrorModal('... Crear PROVEEDOR ...','... Más Datos del Proveedor ...',
                                      'Primero tiene que introducir todos los datos','del Proveedor','','','','','',);
    } else this.createMoreProvModal(true, null);
  }
  editOneMoreProvBtnClick(moreProv: MProv){  
    this.createMoreProvModal(false,moreProv);
  }
  createMoreProvModal(createMoreProvFlag: boolean, updateMoreProv: MProv){    
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->createMoreProvModal->starting->', null);    
    if ( (this.moreProvs === null) || (this.moreProvs.length === 0) ) {this.moreProvs = new Array();}
    this.copyProvFormDataToProv();
    let moreProvTitle: string;
    if (createMoreProvFlag === true) {
      this.newMoreProv = new MProv();      
      moreProvTitle = this.formTitles.moreProvsCreate;      
    } else {
      this.newMoreProv = updateMoreProv;
      moreProvTitle  = this.formTitles.moreProvsEdit;
    }   
    const modalInitialState = {
      createMoreProvFlag  :createMoreProvFlag,
      newProv             :this.newProv,
      newMoreProv         :this.newMoreProv,
      moreProvsList       :this.moreProvs,
      moreProvTitle       :moreProvTitle,
      callback            :'OK',   
    };
    this.globalVar.openModal(MoreProvComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.newMoreProv= result;
        if (createMoreProvFlag === true) {
          let indexMoreProvToAdd = this.checkMoreProvIsInList(this.newMoreProv,this.moreProvs);
          if ( indexMoreProvToAdd === -1) { this.moreProvs.push(this.newMoreProv); }
        } else {    
          let indexMoreProvToDelete = this.checkMoreProvIsInList(updateMoreProv,this.moreProvs);
          if ( indexMoreProvToDelete != -1) {
            this.moreProvs.splice(indexMoreProvToDelete, 1);
            this.moreProvs.push(this.newMoreProv);
          }             
        }  
      }
    })
  }
  checkMoreProvIsInList (newMoreProv: MProv, moreProvsList: MProv[]): number {
    let indexMoreProvToDelete = -1;
    for (let j=0 ; j<moreProvsList.length ; j++) {              
      if ( (newMoreProv.mprovType === moreProvsList[j].mprovType) && (newMoreProv.mprovData1 === moreProvsList[j].mprovData1) ) {
        indexMoreProvToDelete = j;
        break;
      }
    }
    return indexMoreProvToDelete;
  }
  deleteOneMoreProvBtnClick(moreProv: MProv){
    this.moreProvs.splice( this.moreProvs.indexOf(moreProv),1);      
  }
  deleteAllMoreProvsBtnClick(){
    this.moreProvs = new Array();
  }  
  /*---------------------------------------PROVS CREATE -- DATABASE-----------------------------------------------------------*/ 
  createOneProv(newProv: Prov): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->createOneProv->creating->', null);
    this._provsService.createOneProv(newProv)
      .subscribe({next:(data) => { this.newProv = data;
                           if ( (this.moreProvs.length === 0) || (this.moreProvs === null) ) { 
                             this.CloseProvsCreateModalBtnClick(this.newProv);                           
                           } else {
                             this.moreProvs.forEach( (mprov:MProv)=>{ mprov.mprovProvId = this.newProv.provId } )
                             this.createOneMoreProvList(this.moreProvs);
                           }                          
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getOneProv(modProv: Prov): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->getOneProv->getting->', null);
    this._provsService.getOneProv(modProv)
      .subscribe({next:(data) => { this.newProv = data;
                           this.copyNewProvDataToProvForm();
                           this.getAllMoreProvs(this.newProv);
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneProv(modProv: Prov): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->updateOneProv->updating->', null);
    this._provsService.updateOneProv(modProv)
      .subscribe({next:(data) => { this.newProv = data;
                           this.updateOneMoreProvList(this.moreProvs);
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  } 
  updateOneProvSpecial(modProv: Prov): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->updateOneProvSpecial->updating->', null);
    this._provsService.updateOneProvSpecial(modProv)
      .subscribe({next:(data) => { this.newProv = data;
                           this.updateOneMoreProvList(this.moreProvs);
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

  /*---------------------------------------MORE PROVS CREATE -- DATABASE-----------------------------------------------------------*/ 
  getAllMoreProvs(newProv: Prov) {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->getAllMProv->starting->', null);
    this._provsService.getAllMoreProvs(newProv)
      .subscribe({next:(data) => { this.moreProvs = data;
                           this.orderMProvsList = 'mprovType';
                           this.reverseMProvsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.moreProvs,this.orderMProvsList,this.reverseMProvsList,this.caseInsensitive);
                           this.moreProvs = this.sortedCollection;
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }  
  createOneMoreProvList(newMoreProvList: MProv[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->createOneMoreProvList->creating->', null);
    this._provsService.createOneMoreProvList(newMoreProvList)
      .subscribe({next:(data) => { this.moreProvs = data; 
                           this.CloseProvsCreateModalBtnClick(this.newProv);                        
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneMoreProvList(updateMProvList: MProv[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS CREATE->updateOneMoreProvList->updating->', null);
    if ((updateMProvList === null) || (updateMProvList.length < 1)) {
      this.CloseProvsCreateModalBtnClick(this.newProv);
      return;
    }
    this._provsService.updateOneMoreProvList(updateMProvList)
      .subscribe({next:(data) => { this.moreProvs = data; 
                           this.CloseProvsCreateModalBtnClick(this.newProv);                        
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}