import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvsComponent } from './provs.component';

describe('ProvsComponent', () => {
  let component: ProvsComponent;
  let fixture: ComponentFixture<ProvsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
