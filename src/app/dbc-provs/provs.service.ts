import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Prov, MProv } from './prov';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  @Injectable()
  export class ProvsService {
    constructor(private http: HttpClient) { }

    private json: string;
    private params: string;
    private headers: HttpHeaders;
    
  /* ---------------------------------------PROVS-----------------------------------------------------------*/
    getAllProvs(): Observable<Prov[]>  {
        return this.http.get<Prov[]>('/provs/getAll')
      }
    getAllProvsWithFactType(selFactType: string): Observable<Prov[]>  {
        return this.http.post<Prov[]>('/provs/withFactType', selFactType)
    }
    getAllProvsWithPayType(selPayType: string): Observable<Prov[]>  {
      return this.http.post<Prov[]>('/provs/withPayType', selPayType)
    }
    getAllProvsWithFactOrNote(factOrNote: string): Observable<Prov[]>  {
      return this.http.post<Prov[]>('/provs/withFactOrNote', factOrNote)
    }
    createOneProv(newProv: Prov): Observable<any>  {
        return this.http.post('/provs/post', newProv)
    }
    updateOneProv(newProv: Prov): Observable<any>  {
      return this.http.put('/provs/updOne', newProv)
    }
    updateOneProvSpecial(newProv: Prov): Observable<any>  {
      return this.http.put('/provs/updOneSpecial', newProv)
    }
    getOneProv(editProv: Prov): Observable<Prov>  {
      return this.http.post<Prov>('/provs/getOne', editProv)
    }
    getOneProvById(provId :number): Observable<Prov>  {
      return this.http.post<Prov>('/provs/getOneById', provId)
    }
    delOneProv(delProv: Prov): Observable<any> {
        return this.http.post('/provs/delOne', delProv)
    }
    delAllProvs(): Observable<any> {
        return this.http.delete('/provs/')
    }
/* ---------------------------------------More PROVS-----------------------------------------------------------*/
    getAllMoreProvs(newProv: Prov): Observable<MProv[]>  {
      return this.http.post<MProv[]>('/provs/getAllMoreProv', newProv)
    }    
    createOneMoreProvList(newMProvList: MProv[]): Observable<MProv[]>  {
      return this.http.post<MProv[]>('/provs/addMoreProvList', newMProvList)
    }
    updateOneMoreProvList(updateMProvList: MProv[]): Observable<MProv[]>  {
      return this.http.post<MProv[]>('/provs/updateMoreProvList', updateMProvList)
    }

  }