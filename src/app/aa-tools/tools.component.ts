import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { GlobalVarService } from '../aa-common/global-var.service';
import { FormBuilder, Validators, FormGroup, NgForm } from '@angular/forms';
import { ValidationService } from '../aa-common/validation.service';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements OnInit {  
  public formTitles = {
    'tools' :             'Herramientas',
    'moreTools' :         'Mas Herramientas'
  };
  form: FormGroup;

  public logToConsole:  boolean;

  constructor(public formBuilder: FormBuilder, public globalVar: GlobalVarService) { }

  ngOnInit() {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.buildForm();
   }

   /* http://www.coding4developers.com/angular2-4-5-6-7-8/angular-8-reactive-forms-validation-example/ */
   private buildForm() {
    this.form = this.formBuilder.group({
      'numero1': [,      [Validators.required, ValidationService.decimalValidation ]],
      'numero2': [,  [Validators.required, Validators.pattern("^[0-9]+(\.[0-9]{1,2})?$")]],
      'numero3': [,    [Validators.required, ]],
      'numero4': [,    [Validators.required, ]],
      'name': ['', [Validators.required, ValidationService.alpaNumValidator]],
      'emailId': ['', [Validators.required, ValidationService.emailValidator]],
      'dob': ['', [Validators.required, ValidationService.dobValidator]],
      'phone': ['', [Validators.required, ValidationService.numberValidator, Validators.minLength(10), Validators.maxLength(10)]],
      'marks': ['', [Validators.required, ValidationService.decimalValidation]],
      'website': ['', [Validators.required, ValidationService.urlValidator]],
      'password': ['', [Validators.required, Validators.minLength(6)]],
      'confirmPassword': ['', [Validators.required]],
    },
    {
      validator: ValidationService.confirmPasswordValidator
    });
  }
  public setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
}
  public save(data: NgForm) {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      console.log(data)
    }
  }
  changeShowErrorMessageBtnClick() {
    if (this.globalVar.showErrorMsg2 === false) {
      this.globalVar.showErrorMsg2 = true;
    } else {
      this.globalVar.showErrorMsg2 = false;
    }    
  }
  updateFieldsValuesBtnClick(){
    this.globalVar.getFieldsAndValues();
  }
  

}
