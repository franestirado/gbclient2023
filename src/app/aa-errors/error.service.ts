import { Injectable } from '@angular/core';
import { BsModalService,BsModalRef } from 'ngx-bootstrap/modal';

import { ShowErrorComponent } from './show-error.component';
import { ShowMsgObjectComponent } from './show-msg-object.component';
import { GetDeleteComfirmationComponent } from './get-delete-confirmation.component';

@Injectable()
export class ErrorService {
  public showError        :boolean;
  private localModalRef   :BsModalRef;

  constructor (private modalService: BsModalService ) { }

  showErrorModal( title: string, footer:string, message1: string, message2: string, message3: string, message4: string, 
                  message5: string, message6: string, message7: string ){
    this.modalService.show(ShowErrorComponent, {
      initialState: {        
        errorTitle:    title,
        errorMessage1: message1,        
        errorMessage2: message2, 
        errorMessage3: message3, 
        errorMessage4: message4, 
        errorMessage5: message5, 
        errorMessage6: message6, 
        errorMessage7: message7, 
        errorFooter:   footer,
        callback: (result:string) => {
            return;             
        }
      },
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: false,
      class: 'modal-lg'  //modal-dialog-centered
    });
  }
  showMsgObject( color: string, title: string, footer:string, objeto :object, 
                    message1: string, message2: string, message3: string, message4: string, message5: string, message6: string, message7: string ){
    this.localModalRef = this.modalService.show(ShowMsgObjectComponent, {
        initialState: {        
                        msgColor    :color,
                        msgHeader   :title,
                        msgFooter   :footer,
                        msgObject   :objeto,
                        msg1        :message1,        
                        msg2        :message2, 
                        msg3        :message3, 
                        msg4        :message4, 
                        msg5        :message5, 
                        msg6        :message6, 
                        msg7        :message7, 
                        callback: (result:string) => {
                          return;             
                        }
                      },
        animated: true,
        keyboard: true,
        backdrop: true,
        ignoreBackdropClick: false,
        class: 'modal-lm'  //modal-dialog-centered
        });
  } 
  getDeleteMsgConfirmation( color: string, title: string, footer:string, objeto :object, 
                             message1: string, message2: string, message3: string, message4: string, 
                             message5: string, message6: string, message7: string ) : Promise<boolean> {

    this.localModalRef = this.modalService.show(GetDeleteComfirmationComponent, {
      initialState: {        
        msgColor    :color,
        msgHeader   :title,
        msgFooter   :footer,
        msgObject   :objeto,
        msg1        :message1,        
        msg2        :message2, 
        msg3        :message3, 
        msg4        :message4, 
        msg5        :message5, 
        msg6        :message6, 
        msg7        :message7, 
        callback: (result:string) => {
          if ( (result.toLowerCase() === 'yes') ) return true;
          else return false;
        }
      },
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: 'modal-lm'  //modal-dialog-centered
    });
    return new Promise<boolean>((resolve, reject) => this.localModalRef.content.result.subscribe((result) => {
      /*let localResult = true;
      if ( (result.toLowerCase() === 'yes') ) localResult = true;
      else localResult = false; */
      resolve(result)
    } ));
  }
}
