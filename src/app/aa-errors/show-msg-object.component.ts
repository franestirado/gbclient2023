import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector      :'show-msg-object',  
  templateUrl   :'./show-msg-object.component.html',
  styleUrls     :['../aa-common/common.component.css'] 
})

export class ShowMsgObjectComponent implements OnInit {
  public  msgColor      :string;
  public  msgHeader     :string;
  public  msgFooter     :string;
  public  msgObject     :object;
  public  msg1          :string;
  public  msg2          :string;
  public  msg3          :string;
  public  msg4          :string;
  public  msg5          :string;
  public  msg6          :string;
  public  msg7          :string;
  public  callback      :any;

  public  objectKeyValues :string[];
  public  showObjectDetails   :boolean;

  constructor(public bsModalRef: BsModalRef) { }
  
  ngOnInit(): void {
    this.showObjectDetails = false;
    this.objectKeyValues = new Array();
    var index = 0;
    for (var key in this.msgObject) {
        let baseString = '------------------------------->';
        this.objectKeyValues[index] = key + baseString.substring(key.length)+this.msgObject[key];
        index = index + 1;
    }
  }
  changeShowObjectDetailsBtnClick(){
    if (this.showObjectDetails === true) this.showObjectDetails = false;
    else this.showObjectDetails = true;
  }
  closeMessageBtnClick() {
    if (this.bsModalRef.content.callback != null){
      this.bsModalRef.content.callback('OK');
      this.bsModalRef.hide();
    }
  }
  getButtonClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="btn btn-outline-danger rounded"
      case 'deleteMsg': { classList = 'btn btn-outline-primary rounded'; break; }
      case 'errorMsg':  { classList = 'btn btn-outline-danger  rounded'; break; }
      case 'infoMsg':   { classList = 'btn btn-outline-primary rounded'; break; }
      default:          { classList = 'btn btn-outline-danger  rounded'; break; }
    };
    return classList;
  }
  getCardClass(msgType:string){
    var classList='';
    switch (msgType){
                                    // class="card bg-error text-danger rounded border border-danger text-center"
      case 'deleteMsg': { classList = 'card bg-delete  text-primary rounded border border-primary text-center'; break; }
      case 'errorMsg':  { classList = 'card bg-error  text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card bg-msg    text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card bg-error  text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }
  getCardBodyClass(msgType:string){
    var classList='';
    switch (msgType){
                                    // class="card-body"
      case 'deleteMsg': { classList = 'card-body'; break; }
      case 'errorMsg':  { classList = 'card-body'; break; }
      case 'infoMsg':   { classList = 'card-body'; break; }
      default:          { classList = 'card-body'; break; }
    };
    return classList;
  }
  getCardHeaderClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="card-header" 
      case 'deleteMsg': { classList = 'card-header bg-delete text-primary rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'card-header bg-error text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card-header bg-msg   text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card-header bg-error text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }
  getCardFooterClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="card-footer" 
      case 'deleteMsg': { classList = 'card-footer bg-delete text-primary rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'card-footer bg-error text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card-footer bg-msg   text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card-footer bg-error text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }   
  getNavbarClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="navbar navbar-expand-lg navbar-light text-primary bg-facts rounded border border-primary"
      case 'deleteMsg': { classList = 'navbar navbar-expand-lg navbar-light text-primary bg-delete rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-error rounded border border-primary'; break; }
      case 'infoMsg':   { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-msg   rounded border border-primary'; break; }
      default:          { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-error rounded border border-primary'; break; }
    };
    return classList;
  }
  getNavbarBrandClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="navbar-brand text-primary bg-facts rounded"
      case 'deleteMsg': { classList = 'navbar-brand bg-delete text-primary rounded'; break; }
      case 'errorMsg':  { classList = 'navbar-brand bg-error text-danger  rounded'; break; }
      case 'infoMsg':   { classList = 'navbar-brand bg-msg   text-primary rounded'; break; }
      default:          { classList = 'navbar-brand bg-error text-danger  rounded'; break; }
    };
    return classList;
  }
  getTableClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="table table-hover"
      case 'deleteMsg': { classList = 'table table-hover text-primary'; break; }
      case 'errorMsg':  { classList = 'table table-hover text-danger'; break; }
      case 'infoMsg':   { classList = 'table table-hover text-primary'; break; }
      default:          { classList = 'table table-hover text-danger'; break; }
    };
    return classList;
  }



}
