import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-show-error',  
  template: `
    
    <div class="card bg-error text-danger rounded border border-danger text-center">
      <div class="card-header">              
        <a ng-if="! errorTitle" class="navbar-brand text-danger bg-error rounded">{{errorTitle}} <i class="fas fa-long-arrow-alt-right"></i></a>        
        <div class="btn-group mx-auto" role="group">
          <button type="button" class="btn btn-outline-danger rounded mr-2" (click)="closeMessage()"><span><i class="far fa-times-circle"></i></span></button>   
        </div>
      </div>
      <div class="card-body">
        <div ng-if="!errorMessage1"><p>{{errorMessage1}}</p> </div>
        <div ng-if="!errorMessage2"><p>{{errorMessage2}}</p> </div>
        <div ng-if="!errorMessage3"><p>{{errorMessage3}}</p> </div>
        <div ng-if="!errorMessage4"><p>{{errorMessage4}}</p> </div>
        <div ng-if="!errorMessage5"><p>{{errorMessage5}}</p> </div>
        <div ng-if="!errorMessage6"><p>{{errorMessage6}}</p> </div>
        <div ng-if="!errorMessage7"><p>{{errorMessage7}}</p> </div>
      </div>
      <div class="card-footer">        
        <a ng-if="! errorFooter" class="navbar-brand text-danger bg-error rounded">{{errorFooter}} <i class="fas fa-long-arrow-alt-right"></i></a>        
        <div class="btn-group mx-auto" role="group">
          <button type="button" class="btn btn-outline-danger rounded mr-2" (click)="closeMessage()"><span><i class="far fa-times-circle"></i></span></button>          
        </div>  
      </div>       
    </div>
    `
})
export class ShowErrorComponent implements OnInit {
  errorTitle      :string;
  errorMessage1   :string;
  errorMessage2   :string;
  errorMessage3   :string;
  errorMessage4   :string;
  errorMessage5   :string;
  errorMessage6   :string;
  errorMessage7   :string;
  errorFooter     :string;
  callback        :any;

  constructor(public bsModalRef: BsModalRef) { }
  
  ngOnInit(): void {
  }

  closeMessage() {
    if (this.bsModalRef.content.callback != null){
      this.bsModalRef.content.callback('OK');
      this.bsModalRef.hide();
    }
  }

}

