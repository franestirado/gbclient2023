import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router'; 
import { throwError } from 'rxjs';
import { ErrorService } from './error.service';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
    public errorMessage: string;
    public errorMsgType:string;
    public errorStatus: string;
    public errorError: string;
    public errorMsg1: string;
    public errorMsg2: string;
    public showErrorMsg1: boolean;
    public showErrorMsg2: boolean;
    public logToConsole: boolean;

    constructor(    private injector: Injector, private zone: NgZone,
                  //  private _errorService: ErrorService
                    ) {

        this.logToConsole = true;
    }
 
    handleError(error) { 
        //A client-side or network error occurred.
        let router = this.injector.get(Router); 
        let _errorService = this.injector.get(ErrorService);        
        this.errorMessage = 'Client Error Happened'
        this.errorMsgType = 'Client site';
        this.errorStatus =  router.url;
        this.errorMsg1 =  error.message ? error.message : error.toString();
        this.errorError = error.stack;
        this.showErrorMsg1 = true;
        if (this.logToConsole) {
            this.errorMsg2 = 'C->Error Conocido';                        
            this.showErrorMsg2 = true;
            this.showErrorMsg1 = false;
        }
        if (this.logToConsole) {
            console.log('GlobalErrorHandlerService -> URL -> ' + router.url);
            console.log('GlobalErrorHandlerService -> error.message -> error.toString ->' + error.message ? error.message : error.toString());
            console.log('GlobalErrorHandlerService -> error.stack -> ' + error.stack);
        }
        _errorService.showErrorModal('... Ha ocurrido un ERROR ...','... Notifiquelo para que se corrija ...',
                            this.errorMsg1, this.errorMsg2,this.errorMessage, this.errorMsgType, 
                            this.errorStatus, this.errorError, "GlobalErrorHandlerService--> handleError");              
   }
}