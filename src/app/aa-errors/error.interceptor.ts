import {
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

export class ErrorIntercept implements HttpInterceptor {
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    /*
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        console.log(JSON.stringify('ErrorIntercept ->......Client Error.....error.error.message...->'));
                        console.log(error.error.message);                      
                    } else {
                        // server-side error
                        console.log(JSON.stringify('ErrorIntercept ->......Server Error.....error.status...->'));
                        console.log(error.status); 
                        console.log(JSON.stringify('ErrorIntercept ->......Server Error.....error.message...->'));
                        console.log(error.message);                       
                    }
                    */                            
                    return throwError(() => error);
                })
            )
    }
}