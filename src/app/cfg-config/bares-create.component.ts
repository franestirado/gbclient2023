import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { Bares } from './config';
import { ConfigService } from './config.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';

@Component({
  selector      :'app-bares-create',
  templateUrl   :'./bares-create.component.html',
  styleUrls     :['./config.component.css'] 
}) 
export class BaresCreateComponent implements OnInit {
  public  newBar      :Bares;
  public  baresTitle  :string;
  public  baresDate   :Date;
  public  createBar   :boolean;
  callback            :any;
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'barTitle'    :'',   
    'barDate'     :'',
    'bar'         :'Bares',
    'barCreate'   :'Crear Bar',
    'barEdit'     :'Modificar Bar',
  };
  public  formLabels = {
    'id'                    :'#####',
    'barId'                 :'Id',
    'barDate'               :'Fecha',
    'barName'               :'Nombre Bar',
    'barLongName'           :'Nombre Detallado',
    'barLegalName'          :'Razón Social',
    'barCIF'                :'CIF',
    'barAddress'            :'Dirección',
    'barWebSite'            :'Web',
    'barEmail'              :'Email',
    'barPhone'              :'Teléfono',
    'barDbName'             :'BD-Nombre',
    'barDbUrl'              :'BD-Url',
    'barDbPassword'         :'BD-Passwd',
    'barDbUserName'         :'BD-Usuario',
    'barDbDriverClassName'  :'BD-Driver',
    'barDbDdlAuto'          :'BD-DdlAuto',
    'barDbPhysNameStrategy' :'BD-PhysStrat.',
    'barStatus'             :'Estado',    
  };
  public  barForm                           :FormGroup;
  public  localDbUrlsRecordList             :ValueRecord [];
  public  localDbUserNamesRecordList        :ValueRecord [];
  public  localDbDriverClassNamesRecordList :ValueRecord [];
  public  localDbDdlAutosRecordList         :ValueRecord [];
  public  localDbPhysNStrategysRecordList   :ValueRecord [];
  public  emptyRecordList                   :ValueRecord [];
  private logToConsole                      :boolean;
  public  showBar                           :boolean;
  public  editBar                           :boolean;
  private currentDate                       :Date;
  public  barDatePickerConfig               :Partial <BsDatepickerConfig>;
 /*---------------------------------------ARTS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _configService: ConfigService) { 
  this.barDatePickerConfig = Object.assign ( {}, {
    containerClass    :'theme-dark-blue',
    showWeekNumbers   :true,
    minDate           :this.globalVar.workingPreviousYear,
    maxDate           :this.globalVar.workingNextYear,
    dateInputFormat   :'DD-MM-YYYY'
    } );
}
/*---------------------------------------ARTS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;   
    this.globalVar.consoleLog(this.logToConsole,'->BARES CREATE -> ngOnInit ->...Starting...->',null);

    if (this.baresDate === null) {
      this.formTitles.barDate = '';
      this.currentDate = this.globalVar.currentDate;
    } else {
      this.currentDate = this.baresDate;
      this.formTitles.barDate = this.baresDate.toLocaleDateString();
    }
    this.formTitles.barTitle = this.baresTitle;
    this.barForm = this.fb.group({
      barId                 :[{value:'',disabled:true}],
      barDate               :[{value:this.currentDate,disabled:true},[Validators.required]],
      barName               :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      barLongName           :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barLegalName          :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barCIF                :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barAddress            :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barEmail              :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barWebSite            :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barPhone              :[{value:'',disabled:false},[Validators.minLength(3),Validators.maxLength(64)]],
      barDbName             :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      barDbUrl              :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      barDbPassword         :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      barDbUserName         :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      barDbDriverClassName  :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(128)]],
      barDbDdlAuto          :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(128)]],      
      barDbPhysNameStrategy :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(128)]],
      barStatus             :[{value:true,disabled:false},[Validators.required]],
    })
    this.localDbUrlsRecordList              = this.globalVar.dbUrlsRecordList;
    this.localDbUserNamesRecordList         = this.globalVar.dbUserNamesRecordList;
    this.localDbDriverClassNamesRecordList  = this.globalVar.dbDriverClassNamesRecordList;
    this.localDbDdlAutosRecordList          = this.globalVar.dbDdlAutosRecordList;
    this.localDbPhysNStrategysRecordList    = this.globalVar.dbPhysNStrategysRecordList;
    if (this.createBar === true) {
      this.showBar = true;        
      this.editBar = false;
      this.newBar = new Bares();      
      this.cleanBarFormData();
      this.barForm.get('barDate').patchValue(new Date(this.currentDate));
    } else {
      this.createBar = false;
      this.showBar = true;        
      this.editBar = true;
      this.getOneBar(this.newBar);
    }
  }

  /*--------------------------------------- BARES CREATE -- FORMS---------------------------------------------------------------*/
  cleanBarFormData(){
    this.barForm.patchValue( {  barName:'', barLongName:'', barLegalName:'', barCIF:'', barAddress:'', barEmail:'', barWebSite:'', barPhone:'',
                                barDbName:'', barDbUrl:this.localDbUrlsRecordList[0].value,barDbPassword:'',
                                barDbUserName:this.localDbUserNamesRecordList[0].value, barDbDriverClassName:this.localDbDriverClassNamesRecordList[0].value,
                                barDbDdlAuto:this.localDbDdlAutosRecordList[0].value, barDbPhysNameStrategy:this.localDbPhysNStrategysRecordList[0].value, 
                                barStatus:true} );
  }  
  copyBarFormDataToBar(){
    this.newBar.barDate               = this.barForm.controls.barDate.value;
    this.newBar.barName               = this.barForm.controls.barName.value;
    this.newBar.barLongName           = this.barForm.controls.barLongName.value;
    this.newBar.barLegalName          = this.barForm.controls.barLegalName.value;
    this.newBar.barCIF                = this.barForm.controls.barCIF.value;
    this.newBar.barAddress            = this.barForm.controls.barAddress.value;
    this.newBar.barEmail              = this.barForm.controls.barEmail.value;    
    this.newBar.barWebSite            = this.barForm.controls.barWebSite.value;
    this.newBar.barPhone              = this.barForm.controls.barPhone.value;
    this.newBar.barDbName             = this.barForm.controls.barDbName.value;
    this.newBar.barDbUrl              = this.barForm.controls.barDbUrl.value;
    this.newBar.barDbPassword         = this.barForm.controls.barDbPassword.value;
    this.newBar.barDbUserName         = this.barForm.controls.barDbUserName.value;
    this.newBar.barDbDriverClassName  = this.barForm.controls.barDbDriverClassName.value;
    this.newBar.barDbDdlAuto          = this.barForm.controls.barDbDdlAuto.value;
    this.newBar.barDbPhysNameStrategy = this.barForm.controls.barDbPhysNameStrategy.value;
    this.newBar.barStatus             = this.barForm.controls.barStatus.value;
  }
  copyNewBarDataToBarForm(){
    this.barForm.patchValue( {barId:this.newBar.barId,barDate:this.newBar.barDate,barName:this.newBar.barName,barLongName:this.newBar.barLongName,
                              barLegalName:this.newBar.barLegalName,barCIF:this.newBar.barCIF,barAddress:this.newBar.barAddress,  
                              barEmail:this.newBar.barEmail, barWebSite:this.newBar.barWebSite,barPhone:this.newBar.barPhone,
                              barDbName:this.newBar.barDbName,barDbUrl:this.newBar.barDbUrl,barDbPassword:this.newBar.barDbPassword,
                              barDbUserName:this.newBar.barDbUserName,
                              barDbDriverClassName:this.newBar.barDbDriverClassName, barDbDdlAuto:this.newBar.barDbDdlAuto,
                              barDbPhysNameStrategy:this.newBar.barDbPhysNameStrategy,barStatus:this.newBar.barStatus} );
  this.barForm.get('barDate').patchValue(new Date(this.newBar.barDate));
  }
  /*--------------------------------------- BARES CREATE -- GENERAL ------------------------------------------------------------*/
  /*--------------------------------------- BARES CREATE -- BTN CLICK-----------------------------------------------------------*/
  ClosedBaresCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(this.newBar);
        this.result.next(this.newBar);
        this.bsModalRef.hide();
      }
  }
  cleanFormBarBtnClick(){
    this.createBar = true;
    this.editBar = false;
    this.cleanBarFormData();
  }
  createBarBtnClick(barForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> BARES CREATE -> createBarBtnClick ->...Starting...->', null);
    this.copyBarFormDataToBar();
    this.createOneBar(this.newBar);
  }
  updateBarBtnClick(barForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> BARES CREATE -> updateBarBtnClick ->...Starting...->', null);
    this.copyBarFormDataToBar();    
    this.updateOneBar(this.newBar);
  }
  /*--------------------------------------- BARES CREATE -- GET ARTS -----------------------------------------------------------*/
  createOneBar(newBar: Bares): void {
    this.globalVar.consoleLog(this.logToConsole,'-> BARES CREATE -> createOneBar ->...Starting...->', null);
    this._configService.createOneBar(newBar)
      .subscribe({next:(data) => { this.newBar = data;
                           this.ClosedBaresCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  getOneBar(modBar: Bares): void {
    this.globalVar.consoleLog(this.logToConsole,'-> BARES CREATE -> getOneBar ->...Starting...->', null);
    this._configService.getOneBar(modBar)
      .subscribe({next:(data) => { this.newBar = data;
                           this.copyNewBarDataToBarForm();                          
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }
  updateOneBar(modBar: Bares): void {
    this.globalVar.consoleLog(this.logToConsole,'-> BARES CREATE -> updateOneBar ->...Starting...->', null);
    this._configService.updateOneBar(modBar)
      .subscribe({next:(data) => { this.newBar = data;
                           this.ClosedBaresCreateModalBtnClick();
                         },
                error:(error) => { if (this.globalVar.handleError(error)) { throwError(() => error) }; }});
  }

}