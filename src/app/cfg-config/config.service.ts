import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Bares } from './config';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()

export class ConfigService {

  constructor(private http: HttpClient) { }

  private json: string;
  private params: string;
  private headers: HttpHeaders;

  /* ---------------------------------------ARTS-----------------------------------------------------------*/
  getAllBares(): Observable<Bares[]>  {
    return this.http.get<Bares[]>('/config/getAllBares')
  }
  createOneBar(newBar: Bares): Observable<Bares>  {    
    return this.http.post<Bares>('/config/createOneBar', newBar)  
  }
  getOneBar(editBar: Bares): Observable<Bares>  {
    return this.http.post<Bares>('/config/getOneBar', editBar)
  }
  updateOneBar(newBar: Bares): Observable<any>  {
    return this.http.post('/config/updateOneBar', newBar)
  }
  changeWorkingBar(selectedBar: Bares): Observable<any>  {
    return this.http.post('/config/changeWorkingBar', selectedBar)
  }
  delOneBar(delBar: Bares): Observable<any> {
    return this.http.post('/config/delOneBar', delBar)
  }
  delAllBares(): Observable<any> {
    return this.http.post('/config/delAllBares','')
  }

}