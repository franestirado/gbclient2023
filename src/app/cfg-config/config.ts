export class Bares {
    barId               	:number;
	barDate             	:Date;
	barName             	:string;
	barLongName           	:string;
	barLegalName           	:string;
	barCIF             		:string;
	barAddress             	:string;
	barEmail             	:string;
	barWebSite             	:string;
	barPhone             	:string;
	barDbName             	:string;
	barDbUrl              	:string;
	barDbPassword         	:string;
	barDbUserName         	:string;
	barDbDriverClassName  	:string;
	barDbDdlAuto          	:string;
	barDbPhysNameStrategy 	:string;
	barStatus           	:boolean;
}
export class BaresString {
    barId               	:number;
	barDate             	:string;
	barName             	:string;
	barLongName           	:string;
	barLegalName           	:string;
	barCIF             		:string;
	barAddress             	:string;
	barEmail             	:string;
	barWebSite             	:string;
	barPhone             	:string;
	barDbName             	:string;
	barDbUrl              	:string;
	barDbPassword         	:string;
	barDbUserName         	:string;
	barDbDriverClassName  	:string;
	barDbDdlAuto          	:string;
	barDbPhysNameStrategy 	:string;
	barStatus           	:boolean;
}